﻿using ecims.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.Net.Http;

namespace ecims
{
    public class EmailService : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {
            // Plug in your email service here to send an email.
            //return Task.FromResult(0);
            await configSendGridasync(message);
        }

        private async Task configSendGridasync(IdentityMessage message)
        {
            #region Current TLS version 1.2 for sendgrid mail
            // new SendGrid API Key with the name NewEUCAPIKey
            var apiKey = "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";


            // Override host with TLS 1.2+ endpoint
            var host = "https://api.sendgrid.com";
            var client = new SendGridClient(apiKey, host);


            var from = new EmailAddress("euc@nsa.gov.ng", "End-User Certificate Portal");
            var subject = message.Subject;
            var to = new EmailAddress(message.Destination);
            var signature = "<br/><sup>End-User Certificate is free at every stage from processing to issuance," +
                            " and is only approved and issued by the Office of the National Security Adviser.</sup>";
            var plainTextContent = message.Body + signature;
            var htmlContent = message.Body + signature;

            var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);

            // force sendgrid to use TLS version 1.2
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

            var response = await client.SendEmailAsync(msg);
            #endregion

            #region using old TLS versions 1.0 and 1.1 in sendgrid mail pipeline
            //var apiKey = "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";

            //var client = new SendGridClient(apiKey);
            //var from = new EmailAddress("euc@nsa.gov.ng", "End-User Certificate Portal");
            //var subject = message.Subject;
            //var to = new EmailAddress(message.Destination);
            //var signature = "<br/><sup>End-User Certificate is free at every stage from processing to issuance," +
            //                " and is only approved and issued by the Office of the National Security Adviser.</sup>";
            //var plainTextContent = message.Body + signature;
            //var htmlContent = message.Body + signature;

            //var msg = MailHelper.CreateSingleEmail(from, to, subject, plainTextContent, htmlContent);
            //var response = await client.SendEmailAsync(msg);
            #endregion

            #region Old Sendgrid Code Using Username and Password
            //var smtp = new SmtpClient("smtp.sendgrid.net", 587);

            //var creds = new NetworkCredential("azeez.olatundun", "!97tuT6nKaG37PQ");

            //smtp.UseDefaultCredentials = false;
            //smtp.Credentials = creds;
            ////Set to false on localhost
            //smtp.EnableSsl = true;

            //var to = new MailAddress(message.Destination);
            //var from = new MailAddress("euc@nsa.gov.ng", "End-User Certificate Portal");
            //var signature = "<br/><sup>End-User Certificate is free at every stage from processing to issuance, and is only approved and issued by the Office of the National Security Adviser.</sup>";

            //var msg = new MailMessage();

            //msg.To.Add(to);
            //msg.From = from;
            //msg.IsBodyHtml = true;
            //msg.Subject = message.Subject;
            //msg.Body = message.Body + signature;

            //await smtp.SendMailAsync(msg);
            #endregion
        }

    }

    public class SmsService : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {
            //TO-DO Change these to multi-texter credentials soon
            //Send SMS using multitexter.com
            /*const string*/
            var accountSid = System.Configuration.ConfigurationManager.AppSettings["TwilioSid"];
            /*const string*/
            var authToken = System.Configuration.ConfigurationManager.AppSettings["TwilioToken"];

            await SendSmsConfirmation(message);

            //return Task.FromResult(0);
            // Twilio End
        }
        #region Testing SMS Delivery
        public async Task TestSendAsync(IdentityMessage message)
        {
            //Send SMS using multitexter.com
            /*const string*/
            var accountSid = System.Configuration.ConfigurationManager.AppSettings["TwilioSid"];
            /*const string*/
            var authToken = System.Configuration.ConfigurationManager.AppSettings["TwilioToken"];

            await SendSmsConfirmation(message);

            //return Task.FromResult(0);
            // Twilio End
        }
        #endregion
        private async Task SendSmsConfirmation(IdentityMessage message)
        {
            #region Old SMS Code for MultiTexter
            //SMS Sending
            //string sender = "EUC";

            //string url = "https://www.MultiTexter.com/tools/geturl/Sms.php?username=ocedache@gmail.com&password=Omanchala1944&sender="
            //                + sender + "&message=" + message.Body +
            //                "&flash=0&sendtime=2009-10-18%2006:30&listname=friends&recipients="
            //                + message.Destination;

            //Uri myUri = new Uri(url, UriKind.Absolute);
            //try
            //{
            //    HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
            //    webReq.Method = "GET";
            //    var response = await webReq.GetResponseAsync();
            //}
            //catch (Exception ex)
            //{

            //}
            #endregion
            #region Updated SMS Code for MultiTexter
            //var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://app.multitexter.com/v2/app/sms");
            //httpWebRequest.ContentType = "application/json";
            //httpWebRequest.Method = "POST";
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            //{
            //    string email = "ocedache@gmail.com";
            //    string password = "Omanchala1944";
            //    string sms = message.Body;
            //    string sender_name = "EUC";
            //    string recipients = message.Destination;
            //    string forcednd = "1";
            //    string json = "{\"email\":\"" + email + "\",\"password\":\"" + password + "\",\"message\":\"" + sms + "\",\"sender_name\":\"" + sender_name + "\",\"recipients\":\"" + recipients + "\",\"forcednd\":\"" + forcednd + "\"}";
            //    streamWriter.Write(json);
            //    streamWriter.Flush();
            //    streamWriter.Close();
            //}
            //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //{
            //    var result = streamReader.ReadToEnd();
            //    Console.WriteLine(result);
            //}
            #endregion

            //HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
            //webReq.Method = "GET";
            //var response = webReq.GetResponse();
            #region SMS Code
            string sender = "EUC";

            // SMS Code for Bulk SMS NIgeria
            //string url = "https://www.bulksmsnigeria.com/" +
            //                "api/v1/sms/create?api_token=lFotnTAfnQDw5DDBxfVzPh3KLOCOgE1MNseJyz7qCXVEEilwHauYrO1BK7P3" +
            //                "&from=" + sender +
            //                "&to=" + message.Destination +
            //                "&body=" + message.Body +
            //                "&dnd=4";

            // SMS Code for Kudi SMS
            //string url = "https://account.kudisms.net/api/?username=azolatundun@gmail.com&password=4d2b6gmtiyXKcx@" +
            //                "&message=" + message.Body +
            //                "&sender=" + sender +
            //                "&mobiles=" + message.Destination;

            //SMS Code Update 2.0 Kudi SMS
            // Set TLS version explicitly
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            var client = new HttpClient();

            var content = new MultipartFormDataContent();
            content.Add(new StringContent("8KcLr4afwbCVYBs3EFyjmD6PMtiOg7Q2qWRl5nxApHGXhuZTIkSNzodve019UJ"), "token");
            content.Add(new StringContent(sender), "senderID");
            content.Add(new StringContent(message.Destination), "recipients");
            content.Add(new StringContent(message.Body), "message");

            var response = await client.PostAsync("https://my.kudisms.net/api/corporate", content);

            response.EnsureSuccessStatusCode();
            var returnedResponse = await response.Content.ReadAsStringAsync();
            //End of SMS Code Update 2.0 Kudi SMS

            //string url = "https://www.MultiTexter.com/tools/geturl/Sms.php?username=ocedache@gmail.com&password=Omanchala1944&sender="
            //                + sender + "&message=" + message.Body +
            //                "&flash=0&sendtime=2009-10-18%2006:30&listname=friends&recipients="
            //                + message.Destination;

            //Uri myUri = new Uri(url, UriKind.Absolute);
            //try
            //{
            //    ServicePointManager.Expect100Continue = true;
            //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //    HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
            //    webReq.Method = "GET";
            //    var response = await webReq.GetResponseAsync();
            //    int result = 1;
            //}
            //catch (Exception ex)
            //{
            //    string reult = ex.Message;
            //}
            #endregion

        }

        public void SendSmsConfirmationSynchronously(IdentityMessage message)
        {
            //SMS Sending
            string sender = "ONSA ECIMS";

            string url = "https://www.MultiTexter.com/tools/geturl/Sms.php?username=ocedache@gmail.com&password=Omanchala1944&sender="
                            + sender + "&message=" + message.Body +
                            "&flash=0&sendtime=2009-10- 18%2006:30&listname=friends&recipients="
                            + message.Destination;

            Uri myUri = new Uri(url, UriKind.Absolute);

            try
            {
                WebClient client = new WebClient();
                client.OpenRead(myUri);
            }
            catch (Exception)
            {

                throw;
            }


            //HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
            //webReq.Method = "GET";
            //var response = await webReq.GetResponseAsync();
            //try
            //{
            //    var response = await webReq.GetResponseAsync();
            //}
            //catch (Exception ex)
            //{
            //    //Include error handling here
            //}
        }
        #region Testing of SMS
        //Testing SMS delivery
        private async Task TestSendSmsConfirmation(IdentityMessage message)
        {
            //SMS Sending
            string sender = "ONSA ECIMS";

            string url = "https://www.MultiTexter.com/tools/geturl/Sms.php?username=ocedache@gmail.com&password=Omanchala1944&sender="
                            + sender + "&message=" + message.Body +
                            "&flash=0&sendtime=2009-10- 18%2006:30&listname=friends&recipients="
                            + message.Destination;

            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
            webReq.Method = "GET";
            try
            {
                var response = await webReq.GetResponseAsync();
                //response.
            }
            catch (Exception ex)
            {
                //Include error handling here
            }
        }
        #endregion
        private async Task SendSmsAsynchronously(string recipient, string message)
        {
            //SMS Sending
            string sender = "ONSA ECIMS";

            string url = "https://www.MultiTexter.com/tools/geturl/Sms.php?username=ocedache@gmail.com&password=Omanchala1944&sender="
                            + sender + "&message=" + message +
                            "&flash=0&sendtime=2009-10- 18%2006:30&listname=friends&recipients="
                            + recipient;

            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
            webReq.Method = "GET";
            await webReq.GetResponseAsync();
            //Stream answer = webResponse.GetResponseStream();
            //StreamReader _recievedAnswer = new StreamReader(answer);
        }
    }

    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<ApplicationUser>
            {
                MessageFormat = "Your security code is {0}"
            });
            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<ApplicationUser>
            {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }

    public class ApplicationRoleManager : RoleManager<IdentityRole>
    {
        public ApplicationRoleManager(IRoleStore<IdentityRole, string> store)
            : base(store)
        {
        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            var manager = new ApplicationRoleManager(new RoleStore<IdentityRole>(context.Get<ApplicationDbContext>()));
            return manager;
        }
    }
}
