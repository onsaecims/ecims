﻿using System.Web.Optimization;

namespace ecims
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));
            
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/custom.js",
                      "~/Scripts/bootbox.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      
                      "~/Content/Site.css"));

            //Create bundle for jqueryUI
            //js
            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            //css
            bundles.Add(new StyleBundle("~/Content/cssjqryUi").Include(
            "~/Content/themes/base/jquery-ui.css"));

            //Smart wizard
            bundles.Add(new ScriptBundle("~/bundles/smartwizardjs").Include(
                "~/Scripts/smartwizard/js/jquery.smartWizard.js"));
            bundles.Add(new StyleBundle("~/Content/smartwizardcss").Include(
           "~/Scripts/smartwizard/smart_wizard.css"));
        }
    }
}
