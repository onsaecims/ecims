﻿function validateUsername(e) {
    var value = e.value;

    if (value == "") {
        bootbox.alert({
            message: "Username Field is Required.",
            size: 'small',
            title: 'Invalid Entry',
            backdrop: true
        });
    }
}

function validatePassword(e) {
    var value = e.value;

    if (value == "") {
        bootbox.alert({
            message: "Password Field is Required.",
            size: 'small',
            title: 'Invalid Entry',
            backdrop: true
        });
    }
}
