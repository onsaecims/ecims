﻿$("#val-verify-token").click(function (e) {
        e.preventDefault();

        //Validate certificate textbox
        var certificateNumber = $("#val-certificate-number").val();
        var tokenNumber = $("#val-token-number").val();
        if (certificateNumber == "" || tokenNumber == "") {
            alert("Please enter a valid certificate number and token code before clicking the submit button.");
            return false;
        }

        var id = document.getElementById("val-certificate-number").value;
        var value = document.getElementById("val-token-number").value;
        

        //Ensure the token code is different
        
        var hiddenValue = $("#val-hidden-token-number").val();
        
        var tokenNumberDiv = document.getElementById("token-number-form-group");
        
        var tokenNumberSuccessSpan = document.getElementById("token-number-validation-success");
        var tokenNumberErrorSpan = document.getElementById("token-number-validation-error")

        var validationSuccessMessage = document.getElementById("token-number-validation-success");
        var validationErrorMessage = document.getElementById("token-number-validation-error");
        
        if (value == $("#val-hidden-token-number").val()) {
            tokenNumberDiv.classList.remove("has-success");

            tokenNumberSuccessSpan.style.visibility = "hidden";

            tokenNumberDiv.classList.add("has-error");
            tokenNumberDiv.classList.add("has-feedback");

            tokenNumberErrorSpan.style.visibility = "visible";
            tokenNumberErrorSpan.style.display = "block";
            tokenNumberErrorSpan.classList.remove("hide");

            validationErrorMessage.style.visibility = "visible";
            validationErrorMessage.style.display = "block";

            validationSuccessMessage.style.visibility = "hidden";
            validationSuccessMessage.style.display = "none";

            tokenNumberSuccessSpan.style.visibility = "hidden";
            tokenNumberSuccessSpan.style.display = "none";

            document.getElementById('ajax-loader-token-number').style.visibility = "hidden";
            document.getElementById('ajax-loader-token-number').style.display = "none";

            $(".certificate-fields").fadeOut(1000);

            //$('#targetDiv').empty();
            return false;
        }

        document.getElementById('ajax-loader-token-number').style.visibility = "visible";
        document.getElementById('ajax-loader-token-number').style.display = "block";

        tokenNumberDiv = document.getElementById("token-number-form-group");

        tokenNumberSuccessSpan = document.getElementById("token-number-validation-success");
        tokenNumberErrorSpan = document.getElementById("token-number-validation-error");

        validationSuccessMessage = document.getElementById("token-number-validation-success");
        validationErrorMessage = document.getElementById("token-number-validation-error");
        
        //Make ajax call to the controller
        $.ajax({
            url: '/VerifyEUCPortal/VerifyTokenCode',
            data: {
                'id': id,
                'value': value,
                'certificateNumber': certificateNumber,
                'async': false
            },
            success: function (result) {
                if (result.success) {
                    var iframe = $('<iframe>');
                    
                    //var pdfViewer = $('<object>');
                    var pdfViewer = $('<iframe>');
                    pdfViewer.width(550);
                    pdfViewer.height(600);
                    pdfViewer.attr('src', result.UploadUrl + "#toolbar=0");

                    $('#targetDiv').empty();

                    $('#targetDiv').append(pdfViewer);
                    //$('#targetDiv').append(iframe);

                    tokenNumberDiv.classList.remove("has-error");

                    tokenNumberErrorSpan.style.visibility = "hidden";

                    tokenNumberDiv.classList.add("has-success");
                    tokenNumberDiv.classList.add("has-feedback");

                    tokenNumberSuccessSpan.style.visibility = "visible";
                    tokenNumberSuccessSpan.style.display = "block";
                    tokenNumberSuccessSpan.classList.remove("hide");

                    validationSuccessMessage.style.visibility = "visible";
                    validationSuccessMessage.style.display = "block";

                    validationErrorMessage.style.visibility = "hidden";
                    validationErrorMessage.style.display = "none";

                    tokenNumberErrorSpan.style.visibility = "none";
                    tokenNumberErrorSpan.style.display = "hidden";

                    document.getElementById('ajax-loader-token-number').style.visibility = "hidden";
                    document.getElementById('ajax-loader-token-number').style.display = "none";

                    $(".certificate-fields").removeClass("hide");
                    $(".certificate-fields").show();

                    //Get the value of the last token that was used with Google
                    usedTokenCode = $("#val-token-number").val()
                    $("#val-hidden-token-number").val(usedTokenCode);
                }
                else {
                    tokenNumberDiv.classList.remove("has-success");

                    tokenNumberSuccessSpan.style.visibility = "hidden";

                    tokenNumberDiv.classList.add("has-error");
                    tokenNumberDiv.classList.add("has-feedback");

                    tokenNumberErrorSpan.style.visibility = "visible";
                    tokenNumberErrorSpan.style.display = "block";
                    tokenNumberErrorSpan.classList.remove("hide");

                    validationErrorMessage.style.visibility = "visible";
                    validationErrorMessage.style.display = "block";

                    validationSuccessMessage.style.visibility = "hidden";
                    validationSuccessMessage.style.display = "none";

                    tokenNumberSuccessSpan.style.visibility = "hidden";
                    tokenNumberSuccessSpan.style.display = "none";

                    document.getElementById('ajax-loader-token-number').style.visibility = "hidden";
                    document.getElementById('ajax-loader-token-number').style.display = "none";

                    $(".certificate-fields").fadeOut(1000);

                    $('#targetDiv').empty();
                }
            },
            error: function () {
                
            }
        });

        // With the element initially shown, we can hide it slowly:
        $("#ajax-loader-certificate-number").fadeOut(1000);
})
//$("#val-certificate-number").on('keyup keypress', function (e) {
//    var keyCode = e.keyCode || e.which;
//    if (keyCode === 13) {
//        e.preventDefault();
//        var id = "val-certificate-number";
//        var value = document.getElementById("val-certificate-number").value;

//        document.getElementById('ajax-loader-certificate-number').style.visibility = "visible";
//        document.getElementById('ajax-loader-certificate-number').style.display = "block";

//        var certificateNumberDiv = document.getElementById("certificate-number-form-group");

//        var certificateNumberSuccessSpan = document.getElementById("certificate-number-success-span");
//        var certificateNumberErrorSpan = document.getElementById("certificate-number-error-span")

//        var validationSuccessMessage = document.getElementById("certificate-number-validation-success");
//        var validationErrorMessage = document.getElementById("certificate-number-validation-error");

//        //Make ajax call to the controller
//        $.ajax({
//            url: '/VerifyEUCPortal/VerifyCertificate',
//            data: {
//                'id': id,
//                'value': value,
//                'async': false
//            },
//            success: function (result) {
//                if (result.success) {
//                    var iframe = $('<iframe>');
//                    //iframe.width(600);
//                    //iframe.height(650);
//                    //iframe.attr('src', result.UploadUrl);
                    
//                    var pdfViewer = $('<object>');
//                    pdfViewer.width(600);
//                    pdfViewer.height(650);
//                    pdfViewer.attr('data', result.UploadUrl);

//                    $('#targetDiv').empty();

//                    $('#targetDiv').append(pdfViewer);
//                    //$('#targetDiv').append(iframe);
//                }

//                certificateNumberDiv.classList.remove("has-error");

//                certificateNumberErrorSpan.style.visibility = "hidden";

//                certificateNumberDiv.classList.add("has-success");
//                certificateNumberDiv.classList.add("has-feedback");

//                certificateNumberSuccessSpan.style.visibility = "visible";
//                certificateNumberSuccessSpan.style.display = "block";

//                validationSuccessMessage.style.visibility = "visible";
//                validationSuccessMessage.style.display = "block";

//                validationErrorMessage.style.visibility = "hidden";
//                validationErrorMessage.style.display = "none";

//                certificateNumberErrorSpan.style.visibility = "none";
//                certificateNumberErrorSpan.style.display = "hidden";

//                $(".certificate-fields").removeClass("hide");
//                $(".certificate-fields").show();
//            },
//            error: function () {
//                certificateNumberDiv.classList.remove("has-success");

//                certificateNumberSuccessSpan.style.visibility = "hidden";

//                certificateNumberDiv.classList.add("has-error");
//                certificateNumberDiv.classList.add("has-feedback");

//                certificateNumberErrorSpan.style.visibility = "visible";
//                certificateNumberErrorSpan.style.display = "block";

//                validationErrorMessage.style.visibility = "visible";
//                validationErrorMessage.style.display = "block";

//                validationSuccessMessage.style.visibility = "hidden";
//                validationSuccessMessage.style.display = "none";

//                certificateNumberSuccessSpan.style.visibility = "hidden";
//                certificateNumberSuccessSpan.style.display = "none";

//                $(".certificate-fields").fadeOut(1000);

//                $('#targetDiv').empty();
//            }
//        });

//        // With the element initially shown, we can hide it slowly:
//        $("#ajax-loader-certificate-number").fadeOut(1000);
//    }
//})
$("#val-mac-address").click(function (e) {
    e.preventDefault();
    alert("Here");
        //Make ajax call to the controller
        $.ajax({
            url: '/VerifyEUCPortal/GetMacUsingARP',
            data: {
                'async': false
            },
            success: function (result) {
                if (result.success) {
                    alert("User's MAC Address is: " + result.MacAddress);
                }

            },
            error: function () {

            }
        });

        // With the element initially shown, we can hide it slowly:
        $("#ajax-loader-certificate-number").fadeOut(1000);
})
$("#val-application-number").on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        var id = "val-application-number";
        var value = document.getElementById("val-application-number").value;
        var certificateNumber = document.getElementById("val-certificate-number").value;

        document.getElementById('ajax-loader-application-number').style.visibility = "visible";
        document.getElementById('ajax-loader-application-number').style.display = "block";

        var applicationNumberDiv = document.getElementById("application-number-form-group");

        var applicationNumberSuccessSpan = document.getElementById("application-number-success-span");
        var applicationNumberErrorSpan = document.getElementById("application-number-error-span")

        var validationApplicationNumberSuccessMessage = document.getElementById("application-number-validation-success");
        var validationApplicationNumberErrorMessage = document.getElementById("application-number-validation-error");

        //Make ajax call to the controller
        $.ajax({
            url: '/VerifyEUCPortal/VerifyApplicationNumber',
            data: {
                'id': id,
                'value': value,
                'async': true,
                'certificateNumber': certificateNumber
            },
            success: function () {
                applicationNumberDiv.classList.remove("has-error");

                applicationNumberErrorSpan.style.visibility = "hidden";

                applicationNumberDiv.classList.add("has-success");
                applicationNumberDiv.classList.add("has-feedback");

                applicationNumberSuccessSpan.style.visibility = "visible";
                applicationNumberSuccessSpan.style.display = "block";

                validationApplicationNumberSuccessMessage.style.visibility = "visible";
                validationApplicationNumberSuccessMessage.style.display = "block";

                validationApplicationNumberErrorMessage.style.visibility = "hidden";
                validationApplicationNumberErrorMessage.style.display = "none";

                applicationNumberErrorSpan.style.visibility = "none";
                applicationNumberErrorSpan.style.display = "hidden";
            },
            error: function () {
                applicationNumberDiv.classList.remove("has-success");

                applicationNumberSuccessSpan.style.visibility = "hidden";

                applicationNumberDiv.classList.add("has-error");
                applicationNumberDiv.classList.add("has-feedback");

                applicationNumberErrorSpan.style.visibility = "visible";
                applicationNumberErrorSpan.style.display = "block";

                validationApplicationNumberErrorMessage.style.visibility = "visible";
                validationApplicationNumberErrorMessage.style.display = "block";

                validationApplicationNumberSuccessMessage.style.visibility = "hidden";
                validationApplicationNumberSuccessMessage.style.display = "none";

                applicationNumberSuccessSpan.style.visibility = "hidden";
                applicationNumberSuccessSpan.style.display = "none";

            }
        });

        // With the element initially shown, we can hide it slowly:
        $("#ajax-loader-application-number").fadeOut(1000);
    }
})
$("#val-applicant-number").on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        var id = "val-applicant-number";
        var value = document.getElementById("val-applicant-number").value;
        var certificateNumber = document.getElementById("val-certificate-number").value;

        document.getElementById('ajax-loader-applicant-number').style.visibility = "visible";
        document.getElementById('ajax-loader-applicant-number').style.display = "block";

        var applicantNumberDiv = document.getElementById("applicant-number-form-group");

        var applicantNumberSuccessSpan = document.getElementById("applicant-number-success-span");
        var applicantNumberErrorSpan = document.getElementById("applicant-number-error-span")

        var validationApplicantNumberSuccessMessage = document.getElementById("applicant-number-validation-success");
        var validationApplicantNumberErrorMessage = document.getElementById("applicant-number-validation-error");

        //Make ajax call to the controller
        $.ajax({
            url: '/VerifyEUCPortal/VerifyApplicantNumber',
            data: {
                'id': id,
                'value': value,
                'async': true,
                'certificateNumber': certificateNumber
            },
            success: function () {
                applicantNumberDiv.classList.remove("has-error");

                applicantNumberErrorSpan.style.visibility = "hidden";

                applicantNumberDiv.classList.add("has-success");
                applicantNumberDiv.classList.add("has-feedback");

                applicantNumberSuccessSpan.style.visibility = "visible";
                applicantNumberSuccessSpan.style.display = "block";

                validationApplicantNumberSuccessMessage.style.visibility = "visible";
                validationApplicantNumberSuccessMessage.style.display = "block";

                validationApplicantNumberErrorMessage.style.visibility = "hidden";
                validationApplicantNumberErrorMessage.style.display = "none";

                applicantNumberErrorSpan.style.visibility = "none";
                applicantNumberErrorSpan.style.display = "hidden";
            },
            error: function () {
                applicantNumberDiv.classList.remove("has-success");

                applicantNumberSuccessSpan.style.visibility = "hidden";

                applicantNumberDiv.classList.add("has-error");
                applicantNumberDiv.classList.add("has-feedback");

                applicantNumberErrorSpan.style.visibility = "visible";
                applicantNumberErrorSpan.style.display = "block";

                validationApplicantNumberErrorMessage.style.visibility = "visible";
                validationApplicantNumberErrorMessage.style.display = "block";

                validationApplicantNumberSuccessMessage.style.visibility = "hidden";
                validationApplicantNumberSuccessMessage.style.display = "none";

                applicantNumberSuccessSpan.style.visibility = "hidden";
                applicantNumberSuccessSpan.style.display = "none";

            }
        });

        // With the element initially shown, we can hide it slowly:
        $("#ajax-loader-applicant-number").fadeOut(1000);
    }
})
$("#val-vendor-name").on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        var id = "val-vendor-name";
        var value = document.getElementById("val-vendor-name").value;
        var certificateNumber = document.getElementById("val-certificate-number").value;

        document.getElementById('ajax-loader-vendor-name').style.visibility = "visible";
        document.getElementById('ajax-loader-vendor-name').style.display = "block";

        var vendorNameDiv = document.getElementById("vendor-name-form-group");

        var vendorNameSuccessSpan = document.getElementById("vendor-name-success-span");
        var vendorNameErrorSpan = document.getElementById("vendor-name-error-span")

        var validationVendorNameSuccessMessage = document.getElementById("vendor-name-validation-success");
        var validationVendorNameErrorMessage = document.getElementById("vendor-name-validation-error");

        //Make ajax call to the controller
        $.ajax({
            url: '/VerifyEUCPortal/VerifyVendorName',
            data: {
                'id': id,
                'value': value,
                'async': true,
                'certificateNumber': certificateNumber
            },
            success: function () {
                vendorNameDiv.classList.remove("has-error");

                vendorNameErrorSpan.style.visibility = "hidden";

                vendorNameDiv.classList.add("has-success");
                vendorNameDiv.classList.add("has-feedback");

                vendorNameSuccessSpan.style.visibility = "visible";
                vendorNameSuccessSpan.style.display = "block";

                validationVendorNameSuccessMessage.style.visibility = "visible";
                validationVendorNameSuccessMessage.style.display = "block";

                validationVendorNameErrorMessage.style.visibility = "hidden";
                validationVendorNameErrorMessage.style.display = "none";

                vendorNameErrorSpan.style.visibility = "none";
                vendorNameErrorSpan.style.display = "hidden";
            },
            error: function () {
                vendorNameDiv.classList.remove("has-success");

                vendorNameSuccessSpan.style.visibility = "hidden";

                vendorNameDiv.classList.add("has-error");
                vendorNameDiv.classList.add("has-feedback");

                vendorNameErrorSpan.style.visibility = "visible";
                vendorNameErrorSpan.style.display = "block";

                validationVendorNameErrorMessage.style.visibility = "visible";
                validationVendorNameErrorMessage.style.display = "block";

                validationVendorNameSuccessMessage.style.visibility = "hidden";
                validationVendorNameSuccessMessage.style.display = "none";

                vendorNameSuccessSpan.style.visibility = "hidden";
                vendorNameSuccessSpan.style.display = "none";

            }
        });

        // With the element initially shown, we can hide it slowly:
        $("#ajax-loader-vendor-name").fadeOut(1000);
    }
})
$("#val-vendor-address").on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        var id = "val-vendor-address";
        var value = document.getElementById("val-vendor-address").value;
        var certificateNumber = document.getElementById("val-certificate-number").value;

        document.getElementById('ajax-loader-vendor-address').style.visibility = "visible";
        document.getElementById('ajax-loader-vendor-address').style.display = "block";

        var vendorAddressDiv = document.getElementById("vendor-address-form-group");

        var vendorAddressSuccessSpan = document.getElementById("vendor-address-success-span");
        var vendorAddressErrorSpan = document.getElementById("vendor-address-error-span")

        var validationVendorAddressSuccessMessage = document.getElementById("vendor-address-validation-success");
        var validationVendorAddressErrorMessage = document.getElementById("vendor-address-validation-error");

        //Make ajax call to the controller
        $.ajax({
            url: '/VerifyEUCPortal/VerifyVendorAddress',
            data: {
                'id': id,
                'value': value,
                'async': true,
                'certificateNumber': certificateNumber
            },
            success: function () {
                vendorAddressDiv.classList.remove("has-error");

                vendorAddressErrorSpan.style.visibility = "hidden";

                vendorAddressDiv.classList.add("has-success");
                vendorAddressDiv.classList.add("has-feedback");

                vendorAddressSuccessSpan.style.visibility = "visible";
                vendorAddressSuccessSpan.style.display = "block";

                validationVendorAddressSuccessMessage.style.visibility = "visible";
                validationVendorAddressSuccessMessage.style.display = "block";

                validationVendorAddressErrorMessage.style.visibility = "hidden";
                validationVendorAddressErrorMessage.style.display = "none";

                vendorAddressErrorSpan.style.visibility = "none";
                vendorAddressErrorSpan.style.display = "hidden";
            },
            error: function () {
                vendorAddressDiv.classList.remove("has-success");

                vendorAddressSuccessSpan.style.visibility = "hidden";

                vendorAddressDiv.classList.add("has-error");
                vendorAddressDiv.classList.add("has-feedback");

                vendorAddressErrorSpan.style.visibility = "visible";
                vendorAddressErrorSpan.style.display = "block";

                validationVendorAddressErrorMessage.style.visibility = "visible";
                validationVendorAddressErrorMessage.style.display = "block";

                validationVendorAddressSuccessMessage.style.visibility = "hidden";
                validationVendorAddressSuccessMessage.style.display = "none";

                vendorAddressSuccessSpan.style.visibility = "hidden";
                vendorAddressSuccessSpan.style.display = "none";

            }
        });

        // With the element initially shown, we can hide it slowly:
        $("#ajax-loader-vendor-address").fadeOut(1000);
    }
})
$("#val-currency").on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        var id = "currency-form-group";
        var value = $("#val-currency").val();
        var certificateNumber = document.getElementById("val-certificate-number").value;

        document.getElementById('ajax-loader-currency').style.visibility = "visible";
        document.getElementById('ajax-loader-currency').style.display = "block";

        var currencyDiv = document.getElementById("currency-form-group");

        var currencySuccessSpan = document.getElementById("currency-success-span");
        var currencyErrorSpan = document.getElementById("currency-error-span")

        var currencySuccessMessage = document.getElementById("currency-validation-success");
        var currencyErrorMessage = document.getElementById("currency-validation-error");

        //Make ajax call to the controller
        $.ajax({
            url: '/VerifyEUCPortal/VerifyCurrency',
            data: {
                'id': id,
                'value': value,
                'async': true,
                'certificateNumber': certificateNumber
            },
            success: function () {
                currencyDiv.classList.remove("has-error");

                currencyErrorSpan.style.visibility = "hidden";

                currencyDiv.classList.add("has-success");
                currencyDiv.classList.add("has-feedback");

                currencySuccessSpan.style.visibility = "visible";
                currencySuccessSpan.style.display = "block";

                currencySuccessMessage.style.visibility = "visible";
                currencySuccessMessage.style.display = "block";

                currencyErrorMessage.style.visibility = "hidden";
                currencyErrorMessage.style.display = "none";

                currencyErrorSpan.style.visibility = "none";
                currencyErrorSpan.style.display = "hidden";
            },
            error: function () {
                currencyDiv.classList.remove("has-success");

                currencySuccessSpan.style.visibility = "hidden";

                currencyDiv.classList.add("has-error");
                currencyDiv.classList.add("has-feedback");

                currencyErrorSpan.style.visibility = "visible";
                currencyErrorSpan.style.display = "block";

                currencyErrorMessage.style.visibility = "visible";
                currencyErrorMessage.style.display = "block";

                currencySuccessMessage.style.visibility = "hidden";
                currencySuccessMessage.style.display = "none";

                currencySuccessSpan.style.visibility = "hidden";
                currencySuccessSpan.style.display = "none";

            }
        });

        // With the element initially shown, we can hide it slowly:
        $("#ajax-loader-currency").fadeOut(1000);
    }
})
$("#val-total-cost").on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        var id = "vendor-total-cost-form-group";
        var value = document.getElementById("val-total-cost").value;
        var certificateNumber = document.getElementById("val-certificate-number").value;

        document.getElementById('ajax-loader-total-cost').style.visibility = "visible";
        document.getElementById('ajax-loader-total-cost').style.display = "block";

        var vendorTotalCostDiv = document.getElementById("vendor-total-cost-form-group");

        var totalCostSuccessSpan = document.getElementById("total-cost-success-span");
        var totalCostErrorSpan = document.getElementById("total-cost-error-span")

        var validationTotalCostSuccessMessage = document.getElementById("total-cost-validation-success");
        var validationTotalCostErrorMessage = document.getElementById("total-cost-validation-error");

        //Make ajax call to the controller
        $.ajax({
            url: '/VerifyEUCPortal/VerifyTotalCost',
            data: {
                'id': id,
                'value': value,
                'async': true,
                'certificateNumber': certificateNumber
            },
            success: function () {
                vendorTotalCostDiv.classList.remove("has-error");

                totalCostErrorSpan.style.visibility = "hidden";

                vendorTotalCostDiv.classList.add("has-success");
                vendorTotalCostDiv.classList.add("has-feedback");

                totalCostSuccessSpan.style.visibility = "visible";
                totalCostSuccessSpan.style.display = "block";

                validationTotalCostSuccessMessage.style.visibility = "visible";
                validationTotalCostSuccessMessage.style.display = "block";

                validationTotalCostErrorMessage.style.visibility = "hidden";
                validationTotalCostErrorMessage.style.display = "none";

                totalCostErrorSpan.style.visibility = "none";
                totalCostErrorSpan.style.display = "hidden";
            },
            error: function () {
                vendorTotalCostDiv.classList.remove("has-success");

                totalCostSuccessSpan.style.visibility = "hidden";

                vendorTotalCostDiv.classList.add("has-error");
                vendorTotalCostDiv.classList.add("has-feedback");

                totalCostErrorSpan.style.visibility = "visible";
                totalCostErrorSpan.style.display = "block";

                validationTotalCostErrorMessage.style.visibility = "visible";
                validationTotalCostErrorMessage.style.display = "block";

                validationTotalCostSuccessMessage.style.visibility = "hidden";
                validationTotalCostSuccessMessage.style.display = "none";

                totalCostSuccessSpan.style.visibility = "hidden";
                totalCostSuccessSpan.style.display = "none";

            }
        });

        // With the element initially shown, we can hide it slowly:
        $("#ajax-loader-total-cost").fadeOut(1000);
    }
})
$("#val-country").on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        var id = "country-form-group";
        var value = $("#val-country").val();
        var certificateNumber = document.getElementById("val-certificate-number").value;

        document.getElementById('ajax-loader-country').style.visibility = "visible";
        document.getElementById('ajax-loader-country').style.display = "block";

        var countryDiv = document.getElementById("country-form-group");

        var countrySuccessSpan = document.getElementById("country-success-span");
        var countryErrorSpan = document.getElementById("country-error-span")

        var countrySuccessMessage = document.getElementById("country-validation-success");
        var countryErrorMessage = document.getElementById("country-validation-error");

        //Make ajax call to the controller
        $.ajax({
            url: '/VerifyEUCPortal/VerifyDate',
            data: {
                'id': id,
                'value': value,
                'async': true,
                'certificateNumber': certificateNumber
            },
            success: function () {
                countryDiv.classList.remove("has-error");

                countryErrorSpan.style.visibility = "hidden";

                countryDiv.classList.add("has-success");
                countryDiv.classList.add("has-feedback");

                countrySuccessSpan.style.visibility = "visible";
                countrySuccessSpan.style.display = "block";

                countrySuccessMessage.style.visibility = "visible";
                countrySuccessMessage.style.display = "block";

                countryErrorMessage.style.visibility = "hidden";
                countryErrorMessage.style.display = "none";

                countryErrorSpan.style.visibility = "none";
                countryErrorSpan.style.display = "hidden";
            },
            error: function () {
                countryDiv.classList.remove("has-success");

                countrySuccessSpan.style.visibility = "hidden";

                countryDiv.classList.add("has-error");
                countryDiv.classList.add("has-feedback");

                countryErrorSpan.style.visibility = "visible";
                countryErrorSpan.style.display = "block";

                countryErrorMessage.style.visibility = "visible";
                countryErrorMessage.style.display = "block";

                countrySuccessMessage.style.visibility = "hidden";
                countrySuccessMessage.style.display = "none";

                countrySuccessSpan.style.visibility = "hidden";
                countrySuccessSpan.style.display = "none";

            }
        });

        // With the element initially shown, we can hide it slowly:
        $("#ajax-loader-country").fadeOut(1000);
    }
})
$("#val-date").on('keyup keypress', function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
        e.preventDefault();
        var id = "date-form-group";
        var value = $("#val-date").val();
        var certificateNumber = document.getElementById("val-certificate-number").value;

        document.getElementById('ajax-loader-date').style.visibility = "visible";
        document.getElementById('ajax-loader-date').style.display = "block";

        var dateDiv = document.getElementById("date-form-group");

        var dateSuccessSpan = document.getElementById("date-success-span");
        var dateErrorSpan = document.getElementById("date-error-span")

        var dateSuccessMessage = document.getElementById("date-validation-success");
        var dateErrorMessage = document.getElementById("date-validation-error");

        //Make ajax call to the controller
        $.ajax({
            url: '/VerifyEUCPortal/VerifyDate',
            data: {
                'id': id,
                'value': value,
                'async': true,
                'certificateNumber': certificateNumber
            },
            success: function () {
                dateDiv.classList.remove("has-error");

                dateErrorSpan.style.visibility = "hidden";

                dateDiv.classList.add("has-success");
                dateDiv.classList.add("has-feedback");

                dateSuccessSpan.style.visibility = "visible";
                dateSuccessSpan.style.display = "block";

                dateSuccessMessage.style.visibility = "visible";
                dateSuccessMessage.style.display = "block";

                dateErrorMessage.style.visibility = "hidden";
                dateErrorMessage.style.display = "none";

                dateErrorSpan.style.visibility = "none";
                dateErrorSpan.style.display = "hidden";
            },
            error: function () {
                dateDiv.classList.remove("has-success");

                dateSuccessSpan.style.visibility = "hidden";

                dateDiv.classList.add("has-error");
                dateDiv.classList.add("has-feedback");

                dateErrorSpan.style.visibility = "visible";
                dateErrorSpan.style.display = "block";

                dateErrorMessage.style.visibility = "visible";
                dateErrorMessage.style.display = "block";

                dateSuccessMessage.style.visibility = "hidden";
                dateSuccessMessage.style.display = "none";

                dateSuccessSpan.style.visibility = "hidden";
                dateSuccessSpan.style.display = "none";

            }
        });

        // With the element initially shown, we can hide it slowly:
        $("#ajax-loader-date").fadeOut(1000);
    }
})
$("#val-submit").click(function (e) {
    if ($("#val-certificate-number").val() == ""
        || $("#val-application-number").val() == ""
        || $("#val-applicant-number").val() == ""
        || $("#val-vendor-name").val() == ""
        || $("#val-vendor-address").val() == ""
        || $("#val-currency").val() == ""
        || $("#val-total-cost").val() == ""
        || $("#val-country").val() == ""
        || $("#val-date").val() == "") {
        alert("Ensure all fields are entered before clicking the validate button.")
        e.preventDefault();
        return false;
    }
    else if ($('div.has-error').length) {
        alert("Ensure all fields are valid before attempting to verify an End-User Certificate.")
        e.preventDefault();
        return false;
    }
    else {
        var CertNo = $("#val-certificate-number").val();

        //Make ajax call to the controller
        $.ajax({
            url: '/VerifyEUCPortal/ValidateCertificate',
            async: false,
            data: {
                'CertNo': CertNo
            },
            success: function () {
                alert("Completed Successfully");
            },
            error: function () {
                alert("Could not Verify Certificate");
            }
        });
    }

});