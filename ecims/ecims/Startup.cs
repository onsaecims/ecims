﻿using ecims.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ecims.Startup))]
namespace ecims
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesandUsers();
        }

        // In this method we will create default User roles and Admin user for login    
        private void createRolesandUsers()
        {
            ApplicationDbContext _context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_context));


            // Create first Admin Role and default Admin User     
            if (!roleManager.RoleExists("NSA"))
            {
                // first we create Admin role    
                var role = new IdentityRole();
                role.Name = "NSA";
                roleManager.Create(role);                
            }
            // Creating ONSA Admin role     
            if (!roleManager.RoleExists("ONSA Admin"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "ONSA Admin";
                roleManager.Create(role);
            }

            // Creating ONSA Support role     
            if (!roleManager.RoleExists("ONSA Support"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "ONSA Support";
                roleManager.Create(role);
            }

            // Creating Agency role     
            if (!roleManager.RoleExists("Agency"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Agency";
                roleManager.Create(role);
            }
            
            //Creating Client User role     
            if (!roleManager.RoleExists("Client User"))
            {
                var role = new IdentityRole();
                role.Name = "Client User";
                roleManager.Create(role);
            }
            //Creating Customs User role     
            if (!roleManager.RoleExists("Customs User"))
            {
                var role = new IdentityRole();
                role.Name = "Customs User";
                roleManager.Create(role);
            }
            //Creating Exclusive User role     
            if (!roleManager.RoleExists("Exclusive User"))
            {
                var role = new IdentityRole();
                role.Name = "Exclusive User";
                roleManager.Create(role);
            }
            //Creating Exclusive User role     
            if (!roleManager.RoleExists("Certificate Verification User"))
            {
                var role = new IdentityRole();
                role.Name = "Certificate Verification User";
                roleManager.Create(role);
            }
            //Creating ONSA PGSO User role     
            if (!roleManager.RoleExists("ONSA PGSO"))
            {
                var role = new IdentityRole();
                role.Name = "ONSA PGSO";
                roleManager.Create(role);
            }
        }
    }
}
