﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using ecims.Models;
using PagedList;

namespace ecims.ViewModels
{
    public class AllVerifiedEndUserCertififcatesViewModel : BaseDashboardViewModel
    {

        public List<PortalSubCategories> Agencies { get; internal set; }
        public IPagedList<VerifiedCertificates> VerifiedEndUserCertificate { get; internal set; }
        public List<ApplicationUser> UsersToQuery { get; internal set; }
        public int PageSize { get; internal set; }
        public string sortOrder { get; internal set; }
        public object searchTerm { get; internal set; }
        public List<UploadedEUCCertificate> UploadedCertificates { get; internal set; }
    }
}