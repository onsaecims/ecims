﻿using System.IO;
using System.Text;

namespace ecims.ViewModels
{
    public class ImageBuilder
    {
        public ImageBuilder()
        {

        }

        public void buildImage(DirectoryInfo directory, string extension)
        {
            StringBuilder builder = new StringBuilder();
            string fileDirectory = directory.FullName;            
            FileInfo[] files = directory.GetFiles();

            foreach (FileInfo file in files)
            {
                //if (string.IsNullOrEmpty(Path.GetExtension(fileDirectory)))
                fileDirectory += "\\" + file.Name;
                     
                if (string.IsNullOrEmpty(Path.GetExtension(fileDirectory)))
                {
                    System.IO.File.Move(fileDirectory, fileDirectory + extension); 
                }
            }
        }
    }
}