﻿using ecims.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ecims.ViewModels
{
    public class NewEUCApplicationViewModel : BaseDashboardViewModel
    {
        public string ApplicationNumber { get; set; }

        public IEnumerable<EUCItemCategory> itemCategory { get; set; }

        public int itemCategoryId { get; set; }

        //[Required(ErrorMessage = "Please select your country of origin")]
        public IEnumerable<CountryList> Country { get; set; }

        public int countryId { get; set; }

        //[Required(ErrorMessage = "Please select Item country of origin")]
        public IEnumerable<CountryList> ItemCountry { get; set; }

        public int itemCountryId { get; set; }

        public string purposeOfUsage { get; set; }

        public int currencyOfPurchase { get; set; }

        public IEnumerable<Currency> Currencies { get; set; }

        public int portOfLanding { get; set; }

        public IEnumerable<PortOfLanding> PortsOfLanding { get; set; }

        public ApplicationUser userId { get; set; }

        [Required]
        public int UserIdentification { get; set; }

        public EUCApprovedStatus applicationStatus { get; set; }

        //Item section

        public string ApplicationNo { get; set; }

        public int HSCode { get; set; }

        public IEnumerable<HSCode> HSCodes { get; set; }

        public string ItemDescription { get; set; }

        public QuantityUnit Weight { get; set; }

        public double Quantity { get; set; }
        public string Qty { get; set; }

        public double Cost { get; set; }

        public List<EUCItem> Items { get; set; }
        
        //Attachment section

        public HttpPostedFileBase CbnApplicationLetter { get; set; }

        public HttpPostedFileBase MinistryOfAgricApplication { get; set; }

        public HttpPostedFileBase NCCAAApprovalLetter { get; set; }

        public HttpPostedFileBase ARPIdentification { get; set; }

        public HttpPostedFileBase MemorandumOfAssociation { get; set; }

        public HttpPostedFileBase BillOfLadenOrAirwayBill { get; set; }

        public HttpPostedFileBase CertificateOfAnalysis { get; set; }

        public HttpPostedFileBase CertificateOfIncorporation { get; set; }

        public HttpPostedFileBase CompanyDirectorsProfile { get; set; }

        public HttpPostedFileBase CompletionOfApplicationForm { get; set; }

        public HttpPostedFileBase CurrentTaxClearance { get; set; }

        public HttpPostedFileBase DistributionNetworkOfPreviousImportationBeneficiariesStockBalances { get; set; }

        public HttpPostedFileBase NSCDCRegistration { get; set; }

        public HttpPostedFileBase CITLicense { get; set; }

        public HttpPostedFileBase FIRSCertificate { get; set; }

        public HttpPostedFileBase FormCO7 { get; set; }

        public HttpPostedFileBase FormMCbn { get; set; }

        public HttpPostedFileBase IdentificationCard { get; set; }

        public HttpPostedFileBase LastYearEUCS { get; set; }

        public HttpPostedFileBase LetterOfIntroductionFromAcquirer { get; set; }

        public HttpPostedFileBase MagazineLicenses { get; set; }

        public HttpPostedFileBase ManufacturersSafety { get; set; }

        public HttpPostedFileBase NAFDACAttachment { get; set; }

        public HttpPostedFileBase NPFFireworksPermit { get; set; }

        public HttpPostedFileBase PackingList { get; set; }

        public HttpPostedFileBase ExplosivesPermit { get; set; }

        public HttpPostedFileBase PreviousStorageAttachment { get; set; }

        public HttpPostedFileBase ProformaInvoice { get; set; }

        public HttpPostedFileBase QualifiedTechnicalOfficerDetails { get; set; }

        public HttpPostedFileBase RegWithFFD { get; set; }

        public HttpPostedFileBase SpecificationOfGoods { get; set; }

        public HttpPostedFileBase StorageFacilityAttachment { get; set; }

        public HttpPostedFileBase TransitDetails { get; set; }

        public HttpPostedFileBase TransporterAndVehicleParticuulars { get; set; }

        public HttpPostedFileBase VATCertificate { get; set; }

        public HttpPostedFileBase VendorIdentificationDetails { get; set; }

        public HttpPostedFileBase YearsOfOperationAttachment { get; set; }

        //Organisation Application Section
        public bool IsOrganisation { get; set; }

        public string OrganisationName { get; set; }

        public string OrganisationAddress { get; set; }

        public HttpPostedFileBase AwardContract { get; set; }

        public HttpPostedFileBase LicenseFromNBCOrNCC { get; set; }
    }
}