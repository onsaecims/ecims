﻿using ecims.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.ViewModels
{
    public class AllUserSignUpRequestsViewModel : BaseDashboardViewModel
    {
        public IPagedList<ApplicationUser> usersToShow { get; internal set; }
        public List<ApplicationUser> users { get; internal set; }
        public List<BlackListApplicant> BlacklistedApplicants { get; set; }
        public int? PageSize { get; internal set; }
        //List of page sizes to be shown in view 
        public List<int> pageSizeList { get; set; }
        public string searchTerm { get; set; }
        public string sortOrder { get; set; }
        public List<PortalSubCategories> UserAgency { get; set; }
    }
}