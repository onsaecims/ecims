﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ecims.Models;
using PagedList;

namespace ecims.ViewModels
{
    public class EUCViewModel : BaseDashboardViewModel
    {
        public IPagedList<EUCCertificates> certificatesToShow { get; internal set; }
        public IPagedList<EndUserCertificate> eucapplication { get; internal set; }
        public List<ApplicationUser> users { get; internal set; }
        public int? PageSize { get; internal set; }
        //List of page sizes to be shown in view 
        public List<int> pageSizeList { get; set; }
        public string searchTerm { get; set; }
        public string sortOrder { get; set; }
        public List<PortalSubCategories> Agencies { get; internal set; }      
        public List<ApplicationUser> UsersToQuery { get; internal set; }
        public IPagedList<EUCWorkFlow> applicationsFromWorflowToShow { get; internal set; }
        public List<EUCItemCategory> itemCategories { get; set; }
        public List<ApplicationUser> userList { get; internal set; }
        public List<UploadedEUCCertificate> UploadedCertificates { get; set; }
    }
}