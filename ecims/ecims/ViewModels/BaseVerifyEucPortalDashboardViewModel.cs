﻿using ecims.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace ecims.ViewModels
{
    public abstract class BaseVerifyEucPortalDashboardViewModel
    {
        public List<Claim> role { get; set; }
        public ApplicationUser user { get; set; }
        public string UserId { get; set; }
        public List<PortalSubCategories> Agency { get; set; }
    }
}