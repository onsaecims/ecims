﻿using ecims.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.ViewModels
{
    public class BlacklistedViewModel : BaseDashboardViewModel
    {
        public List<ApplicationUser> BlackListedApplicant { get; set; }
        public List<ApplicationUser> UserList { get; set; }
        public List<ApplicationUser> AllUsers { get; set; }
        public IPagedList<BlackListApplicant> BlacklistHistory { get; set; }
        public List<BlackListApplicant> BlacklistDetails { get; set; }
        public string ApplicantID { get; set; }
        public ApplicationUser ApplicantDetail { get; set; }
        public int? PageSize { get; internal set; }
        //List of page sizes to be shown in view 
        public List<int> pageSizeList { get; set; }
        public string searchTerm { get; set; }
        public string sortOrder { get; set; }
    }
}