﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ecims.Models;

namespace ecims.ViewModels.UserDataViaExcel
{
    public class UserDataViaExcelViewmodel
    {
        public int? Page { get; set; }
        [Display(Name ="First Name")]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public IPagedList<ApplicationUser> SearchResults { get; set; }
        public string SearchButton { get; set; }
    }
}