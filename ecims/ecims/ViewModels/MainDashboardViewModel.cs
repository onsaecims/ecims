﻿using ecims.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System;
using System.Web.Mvc;
using System.Web;
using PagedList;
using System.Linq;

namespace ecims.ViewModels
{
    public abstract class BaseDashboardViewModel
    {
        public ApplicationUser user { get; set; }
        public List<System.Security.Claims.Claim> role { get; set; }
        public string serverResponse { get; set; }
        public int pendingSignupRequest { get; set; }
        public int approvedUserRequest { get; set; }
        public int rejectedUserRequest { get; set; }
        public int pendingEUCRequest { get; set; }
        public int approvedEUCRequest { get; set; }
        public int disapprovedEUCRequest { get; set; }
        public int endorsedEUCRequest { get; set; }
        public int requireApproval { get; set; }
        public int rejectedEUCRequest { get; set; }
        public int allEUC { get; set; }
        public int allSignUpRequest { get; set; }
        public List<PortalSubCategories> Agency { get; set; }
        //pie chart data
        public int pendingPie{ get; set; }
        public int approvedPie { get; set; }
        public int rejectPie { get; set; }
        //Bar chart data
        public int forwardedEUC { get; set; }
        public int disendorsedEUC { get; set; }
        //agency pie chart values
        public int pendingAgencyData { get; set; }
        public int approvedAgencyData { get; set; }
        //EUC Cert Chart Value
        public int DisputedCert { get; set; }
        public int ExpiredCert { get; set; }
        public int IssuedCert { get; set; }
        public int PendingCert { get; set; }
        public int RecalledCert { get; set; }
        public int UtilizedCert { get; set; }
        public int allCert { get; set; }
        public int endorsedEUC { get; set; }
        public int workedOn { get; set; }
        public string User_Id { get; set; }
    }
    public class DashboardViewModel : BaseDashboardViewModel
    {
        public List<ApplicationUser> userList { get; set; }
        public string AccountType { get; set; }
        public string UserId { get; set; }
    }

    public class applicantDetailsViewModel : BaseDashboardViewModel
    {
        public ApplicationUser Applicant { get; set; }
        public Company Company { get; set; }
        public List<EndUserCertificate> EndUserCertificate { get; set; }
        public List<ApplicationUser> UsersToQuery { get; set; }
        public List<EUCItemCategory> EndUserCategories { get; internal set; }
        public List<MeansOfIdentification> MeansOfIdentification { get; internal set; }
        [Required]
        public string RejectReason { get; set; }
        public List<CountryList> Countries { get; set; }
        public IQueryable<EUCOrganisations> Organisations { get; internal set; }
        public int DoesCompanyExist { get; set; }
    }
    public class EUCApplicationDetailsViewModel : BaseDashboardViewModel
    {
        public List<ApplicationUser> Applicant { get; set; }
        public List<Company> Company { get; set; }
        public List<EndUserCertificate> EUCAppDetails { get; set; }
        //public EndUserCertificate EUCAppDetails { get; set; }
        public List<EUCApplicationAttachments> EUCAppAttach { get; set; }
        public List<EndUserCertificateDetails> EUCAppItemDetails { get; set; }
        public List<AgencyDesk> AgencyDesk { get; set; }
        public List<HSCode> hsCode { get; set; }
        public List<EUCItemCategory> itemCategory { get; set; }
        public List<PortOfLanding> landingPort { get; set; }
        public EUCApprovedStatus approvalStatus { get; set; }
        public List<Currency> Currencies { get; set; }
        public List<EUCApplicationAttachments> supportingDocument { get; set; }
        public int FlagCount { get; set; }
        public List<EUCApplicationFlags> FlagList { get; set; }
        public int CommentCount { get; set; }
        public List<EUCApplicationComments> CommentList { get; set; }
        public int EndorseCount { get; set; }
        public List<EUCApplicationEndorsers> EndorseList { get; set; }
        public int rejectCount { get; set; }
        public List<EUCRejectComments> rejectList { get; set; }
        public int disendorse { get; set; }
        public List<DisendorsedEUCAppComments> disendorseList { get; set; }
        public List<PortalCategories> PortalCat { get; set; }
        public List<PortalSubCategories> PortalSubCat { get; set; }
        public List<EUCOrganisations> EUCOrgList { get; set; }
        public List<CountryList> Countries { get; set; }
    }
    public class allEUCApplicationsViewModel : BaseDashboardViewModel
    {
        public List<PortalSubCategories> Agencies { get; internal set; }
        public List<EUCItemCategory> EndUserCategories { get; internal set; }
        public List<EndUserCertificate> EndUserCertificate { get; set; }
        public EUCItemCategory EUCItemCategory { get; set; }
        public List<ApplicationUser> UsersToQuery { get; set; }
    }
    public class forwardedEUCApplicationViewModel : BaseDashboardViewModel
    {
        public List<PortalSubCategories> Agencies { get; internal set; }
        public List<EUCItemCategory> EndUserCategories { get; internal set; }
        public List<EndUserCertificate> EndUserCertificate { get; set; }
        public EUCItemCategory EUCItemCategory { get; set; }
        public List<ApplicationUser> UsersToQuery { get; set; }
    }
    public class AwaitingApprovalViewModel : BaseDashboardViewModel
    {
        public List<EUCItemCategory> EndUserCategories { get; internal set; }
        public List<EndUserCertificate> EndUserCertificate { get; set; }
        public EUCItemCategory EUCItemCategory { get; set; }
        public List<ApplicationUser> UsersToQuery { get; set; }
    }
    public class approvedEUCApplicationViewModel : BaseDashboardViewModel
    {
        public List<EUCItemCategory> EndUserCategories { get; internal set; }
        public List<EndUserCertificate> EndUserCertificate { get; set; }
        public EUCItemCategory EUCItemCategory { get; set; }
        public List<ApplicationUser> UsersToQuery { get; set; }
    }

    public class pendingEUCApplicationViewModel : BaseDashboardViewModel
    {
        public List<EUCItemCategory> EndUserCategories { get; internal set; }
        public List<EndUserCertificate> EndUserCertificate { get; set; }
        public EUCItemCategory EUCItemCategory { get; set; }

        public List<ApplicationUser> UsersToQuery { get; set; }
    }

    public class rejectedEUCApplicationViewModel : BaseDashboardViewModel
    {
        public List<EUCItemCategory> EndUserCategories { get; internal set; }
        public List<EndUserCertificate> EndUserCertificate { get; set; }
        public EUCItemCategory EUCItemCategory { get; set; }
        public List<ApplicationUser> UsersToQuery { get; set; }
        
    }

    public class endorsedEUCApplicationViewModel : BaseDashboardViewModel
    {
        public List<EUCItemCategory> EndUserCategories { get; internal set; }
        public List<EndUserCertificate> EndUserCertificate { get; set; }
        public List<EUCWorkFlow> EUCWorkflow { get; set; }
        public EUCItemCategory EUCItemCategory { get; set; }
        public List<ApplicationUser> UsersToQuery { get; set; }
        public List<EUCWorkFlow> EndorsedEUCCertificates { get; set; }
        public ApplicationUser UserAgency { get; set; }
    }

    public class disendorsedEUCApplicationViewModel : BaseDashboardViewModel
    {
        public List<EUCItemCategory> EndUserCategories { get; internal set; }
        public List<EndUserCertificate> EndUserCertificate { get; set; }
        public List<EUCWorkFlow> WorkFlow { get; set; }
        public EUCItemCategory EUCItemCategory { get; set; }
        public List<ApplicationUser> UsersToQuery { get; set; }
        public ApplicationUser UserAgency { get; set; }
    }

    public class TrackEUCAppViewModel : BaseDashboardViewModel
    {
        public string EucAppNo { get; set; }
        public List<EndUserCertificate> EUCAppDetails { get; set; }
        public List<PortalSubCategories> AgencyList { get; set; }
        //EUC Track List
        public List<EUCWorkFlow> EUCAppList { get; set; }
        public List<ApplicationUser> UserList { get; set; }
    }

    public class AgencyDataViewModel : BaseDashboardViewModel
    {
        [Required]
        public int Id { get; set; }
        public int DeskId { get; set; }
        public List<PortalSubCategories> AgencyList { get; set; }
        public List<AgencyDesk> AgencyDesk { get; set; }
        public List<PortalCategories> AgencyType { get; set; }
        public IEnumerable<PortalCategories> agency_Type { get; set; }
        [Required(ErrorMessage = "Please select Agency Type")]
        public int agency_TypeId { get; set; }
        //[Required(ErrorMessage = "Please Enter Agency Name")]
        public string agency_Name { get; set; }
        //[Required(ErrorMessage = "Please Enter Contact Name")]
        public string contact_Name { get; set; }
        //[Required(ErrorMessage = "Please Enter Agency Phone")]
        [DataType(DataType.PhoneNumber)]
        public string agency_Phone { get; set; }
        //[Required(ErrorMessage = "Please Enter Agency Email")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string agency_Email { get; set; }
        public string agency_TypeName { get; set; }
        public IEnumerable<AgencyDesk> agency_NameDesk { get; set; }
        [Required(ErrorMessage = "Please Select Agency Name")]
        public int agency_NameId { get; set; }
        //[Required(ErrorMessage = "Please Enter Agency Desk Name")]
        public string desk_Name { get; set; }
    }
    public class CurrencyMgtViewModel : BaseDashboardViewModel
    {
        public List<Currency> CurrencyList { get; set; }
        [Required(ErrorMessage = "Please Enter Currency Name")]
        public string currencyName { get; set; }
        [Required(ErrorMessage = "Please Enter Currency Symbol")]
        public string currencySymbol { get; set; }
    }

    public class HSCodeViewModel : BaseDashboardViewModel
    {
        public int Id { get; set; }
        public int CodeId { get; set; }
        public List<HSCode> HSCodeList { get; set; }
        public List<EUCItemCategory> itemCatList { get; set; }
        public string HSCodeName { get; set; }
        //[Required(ErrorMessage = "Please Enter Item Category Name")]
        public string itemCatName { get; set; }
        public IEnumerable<HSCodeUserCat> HsCodeUserCategory { get; set; }
        //[Required(ErrorMessage = "Please Select Which user can access the Category")]
        public int SelApplicantType { get; set; }
        public IEnumerable<EUCItemCategory> EUCItemCat { get; set; }
        //[Required(ErrorMessage = "Please Select Item Category")]
        public int itemCat { get; set; }
        //[Required(ErrorMessage = "Please Enter Item Sub Category Name")]
        public string itemSubCatName { get; set; }
        //[Required(ErrorMessage = "Please Enter HS Code")]
        public string itemSubCatHSCode { get; set; }
    }
    public class PortMgtViewModel : BaseDashboardViewModel
    {
        public int Id { get; set; }
        public List<PortOfLanding> PortOfLanding { get; set; }
        public IEnumerable<StateOfOrigin> State { get; set; }
        [Required(ErrorMessage = "Please Select State")]
        public int StateId { get; set; }
        [Required(ErrorMessage = "Please Enter Port Code")]
        public string PortCode { get; set; }
    }
    public class BlacklistViewModel : BaseDashboardViewModel
    {
        public List<ApplicationUser> BlackListedApplicant { get; set; }
        public ApplicationUser UserList { get; set; }
        public List<ApplicationUser> AllUsers { get; set; }
        public List<BlackListApplicant> BlacklistHistory { get; internal set; }
        public BlackListApplicant BlacklistDetails { get; set; }
        public string ApplicantID { get; set; }
        public ApplicationUser ApplicantDetail { get; set; }
        public DateTime BlacklistDate { get; set; }
        public string action { get; set; }
        public string BlacklistedBy { get; set; }
        [Required(ErrorMessage = "Please Provide reason for Blacklist this user")]
        public string BlacklistReason { get; set; }
        public int? PageSize { get; internal set; }
        //List of page sizes to be shown in view 
        public List<int> pageSizeList { get; set; }
        public string searchTerm { get; set; }
        public string sortOrder { get; set; }
    }
    public class allCertViewModel : BaseDashboardViewModel
    {
        public List<EUCCertificates> EUCCertificates { get; set; }
        public List<ApplicationUser> UsersToQuery { get; set; }
        public List<UploadedEUCCertificate> upload { get; set; }
    }
    public class expiredCertViewModel : BaseDashboardViewModel
    {
        public List<EUCCertificates> EUCCertificates { get; set; }
        public List<ApplicationUser> UsersToQuery { get; set; }
    }
    public class recalledCertViewModel : BaseDashboardViewModel
    {
        public List<EUCCertificates> EUCCertificates { get; set; }
        public List<ApplicationUser> UsersToQuery { get; set; }
    }
    public class utilizedCertViewModel : BaseDashboardViewModel
    {
        public List<EUCCertificates> EUCCertificates { get; set; }
        public List<ApplicationUser> UsersToQuery { get; set; }
    }
    public class disputeCertViewModel : BaseDashboardViewModel
    {
        public List<EUCCertificates> EUCCertificates { get; set; }
        public List<ApplicationUser> UsersToQuery { get; set; }
    }
    public class EUCCertDetailsViewModel : BaseDashboardViewModel
    {
        public List<EUCCertificates> EUCCertificates { get; set; }
        public List<ApplicationUser> UsersToQuery { get; set; }
        public List<DisputeEUCCertificates> disputeList { get; set; }
    }

    public class ForwardApplicationViewModel : BaseDashboardViewModel
    {
        public IEnumerable<SelectListItem> Agency { get; set; }
        public int[] SelectedAgency { get; set; }
        public IEnumerable<PortalSubCategories> GetONSAAgencies { get; set; }
        public int ONSAAgID { get; set; }
        public string ForwardComment { get; set; }
        public DateTime ForwardDate { get; set; }
        public string EUCID { get; set; }
        
        public string ForwardedBy { get; set; }
    }
    public class RejectApplicationViewModel : BaseDashboardViewModel
    {
        [Required(ErrorMessage = "Kindly provide the reason(s) for rejecting this EUC application")]
        public string RejectComment { get; set; }
        public DateTime RejectDate { get; set; }
        public string EUCID { get; set; }
        public string RejectedBy { get; set; }
    }
    public class RecallApplicationViewModel : BaseDashboardViewModel
    {
        [Required(ErrorMessage = "Please Provide reason for Recalling this Application")]
        public string RecallComment { get; set; }
        public DateTime RecallDate { get; set; }
        public string EUCID { get; set; }
        public string RecalledBy { get; set; }
        public string CertificateNumber { get; internal set; }
    }
    public class DisendorseApplicationViewModel : BaseDashboardViewModel
    {
        [Required(ErrorMessage = "Please Provide reason for Dis-Endorsing this Application")]
        public string DisendorseComment { get; set; }
        public DateTime DisendorseDate { get; set; }
        public string EUCID { get; set; }
        public string DisendorseBy { get; set; }
    }
    public class EndorseApplicationViewModel : BaseDashboardViewModel
    {
        [Required(ErrorMessage = "Provide comment for Endorsing this Application")]
        [DataType(DataType.MultilineText)]
        public string EndorseComment { get; set; }
        public bool Selected { get; set; }
        public DateTime DisendorseDate { get; set; }
        public string EUCID { get; set; }
        public string EndorseBy { get; set; }
        public int EndorseDesk { get; set; }
        public int EndorseAgency { get; set; }
        [DataType(DataType.MultilineText)]
        public string flagComment { get; set; }
        public DateTime flagDate { get; set; }
        public List<AgencyDesk> agencyDesks { get; internal set; }
        public string agencyName { get; internal set; }
    }

    public class AuditTrailViewModel : BaseDashboardViewModel
    {
        public IPagedList<AuditTrail> AuditTrailList { get; set; }
        public List<AuditTrail> AuditTrails { get; set; }
        public List<ApplicationUser> UserList { get; set; }
        public int? PageSize { get; internal set; }
        //List of page sizes to be shown in view 
        public List<int> pageSizeList { get; set; }
        public string searchTerm { get; set; }
        public string sortOrder { get; set; }
    }

    public class IssueCertificateViewModel : BaseDashboardViewModel
    {
        public string EUCID { get; set; }
    }

    public class ApproveApplicationViewModel : BaseDashboardViewModel
    {
        public List<EndUserCertificate> EUCDetails { get; set; }
        public List<EUCAppEndorsersDesk> EndorsementDesk { get; set; }
        public List<ForwardEUCAppComment> CommentList { get; set; }
        public List<ApplicationUser> Applicant { get; set; }
        public List<PortalCategories> PortalCat { get; set; }
        public List<AgencyDesk> AgencyDesk { get; set; }
        public List<PortalSubCategories> Agency { get; set; }
        public List<PortalCategories> AgencyCat { get; set; }
        public string EUCID { get; set; }
        public DateTime ApproveDate { get; set; }
        [Required(ErrorMessage = "Please Provide the Generated Token to proceed with the approval")]
        public string TokenCode { get; set; }
        public string AprovedBy { get; set; }
        public string token { get; internal set; }
    }

    public class CollectCertificateViewModel : BaseDashboardViewModel
    {
        public string CertNumber { get; set; }
        public string CollectionToken { get; set; }
        public string NewCertNumber { get; set; }
    }

    public class UploadCertificateViewModel : BaseDashboardViewModel
    {
        public string CertNumber { get; set; }
        [Required(ErrorMessage = "Please upload a certificate.")]
        public HttpPostedFileBase UploadedCert { get; set; }
        public string EUCUserID { get; set; }
        public List<UploadedEUCCertificate> UploadedCertList { get; set; }
        public List<EUCCertificates> EUCCertificates { get; set; }
        public List<ApplicationUser> UsersToQuery { get; set; }
    }
    public class DisputeCertificateViewModel : BaseDashboardViewModel
    {
        public string CERTID { get; set; }
        [Required(ErrorMessage = "Please Provide Comment for Disputing this Certificate")]
        public string DisputeComment { get; set; }
        public string DisputeStatus { get; set; }
    }
    public class AnalyticsViewModel : BaseDashboardViewModel
    {
        public ReportType RptType { get; set; }
        public CertReportType CertRptType { get; set; }
        public UserReportType UserRptType { get; set; }
        public ChartRptType ChartRptType { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string ApplicantNo { get; set; }
        public EUCApprovedStatus ApproveStatus { get; set; }
        public IEnumerable<CountryList> Country { get; set; }
        public int CountryID { get; set; }
        public IEnumerable<EUCItemCategory> ItemCat { get; set; }
        public int ItemCatID { get; set; }
        public IEnumerable<PortOfLanding> PortOfLaden { get; set; }
        public int PortID { get; set; }
        public CertificateStatus CertStatus { get; set; }
        public string ApplicationNumber { get; set; }
        public DateTime CertDateFrom { get; set; }
        public DateTime CertDateTo { get; set; }
        public DateTime UserDateFrom { get; set; }
        public DateTime UserDateTo { get; set; }
        public DateTime ChartDateFrom { get; set; }
        public DateTime ChartDateTo { get; set; }
        public Gender Gender { get; set; }
        public MaritalStatus MaritalStatus { get; set; }
        public IEnumerable<StateOfOrigin> State { get; set; }
        public int StateID { get; set; }
        public IEnumerable<PortalSubCategories> AgencyList { get; set; }
        public int AgencyID { get; set; }
        public UserType UserType { get; set; }
        public ApprovedStatus UserApprovedStatus { get; set; }
    }
    public class FeedbackViewModel : BaseDashboardViewModel
    {
        public string UserId { get; set; }
        [Required(ErrorMessage = "Please provide subject")]
        public string Subject { get; set; }
        [Required(ErrorMessage = "Please enter feedback message")]
        public string Message { get; set; }
        public DateTime FeedbackDate { get; set; }
        public List<FeedBack> AllFeedBacks { get; set; }
        public List<FeedBack> PendingFeedbacks { get; set; }
        public List<FeedBack> ResolvedFeedbacks { get; set; }
        public List<ApplicationUser> UsersToQuery { get; set; }
        public DateTime DateResolved { get; set; }
        public string ResolvedBy { get; set; }
        [Required(ErrorMessage = "Please enter Resolve Action")]
        public string ResolveMessage { get; set; }
        public FeedBack FeedBackDetails { get; set; }
        public int FeedBackId { get; set; }
    }
    public class ApproveCommentViewModel : BaseDashboardViewModel
    {
        public string EUCID { get; set; }
        [Required(ErrorMessage = "Please enter Comment")]
        public string Comment { get; set; }
        public DateTime CommentDate { get; set; }
    }
}