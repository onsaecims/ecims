﻿using ecims.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ecims.ViewModels
{
    public class CreatePortalUserViewModel : BaseDashboardViewModel
    {
        public string UserId { get; set; }
        public List<ApplicationUser> UserList { get; set; }
        public int Category { get; set; }

        public IEnumerable<PortalCategories> Categories { get; set; }

        public int SubCategory { get; set; }

        public IEnumerable<PortalSubCategories> SubCategories { get; set; }

        public int? SelectedCategory { get; set; }

        //public int SubCategoryInformationUser { get; set; }

        //public IEnumerable<PortalSubCategories> SubCategoriesInformationUser { get; set; }
        
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        //[DataType(DataType.Password)]
        //[Display(Name = "Confirm password")]
        //[System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        //public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Please enter your email.")]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your first name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter your last name")]
        public string LastName { get; set; }

        public string OtherName { get; set; }

        //[Required(ErrorMessage = "Please select a means of identification")]
        public Identification Identification { get; set; }

        [Required(ErrorMessage = "Please select a means of identification")]
        public Gender Gender { get; set; }

        [Required(ErrorMessage = "Please select your marital status")]
        public MaritalStatus MaritalStatus { get; set; }

        [Required(ErrorMessage = "Please select a passport photograph.")]
        public HttpPostedFileBase passportUpload { get; set; }

        //[Required(ErrorMessage = "Please enter your address")]
        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        [Required(ErrorMessage = "Please enter your phone number")]
        public string PhoneNumber { get; set; }

        
        public IEnumerable<CountryList> Country { get; set; }
        [Required(ErrorMessage = "Please select your country of origin")]
        public int countryId { get; set; }

        public IEnumerable<StateOfOrigin> States { get; set; }

        //[Required(ErrorMessage = "Please enter your state.")]
        public int State { get; set; }

        //[Required(ErrorMessage = "Please enter your city.")]
        public string City { get; set; }

        //[Required(ErrorMessage = "Please enter your Tax Identification Number")]
        public string TIN { get; set; }

        public int MeansOfIdentification { get; set; }

        public IEnumerable<MeansOfIdentification> MeansOfIdentifications { get; set; }

        [Required(ErrorMessage = "Please enter your ID Number.")]
        public string LicenseNumber { get; set; }

        [Required(ErrorMessage = "Please enter your place your ID was issued.")]
        public string PlaceOfIssue { get; set; }

        [Display(Name = "Date of Issuance")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "0:d MMM yyyy")]
        public DateTime LicenseDateOfIssuance { get; set; }

        [Display(Name = "Date of Expiry")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "0:d MMM yyyy")]
        public DateTime LicenseDateOfExpiry { get; set; }

        //[Required(ErrorMessage = "Enter your next of Kin's Name.")]
        public string NextOfKinFullname { get; set; }

        //[Required(ErrorMessage = "Enter your next of kin's address.")]
        public string NextOfKinAddress { get; set; }

        //[Required(ErrorMessage = "Next of Kin Relationship")]
        public string NextOfKinRelationship { get; set; }

        //[Required(ErrorMessage = "Next of Kin Phone Number")]
        public string NextOfKinPhoneNumber { get; set; }

        [Required(ErrorMessage = "Please upload a scanned copy of your ID")]
        public HttpPostedFileBase IdUpload { get; set; }

        public string NationalIdNumber { get; set; }

        public string InternationalPassportIdNumber { get; set; }

        public string NationalVotersCardIdNumber { get; set; }

        [Display(Name = "Date of Birth")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMM/yyyy}")]
        public DateTime DOB { get; set; }

        public string CompanyName { get; set; }
        public string CategoryName { get; internal set; }
        public string OrganizationName { get; set; }
        public bool CanVerifyEUC { get; set; }
    }
}