﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.ViewModels
{
    public class SmsTestViewModel : BaseDashboardViewModel
    {
        public string PhoneNumber { get; set; }
        public string Message { get; set; }

    }
}