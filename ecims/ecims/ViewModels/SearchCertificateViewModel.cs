﻿using ecims.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.ViewModels
{
    public class SearchCertificateViewModel : BaseDashboardViewModel
    {
        public string CertificateNumber { get; set; }
        public string ApplicationNumber { get; set; }
        public UploadedEUCCertificate Certificate { get; set; }
        public List<EUCWorkFlow> Workflow { get; set; }
        public List<CertificateWorkflow> CertificateWorkflow { get; set; }
        public List<PortalSubCategories> AgencyList { get; set; }
        public EndUserCertificate Application { get; set; }
        public EUCCertificates EndUserCertificate { get; set; }
    }
}