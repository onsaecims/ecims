﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ecims.Models;

namespace ecims.ViewModels
{
    public class VerifyEucPortalDashboardViewModel : BaseVerifyEucPortalDashboardViewModel
    {
        public List<CountryList> countries { get; internal set; }
        public int countryId { get; set; }
        public string Message { get; set; }
        public string Subject { get; set; }
        public string certificateNumber { get; set; }
    }
}