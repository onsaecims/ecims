﻿using ecims.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace ecims.ViewModels
{
    public class UserViewModel
    {
        public string UserId { get; set; }
        [Required(ErrorMessage = "Please select an account category.")]
        public int Category { get; set; }

        public IEnumerable<Category> Categories { get; set; }

        [Required]
        public string Id { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Username")]
        public string Username { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(100)]
        public string OtherName { get; set; }

        [Required(ErrorMessage = "Please select your gender.")]
        public Gender Gender { get; set; }

        [Required(ErrorMessage = "Please select your marital status.")]
        public MaritalStatus MaritalStatus { get; set; }

        public HttpPostedFileBase passportUpload { get; set; }

        public string PassportURL { get; set; }

        [MaxLength(200, ErrorMessage = "Address line 1 Should be less than 200 Characters.")]
        [Required(ErrorMessage = "Address line 1 Required")]
        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        [MaxLength(14, ErrorMessage = "Phone number should be less than 14 characters")]
        [Required(ErrorMessage = "Phone number required")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Please select a country")]
        public Country Country { get; set; }

        [Required(ErrorMessage = "Please enter a state")]
        public string State { get; set; }

        public string City { get; set; }

        [Required(ErrorMessage = "Please enter your Tax Identification Number")]
        public string TIN { get; set; }

        [Required(ErrorMessage = "Please select your means of identification")]
        public int MeansOfIdentification { get; set; }

        public IEnumerable<MeansOfIdentification> MeansOfIdentifications { get; set; }

        public string LicenseNumber { get; set; }

        public string PlaceOfIssue { get; set; }

        [Display(Name = "Date of Issuance")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "0:d MMM yyyy")]
        public DateTime LicenseDateOfIssuance { get; set; }

        [Display(Name = "Date of Expiry")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "0:d MMM yyyy")]
        public DateTime LicenseDateOfExpiry { get; set; }

        //[ValidTime]
        //public string Time = "12:00";        

        [Required(ErrorMessage = "Please enter your next of kin's full name")]
        public string NextOfKinFullname { get; set; }

        [Required(ErrorMessage = "Please enter your next of kin's address")]
        public string NextOfKinAddress { get; set; }

        [Required(ErrorMessage = "Please enter your next of kin's relationship")]
        public string NextOfKinRelationship { get; set; }

        [Required(ErrorMessage = "Please enter your next of kin's phone number")]
        public string NextOfKinPhoneNumber { get; set; }

        public HttpPostedFileBase IdUpload { get; set; }

        public string NationalIdNumber { get; set; }

        public string InternationalPassportIdNumber { get; set; }

        public string NationalVotersCardIdNumber { get; set; }

        [Display(Name = "Date Of Birth")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "0:d MMM yyyy")]
        public DateTime DOB { get; set; }

        //public string CompanyName { get; set; }

        //public string CompanyAddress { get; set; }

        //public State CompanyState { get; set; }

        //public string CompanyPhoneNumber { get; set; }

        //public string CompanyEmail { get; set; }

        //public string CompanyWebsite { get; set; }

        //public string CACNo { get; set; }

        //[Display(Name = "Date Of Birth")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "0:d MMM yyyy")]
        //public DateTime DateOfIncorporation { get; set; }

        //public HttpPostedFileBase Logo { get; set; }

        //public HttpPostedFileBase CertificateOfIncorporation { get; set; }
        public List<CountryList> CountryList { get; set; }
        public ApplicationUser Applicant { get; set; }
    }
}