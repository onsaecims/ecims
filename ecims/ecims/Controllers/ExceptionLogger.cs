﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ecims.Controllers
{
    public class ExceptionLogger
    {
        public void LogException(string filename, string logFile, string exceptionMessage)
        {
            //Name and path of our passport log
            string logFileName = "Exception Log-" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + ".txt";
            //string logPath = "~/FileUploadLogs/" + logFileName;
            //string logPath = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));
            //Check if the file log exists
            //If it doesnt, create and write to it
            try
            {
                logFile += "\\" + logFileName;

                if (!System.IO.File.Exists(logFile))
                {
                    //Write to the log
                    System.IO.File.WriteAllText(logFile, filename + " could not be found at ");
                }
                else if (System.IO.File.Exists(logFile))
                {
                    using (StreamWriter file = new StreamWriter(logFile, true))
                    {
                        file.WriteLine(DateTime.Now.ToString() + "-" + filename + " could not be found at ");
                    }
                }
            }
            //If it does, then append to it
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    } 
}