﻿using ecims.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecims.Controllers
{
    public class PasswordCheckController : Controller
    {
        //Call to DB context
        ApplicationDbContext _context = new ApplicationDbContext();

        // GET: PasswordCheck
        public bool CheckIfPasswordExists(string username)
        {
            //Check if ID exists
            if (username != null)
            {
                var userToCheck = _context.Users.Where(user => user.UserName == username).FirstOrDefault();
                if (userToCheck != null)
                {
                    if (userToCheck.PasswordHash == null)
                        return false;
                    else if (userToCheck.PasswordHash != null)
                        return true;
                    else
                        return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
    }
}