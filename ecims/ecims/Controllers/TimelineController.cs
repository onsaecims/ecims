﻿using ecims.Models;
using ecims.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace ecims.Controllers
{
    public class TimelineController : Controller
    {
        ApplicationDbContext _context;
        private ApplicationUserManager _userManager;
        AuditTrailGenerator.AuditTrail _auditTrail = new AuditTrailGenerator.AuditTrail();

        public TimelineController()
        {
            _context = new ApplicationDbContext();

        }
        // GET: Timeline
        public ActionResult Index()
        {
            return View();
        }
        //Track application view
        [Authorize]
        [HttpGet]
        public ActionResult ApplicationTimeline()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            return View(new SearchCertificateViewModel
            {
                user = user,
                role = roles
            });
        }
        [Authorize]
        [HttpPost]
        public ActionResult ApplicationTimeline(SearchCertificateViewModel model)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            var ApplicationNumber = model.ApplicationNumber;

            //Retrieve the application in question
            var Application = _context.EndUserCertificate.Include(app => app.User).Where(app => app.applicationNo.Contains(ApplicationNumber)).FirstOrDefault();

            //List of agencies to match IDs against
            var AgencyList = _context.PortalSubCategories.ToList();

            //Return all workflow entries as long as the application number is valid
            if (!string.IsNullOrEmpty(ApplicationNumber))
            {
                var applicationWorkflow = _context.EUCWorkFlow
                                            .Include(app => app.Source)
                                            .Include(app => app.Source.PortalUserCategory)
                                            .Include(app => app.PortalCat)
                                            .Include(app => app.UserAgency)
                                            .Where(app => app.applicationNumber.Contains(ApplicationNumber)).ToList();

                return View(new SearchCertificateViewModel
                {
                    user = user,
                    role = roles,
                    Workflow = applicationWorkflow,
                    Application = Application,
                    ApplicationNumber = ApplicationNumber,
                    AgencyList = AgencyList
                });
            }
            else
            {
                TempData["Error"] = "Please enter a valid application number";
                //var eucAppList = _context.EUCWorkFlow.Where(e => e.applicationNumber == eucappNo).ToList();
                return View(new SearchCertificateViewModel
                {
                    user = user,
                    role = roles
                });
            }
        }
        //Track application view
        [Authorize]
        [HttpGet]
        public ActionResult CertificateTimeline()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            return View(new SearchCertificateViewModel
            {
                user = user,
                role = roles
            });
        }
        [Authorize]
        [HttpPost]
        public ActionResult CertificateTimeline(SearchCertificateViewModel model)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            var CertificateNumber = model.CertificateNumber;

            var Cert = _context.EUCCertificates.Where(cert => cert.CertNo == model.CertificateNumber).FirstOrDefault();
            //Retrieve the application in question
            var EndUserCertificate = _context.CertificateWorkflow.Include(app => app.User).Include(app => app.Certificate).Where(app => app.Certificate.CertNo == CertificateNumber).FirstOrDefault();

            //Return all workflow entries as long as the certificate number is valid
            if (!string.IsNullOrEmpty(CertificateNumber))
            {
                var certificateWorkflow = _context.CertificateWorkflow.Include(app => app.User).Where(app => app.Certificate.CertNo == CertificateNumber).ToList();

                //Get Application associated with this certificate
                var Application = _context.EndUserCertificate.Where(app => app.applicationNo == Cert.applicationNo).FirstOrDefault();

                return View(new SearchCertificateViewModel
                {
                    user = user,
                    role = roles,
                    CertificateWorkflow = certificateWorkflow,
                    CertificateNumber = CertificateNumber,
                    EndUserCertificate = Cert,
                    Application = Application
                });
            }
            else
            {
                TempData["Error"] = "Please enter a valid certificate number";
                //var eucAppList = _context.EUCWorkFlow.Where(e => e.applicationNumber == eucappNo).ToList();
                return View(new SearchCertificateViewModel
                {
                    user = user,
                    role = roles
                });
            }
        }
    }
}