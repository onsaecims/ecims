﻿using ecims.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Controllers.GroupUsersByRole
{
    public class GroupUsersByRole
    {
        ApplicationDbContext _context = new ApplicationDbContext();
        public List<ApplicationUser> GetUsersInRole(string roleName)
        {
            var roleManager =
             new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
            var role = roleManager.FindByName(roleName).Users.First();
            var usersInRole =
             _context.Users.Where(u => u.Roles.Select(r => r.RoleId).Contains(role.RoleId)).ToList();
            return usersInRole;
        }
    }
}