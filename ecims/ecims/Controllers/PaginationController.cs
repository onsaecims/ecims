﻿using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using ecims.Models;
using ecims.ViewModels;

namespace PagingAndSorting.Controllers
{
    public class PaginationController : Controller
    {
        //  
        // GET: /Employee/  
        public ActionResult Pagination(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            IPagedList<EndUserCertificate> certificates = db.EndUserCertificate.Include(euc => euc.User).OrderByDescending
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;

            if (SearchTerm != null || SearchTerm != "")
            {
                certificates = db.EndUserCertificate
                    .Where(
                       euc => euc.User.LastName.Contains(SearchTerm) 
                    || euc.User.FirstName.Contains(SearchTerm)
                    || euc.User.Email.Contains(SearchTerm)
                    || euc.applicationNo.Contains(SearchTerm)
                    || euc.applicationNo.Contains(SearchTerm))

                    .Include(euc => euc.User).OrderByDescending
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                return View(new EndUserCertificatesViewModel
                {
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm
                });
            }

            switch (sortOrder)
            {
                case "DateCreated":
                    if (sortOrder.Equals(CurrentSort))
                        certificates = db.EndUserCertificate.Include(euc => euc.User).OrderByDescending
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                    else
                        certificates = db.EndUserCertificate.OrderBy
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                    break;
                case "ApplicationNo":
                    if (sortOrder.Equals(CurrentSort))
                        certificates = db.EndUserCertificate.Include(euc => euc.User).OrderByDescending
                                (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                    else
                        certificates = db.EndUserCertificate.OrderBy
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                    break;
                case "ApplicantNo":
                    if (sortOrder.Equals(CurrentSort))
                        certificates = db.EndUserCertificate.Include(euc => euc.User).OrderByDescending
                                (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                    else
                        certificates = db.EndUserCertificate.OrderBy
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                    break;
                case "ApplicantName":
                    if (sortOrder.Equals(CurrentSort))
                        certificates = db.EndUserCertificate.Include(euc => euc.User).OrderByDescending
                                (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                    else
                        certificates = db.EndUserCertificate.OrderBy
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                    break;
                case "Default":
                    certificates = db.EndUserCertificate.OrderBy
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                    break;
            }
            return View(new EndUserCertificatesViewModel
            {
                certificatesToShow = certificates,
                PageSize = pageSize,
                sortOrder = sortOrder,
                searchTerm = SearchTerm
            });
        }


        public ActionResult Add()
        {
            return View();
        }
    }
}