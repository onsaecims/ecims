﻿using System.Web.Mvc;

namespace ecims.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var signedIn = User.Identity.IsAuthenticated;
            if (signedIn)
                return RedirectToAction("MainDashboard", "Admin");
            return Redirect("https://euc.nsa.gov.ng/");
        }

        public ActionResult Guideline()
        {
            ViewBag.Message = "Your application description page.";

            return Redirect("https://euc.nsa.gov.ng/guidelines/");
        }

        public ActionResult Support()
        {
            ViewBag.Message = "Your contact page.";

            return Redirect("https://euc.nsa.gov.ng/contact/");
        }

        public ActionResult SetSession()
        {
            Session["Test"] = "Test Value";
            return View();
        }

        public ActionResult Keepalive()
        {
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return View();
        }
    }
}