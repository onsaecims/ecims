﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ecims.Controllers.SMSTestController
{
    public class SendSMS
    {
        public async Task SendTestSMS()
        {
            //Send user verification SMS
            IdentityMessage SmsMessage = new IdentityMessage();
            //send accompanying SMS reminding user to verify their email
            string smsBody = "Test SMS";

            SmsMessage.Body = smsBody;
            SmsMessage.Destination = "+2348141621668";
            SmsMessage.Subject = "Test Subject";

            SmsService smsSerice = new SmsService();
            await smsSerice.SendAsync(SmsMessage);
        }
    }
}