﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecims.Controllers
{
    public class ErrorHandlingController : Controller
    {
        public ActionResult HandleError()
        {
            var user = User.Identity.IsAuthenticated;
            if (user == true)
            {
                return RedirectToAction("MainDashboard", "Admin");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }
        
    }
}