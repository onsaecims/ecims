﻿using ecims.Controllers.ONSASupportNotificationController;
using ecims.Models;
using ecims.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ecims.Controllers
{
    public class CreateEUCApplicationController : Controller
    {
        ApplicationDbContext _context;

        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public CreateEUCApplicationController()
        {
            _context = new Models.ApplicationDbContext();
        }

        // GET: NewEUCApplication
        public ActionResult Index()
        {
            return View();
        }

        // GET: NewEUCApplication/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: NewEUCApplication/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: NewEUCApplication/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // POST: NewEUCApplication/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> NewEUCApplication(NewEUCApplicationViewModel model)
        {
            //model.HSCode = 1;
            //model.itemCountryId = 1;

            var userId = User.Identity.GetUserId();

            var user = _context.Users.Find(userId);

            //if (ModelState.IsValid)
            //{
            #region attachment section
            var applicationNumber = model.ApplicationNumber;

            //Get ID of the parent EUC application for use with every application attachment.
            var parentEUCApplication = _context.EndUserCertificate.Where(euc => euc.applicationNo == applicationNumber).FirstOrDefault();

            //If the application doesn't exist, redirect back to main dashboard and 
            if (parentEUCApplication == null)
            {
                //Set success message after successful submit
                TempData["Failure"] = "Your application could not be submitted because there was an error in the imported items selected. Kindly re-apply, and if the problem persists contact ONSA support for assistance.";

                return RedirectToAction("MainDashboard", "Admin");
            }

            int parentEUCApplicationID = parentEUCApplication.id;

            //Create a new application attachment record in the DB using the application's application number.
            //Every uploaded attachment will update this record using its unique ID(primary key)
            var eucApplicationAttachment = new EUCApplicationAttachments
            {
                applicationNo = applicationNumber
            };
            _context.EUCApplicationAttachments.Add(eucApplicationAttachment);
            _context.SaveChanges();

            //Check to see if user is uploading on behalf of an organisation
            if (model.IsOrganisation)
            {
                //Mark the EUC certificate as an organisations application
                var EUCApplicationToUpdate = _context.EndUserCertificate.Where(euc => euc.applicationNo == applicationNumber).FirstOrDefault();
                EUCApplicationToUpdate.IsOrganisation = true;
                _context.SaveChanges();

                //Create an entry into the database if the user is applying on behalf of an organisation
                //i.e. the organisation checkbox is checked
                var eucOrganisation = new EUCOrganisations
                {
                    ApplicationNumber = applicationNumber,
                    Name = model.OrganisationName,
                    Address = model.OrganisationAddress
                };
                _context.EUCOrganisations.Add(eucOrganisation);
                _context.SaveChanges();

                var awardContract = model.AwardContract;
                if (awardContract != null)
                {
                    var extension = Path.GetExtension(awardContract.FileName);

                    var fileNameWithExtension = "AWARD LETTER" + applicationNumber + extension;
                    //var fileNameWithExtension = Path.GetFileName(cbnLetter.FileName);
                    //var fileNameWithExtension = Path.GetFileName(awardContract.FileName);
                    var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(awardContract.FileName);

                    string image = "/images/profile/" + user.Email + "/Supporting Documents/Award Contract/" + fileNameWithExtension;
                    string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/Award Contract/");

                    string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/Award Contract/"), fileNameWithExtension);

                    var directory = Directory.CreateDirectory(path);

                    awardContract.SaveAs(imageUrl);

                    //Update the database record with the document's URL in the DB    
                    var organisationToUpdate = _context.EUCOrganisations.Where(eucOrg => eucOrg.ApplicationNumber == applicationNumber).FirstOrDefault();
                    organisationToUpdate.AwardContract = image;
                    _context.SaveChanges();
                }
            }
            else
            {
                //Mark the EUC certificate as an individual's application
                var EUCApplicationToUpdate = _context.EndUserCertificate.Where(euc => euc.applicationNo == applicationNumber).FirstOrDefault();
                EUCApplicationToUpdate.IsOrganisation = false;
                _context.SaveChanges();
            }

            //Get ID of created record(EUC application attachment)
            var eucAttachmentRecord = _context.EUCApplicationAttachments.Where(eucAttachment => eucAttachment.applicationNo == applicationNumber).FirstOrDefault();
            //Every attachment will be saved as a reference in the database using this ID
            int eucAttachmentId = eucAttachmentRecord.id;

            //Application attachment record to update
            var applicationToUpdate = _context.EUCApplicationAttachments.Find(eucAttachmentId);

            //---------CBN letter upload------------------
            var cbnLetter = model.CbnApplicationLetter;
            if (cbnLetter != null)
            {
                var extension = Path.GetExtension(cbnLetter.FileName);

                var fileNameWithExtension = "CBNLetter" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(cbnLetter.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(cbnLetter.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/CBN Letter/" + fileNameWithExtension;
                string path = Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/CBN Letter/");

                string imageUrl = Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/CBN Letter/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                cbnLetter.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB    
                applicationToUpdate.cbnApplication = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------APPLICATION TO MINISTRY OF AGRICULTURE--------
            var agricMin = model.MinistryOfAgricApplication;
            if (agricMin != null)
            {
                var extension = Path.GetExtension(agricMin.FileName);

                var fileNameWithExtension = "AgricMin" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(agricMin.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(agricMin.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/APPLICATION TO MINISTRY OF AGRICULTURE/" + fileNameWithExtension;
                string path = Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/APPLICATION TO MINISTRY OF AGRICULTURE/");

                string imageUrl = Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/APPLICATION TO MINISTRY OF AGRICULTURE/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                agricMin.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.minOfAgricApplication = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------APPROVAL LETTER FROM NCAA--------
            var ncaaLetter = model.NCCAAApprovalLetter;
            if (ncaaLetter != null)
            {
                var extension = Path.GetExtension(ncaaLetter.FileName);

                var fileNameWithExtension = "NCAALetter" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(ncaaLetter.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(ncaaLetter.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/APPROVAL LETTER FROM NCAA/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/APPROVAL LETTER FROM NCAA/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/APPROVAL LETTER FROM NCAA/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                ncaaLetter.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.NCCALetter = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------APPROVED RESPONSIBLE OFFICER (ARP) IDENTIFICATION & LICENSES--------
            var arpLicense = model.ARPIdentification;
            if (arpLicense != null)
            {
                var extension = Path.GetExtension(arpLicense.FileName);

                var fileNameWithExtension = "ARP" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(arpLicense.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(arpLicense.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/APPROVED RESPONSIBLE OFFICER/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/APPROVED RESPONSIBLE OFFICER/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/APPROVED RESPONSIBLE OFFICER/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                arpLicense.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.ARPIdentification = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------MEMORANDUM OF ASSOCIATION--------
            var memorandum = model.MemorandumOfAssociation;
            if (memorandum != null)
            {
                var extension = Path.GetExtension(memorandum.FileName);

                var fileNameWithExtension = "Memorandum" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(memorandum.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(memorandum.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/MEMORANDUM OF ASSOCIATION/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/MEMORANDUM OF ASSOCIATION/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/MEMORANDUM OF ASSOCIATION/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                memorandum.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.memoOfAssociation = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------BILL OF LADEN OR AIRWAY BILL--------
            var billOfLaden = model.BillOfLadenOrAirwayBill;
            if (billOfLaden != null)
            {
                var extension = Path.GetExtension(billOfLaden.FileName);

                var fileNameWithExtension = "BillOfLaden" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(billOfLaden.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(billOfLaden.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/BILL OF LADEN OR AIRWAY BILL/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/BILL OF LADEN OR AIRWAY BILL/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/BILL OF LADEN OR AIRWAY BILL/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                billOfLaden.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.bilOfLaden = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------CERTIFICATE OF ANALYSIS--------
            var analysisCert = model.CertificateOfAnalysis;
            if (analysisCert != null)
            {
                var extension = Path.GetExtension(analysisCert.FileName);

                var fileNameWithExtension = "AnalysisCert" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(analysisCert.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(analysisCert.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/CERTIFICATE OF ANALYSIS/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/CERTIFICATE OF ANALYSIS/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/CERTIFICATE OF ANALYSIS/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                analysisCert.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.analysisCert = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------CERTIFICATE OF INCORPORATION (CORPORATION)--------
            var certOfIncorp = model.CertificateOfIncorporation;
            if (certOfIncorp != null)
            {
                var extension = Path.GetExtension(certOfIncorp.FileName);

                var fileNameWithExtension = "CertOfIncorp" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(certOfIncorp.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(certOfIncorp.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/CERTIFICATE OF INCORPORATION/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/CERTIFICATE OF INCORPORATION/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/CERTIFICATE OF INCORPORATION/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                certOfIncorp.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.incorporationCert = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------COMPANY’S DIRECTOR’S PROFILE--------
            var directorProfile = model.CompanyDirectorsProfile;
            if (directorProfile != null)
            {
                var extension = Path.GetExtension(directorProfile.FileName);

                var fileNameWithExtension = "DirectorsProfile" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(directorProfile.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(directorProfile.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/COMPANY’S DIRECTOR’S PROFILE/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/COMPANY’S DIRECTOR’S PROFILE/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/COMPANY’S DIRECTOR’S PROFILE/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                directorProfile.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.companyDirectorsProfile = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------AC-GAD001--------
            var ac = model.CompletionOfApplicationForm;
            if (ac != null)
            {
                var extension = Path.GetExtension(ac.FileName);

                var fileNameWithExtension = "AC" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(ac.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(ac.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/AC-GAD001/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/AC-GAD001/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/AC-GAD001/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                ac.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.ac_gad001 = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------CURRENT TAX CLEARANCE--------
            var taxClerance = model.CurrentTaxClearance;
            if (taxClerance != null)
            {
                var extension = Path.GetExtension(taxClerance.FileName);

                var fileNameWithExtension = "TaxClearance" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(taxClerance.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(taxClerance.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/CURRENT TAX CLEARANCE/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/CURRENT TAX CLEARANCE/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/CURRENT TAX CLEARANCE/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                taxClerance.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.taxClerance = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------DISTRIBUTION NETWORK OF PREVIOUS IMPORTATION--------
            var stockBal = model.DistributionNetworkOfPreviousImportationBeneficiariesStockBalances;
            if (stockBal != null)
            {
                var extension = Path.GetExtension(stockBal.FileName);

                var fileNameWithExtension = "StockBal" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(stockBal.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(stockBal.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/DISTRIBUTION NETWORK OF PREVIOUS IMPORTATION/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/DISTRIBUTION NETWORK OF PREVIOUS IMPORTATION/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/DISTRIBUTION NETWORK OF PREVIOUS IMPORTATION/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                stockBal.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.StockBalance = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------EVIDENCE OF REGISTRATION WITH NSCDC--------
            var privateSecOpt = model.NSCDCRegistration;
            if (privateSecOpt != null)
            {
                var extension = Path.GetExtension(privateSecOpt.FileName);

                var fileNameWithExtension = "PrivateSecOpt" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(privateSecOpt.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(privateSecOpt.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/EVIDENCE OF REGISTRATION WITH NSCDC/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/EVIDENCE OF REGISTRATION WITH NSCDC/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/EVIDENCE OF REGISTRATION WITH NSCDC/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                privateSecOpt.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.privateSecurityReg = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------CIT LICENSE--------
            var citLicense = model.CITLicense;
            if (citLicense != null)
            {
                var extension = Path.GetExtension(citLicense.FileName);

                var fileNameWithExtension = "CITLicense" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(citLicense.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(citLicense.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/CIT LICENSE/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/CIT LICENSE/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/CIT LICENSE/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                citLicense.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.citLicense = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------FIRS CERTIFICATE--------
            var firsCert = model.FIRSCertificate;
            if (firsCert != null)
            {
                var extension = Path.GetExtension(firsCert.FileName);

                var fileNameWithExtension = "FIRSCert" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(firsCert.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(firsCert.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/FIRS CERTIFICATE/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/FIRS CERTIFICATE/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/FIRS CERTIFICATE/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                firsCert.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.firsCertificate = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------FORM CO7--------
            var formCO7 = model.FormCO7;
            if (formCO7 != null)
            {
                var extension = Path.GetExtension(formCO7.FileName);

                var fileNameWithExtension = "FormCO7" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(formCO7.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(formCO7.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/FORM CO7/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/FORM CO7/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/FORM CO7/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                formCO7.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.formCO7 = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------FORM M FROM CBN--------
            var cbnFormM = model.FormMCbn;
            if (cbnFormM != null)
            {
                var extension = Path.GetExtension(cbnFormM.FileName);

                var fileNameWithExtension = "FormM" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(cbnFormM.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(cbnFormM.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/FORM M FROM CBN/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/FORM M FROM CBN/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/FORM M FROM CBN/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                cbnFormM.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.cbnFormM = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------IDENTIFICATION CARDS--------
            var idCard = model.IdentificationCard;
            if (idCard != null)
            {
                var extension = Path.GetExtension(idCard.FileName);

                var fileNameWithExtension = "IDCard" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(idCard.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(idCard.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/IDENTIFICATION CARDS/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/IDENTIFICATION CARDS/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/IDENTIFICATION CARDS/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                idCard.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.identityCard = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------IDENTIFICATION CARDS--------
            var lastEuc = model.LastYearEUCS;
            if (lastEuc != null)
            {
                var extension = Path.GetExtension(lastEuc.FileName);

                var fileNameWithExtension = "LastEUC" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(lastEuc.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(lastEuc.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/LAST 1 YEAR EUCS/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/LAST 1 YEAR EUCS/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/LAST 1 YEAR EUCS/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                lastEuc.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.last1YearEUCS = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------MAGAZINE LICENSES--------
            var introAcquirer = model.LetterOfIntroductionFromAcquirer;
            if (introAcquirer != null)
            {
                var extension = Path.GetExtension(introAcquirer.FileName);

                var fileNameWithExtension = "IntroAcquirer" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(introAcquirer.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(introAcquirer.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/MAGAZINE LICENSES/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/MAGAZINE LICENSES/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/MAGAZINE LICENSES/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                introAcquirer.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.acquirerIntroLetter = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------MANUFACTURERS SAFETY--------
            var magLicense = model.MagazineLicenses;
            if (magLicense != null)
            {
                var extension = Path.GetExtension(magLicense.FileName);

                var fileNameWithExtension = "MagLicense" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(magLicense.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(magLicense.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/MAGAZINE LICENSES/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/MAGAZINE LICENSES/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/MAGAZINE LICENSES/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                magLicense.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.magazineLicenses = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------MANUFACTURERS SAFETY--------
            var manSafety = model.ManufacturersSafety;
            if (manSafety != null)
            {
                var extension = Path.GetExtension(manSafety.FileName);

                var fileNameWithExtension = "ManSafety" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(manSafety.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(manSafety.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/MANUFACTURERS SAFETY/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/MANUFACTURERS SAFETY/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/MANUFACTURERS SAFETY/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                manSafety.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.manufacturersSafety = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------NAFDAC ATTACHMENT--------
            var nafdac = model.NAFDACAttachment;
            if (nafdac != null)
            {
                var extension = Path.GetExtension(nafdac.FileName);

                var fileNameWithExtension = "NAFDAC" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(nafdac.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(nafdac.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/NAFDAC ATTACHMENT/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/NAFDAC ATTACHMENT/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/NAFDAC ATTACHMENT/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                nafdac.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.NAFDACAttachment = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------NPF FIREWORKS PERMIT--------
            var npfFireworks = model.NPFFireworksPermit;
            if (npfFireworks != null)
            {
                var extension = Path.GetExtension(npfFireworks.FileName);

                var fileNameWithExtension = "NPFFireworks" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(npfFireworks.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(npfFireworks.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/NPF FIREWORKS PERMIT/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/NPF FIREWORKS PERMIT/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/NPF FIREWORKS PERMIT/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                npfFireworks.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.NPFFIreworksPermit = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------PACKING LIST--------
            var packingList = model.PackingList;
            if (packingList != null)
            {
                var extension = Path.GetExtension(packingList.FileName);

                var fileNameWithExtension = "PackingList" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(packingList.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(packingList.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/PACKING LIST/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/PACKING LIST/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/PACKING LIST/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                packingList.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.packingList = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------PERMIT TO IMPORT--------
            var importPermit = model.ExplosivesPermit;
            if (importPermit != null)
            {
                var extension = Path.GetExtension(importPermit.FileName);

                var fileNameWithExtension = "ImportPermit" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(importPermit.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(importPermit.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/PERMIT TO IMPORT/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/PERMIT TO IMPORT/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/PERMIT TO IMPORT/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                importPermit.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.importPermitforExplosive = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------PREVIOUS STORAGE ATTACHMENT--------
            var previousStorage = model.PreviousStorageAttachment;
            if (previousStorage != null)
            {
                var extension = Path.GetExtension(previousStorage.FileName);

                var fileNameWithExtension = "PreviousStorage" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(previousStorage.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(previousStorage.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/PREVIOUS STORAGE ATTACHMENT/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/PREVIOUS STORAGE ATTACHMENT/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/PREVIOUS STORAGE ATTACHMENT/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                previousStorage.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.prevStorageAttachment = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------PROFORMA INVOICE--------
            var proformaInvoice = model.ProformaInvoice;
            if (proformaInvoice != null)
            {
                var extension = Path.GetExtension(proformaInvoice.FileName);

                var fileNameWithExtension = "ProformaInvoice" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(proformaInvoice.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(proformaInvoice.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/PROFORMA INVOICE/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/PROFORMA INVOICE/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/PROFORMA INVOICE/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                proformaInvoice.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.proformaInvoice = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------QUALIFIED TECHNICAL OFFICER DETAILS--------
            var technicalOfficer = model.QualifiedTechnicalOfficerDetails;
            if (technicalOfficer != null)
            {
                var extension = Path.GetExtension(technicalOfficer.FileName);

                var fileNameWithExtension = "TechnicalOfficer" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(technicalOfficer.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(technicalOfficer.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/QUALIFIED TECHNICAL OFFICER DETAILS/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/QUALIFIED TECHNICAL OFFICER DETAILS/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/QUALIFIED TECHNICAL OFFICER DETAILS/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                technicalOfficer.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.qualifiedTechOfficerDetails = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------REGISTRATION WITH FFD--------
            var registrationFFD = model.RegWithFFD;
            if (registrationFFD != null)
            {
                var extension = Path.GetExtension(registrationFFD.FileName);

                var fileNameWithExtension = "RegistrationFFD" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(registrationFFD.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(registrationFFD.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/REGISTRATION WITH FFD/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/REGISTRATION WITH FFD/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/REGISTRATION WITH FFD/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                registrationFFD.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.FFDReg = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------SPECIFICATION OF GOODS--------
            var specificGoods = model.SpecificationOfGoods;
            if (specificGoods != null)
            {
                var extension = Path.GetExtension(specificGoods.FileName);

                var fileNameWithExtension = "SpecificGoods" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(specificGoods.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(specificGoods.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/SPECIFICATION OF GOODS/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/SPECIFICATION OF GOODS/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/SPECIFICATION OF GOODS/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                specificGoods.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.specificationOfGoods = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------STORAGE FACILITY ATTACHMENT--------
            var storageFacility = model.StorageFacilityAttachment;
            if (storageFacility != null)
            {
                var extension = Path.GetExtension(storageFacility.FileName);

                var fileNameWithExtension = "StorageFacility" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(storageFacility.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(storageFacility.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/STORAGE FACILITY ATTACHMENT/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/STORAGE FACILITY ATTACHMENT/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/STORAGE FACILITY ATTACHMENT/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                storageFacility.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.storageFacilityAttachment = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------TRANSIT DETAILS--------
            var transitDetails = model.TransitDetails;
            if (transitDetails != null)
            {
                var extension = Path.GetExtension(transitDetails.FileName);

                var fileNameWithExtension = "TransitDetails" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(transitDetails.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(transitDetails.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/TRANSIT DETAILS/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/TRANSIT DETAILS/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/TRANSIT DETAILS/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                transitDetails.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.transitDetails = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------VEHICLE PARTICULARS--------
            var vehicleParticulars = model.TransporterAndVehicleParticuulars;
            if (vehicleParticulars != null)
            {
                var extension = Path.GetExtension(vehicleParticulars.FileName);

                var fileNameWithExtension = "VehicleParticulars" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(vehicleParticulars.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(vehicleParticulars.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/VEHICLE PARTICULARS/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/VEHICLE PARTICULARS/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/VEHICLE PARTICULARS/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                vehicleParticulars.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.transporterParticulars = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------VAT CERTIFICATE--------
            var vatCert = model.VATCertificate;
            if (vatCert != null)
            {
                var extension = Path.GetExtension(vatCert.FileName);

                var fileNameWithExtension = "VATCert" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(vatCert.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(vatCert.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/VAT CERTIFICATE/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/VAT CERTIFICATE/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/VAT CERTIFICATE/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                vatCert.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.vatCert = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------VENDOR IDENTIFICATION DETAILS--------
            var vendorIdentity = model.VendorIdentificationDetails;
            if (vendorIdentity != null)
            {
                var extension = Path.GetExtension(vendorIdentity.FileName);

                var fileNameWithExtension = "VendorIdentity" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(vendorIdentity.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(vendorIdentity.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/VENDOR IDENTIFICATION DETAILS/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/VENDOR IDENTIFICATION DETAILS/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/VENDOR IDENTIFICATION DETAILS/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                vendorIdentity.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.vendorIDDetails = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------YEARS OF OPERATION ATTACHMENT--------
            var operationYear = model.YearsOfOperationAttachment;
            if (operationYear != null)
            {
                var extension = Path.GetExtension(operationYear.FileName);

                var fileNameWithExtension = "OperationYear" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(operationYear.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(operationYear.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/YEARS OF OPERATION ATTACHMENT/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/YEARS OF OPERATION ATTACHMENT/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/YEARS OF OPERATION ATTACHMENT/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                operationYear.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.operationYears = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //---------LICENSE FROM NBC/NCC--------
            var LicenseFromNBCOrNCC = model.LicenseFromNBCOrNCC;
            if (LicenseFromNBCOrNCC != null)
            {
                var extension = Path.GetExtension(LicenseFromNBCOrNCC.FileName);

                var fileNameWithExtension = "LicenseFromNBCOrNCC" + applicationNumber + extension;
                //var fileNameWithExtension = Path.GetFileName(operationYear.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(LicenseFromNBCOrNCC.FileName);

                string image = "/images/profile/" + user.Email + "/Supporting Documents/LICENSE FROM NBC OR NCC/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/Supporting Documents/LICENSE FROM NBC OR NCC/");

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Supporting Documents/LICENSE FROM NBC OR NCC/"), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                LicenseFromNBCOrNCC.SaveAs(imageUrl);

                //Update the database record with the document's URL in the DB
                applicationToUpdate.LicenseFromNBCOrNCC = image;
                _context.SaveChanges();

                //Create a validation entry in th database on EUCAttachmentValidationStatus
                var validationStatus = new EUCAttachmentValidationStatus
                {
                    EUCId = parentEUCApplicationID,
                    ApplicationNumber = applicationNumber,
                    DocumentName = image,
                    isValidated = false
                };
                _context.EUCAttachmentValidations.Add(validationStatus);
                _context.SaveChanges();
            }
            //Make certificate valid if all updates have been made successfully
            var certificateToMakeValid = _context.EndUserCertificate.Where(certificate => certificate.applicationNo == applicationNumber).FirstOrDefault();
            certificateToMakeValid.IsApplicationValid = true;
            _context.SaveChanges();

            //Add entry to EUC workflow
            //var eucWorkFlow = new EUCWorkFlow
            //{
            //    activityDate = DateTime.Now,
            //    applicationNumber = applicationNumber,
            //    actionSource = User.Identity.GetUserId(),
            //    actionReciepient = "",
            //    handledStatus = "",
            //    currentStatus = ""
            //};
            //_context.EUCWorkFlow.Add(eucWorkFlow);
            //int workflowEntry = _context.SaveChanges();
            //};


            #endregion
            //_context.EndUserCertificate.Add(EUCCertificate);

            var result = _context.SaveChanges();

            //Inform all support users of a new EUC application request via SMS and EMail
            string roleName = "ONSA Support";
            //Generate a list of all users in ONSA Support role
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

            //Find the role to search for(ONSA Support)
            var role = await roleManager.FindByNameAsync(roleName);

            var users = new List<ApplicationUser>();

            GroupUsersByRole.GroupUsersByRole groupUsers = new GroupUsersByRole.GroupUsersByRole();

            users = groupUsers.GetUsersInRole("ONSA Support");
            
            //Call method to send SMS and E-Mail notifications
            ONSASupportNotification onsaSupportContacter = new ONSASupportNotification();
            await onsaSupportContacter.NotifyONSASupportOfNewEUCApplication(users, user, model.ApplicationNumber);


            //Send successs message to dashboard through viewbag
            ViewBag.Message = "Application has been made sucessfully. An email and SMS will be sent to you when it has been either approved or disapproved.";
            //Return the URL encoded with the hashtag to be returned after creating the EUC application.
            //string returnUrl = String.Format("NewEUCApplication{0}step2", Server.UrlEncode("#"));

            //Send user email notifying them their application was sent successfully
            IdentityMessage EmailMessage = new IdentityMessage();

            EmailMessage.Body = "Dear " + user.FirstName + " " + user.LastName
                                        + " <br/>"
                                        + " This is to confirm that your application has been sent successfully on the ONSA"
                                        + " End-User Certificate"
                                        + " portal."
                                        + " <br/>"
                                        + " An E-mail and SMS will be sent notifying you once your application has been either approved or disapproved.";
            ;
            EmailMessage.Destination = user.Email;
            EmailMessage.Subject = "Application Successful";
            //Send email
            EmailService emailService = new EmailService();
            await emailService.SendAsync(EmailMessage);

            //Send user verification SMS
            IdentityMessage SmsMessage = new IdentityMessage();
            //send accompanying SMS reminding user to verify their email
            string smsBody = "This is to confirm that your application has been sent successfully on the EUC web portal. An E-mail and SMS will be sent on approval or disapproval.";

            SmsMessage.Body = smsBody;
            SmsMessage.Destination = user.PhoneNumber;
            SmsMessage.Subject = "Confirm your account-Resend";
            //Send SMS
            SmsService smsSerice = new SmsService();
            await smsSerice.SendAsync(SmsMessage);

            //Set success message after successful submit
            TempData["Success"] = "Your application has been sent successfully. You will be notified via e-mail and SMS if your application is approved or rejected.";

            return RedirectToAction("MainDashboard", "Admin");

        }
        //Execute this if something wrong with the view model
        //return View(model);
        // GET: NewEUCApplication/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: NewEUCApplication/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // POST: NewEUCApplication/Delete/5
        //[HttpPost]
        public ActionResult Delete(int? id)
        {
            //Return bad result if id is null
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Get the item with the specified ID
            EndUserCertificateDetails itemToDelete = _context.EndUserCertificateDetails.Find(id);
            try
            {
                //Delete logic
                _context.EndUserCertificateDetails.Remove(itemToDelete);
                _context.SaveChanges();
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch
            {
                return View();
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private async Task<string> SendEmailConfirmationTokenAsync(string userID, string subject, string message)
        {
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(userID);
            var callbackUrl = Url.Action("ConfirmEmail", "Account",
               new { userId = userID, code = code }, protocol: Request.Url.Scheme);
            await UserManager.SendEmailAsync(userID, subject, message);

            return callbackUrl;
        }

        private async Task<string> SendSmsConfirmationTokenAsync(string userID, string subject)
        {
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(userID);
            var callbackUrl = Url.Action("ConfirmEmail", "Account",
               new { userId = userID, code = code }, protocol: Request.Url.Scheme);
            await UserManager.SendSmsAsync(userID, "Please confirm your account by clicking <a href=\""
                                                    + callbackUrl + "\">here</a>");

            return callbackUrl;
        }

        private void SendSmsConfirmation(string recipient, string message)
        {
            //SMS Sending
            string sender = "ONSA ECIMS";

            string url = "https://www.MultiTexter.com/tools/geturl/Sms.php?username=ocedache@gmail.com&password=Omanchala1944&sender="
                            + sender + "&message=" + message +
                            "&flash=0&sendtime=2009-10- 18%2006:30&listname=friends&recipients="
                            + recipient;

            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
            webReq.Method = "GET";
            HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();
            Stream answer = webResponse.GetResponseStream();
            StreamReader _recievedAnswer = new StreamReader(answer);
        }

        //This method filters HSCodes and returns them to the view based on th eitem category selected
        public JsonResult filterHSCodes(int ? itemCategoryName)
        {
            var hsCodeList = _context.HSCodes.Where(hscode => hscode.itemCatName == itemCategoryName).AsEnumerable().Select(hscodes => new
            {
                hsCodeId = hscodes.Id,
                Description = string.Format("{0}--{1}", hscodes.itemSubCatName, hscodes.Name)
            }).ToList();

            return Json(new SelectList(hsCodeList, "hsCodeId", "Description"));
        }
    }
}
