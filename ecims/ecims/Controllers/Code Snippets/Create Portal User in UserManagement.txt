﻿<div class="tab-pane active" id="1a">
                                        <h3>Create A Portal User</h3>
                                        <div id="createUser">
                                            @* Basic User Information *@
                                            <div class="row col-md-6">
                                                @Html.ValidationSummary()

                                                <div class="col-md-12">
                                                    @if (Model.SelectedCategory == null)
                                                    {
                                                        @Html.LabelFor(m => m.Category, "Select User Type", new { @class = "", })
                                                        @* Change this to Agency List *@
                                                        @*@Html.DropDownListFor(m => m.Category, new SelectList(Model.Categories, "Id", "Name"), new { @class = "dropselectsec", @Id = "userCategory" })*@
                                                    {

                                                        <select name="UserCategory" id="UserCategory" class="dropselectsec" onchange="NavigatePage()">
                                                            <option value=""> Select an Agency this User Belongs to</option>
                                                            @foreach (var category in Model.Categories)
                                                            {
                                                                if (category.Id != 1)
                                                                {
                                                                    <option value="@category.Id">@category.Name</option>
                                                                }
                                                            }
                                                        </select>
                                                        }
                                                    }
                                                    else
                                                    {
                                                        <h3>Selected user type: <label class="label label-primary">@Model.CategoryName</label></h3>
                                                    }
                                                </div>

                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.SubCategory, "Select Agency the User Belongs To", new { @class = "" })
                                                    @* Change this to Agency List *@
                                                    @Html.DropDownListFor(m => m.SubCategory, new SelectList(Model.SubCategories, "Id", "Name"), "", new { @class = "dropselectsec", @Id = "SubCategoryList" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.LastName, "Surname", new { @class = "col-form-label required-field" })
                                                    @Html.TextBoxFor(m => m.LastName, new { @class = "form-control", placeholder = "Surname", required = "required" })
                                                    @Html.ValidationMessageFor(m => m.LastName, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.FirstName, "First Name", new { @class = "col-form-label required-field" })
                                                    @Html.TextBoxFor(m => m.FirstName, new { @class = "form-control", placeholder = "First Name", required = "required" })
                                                    @Html.ValidationMessageFor(m => m.FirstName, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.OtherName, "Middle Name", new { @class = "col-form-label" })
                                                    @Html.TextBoxFor(m => m.OtherName, new { @class = "form-control", placeholder = "Other Name" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.Email, "E-mail", new { @class = "col-form-label required-field" })
                                                    @Html.TextBoxFor(m => m.Email, new { @class = "form-control", placeholder = "e.g. Nonso.Ayo@xyz.com", required = "required" })
                                                    @Html.ValidationMessageFor(m => m.Email, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.Password, new { @class = "col-form-label control-label required-field" })

                                                    @Html.PasswordFor(m => m.Password, new { @class = "form-control" })
                                                    @Html.ValidationMessageFor(m => m.Password, "", new { @class = "label label-warning lb-sm" })

                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.ConfirmPassword, "Confirm Password", new { @class = "col-form-label control-label required-field" })

                                                    @Html.PasswordFor(m => m.ConfirmPassword, new { @class = "form-control" })
                                                    @Html.ValidationMessageFor(m => m.ConfirmPassword, "", new { @class = "label label-warning lb-sm" })

                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.DOB, "Date of Birth", new { @class = "col-form control-label required-field", placeholder = "eg 1 Jan 2015" })
                                                    @Html.EditorFor(m => m.DOB, new { htmlAttributes = new { @class = "form-control", placeholder = "Employee Date of Birth", @readonly = "true" } })
                                                    @Html.ValidationMessageFor(m => m.DOB, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.PhoneNumber, "Phone Number", new { @class = "col-form-label required-field" })
                                                    @Html.TextBoxFor(m => m.PhoneNumber, new { @class = "form-control", placeholder = "e.g. +2348121321668", required = "required" })
                                                    @Html.ValidationMessageFor(m => m.PhoneNumber, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.Country, new { @class = "col-form-label required-field" })
                                                    @Html.EnumDropDownListFor(m => m.Country, new { @class = "form-control", required = "required" })
                                                    @Html.ValidationMessageFor(m => m.Country, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.State, "State of Origin", new { @class = "required-field" })
                                                    @Html.TextBoxFor(m => m.State, new { @class = "form-control", placeholder = "e.g. Abia State", required = "required" })
                                                    @Html.ValidationMessageFor(m => m.State, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                @*Passport Photo Graph*@
                                                <div class="col-md-12">
                                                    <label class="required-field">Passport Photograph</label>
                                                    <input type="file" id="passportUpload" name="passportUpload" class="form-control" required="required" />
                                                </div>

                                            </div>
                                            @* User Means of ID *@
                                            <div class="row col-md-6">
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.MeansOfIdentification, "Means of Identification", new { @class = "required-field" })
                                                    @Html.EnumDropDownListFor(m => m.Identification, new { @class = "dropselectsec", @onchange = "show()", @id = "idType", required = "required" })
                                                    @Html.ValidationMessageFor(m => m.Identification, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                <div id="natId" style="display:none;">
                                                    <div class="row col-md-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label required-field">Your National ID Photo:</label>
                                                            <div class="col-md-12">
                                                                <input type="file" name="IdUpload" class="form-control" />
                                                                @Html.ValidationMessageFor(m => m.IdUpload, "", new { @class = "label label-warning lb-sm", required = "required" })
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        @Html.LabelFor(m => m.LicenseNumber, "ID Number", new { @class = "required-field" })
                                                        @Html.TextBoxFor(m => m.LicenseNumber, "Your National ID Number", new { @class = "form-control", placeholder = "e.g. A011001" })
                                                        @Html.ValidationMessageFor(m => m.LicenseNumber, "", new { @class = "label label-warning lb-sm" })
                                                    </div>
                                                    <div class="col-md-12">
                                                        @Html.LabelFor(m => m.LicenseDateOfIssuance, "Date of Issue", new { @class = "col-form-label required-field" })
                                                        @Html.EditorFor(m => m.LicenseDateOfIssuance, new { htmlAttributes = new { @class = "form-control", placeholder = "Date of Issuance", @readonly = "true" } })
                                                        @Html.ValidationMessageFor(m => m.LicenseDateOfIssuance, "", new { @class = "label label-warning lb-sm" })
                                                    </div>
                                                    <div class="col-md-12">
                                                        @Html.LabelFor(m => m.LicenseDateOfExpiry, "Date of Expiry", new { @class = "col-form-label required-field" })
                                                        @Html.EditorFor(m => m.LicenseDateOfExpiry, new { htmlAttributes = new { @class = "form-control", placeholder = "Date of Expiry", @readonly = "true" } })
                                                        @Html.ValidationMessageFor(m => m.LicenseDateOfExpiry, "", new { @class = "label label-warning lb-sm" })
                                                    </div>
                                                    <div class="col-md-12">
                                                        @Html.LabelFor(m => m.PlaceOfIssue, new { @class = "col-form-label required-field" })
                                                        @Html.TextBoxFor(m => m.PlaceOfIssue, "Place of Issue", new { @class = "form-control", placeholder = "e.g. Mabushi, Abuja" })
                                                        @Html.ValidationMessageFor(m => m.PlaceOfIssue, "", new { @class = "label label-warning lb-sm" })
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.Gender, new { @class = "col-form-label required-field" })
                                                    @Html.EnumDropDownListFor(m => m.Gender, new { @class = "form-control" })
                                                    @Html.ValidationMessageFor(m => m.Gender, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.MaritalStatus, "Marital Status", new { @class = "col-form-label required-field" })
                                                    @Html.EnumDropDownListFor(m => m.MaritalStatus, new { @class = "form-control" })
                                                    @Html.ValidationMessageFor(m => m.MaritalStatus, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.AddressLine1, "Address Line 1", new { @class = "col-form-label  required-field" })
                                                    @Html.TextAreaFor(m => m.AddressLine1, new { @class = "form-control", placeholder = "e.g. Plot 67, Independence Close" })
                                                    @Html.ValidationMessageFor(m => m.AddressLine1, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.State, "State of Origin", new { @class = "col-form-label required-field" })
                                                    @Html.TextBoxFor(m => m.State, new { @class = "form-control", placeholder = "e.g. Abia State" })
                                                    @Html.ValidationMessageFor(m => m.State, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.City, "City", new { @class = "col-form-label required-field" })
                                                    @Html.TextBoxFor(m => m.City, new { @class = "form-control", placeholder = "City" })
                                                    @Html.ValidationMessageFor(m => m.City, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.NextOfKinFullname, "Next of Kin Fullname", new { @class = "col-form-label required-field" })
                                                    @Html.TextBoxFor(m => m.NextOfKinFullname, new { @class = "form-control" })
                                                    @Html.ValidationMessageFor(m => m.NextOfKinFullname, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.NextOfKinRelationship, "Next of Kin Relationship", new { @class = "col-form-label required-field" })
                                                    @Html.TextBoxFor(m => m.NextOfKinRelationship, new { @class = "form-control", placeholder = "e.g. Brother" })
                                                    @Html.ValidationMessageFor(m => m.State, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.NextOfKinPhoneNumber, "Next of Kin Phone Number", new { @class = "col-form-label required-field" })
                                                    @Html.TextBoxFor(m => m.NextOfKinPhoneNumber, new { @class = "form-control", placeholder = "e.g. +2341231221223" })
                                                    @Html.ValidationMessageFor(m => m.State, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.NextOfKinAddress, "Next of Kin's Address", new { @class = "col-form-label required-field" })
                                                    @Html.TextBoxFor(m => m.NextOfKinAddress, new { @class = "form-control", placeholder = "e.g. Next of Kin's Address" })
                                                    @Html.ValidationMessageFor(m => m.NextOfKinAddress, "", new { @class = "label label-warning lb-sm" })
                                                </div>
                                                <div class="col-md-12">
                                                    @Html.LabelFor(m => m.TIN, "Tax Identification Number", new { @class = "col-form-label required-field" })
                                                    @Html.TextBoxFor(m => m.TIN, new { @class = "form-control", placeholder = "TIN" })
                                                    @Html.ValidationMessageFor(m => m.TIN, "", new { @class = "label label-warning lb-sm" })
                                                </div>

                                            </div>
                                            <div class="col-md-12">
                                                <input type="submit" class="btn btn-success" value="Register" />
                                                <input type="reset" class="btn btn-danger" value="Reset" />
                                            </div>
                                        </div>
                                    </div>