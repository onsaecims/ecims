﻿using ecims.Controllers.AuditTrailGenerator;
using ecims.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Controllers.AgencyNotificationController
{
    //Class to send agencies emails after EUC applications have not been acted upon
    public class OutstandingEUCNotification
    {
        ApplicationDbContext _context;
        AuditTrailGenerator.AuditTrail _auditTrail;
        private void NotifyAgenciesOfOutstandingEUCApplications()
        {
            //Get a list of all valid certificates which have been forwarded i.e. requiring action from an agency 
            var outstandingEUCApplication = _context.EndUserCertificate.Where(eucApplication => eucApplication.EUCApprovalStatus == EUCApprovedStatus.Forwarded).ToList();
            //Iterate through every forwarded application
            foreach (var eucApplication in outstandingEUCApplication)
            {
                if (eucApplication.dateForwarded != null)
                {
                    //Check if the application was made over 7 days ago
                    if (IsOver7Days(eucApplication.dateForwarded))
                    {
                        var applicationNumber = eucApplication.applicationNo;
                        var applicationDate = eucApplication.dateForwarded;
                        var agencyForwardedTo = eucApplication.AgencyId;
                        
                        //Notify all users in that agency of the pending request
                        NotifyAgency(applicationNumber, applicationDate, agencyForwardedTo);
                    }
                }
            }
        }
        //Send email and SMS to all users in the highlighted category
        private async void NotifyAgency(string applicationNumber, DateTime applicationDate, int agencyForwardedTo)
        {
            //List of all users in the agency
            var usersInAgency = _context.Users.Where(agencyUsers => agencyUsers.PortalUserCategoryId == agencyForwardedTo).ToList();

            foreach (var agencyUser in usersInAgency)
            {
                //Send user verification email
                IdentityMessage EmailMessage = new IdentityMessage();

                EmailMessage.Body = "Dear " + agencyUser.FirstName + " " + agencyUser.LastName + ". "
                                    + "This E-mail is to notify you of an End-User Certificate application which was forwarded to your agency and has not been acted on in over 7 days." +
                                    "<br/>" +
                                    "Application Number: " + applicationNumber +
                                    "<br/>" +
                                    "Please log into your account and act on it as soon as possible." +
                                    "<br/>" +
                                    "Thank you.";
                ;
                EmailMessage.Destination = agencyUser.Email;
                EmailMessage.Subject = "Outstanding EUC Application: " + applicationNumber + " Awaiting Action";


                EmailService emailService = new EmailService();
                await emailService.SendAsync(EmailMessage);

                _auditTrail = new AuditTrailGenerator.AuditTrail();
                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Email reminder for outstanding application: " + applicationNumber + ". Email Sent to:" + agencyUser.Email + " Has been sent to " + agencyUser.LastName + " " + agencyUser.FirstName + " reminding them of pending actions to be taken.",
                   "Application Automated Messaging",
                   "EUC Application Forwarded For Vetting",
                   "Application Automated Messaging");

                //Send SMS to agency user
                //Send user verification SMS
                IdentityMessage SmsMessage = new IdentityMessage();
                //send accompanying SMS reminding user to verify their email
                string smsBody = "Dear " + agencyUser.FirstName + " " + agencyUser.LastName
                                    + "This E-mail is to notify you of an End-User Certificate application which was forwarded to your agency and has not been acted on in over 7 days. " +
                                    "Application Number: " + applicationNumber +
                                    "Please log into your account and act on it as soon as possible. " +
                                    "Thank you.";

                SmsMessage.Body = smsBody;
                SmsMessage.Destination = agencyUser.PhoneNumber;
                SmsMessage.Subject = "Outstanding EUC Application: " + applicationNumber + " Awaiting Action";

                SmsService smsSerice = new SmsService();
                await smsSerice.SendAsync(SmsMessage);
            }
        }

        //Check if the application was forwarded over 7 days ago 
        public bool IsOver7Days(DateTime dateCreated)
        {
            var today = DateTime.Now;
            var daysPast = (today - dateCreated).TotalDays;
            if (daysPast >= 7)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}