﻿using ecims.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.AspNet.Identity.Owin;
using System.Net;
using System.IO;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace ecims.Controllers.AgencyNotificationController
{
    public class RejectEUCNotification
    {
        private ApplicationDbContext _context;
        private ApplicationRoleManager _roleManager;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        AuditTrailGenerator.AuditTrail _auditTrail = new AuditTrailGenerator.AuditTrail();

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //Send emails to every agency user informing them that a new application has been forwarded for their action
        public async Task<ActionResult> RejectApplicantEUCApplication(ApplicationUser user, string applicationNumber, string rejectionReason, string forwardingUserID, string forwardingUserIPAddress)
        {
            //Get every user belonging to this agency
            //var agencyUserList = _context.Users.Where(agencyUsers => agencyUsers.PortalUserCategoryId == portalSubCategoryID).ToList();
            //Message to be sent via SMS and email
            string emailMessage = " An End-User Certificate application has been sent to you and your colleagues for vetting and subsequent approval or rejection. " +
                            "<br/>" +
                            "Application Number: " + applicationNumber +
                            "<br/>" +
                            "Please log into your account and act on it as soon as possible. " +
                            "<br/>" +
                            "Thank you.";
            //SMS Message
            string smsMessage = "EUC application: " + applicationNumber + " has been sent for vetting, approval or rejection.Please log into your account and act on it as soon as possible.";

            string subject = applicationNumber + " New Application Request Awaits Your Endorsement or Dis-Endorsement";

            //Check user list for null
            if (user != null)
            {
                    //Send user verification email
                    IdentityMessage EmailMessage = new IdentityMessage();

                    EmailMessage.Body = "Dear " + user.FirstName + " " + user.LastName + ". "
                                        + "Your End-User Certificate application with Application Number: " + applicationNumber + " has been rejected. "
                                        + "<br/>"
                                        +"Reason for rejection: \"" + rejectionReason
                                        + "\" <br/>"
                                        +"Kindly rectify the highlighted issue and re-apply when you are ready. "
                                        +"<br/>"
                                        +"Thank you.";
                    ;
                    EmailMessage.Destination = user.Email;
                    EmailMessage.Subject = "EUC Application Rejected";


                    EmailService emailService = new EmailService();
                    await emailService.SendAsync(EmailMessage);
                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Email notification for rejection of Application: " + applicationNumber + ". Email Sent to:" + user.Email + " Has been sent to " + user.LastName + " " + user.FirstName + " for approval/rejection.",
                       forwardingUserID,
                       "EUC Application Rejection Notification Sent.",
                       forwardingUserIPAddress);

                    //Send SMS to agency user
                    //Send user verification SMS
                    IdentityMessage SmsMessage = new IdentityMessage();
                    //send accompanying SMS reminding user to verify their email
                    string smsBody = "Dear " + user.FirstName + " " + user.LastName + ". "
                                        + "Your End-User Certificate application with Application Number: " + applicationNumber + " has been rejected. "
                                        + "Reason for rejection: \"" + rejectionReason
                                        + "\" Kindly rectify the highlighted issue and re-apply when you are ready. "
                                        + "Thank you.";

                SmsMessage.Body = smsBody;
                    SmsMessage.Destination = user.PhoneNumber;
                    SmsMessage.Subject = "EUC Application Rejected";

                    SmsService smsSerice = new SmsService();
                    await smsSerice.SendAsync(SmsMessage);

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("SMS rejection notification for Application: " + applicationNumber + ". Phone Number: " + user.PhoneNumber + "Has been sent to " + user.LastName + " " + user.FirstName + " for approval/rejection.",
                       forwardingUserID,
                       "SMS rejection Notification Sent.",
                       forwardingUserIPAddress);

                //Return server result of 'OK' if  messages are sent successfully
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            else
            {
                //Return server result of 'Bad Request' if  the user list is null
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
        //Send email asynchronously
        private async Task<string> SendEmailConfirmationTokenAsync(string userID, string subject, string message)
        {
            await UserManager.SendEmailAsync(userID, subject, message);

            return message;
        }
        //Send SMS asynchronously
        private async Task<string> SendSmsAsync(string recipient, string subject, string message)
        {
            await SendSmsAsynchronously(recipient, message);

            return message;
        }

        private async Task SendSmsAsynchronously(string recipient, string message)
        {
            //SMS Sending
            string sender = "ONSA ECIMS";

            string url = "https://www.MultiTexter.com/tools/geturl/Sms.php?username=ocedache@gmail.com&password=Omanchala1944&sender="
                            + sender + "&message=" + message +
                            "&flash=0&sendtime=2009-10- 18%2006:30&listname=friends&recipients="
                            + recipient;

            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
            webReq.Method = "GET";
            await webReq.GetResponseAsync();
            //Stream answer = webResponse.GetResponseStream();
            //StreamReader _recievedAnswer = new StreamReader(answer);
        }

        private void SendSmsConfirmation(string recipient, string message)
        {
            //SMS Sending
            string sender = "ONSA ECIMS";

            string url = "https://www.MultiTexter.com/tools/geturl/Sms.php?username=ocedache@gmail.com&password=Omanchala1944&sender="
                            + sender + "&message=" + message +
                            "&flash=0&sendtime=2009-10- 18%2006:30&listname=friends&recipients="
                            + recipient;

            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
            webReq.Method = "GET";
            HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();
            Stream answer = webResponse.GetResponseStream();
            StreamReader _recievedAnswer = new StreamReader(answer);
        }
    }
}