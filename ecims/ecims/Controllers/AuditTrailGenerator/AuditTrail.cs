﻿using ecims.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Controllers.AuditTrailGenerator
{
    public class AuditTrail
    {
        ApplicationDbContext _context = new ApplicationDbContext();

        //Create a new row in the audit trail table
        public void CreateAuditTrail(string actionCarriedOut, string carriedOutBy, string actionDescription, string ipAddress)
        {
            if (carriedOutBy == null)
            {
                var auditTrail = new ecims.Models.AuditTrail
                {
                    ActionCarriedOut = actionCarriedOut,
                    CarriedOutBy = "Anonymous User",
                    DateOfActivity = DateTime.Now,
                    ActionDescription = actionDescription,
                    UserIPAddress = ipAddress
                };
                _context.AuditTrail.Add(auditTrail);
                _context.SaveChanges();
            }
            else
            {
                var auditTrail = new ecims.Models.AuditTrail
                {
                    ActionCarriedOut = actionCarriedOut,
                    CarriedOutBy = carriedOutBy,
                    DateOfActivity = DateTime.Now,
                    ActionDescription = actionDescription,
                    UserIPAddress = ipAddress
                };
                _context.AuditTrail.Add(auditTrail);
                _context.SaveChanges();
            }
        }
        
    }
}