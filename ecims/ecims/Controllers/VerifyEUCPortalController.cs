﻿using ecims.Models;
using ecims.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Runtime.InteropServices;
using Google.Authenticator;
using ecims.ViewModels.GoogleAuthenticator;

namespace ecims.Controllers
{
    [Authorize]
    public class VerifyEUCPortalController : Controller
    {
        private ApplicationDbContext _context;
        private ApplicationRoleManager _roleManager;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        AuditTrailGenerator.AuditTrail _auditTrail = new AuditTrailGenerator.AuditTrail();
        private const string key = "qazqaz12345";

        public VerifyEUCPortalController()
        {
            _context = new ApplicationDbContext();
        }

        public VerifyEUCPortalController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            RoleManager = roleManager;
        }
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        // GET: VerifyEUCPortal
        public ActionResult Index()
        {
            return View();
        }
        // GET: Login page for "Verify EUC Portal"
        [AllowAnonymous]
        public ActionResult Login()
        {
            //Close any session in progress
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            return View();
        }
        //POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            //Close any session in progress
            //Session.Abandon();
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            #region password validation
            //Check if the user has a password before moving forward
            PasswordCheckController PasswordCheck = new PasswordCheckController();
            var passwordExists = PasswordCheck.CheckIfPasswordExists(model.Email);

            //If user password exists set a message in the view bag and redirect the user to the password reset page
            if (passwordExists == false)
            {
                TempData["PasswordCheckResult"] = "In order to serve all end-users better, and to improve data security on the EUC "
                                                + "platform, all users are required to reset their passwords. "
                                                + "Please use the form below to enter your e-mail and reset your password.";

                return RedirectToAction("ForgotPassword", "Account");
            }
            #endregion
            #region check model state
            if (!ModelState.IsValid)
            {
                TempData["InvalidModelState"] = "One or more of your credentials were entered incorrectly."
                    + " Please ensure you entered your username and password correctly before submitting.";
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                return RedirectToAction("Login", "VerifyEUCPortal");
            }
            #endregion
            #region user validation: if email is not confirmed, send confirmation link
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);

            if (result == SignInStatus.Failure)
            {
                TempData["InvalidModelState"] = "One or more of your credentials were entered incorrectly."
                    + " Please ensure you entered your username and password correctly before submitting.";

                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                return RedirectToAction("Login", "VerifyEUCPortal");
            }

            var user = SignInManager.UserManager.Find(model.Email, model.Password);
            if (!user.canVerifyEndUserCertificates || user.PortalUserCategoryId != 32)
            {
                TempData["UnauthorizedAccess"] = "You are not authorized to access this portal and this attempt has been logged."
                    + "Another attempt to access this portal will lead to your credentials being stored and permanently blacklisted.";

                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                return RedirectToAction("Login", "VerifyEUCPortal");
            }
            if (user != null)
            {
                if (!await UserManager.IsEmailConfirmedAsync(user.Id))
                {
                    //Add message to view
                    ModelState.AddModelError("", "Check your email to verify your account before you can login.");

                    //Send user verification email
                    var confirmationLink = GenerateConfirmationTokenAsync(user.Id);

                    IdentityMessage EmailMessage = new IdentityMessage();

                    EmailMessage.Body = "Dear " + user.FirstName + " " + user.LastName + ". "
                                        + "<br/>"
                                        + "You are unable to login because your account has not been verified on the ONSA"
                                        + " End-User Certificate"
                                        + " portal."
                                        + "<br/>"
                                        + " To verify your account, please access your registered email and click the link in the email which has just been sent."
                                        + "<br/>"
                                        + confirmationLink.Result;
                    ;
                    EmailMessage.Destination = user.Email;
                    EmailMessage.Subject = "Confirm your account-Resend";

                    EmailService emailService = new EmailService();
                    await emailService.SendAsync(EmailMessage);

                    _auditTrail.CreateAuditTrail("Applicant: " + user.ApplicationNumber + ". Name: " + user.LastName + " " + user.FirstName + " attempted to log in without confirming email.",
                       User.Identity.GetUserId(),
                       "Attempted to log in.",
                       Request.UserHostAddress);


                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                    return View(model);
                }
            }
            #endregion
            #region user validation: if user is null, log it on the audit trail and reply to view model without letting the user know that the credentials are invalid
            else if (user == null)
            {
                ModelState.AddModelError("", "Make sure your Email address and Password are correctly typed, otherwise create an account to sign in");

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Attempted to log into EUC verification portal with an account that does not exist.",
                   User.Identity.GetUserId(),
                   "No account.",
                   Request.UserHostAddress);


                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                return View(model);
            }
            else
            {
                ModelState.AddModelError("", "Invalid username or password");

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Applicant: " + user.ApplicationNumber
                    + ". Name: " + user.LastName
                    + " " + user.FirstName
                    + " attempted to log in to the EUC verification portal with wrong credentials.",
                   User.Identity.GetUserId(),
                   "Wrong credentials.",
                   Request.UserHostAddress);

                return View(model);
            }
            #endregion
            #region user validation: check if user has been approved by support
            var approvalStatus = user.ApprovalStatus;

            if (approvalStatus == ApprovedStatus.Pending)
            {
                //return View(model);
                ModelState.AddModelError("", "This account has not been approved yet, a notification will be sent to you via SMS and email once your account has been approved or disapproved.");

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Applicant: " + user.ApplicationNumber
                    + ". Name: " + user.LastName + " "
                    + user.FirstName
                    + " attempted to log in with an un-approved account.",
                   User.Identity.GetUserId(),
                   "Acount has not been approved.",
                   Request.UserHostAddress);

                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                return View(model);
            }
            if (approvalStatus == ApprovedStatus.Rejected)
            {
                //return View(model);
                ModelState.AddModelError("", "This account has been rejected and permanently banned from accessing the EUC portal.");

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Applicant: " + user.ApplicationNumber + ". Name: " + user.LastName + " " + user.FirstName + " attempted to log in with a rejected account.",
                   User.Identity.GetUserId(),
                   "Rejected account attempting to access the EUC Verification Portal.",
                   Request.UserHostAddress);

                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                return View(model);
            }
            #endregion
            #region user validation: check if user is blacklisted
            if (user.IsBlackListed == true)
            {
                TempData["Blacklisted"] = "This account has been blacklisted from applying for EUC certificates. Kindly contact support for more information";
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                return RedirectToAction("Login", "Account");
            }
            #endregion
            #region sign the user in based on their sign in status
            switch (result)
            {
                case SignInStatus.Success:

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Applicant: " + user.ApplicationNumber + ". Name: " + user.LastName + " " + user.FirstName + " signed in successfully.",
                       User.Identity.GetUserId(),
                       "redirected to EUC Verification Portal.",
                       Request.UserHostAddress);

                    return RedirectToAction("Dashboard", "VerifyEUCPortal");
                case SignInStatus.LockedOut:

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Applicant: " + user.ApplicationNumber + ". Name: " + user.LastName + " " + user.FirstName + " locked user out.",
                       User.Identity.GetUserId(),
                       "user locked out from EUC Verification Portal.",
                       Request.UserHostAddress);

                    return View("Lockout");
                case SignInStatus.RequiresVerification:

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Applicant: " + user.ApplicationNumber + ". Name: " + user.LastName + " " + user.FirstName + " sent user verification code because of un-verified account.",
                       User.Identity.GetUserId(),
                       "sent user verification code for EUC Verification Portal.",
                       Request.UserHostAddress);

                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
            #endregion
        }

        //[Authorize(Roles = "Certificate Verification User")]
        public ActionResult Dashboard()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();

            if (userId != null)
            {
                var userToVerify = _context.Users.Where(u => u.Id == userId).FirstOrDefault();

                //Only users with with the portal user category of 32(Certificate Verififcation User) should be able to access the dashboard.
                if (userToVerify.PortalUserCategoryId != 32)
                {
                    TempData["UnauthorizedAccess"] = "You are not authorized to access this portal and this attempt has been logged."
                    + "Another attempt to access this portal will lead to your credentials being stored and permanently blacklisted.";

                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                    return RedirectToAction("Login", "VerifyEUCPortal");
                }
            }

            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //if (!user.canVerifyEndUserCertificates)
            //{
            //    return RedirectToAction("Login", "VerifyEUCPortal");
            //}

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var country = _context.CountryList.ToList();

            if (!user.isGoogleAuthenticatorActive)
            {
                return RedirectToAction("BarCodeCreator", "VerifyEUCPortal");
            }

            return View(new VerifyEucPortalDashboardViewModel
            {
                user = user,
                role = roles,
                UserId = user.Id,
                countries = country
            });
        }
        //Generate account confirmation link/token for EMAIL
        private async Task<string> GenerateConfirmationTokenAsync(string userID)
        {
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(userID);
            var callbackUrl = Url.Action("ConfirmEmail", "Account",
               new { userId = userID, code = code }, protocol: Request.Url.Scheme);
            string clickCallbackUrl = "Please confirm your account by clicking <a href=\""
                                                    + callbackUrl + "\">here</a>";

            return clickCallbackUrl;
        }

        public ActionResult VerifyCertificate(string id, string value)
        {
            var certificate = _context.EUCCertificates.Where(euc => euc.CertNo == value).FirstOrDefault();

            if (certificate != null)
            {
                _auditTrail.CreateAuditTrail("Verified certificate number on EUC verification portal",
                    User.Identity.GetUserId(), "VERIFIED CERTIFICATE NUMBER: " + certificate.CertNo +
                    ". APPLICATION NUMBER: " + certificate.applicationNo, Request.UserHostAddress);

                var uploadedCertificate = _context.UploadedEUCCertificate.Where(cert => cert.CertNo == certificate.CertNo).FirstOrDefault();

                var userId = User.Identity.GetUserId();

                var certificateToVerify = new VerifiedCertificates
                {
                    UserId = userId,
                    CertificateId = certificate.id,
                    DateVerified = DateTime.Now
                };
                try
                {
                    _context.VerifiedCertificates.Add(certificateToVerify);
                    int result = _context.SaveChanges();
                    if (result > 0)
                    {
                        _auditTrail.CreateAuditTrail("Verified certificate on EUC verification portal",
                   User.Identity.GetUserId(), "CERTIFICATE NUMBER: " + certificate.CertNo, Request.UserHostAddress);
                    }
                }
                catch (Exception)
                {

                }

                return Json(new
                {
                    success = true,
                    UploadUrl = uploadedCertificate.UploadUrl
                },
                JsonRequestBehavior.AllowGet);
            }

            else
            {
                _auditTrail.CreateAuditTrail("Failed certificate number verification on EUC verification portal",
                    User.Identity.GetUserId(), "FAILED CERTIFICATE NUMBER: " + value, Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
        }
        public ActionResult VerifyApplicationNumber(string id, string value, string certificateNumber)
        {
            var certificate = _context.EUCCertificates.Where(euc => euc.applicationNo == value && euc.CertNo == certificateNumber).FirstOrDefault();

            if (certificate != null)
            {
                _auditTrail.CreateAuditTrail("Verified certificate application number on EUC verification portal",
                    User.Identity.GetUserId(), "APPLICATION NUMBER: " + certificate.applicationNo + " CERTIFICATE NUMBER: " + certificate.CertNo, Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            else
            {
                _auditTrail.CreateAuditTrail("Failed to verify application number on EUC verification portal",
                    User.Identity.GetUserId(), "FAILED APPLICATION NUMBER: " + value + " CERTIFICATE NUMBER: " + certificateNumber, Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
        }
        public ActionResult VerifyApplicantNumber(string id, string value, string certificateNumber)
        {
            var certificate = _context.EUCCertificates.Where(euc => euc.CertNo == certificateNumber).FirstOrDefault();

            if (certificate == null)
            {
                _auditTrail.CreateAuditTrail("Failed to verify applicant because of invalid certificate number",
                    User.Identity.GetUserId(), "FAILED TO VERIFY APPLICANT NUMBER: " + value + " CERTIFICATE NUMBER: " + certificateNumber, Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            var applicant = _context.Users.Where(user => user.ApplicationNumber == value).FirstOrDefault();

            if (applicant == null)
            {
                _auditTrail.CreateAuditTrail("Failed to verify applicant number",
                    User.Identity.GetUserId(), "FAILED TO VERIFY APPLICANT NUMBER: " + value + " CERTIFICATE NUMBER: " + certificateNumber, Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            else
            {
                _auditTrail.CreateAuditTrail("Verified applicant number on EUC verification portal",
                    User.Identity.GetUserId(), "APPLICANT NUMBER: " + applicant.ApplicationNumber + " CERTIFICATE NUMBER: " + certificate.CertNo, Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
        }
        public ActionResult VerifyVendorName(string id, string value, string certificateNumber)
        {
            var certificate = _context.EUCCertificates.Where(euc => euc.CertNo == certificateNumber).FirstOrDefault();
            var application = _context.EndUserCertificate.Where(app => app.applicationNo == certificate.applicationNo).FirstOrDefault();

            if (application.IsOrganisation)
            {
                var organisationName = _context.EUCOrganisations.Where(organisation => organisation.ApplicationNumber == application.applicationNo).FirstOrDefault();
                if (organisationName.Name == value)
                {
                    _auditTrail.CreateAuditTrail("Verified vendor name on EUC verification portal",
                    User.Identity.GetUserId(), "VENDOR NAME: " + organisationName.Name + " CERTIFICATE NUMBER: " + certificate.CertNo, Request.UserHostAddress);

                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
                else
                {
                    _auditTrail.CreateAuditTrail("Failed to verify vendor name on EUC verification portal",
                    User.Identity.GetUserId(), "FAILED VENDOR NAME: " + organisationName.Name + " CERTIFICATE NUMBER: " + certificate.CertNo, Request.UserHostAddress);

                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                }
            }
            else
            {
                var names = value.Split(' ');
                var lastName = names[0];
                var firstName = names[1];
                var otherName = names[2];
                var applicantName = _context.Users.Where(user => user.LastName == lastName
                                                        && user.FirstName == firstName).First();
                if (applicantName != null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                }
            }
        }
        public ActionResult VerifyTokenCode(string id, string value, string certificateNumber)
        {
            //Validate user's token using google authenticator app
            GoogleAuthenticator.GoogleAuthenticatorController googleAuthenticator = new GoogleAuthenticator.GoogleAuthenticatorController();
            bool isTokenValid = googleAuthenticator.IsTwoFactorAuthentic(value, User.Identity.GetUserId());

            if (isTokenValid)
            {
                var certificate = _context.EUCCertificates.Where(euc => euc.CertNo == certificateNumber).FirstOrDefault();

                if (certificate != null)
                {
                    _auditTrail.CreateAuditTrail("Verified certificate number on EUC verification portal",
                        User.Identity.GetUserId(), "VERIFIED CERTIFICATE NUMBER: " + certificate.CertNo +
                        ". APPLICATION NUMBER: " + certificate.applicationNo, Request.UserHostAddress);

                    var uploadedCertificate = _context.UploadedEUCCertificate.Where(cert => cert.CertNo == certificate.CertNo).FirstOrDefault();

                    var userId = User.Identity.GetUserId();

                    var certificateToVerify = new VerifiedCertificates
                    {
                        UserId = userId,
                        CertificateId = certificate.id,
                        DateVerified = DateTime.Now
                    };
                    try
                    {
                        _context.VerifiedCertificates.Add(certificateToVerify);
                        int result = _context.SaveChanges();
                        if (result > 0)
                        {
                            _auditTrail.CreateAuditTrail("Verified certificate on EUC verification portal",
                       User.Identity.GetUserId(), "CERTIFICATE NUMBER: " + certificate.CertNo, Request.UserHostAddress);

                            return Json(new
                            {
                                success = true,
                                UploadUrl = uploadedCertificate.UploadUrl
                            },
                                                JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new
                            {
                                success = false
                            },
                JsonRequestBehavior.AllowGet);
                        }
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            else
            {
                return Json(new
                {
                    success = false
                },
                JsonRequestBehavior.AllowGet);
            }
            return Json(new
            {
                success = false
            },
                JsonRequestBehavior.AllowGet);
        }
        public ActionResult VerifyVendorAddress(string id, string value, string certificateNumber)
        {
            var certificate = _context.EUCCertificates.Where(euc => euc.CertNo == certificateNumber).FirstOrDefault();

            if (certificate == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            var organisation = _context.EUCOrganisations.Where(euc => euc.ApplicationNumber == certificate.applicationNo).FirstOrDefault();

            if (organisation == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            if (organisation.Address == value)
            {
                _auditTrail.CreateAuditTrail("Verified vendor address on EUC verification portal",
                    User.Identity.GetUserId(), "VENDOR ADDRESS: " + organisation.Address + " CERTIFICATE NUMBER: " + certificate.CertNo, Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            else
            {
                _auditTrail.CreateAuditTrail("Failed to Verify vendor address on EUC verification portal",
                   User.Identity.GetUserId(), "FAILED VENDOR ADDRESS: " + organisation.Address + " CERTIFICATE NUMBER: " + certificate.CertNo, Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
        }
        public ActionResult VerifyCurrency(string id, string value, string certificateNumber)
        {
            var certificate = _context.EUCCertificates.Where(euc => euc.CertNo == certificateNumber).FirstOrDefault();

            if (certificate == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            var application = _context.EndUserCertificate.Where(euc => euc.applicationNo == certificate.applicationNo).FirstOrDefault();

            if (application == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            if (application.currencyOfPurchase == Convert.ToInt32(value))
            {
                _auditTrail.CreateAuditTrail("Verified currency of purchase on EUC Verification Portal.",
                    User.Identity.GetUserId(), "CURRENCY: " + application.currencyOfPurchase + " CERTIFICATE NUMBER: " + certificate.CertNo, Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            else
            {
                _auditTrail.CreateAuditTrail("Failed to verify currency of purchase on EUC Verification Portal.",
                    User.Identity.GetUserId(), "FAILED CURRENCY: " + application.currencyOfPurchase + " CERTIFICATE NUMBER: " + certificate.CertNo, Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
        }
        public ActionResult VerifyTotalCost(string id, string value, string certificateNumber)
        {
            var certificate = _context.EUCCertificates.Where(euc => euc.CertNo == certificateNumber).FirstOrDefault();

            if (certificate == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            var applicationDetails = _context.EndUserCertificateDetails.Where(euc => euc.applicationNo == certificate.applicationNo).ToList();

            if (applicationDetails == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            double TotalCost = 0;
            foreach (var detail in applicationDetails)
            {
                TotalCost += detail.cost;
            }

            if (TotalCost == Convert.ToDouble(value))
            {
                _auditTrail.CreateAuditTrail("Verified total cost on EUC verification portal",
                    User.Identity.GetUserId(), "TOTAL COST: " + TotalCost + " CERTIFICATE NUMBER: " + certificate.CertNo, Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            else
            {
                _auditTrail.CreateAuditTrail("Failed to verify total cost on EUC verification portal",
                    User.Identity.GetUserId(), "FAILED TOTAL COST: " + TotalCost + " CERTIFICATE NUMBER: " + certificate.CertNo, Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
        }
        public ActionResult VerifyCountry(string id, string value, string certificateNumber)
        {
            var certificate = _context.EUCCertificates.Where(euc => euc.CertNo == certificateNumber).FirstOrDefault();

            if (certificate == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            var application = _context.EndUserCertificate.Where(euc => euc.applicationNo == certificate.applicationNo).FirstOrDefault();

            if (application == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            if (Convert.ToInt32(application.originCountry) == Convert.ToInt32(value))
            {
                _auditTrail.CreateAuditTrail("Verified country of application on EUC verification portal",
                    User.Identity.GetUserId(), "COUNTRY: " + Convert.ToInt32(application.originCountry) + " CERTIFICATE NUMBER: " + certificate.CertNo, Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            else
            {
                _auditTrail.CreateAuditTrail("Failed to verify country of application on EUC verification portal",
                    User.Identity.GetUserId(), "FAILED COUNTRY: " + Convert.ToInt32(application.originCountry) + " CERTIFICATE NUMBER: " + certificate.CertNo, Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
        }
        public ActionResult VerifyDate(string id, string value, string certificateNumber)
        {
            var certificate = _context.EUCCertificates.Where(euc => euc.CertNo == certificateNumber).FirstOrDefault();

            if (certificate == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }

            //var application = _context.EndUserCertificate.Where(euc => euc.applicationNo == certificate.applicationNo).FirstOrDefault();

            //if (application == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            //}

            if (certificate.IssuanceDate.Date == Convert.ToDateTime(value).Date)
            {
                _auditTrail.CreateAuditTrail("Verified date of issuance on EUC verification portal",
                   User.Identity.GetUserId(), "DATE OF ISSUANCE: " + certificate.IssuanceDate + " CERTIFICATE NUMBER: " + certificate.CertNo, Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            else
            {
                _auditTrail.CreateAuditTrail("Failed to verify date of issuance on EUC verification portal",
                   User.Identity.GetUserId(), "FAILED DATE OF ISSUANCE: " + certificate.IssuanceDate + " CERTIFICATE NUMBER: " + certificate.CertNo, Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
        }
        #region verify the certificate
        public ActionResult ValidateCertificate(string CertNo)
        {
            var certificateToVerify = _context.EUCCertificates.Where(euc => euc.CertNo == CertNo).FirstOrDefault();

            if (certificateToVerify == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
            else
            {
                var existingCertificate = _context.VerifiedCertificates.Where(euc => euc.CertificateId == certificateToVerify.id).Count();

                if (existingCertificate > 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.Created);
                }

                var certificateId = certificateToVerify.id;
                var userId = User.Identity.GetUserId();

                var certificate = new VerifiedCertificates
                {
                    UserId = userId,
                    CertificateId = certificateId,
                    DateVerified = DateTime.Now
                };
                try
                {
                    _context.VerifiedCertificates.Add(certificate);
                    int result = _context.SaveChanges();
                    if (result > 0)
                    {
                        _auditTrail.CreateAuditTrail("Verified certificate on EUC verification portal",
                   User.Identity.GetUserId(), "CERTIFICATE NUMBER: " + certificateToVerify.CertNo, Request.UserHostAddress);

                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                }
                catch (Exception)
                {

                }

                return new HttpStatusCodeResult(HttpStatusCode.NotFound);
            }
        }
        #endregion
        public ActionResult Verify()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var country = _context.CountryList.ToList();

            return View(new VerifyEucPortalDashboardViewModel
            {
                user = user,
                role = roles,
                UserId = user.Id,
                countries = country
            });
        }
        /// <summary>
        /// Finds the MAC address of the NIC with maximum speed.
        /// </summary>
        /// <returns>The MAC address.</returns>
        public ActionResult GetMacAddress()
        {
            //const int MIN_MAC_ADDR_LENGTH = 12;
            //string macAddress = string.Empty;
            //long maxSpeed = -1;

            //foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            //{
            //    Console.WriteLine(
            //        "Found MAC Address: " + nic.GetPhysicalAddress() +
            //        " Type: " + nic.NetworkInterfaceType);

            //    string tempMac = nic.GetPhysicalAddress().ToString();
            //    if (nic.Speed > maxSpeed &&
            //        !string.IsNullOrEmpty(tempMac) &&
            //        tempMac.Length >= MIN_MAC_ADDR_LENGTH)
            //    {
            //        Console.WriteLine("New Max Speed = " + nic.Speed + ", MAC: " + tempMac);
            //        maxSpeed = nic.Speed;
            //        macAddress = tempMac;
            //    }
            //}

            return Json(new
            {
                success = true,
                MacAddress = NetworkInterface.GetAllNetworkInterfaces().Where(nic => nic.OperationalStatus == OperationalStatus.Up).Select(nic => nic.GetPhysicalAddress().ToString()).FirstOrDefault()
            },
            JsonRequestBehavior.AllowGet);
        }

        public ActionResult RenderPDF(VerifyEucPortalDashboardViewModel model)
        {
            return PartialView(model);
        }

        //Generate bar code for first time verififcation users
        // GET: Admin
        //Open dashboard page
        //[Authorize(Roles = "ONSA Support, NSA")]
        // GET: GoogleAuthenticator
        public ActionResult BarCodeCreator()
        {
            //Only move forward if a user is signed in
            if (User.Identity.GetUserId() == null)
            {
                TempData["UnauthorizedAccess"] = "You are not authorized to access this portal and this attempt has been logged."
                    + "Another attempt to access this portal will lead to your credentials being stored and permanently blacklisted.";

                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                return RedirectToAction("Login", "VerifyEUCPortal");
            }
            else
            {
                //Get ID of the currently logged in user
                var userId = User.Identity.GetUserId();
                //Create user object based off user ID
                var user = _context.Users.Where(u => u.Id == userId).Single();
                //Redirect any user who is not ONSA Support or NSA back to their dashboard
                if (!user.canVerifyEndUserCertificates || user.PortalUserCategoryId != 32)
                {
                    TempData["UnauthorizedAccess"] = "You are not authorized to access this portal and this attempt has been logged."
                        + "Another attempt to access this portal will lead to your credentials being stored and permanently blacklisted.";

                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                    return RedirectToAction("Login", "VerifyEUCPortal");
                }
                if (user.isGoogleAuthenticatorActive)
                {
                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Attempted to visit Google Authenticator barcode page but already had Google Authentication active. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
                       User.Identity.GetUserId(),
                       "Visited Google Authenticator barcode page.",
                       Request.UserHostAddress);
                    //Google authenticator logic
                    Session["GoogleAuthenticatorMessage"] = "Invalid Request. User already has google authentication active on the account!";
                    return RedirectToAction("Login", "VerifyEUCPortal");
                }
                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Visited Google Authenticator barcode page. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
                   User.Identity.GetUserId(),
                   "Visited Google Authenticator barcode page.",
                   Request.UserHostAddress);

                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

                //Google authenticator logic
                Session["UserName"] = user.UserName;
                //Two Factor Authentication Setup
                TwoFactorAuthenticator TwoFacAuth = new TwoFactorAuthenticator();
                string UserUniqueKey = (user.UserName + key);
                Session["UserUniqueKey"] = UserUniqueKey;
                var setupInfo = TwoFacAuth.GenerateSetupCode("End User Certificate Authorization", user.UserName, UserUniqueKey, 300, 300);
                ViewBag.BarcodeImageUrl = setupInfo.QrCodeSetupImageUrl;
                ViewBag.SetupCode = setupInfo.ManualEntryKey;

                return View(new BarCodeCreatorViewModel
                {
                    user = user,
                    role = roles,
                });
            }
        }
        public ActionResult TwoFactorAuthenticate()
        {
            var token = Request["CodeDigit"];
            TwoFactorAuthenticator TwoFacAuth = new TwoFactorAuthenticator();
            string UserUniqueKey = Session["UserUniqueKey"].ToString();
            bool isValid = TwoFacAuth.ValidateTwoFactorPIN(UserUniqueKey, token);
            if (isValid)
            {
                Session["IsValidTwoFactorAuthentication"] = true;
                //Get ID of the currently logged in user
                var userId = User.Identity.GetUserId();
                //Create user object based off user ID
                var user = _context.Users.Where(u => u.Id == userId).Single();

                //Update Database to enable two fact authorization
                var userToEnableGoogleAuthorizationFor = _context.Users.Find(user.Id);
                userToEnableGoogleAuthorizationFor.isGoogleAuthenticatorActive = true;
                _context.SaveChanges();

                Session["GoogleAuthenticationSuccessfulMessage"] = "Google authentication has been activated successfully for this account!";

                //If the user is not a support user and can verify EUCs redirect them to the verify EUC portal
                if (user.PortalUserCategoryId == 32)
                {
                    return RedirectToAction("Dashboard", "VerifyEUCPortal");
                }
                else
                {
                    TempData["UnauthorizedAccess"] = "You are not authorized to access this portal and this attempt has been logged."
                        + "Another attempt to access this portal will lead to your credentials being stored and permanently blacklisted.";

                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                    return RedirectToAction("Dashboard", "VerifyEUCPortal");
                }
            }
            Session["GoogleAuthenticationUnsuccessfulMessage"] = "Google authentication activation failed for this account!. Please re-enter the generated token.";
            return RedirectToAction("BarCodeCreator", "VerifyEUCPortal");
        }
        //Check if a generated token code is authentic
        public bool IsTwoFactorAuthentic(string tokenCode, string userId)
        {
            //Get ID of the currently logged in user
            //var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Find(userId);

            var token = tokenCode;
            TwoFactorAuthenticator TwoFacAuth = new TwoFactorAuthenticator();
            string UserUniqueKey = (user.UserName + key); ;
            bool isValid = TwoFacAuth.ValidateTwoFactorPIN(UserUniqueKey, token);
            if (isValid)
            {
                bool IsValidTwoFactorAuthentication = true;

                return IsValidTwoFactorAuthentication;
            }
            return false;
        }
    }
}