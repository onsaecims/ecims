﻿using ecims.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ecims.Controllers.ApplicationNumberGenerator
{
    public class ApplicationNumberGeneratorForNullApplicantsController
    {
        ApplicationDbContext _context;

        public ActionResult GenerateApplicationNumberForAplicantsWithNullValues()
        {
            _context = new ApplicationDbContext();

            var usersInDb = _context.Users.ToList();

            //iterate trough every user
            foreach (var user in usersInDb)
            {
                if (user.ApplicationNumber == null)
                {
                    user.ApplicationNumber = ApplicationNumberGenerator.GenerateApplicationNumber();

                    var result = _context.SaveChanges();
                }
            }
           

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}