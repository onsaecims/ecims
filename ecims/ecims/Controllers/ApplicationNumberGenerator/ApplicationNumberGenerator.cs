﻿using ecims.Models;
using System;
using System.Security.Cryptography;
using System.Web.Mvc;

namespace ecims.Controllers.ApplicationNumberGenerator
{
    public static class ApplicationNumberGenerator
    {
        public static string GenerateApplicationNumber()
        {
            string applicantNumber;
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                // Buffer storage.
                byte[] data = new byte[4];

                // Fill buffer.
                rng.GetBytes(data);

                // Convert to int 32.
                object value = CastToUnsigned(BitConverter.ToInt32(data, 0));

                int valueDigitCount = value.ToString().Length;

                string valueString = value.ToString();

                if (valueDigitCount >= 8)
                {
                    valueString = valueString.Substring(0, 8);

                    applicantNumber = "APN" + valueString;

                    return applicantNumber;
                }
                else
                {
                    applicantNumber = "APN" + valueString;

                    return applicantNumber;
                }
            }
        }
        private static object CastToUnsigned(object number)
        {
            Type type = number.GetType();
            unchecked
            {
                if (type == typeof(int)) return (uint)(int)number;
            }
            return null;
        }
        public static string GenerateEUCApplicationNumber()
        {
            string applicantNumber;
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                // Buffer storage.
                byte[] data = new byte[4];

                // Fill buffer.
                rng.GetBytes(data);

                // Convert to int 32.
                int value = BitConverter.ToInt32(data, 0);

                int valueDigitCount = value.ToString().Length;

                string valueString = value.ToString();

                if (valueDigitCount >= 6)
                {
                    valueString = valueString.Substring(1, 6);

                    applicantNumber = "APPLN" + valueString;

                    return applicantNumber;
                }
                else
                {
                    applicantNumber = "APPLN" + valueString;

                    return applicantNumber;
                }
            }
        }
        public static string GenerateItemNumber()
        {
            string applicantNumber;
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                // Buffer storage.
                byte[] data = new byte[4];

                // Fill buffer.
                rng.GetBytes(data);

                // Convert to int 32.
                int value = BitConverter.ToInt32(data, 0);

                int valueDigitCount = value.ToString().Length;

                string valueString = value.ToString();

                if (valueDigitCount >= 6)
                {
                    valueString = valueString.Substring(1, 6);

                    return valueString;
                }
                else
                {
                    applicantNumber = valueString;

                    return applicantNumber;
                }
            }
        }
        public static string GenerateCertificateNumber()
        {
            string certificateNumber;
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                // Buffer storage.
                byte[] data = new byte[4];

                // Fill buffer.
                rng.GetBytes(data);

                // Convert to int 32.
                int value = BitConverter.ToInt32(data, 0);

                int valueDigitCount = value.ToString().Length;

                string valueString = value.ToString();

                if (valueDigitCount >= 6)
                {
                    valueString = valueString.Substring(1, 6);

                    return valueString;
                }
                else
                {
                    certificateNumber = valueString;

                    return certificateNumber;
                }
            }
        }
    }
}