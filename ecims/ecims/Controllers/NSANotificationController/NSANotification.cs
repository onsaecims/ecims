﻿using ecims.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ecims.Controllers.NSANotificationController
{
    public class NSANotification
    {
        //Send the NSA 
        public async Task NotifyNSAOfNewEUCApplication(List<ApplicationUser> users, string applicationNumber)
        {
            foreach (var user in users)
            {
                //Send user verification email            
                IdentityMessage EmailMessage = new IdentityMessage();

                EmailMessage.Body = "Dear " + user.FirstName + " " + user.LastName + ". "
                                    + "<br/>"
                                    + "An EUC application has just been forwarded, "
                                    + "<br/>"
                                    + "Application Number: " + applicationNumber
                                    + "<br/>"
                                    + " Kindly review for rejection or approval."
                                    + "<br/>"
                                    + " Thank you.";
                EmailMessage.Destination = user.Email;
                EmailMessage.Subject = "New Application Received: " + applicationNumber;


                EmailService emailService = new EmailService();
                await emailService.SendAsync(EmailMessage);

                //Send user verification SMS
                IdentityMessage SmsMessage = new IdentityMessage();
                //send accompanying SMS reminding user to verify their email
                string smsBody = "An EUC application has just been forwarded: " + applicationNumber
                                    + ". Kindly review for rejection or approval.";

                SmsMessage.Body = smsBody;
                SmsMessage.Destination = user.PhoneNumber;
                SmsMessage.Subject = "Confirm your account-Resend";

                SmsService smsSerice = new SmsService();
                await smsSerice.SendAsync(SmsMessage);

            }
        }
        public async Task SendToken(List<ApplicationUser> users, string applicationNumber)
        {
            foreach (var user in users)
            {
                //Send user verification email            
                IdentityMessage EmailMessage = new IdentityMessage();

                EmailMessage.Body = "Dear " + user.FirstName + " " + user.LastName + ". "
                                    + "<br/>"
                                    + "An EUC application has just been marked for approval on the EUC portal using your account:"
                                    + "<br/>"
                                    + "Application Number: <b>" + applicationNumber + "</b>"
                                    + "<br/>"
                                    + "If you are not the one who initiated this process, kindly contact ONSA Support as soon as possible."
                                    + "<br/>"
                                    + " Thank you.";
                EmailMessage.Destination = user.Email;
                EmailMessage.Subject = "EUC Application Approved: " + applicationNumber;


                EmailService emailService = new EmailService();
                await emailService.SendAsync(EmailMessage);

                //Send user verification SMS
                IdentityMessage SmsMessage = new IdentityMessage();
                //send accompanying SMS reminding user to verify their email
                string smsBody = "Dear " + user.FirstName + " " + user.LastName + ". "
                                    + "An EUC application has just been marked for approval on the EUC portal using your account:"
                                    + " Application Number: " + applicationNumber
                                    + " If you are not the one who initiated this process, kindly contact ONSA Support as soon as possible."
                                    + " Thank you.";

                SmsMessage.Body = smsBody;
                SmsMessage.Destination = user.PhoneNumber;
                SmsMessage.Subject = "EUC Application Approved: " + applicationNumber;

                SmsService smsSerice = new SmsService();
                await smsSerice.SendAsync(SmsMessage);

            }
        }
    }
}