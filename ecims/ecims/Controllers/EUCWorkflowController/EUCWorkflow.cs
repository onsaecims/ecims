﻿using ecims.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Controllers.EUCWorkflowController
{
    public class EUCWorkflow
    {
        ApplicationDbContext _context = new ApplicationDbContext();
        //Create a new EUC workflow entry
        public void UpdateEUCWorkflow(DateTime dateOfAction, string applicationNumber, string actionSource, string actionRecipient, string handledStatus, string currentStatus, int sourceagency,int isWorkedOn, int AgencyWorkedOn)
        {
            var EUCWorkFlowEntry = new EUCWorkFlow
            {
                activityDate = dateOfAction,
                applicationNumber = applicationNumber,
                actionSource = actionSource,
                actionReciepient = actionRecipient,
                handledStatus = handledStatus,
                currentStatus = currentStatus,
                SourceAgency = sourceagency,
                isWorkedOn = isWorkedOn,
                AgencyWorkedOn = AgencyWorkedOn
            };
            //Save
            _context.EUCWorkFlow.Add(EUCWorkFlowEntry);
            int result = _context.SaveChanges();
            if (result > 0)
            {

            }
        }
    }
}