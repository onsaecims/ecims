﻿using ecims.Controllers.AgencyNotificationController;
using ecims.Controllers.ApplicantNotificationController;
using ecims.Controllers.NSANotificationController;
using ecims.Controllers.ONSASupportNotificationController;
using ecims.Models;
using ecims.Reports.Analytics;
using ecims.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ecims.Controllers
{
    public class AdminController : Controller
    {
        ApplicationDbContext _context;
        private ApplicationUserManager _userManager;
        AuditTrailGenerator.AuditTrail _auditTrail = new AuditTrailGenerator.AuditTrail();

        public AdminController()
        {
            _context = new ApplicationDbContext();

        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        // GET: Admin
        //Open dashboard page
        [Authorize(Roles = "Agency, Client User, ONSA Admin, ONSA Support, NSA, Customs User, Exclusive User, ONSA PGSO")]
        public ActionResult MainDashboard()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            if (user.PortalUserCategoryId == 32)
            {
                //Close any session in progress
                //Session.Abandon();
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            }
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Count Pending User Request
            int pendingUserRqstCount = _context.Users.Where(o => o.ApprovalStatus == ApprovedStatus.Pending && o.EmailConfirmed == true).Count();
            int approvedUserRqstCount = _context.Users.Where(o => o.ApprovalStatus == ApprovedStatus.Approved).Count();
            int rejectedUserRqstCount = _context.Users.Where(o => o.ApprovalStatus == ApprovedStatus.Rejected).Count();
            int allRequest = _context.Users.Count();
            //Count Pending EUC Application Request
            int pendingEUCRqstCount = _context.EndUserCertificate.Where(i => i.EUCApprovalStatus == EUCApprovedStatus.Pending).Count();
            int approvedEUCRqstCount;
            //Count Approved EUC Application Request
            if (user.PortalUserCategoryId == 5)
            {
                approvedEUCRqstCount = _context.EndUserCertificate.Where(i => i.EUCApprovalStatus == EUCApprovedStatus.Approved || i.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued).Count();
            }
            else
            {
                approvedEUCRqstCount = _context.EndUserCertificate.Where(i => i.EUCApprovalStatus == EUCApprovedStatus.Approved || i.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued).Where(i => i.AgencyForwardedFrom == 45).Count();
            }
            //Count DisApproved EUC Application Request
            int disapprovedEUCRqstCount = _context.EndUserCertificate.Where(i => i.EUCApprovalStatus == EUCApprovedStatus.Rejected).Count();

            //Count Endorsed EUC Application Request
            //int endorsedEUCRqstCount = _context.EUCWorkFlow.Where(o => o.handledStatus == "ENDORSED" && o.isWorkedOn == 0).Count();
            int endorsedEUCRqstCount = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed).Count();
            //Count Awaiting Approval
            int requiringApproval;

            if (user.PortalUserCategoryId == 5)
            {
                requiringApproval = _context.EndUserCertificate.Where(o => o.EUCApprovalStatus == EUCApprovedStatus.Requiring_Approval && o.AgencyForwardedTo == 5).Count();
            }
            else
            {
                requiringApproval = _context.EndUserCertificate.Where(o => o.EUCApprovalStatus == EUCApprovedStatus.Requiring_Approval
                && o.AgencyForwardedTo == 45).Count();
            }
            //Pie Chart Value Count for Signup User Request Status
            var pendingValue = _context.Users.Where(i => i.ApprovalStatus == ApprovedStatus.Pending).Count();
            var approvedValue = _context.Users.Where(i => i.ApprovalStatus == ApprovedStatus.Approved).Count();
            var rejectValue = _context.Users.Where(i => i.ApprovalStatus == ApprovedStatus.Rejected).Count();
            //Pie Chart Value Count for EUC Application Status
            var pendingEUCCount = _context.EndUserCertificate.Where(i => i.EUCApprovalStatus == EUCApprovedStatus.Pending).Count();
            var approvedEUCCount = _context.EndUserCertificate.Where(i => i.EUCApprovalStatus == EUCApprovedStatus.Approved || i.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued).Count();
            var rejectedEUCCount = _context.EndUserCertificate.Where(i => i.EUCApprovalStatus == EUCApprovedStatus.Rejected).Count();
            var endorsedEUCCount = _context.EndUserCertificate.Where(i => i.EUCApprovalStatus == EUCApprovedStatus.Endorsed).Count();
            var disendorsedEUCCount = _context.EndUserCertificate.Where(i => i.EUCApprovalStatus == EUCApprovedStatus.Disendorsed).Count();
            var forwardedEUCCount = _context.EndUserCertificate.Where(i => i.EUCApprovalStatus == EUCApprovedStatus.Forwarded).Count();
            //Pie Chart for Agency User
            var agencyPendingEUCCount = _context.EUCWorkFlow.Where(i => i.AgencyWorkedOn == user.PortalUserCategoryId && i.isWorkedOn == 0).Count();
            //var agencyPendingEUCCount = 0;
            var agencyApprovedEUCCount = _context.EUCWorkFlow.Where(i => i.AgencyWorkedOn == user.PortalUserCategoryId && i.isWorkedOn == 1).Count();
            //Bar Chart Value Count for EUC Certificate Status
            var disputeCertCount = _context.EUCCertificates.Where(i => i.CertStatus == CertificateStatus.Disputed).Count();
            var expiredCertCount = _context.EUCCertificates.Where(euc => DateTime.Now > euc.ExpiryDate).OrderByDescending(date => date.ExpiryDate).Count();
            var issuedCertCount = _context.EUCCertificates.Where(i => i.CertStatus == CertificateStatus.Issued).Count();
            //var pendingCertCount = _context.EUCWorkFlow.Where(i => i.handledStatus == "APPROVED" && i.isWorkedOn == 0).Count();
            var pendingCertCount = _context.EUCCertificates.Where(i => i.CertStatus == CertificateStatus.Pending).Count();
            var recalledCertCount = _context.EUCCertificates.Where(i => i.CertStatus == CertificateStatus.Recalled).Count();
            var utilizedCertCount = _context.EUCCertificates.Where(i => i.CertStatus == CertificateStatus.Utilized).Count();
            var allEUC = _context.EUCCertificates.Count();
            var AgencyList = _context.PortalSubCategories.ToList();

            //TempData["Success"] = "Success Message";

            return View(new DashboardViewModel
            {
                user = user,
                role = roles,
                //Return Count for Signup Request and EUC Request
                pendingSignupRequest = pendingUserRqstCount,
                pendingEUCRequest = pendingEUCRqstCount,
                approvedEUCRequest = approvedEUCRqstCount,
                disapprovedEUCRequest = disapprovedEUCRqstCount,
                endorsedEUCRequest = endorsedEUCRqstCount,
                allSignUpRequest = allRequest,
                //Return Pie Chart Data for Sign Up Request
                pendingPie = pendingValue,
                approvedPie = approvedValue,
                rejectPie = rejectValue,
                //Return Bar Chart Value for EUC Application Statu
                forwardedEUC = forwardedEUCCount,
                disendorsedEUC = disendorsedEUCCount,
                //Return Agency Pie Data
                pendingAgencyData = agencyPendingEUCCount,
                approvedAgencyData = agencyApprovedEUCCount,
                //EUC Cert Bar Chart Data
                DisputedCert = disputeCertCount,
                ExpiredCert = expiredCertCount,
                IssuedCert = issuedCertCount,
                PendingCert = pendingCertCount,
                RecalledCert = recalledCertCount,
                UtilizedCert = utilizedCertCount,
                requireApproval = requiringApproval,
                //approvedUserRequest = approvedUserRqstCount,
                //rejectedUserRequest = rejectedUserRqstCount,
                allEUC = allEUC,
                Agency = AgencyList,
                UserId = user.Id
            });
            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User is logged in and was redirected to the dashboard. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Application Number: " + user.ApplicationNumber,
               User.Identity.GetUserId(),
               "redirected to dashboard page.",
               Request.UserHostAddress);
        }

        //Get guideline page
        [Authorize(Roles = "Agency, Client User, ONSA Admin, ONSA Support, NSA, Customs User, Exclusive User, ONSA PGSO")]
        public ActionResult Guideline()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User visited guideline page. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Application Number: " + user.ApplicationNumber,
               User.Identity.GetUserId(),
               "redirected to guideline page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(new DashboardViewModel
            {
                user = user,
                role = roles
            });
        }

        //Get new EUC application page
        [Authorize(Roles = "Client User, Agency, Exclusive User")]
        public ActionResult NewEUCApplication()
        {
            //return RedirectToAction("MainDashboard", "Admin");
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User visited new EUC application page. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Application Number: " + user.ApplicationNumber,
               User.Identity.GetUserId(),
               "redirected to new EUC application page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var itemCategories = _context.EUCItemCategory.ToList();
            var currencies = _context.Currencies.ToList();
            var portsOfLanding = _context.PortsOfLanding.Where(p => p.Id != 102).OrderBy(p => p.Id).ToList();
            var country = _context.CountryList.Where(c => c.id != 270).OrderBy(c => c.CountryName).ToList();
            var itemCountry = _context.CountryList.Where(c => c.id != 270).OrderBy(c => c.CountryName).ToList();

            var hsCodes = _context.HSCodes.ToList();
            var hsCodeList = _context.HSCodes.AsEnumerable().Select(hscodes => new
            {
                hsCodeId = hscodes.Id,
                id = hscodes.itemCatName,
                Description = string.Format("{0}--{1}", hscodes.itemSubCatName, hscodes.Name)
            }).ToList();

            ViewBag.HSCodeId = new SelectList(hsCodeList, "hsCodeId", "Description");

            string applicationNumber = ApplicationNumberGenerator.
                                    ApplicationNumberGenerator.
                                     GenerateEUCApplicationNumber();

            //Check if the application number exists
            var applicationNumberToCheck = _context.EndUserCertificate.Where(euc => euc.applicationNo == applicationNumber).Count();
            //_context.EndUserCertificate.Where(euc => euc.applicationNo == applicationNumber).Count();

            if (applicationNumberToCheck > 0)
            {
                applicationNumber = ApplicationNumberGenerator.
                                    ApplicationNumberGenerator.
                                     GenerateEUCApplicationNumber();

                var uniqueApplicationNumber = _context.EndUserCertificate.Where(euc => euc.applicationNo == applicationNumber).Count();

                while (uniqueApplicationNumber > 0)
                {
                    applicationNumber = ApplicationNumberGenerator.
                                    ApplicationNumberGenerator.
                                     GenerateEUCApplicationNumber();

                    if (_context.EndUserCertificate.Where(euc => euc.applicationNo == applicationNumber).Count() == 0)
                    {
                        uniqueApplicationNumber += 1;
                    }
                }
            }

            List<EUCItem> items = new List<EUCItem>();

            return View(new NewEUCApplicationViewModel
            {
                user = user,
                role = roles,
                itemCategory = itemCategories,
                Currencies = currencies,
                PortsOfLanding = portsOfLanding,
                HSCodes = hsCodes,
                Items = items,
                ApplicationNumber = applicationNumber,
                Country = country,
                ItemCountry = itemCountry
            });
        }

        // POST: NewEUCApplication/Create
        //[HttpPost]
        public ActionResult EUCApplicationStepOne(string applicationNumber, int itemCategory, int originCountry, string purposeOfUsage, int currencyOfPurchase, int portOfLanding)
        {
            //var userId = User.Identity.GetUserId();

            //var user = _context.Users.Find(userId);

            ////Cast integer to enum to get country
            //var country = (Country)originCountry;

            ////Check if the application number exists
            //var applicationToCheck = _context.EndUserCertificate.Where(certificate => certificate.applicationNo == applicationNumber).FirstOrDefault();
            //if (applicationToCheck == null)
            //{
            //    var EUCCertificate = new EndUserCertificate
            //    {
            //        applicationNo = applicationNumber,
            //        itemCat = itemCategory,
            //        originCountry = country,
            //        purposeOfUsage = purposeOfUsage,
            //        currencyOfPurchase = currencyOfPurchase,
            //        portOfLanding = portOfLanding,
            //        dateCreated = DateTime.Now,
            //        UserID = userId,
            //        EUCApprovalStatus = EUCApprovedStatus.Pending,
            //        certificateStatus = CertificateStatus.Pending,
            //        AgencyId = user.PortalUserCategoryId,
            //        dateForwarded = DateTime.Parse("1900-01-01 00:00:00.000")
            //    };

            //    _context.EndUserCertificate.Add(EUCCertificate);

            //    int queryResult = _context.SaveChanges();

            //    //Document the entry in the audit trail
            //    if (queryResult > 0)
            //    {
            //        _auditTrail.CreateAuditTrail("User created new EUC application. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Application Number: " + user.ApplicationNumber,
            //       User.Identity.GetUserId(),
            //       "created new EUC application.",
            //       Request.UserHostAddress);
            //    }

            //    return new HttpStatusCodeResult(HttpStatusCode.OK);
            //}
            ////If the application number exists, update the certificate details instead of saving.
            //else
            //{
            //    var applicationId = applicationToCheck.id;
            //    var applicationToUpdate = _context.EndUserCertificate.Find(applicationId);

            //    applicationToUpdate.itemCat = itemCategory;
            //    applicationToUpdate.originCountry = country;
            //    applicationToUpdate.purposeOfUsage = purposeOfUsage;
            //    applicationToUpdate.currencyOfPurchase = currencyOfPurchase;
            //    applicationToUpdate.portOfLanding = portOfLanding;

            //    int queryResult = _context.SaveChanges();
            //    if (queryResult > 0)
            //    {
            //        //Document the entry in the audit trail
            //        _auditTrail.CreateAuditTrail("User updated existing EUC application. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Application Number: " + user.ApplicationNumber,
            //           User.Identity.GetUserId(),
            //           "updated existing EUC application.",
            //           Request.UserHostAddress);
            //    }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
            //}
        }

        //public ActionResult CreateNewEUCItem(string applicationnumber, string weightunit, string hscodename, int hscode, int itemCountry, int quantity, string itemdescription, int weight, double cost, string currency)
        //{

        //    //    //Get ID of the most recent EUC Item ID for past back to the view
        //    //    var mostRecentEUCItem = _context.EndUserCertificateDetails.OrderByDescending(eucDetail => eucDetail.id).FirstOrDefault();
        //    //    int eucItemId = mostRecentEUCItem.id;
        //    //    var itemNumber = ApplicationNumberGenerator.ApplicationNumberGenerator.GenerateItemNumber();

        //    //    var eucItem = new EUCItem();

        //    //    eucItem.ItemNumber = itemNumber;
        //    //    eucItem.ItemId = eucItemId;
        //    //    eucItem.WeightUnit = weightunit;
        //    //    eucItem.ApplicationNumber = applicationnumber;
        //    //    eucItem.HsCodeName = hscodename;
        //    //    eucItem.HSCode = hscode;
        //    //    eucItem.Quantity = quantity;
        //    //    eucItem.ItemDescription = itemdescription;
        //    //    eucItem.Weight = weight;
        //    //    eucItem.Cost = cost;
        //    //    eucItem.Currency = currency;

        //    //    return PartialView("~/Views/NewEUCApplication/_newRowPartial.cshtml", eucItem);
        //    //}
        //}
        [HttpPost]
        //Insert Application details to EUC Details Table
        public JsonResult InsertValue(NewEUCApplicationViewModel[] itemlist,
                                    string applicationNumber,
                                    string ItemCat,
                                    string OriginCountry,
                                    string PurposeOfUsage,
                                    string CurrencyOfPurchase,
                                    string PortOfLanding)
        {
            //Create the EUC Certifcate application in the DB
            var userId = User.Identity.GetUserId();

            var user = _context.Users.Find(userId);

            //Cast integer to enum to get country
            var country = (Country)Int32.Parse(OriginCountry);

            if (PortOfLanding == null)
                PortOfLanding = "102";

            //Check if the application number exists
            var applicationToCheck = _context.EndUserCertificate.Where(certificate => certificate.applicationNo == applicationNumber).FirstOrDefault();
            if (applicationToCheck == null && itemlist != null)
            {
                var usagepurpose = "";
                if (PurposeOfUsage.Length > 100)
                {
                    usagepurpose = PurposeOfUsage.Substring(0, 100);
                }
                else
                {
                    usagepurpose = PurposeOfUsage;
                }
                var EUCCertificate = new EndUserCertificate
                {
                    applicationNo = applicationNumber,
                    itemCat = Int32.Parse(ItemCat),
                    originCountry = country,
                    purposeOfUsage = usagepurpose,
                    currencyOfPurchase = Int32.Parse(CurrencyOfPurchase),
                    portOfLanding = Int32.Parse(PortOfLanding),
                    dateCreated = DateTime.Now,
                    UserID = userId,
                    EUCApprovalStatus = EUCApprovedStatus.Pending,
                    certificateStatus = CertificateStatus.Pending,
                    AgencyId = user.PortalUserCategoryId,
                    dateForwarded = DateTime.Now
                };

                _context.EndUserCertificate.Add(EUCCertificate);

                int queryResult = _context.SaveChanges();

                //Document the entry in the audit trail
                if (queryResult > 0)
                {
                    _auditTrail.CreateAuditTrail("User created new EUC application. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Application Number: " + user.ApplicationNumber,
                   User.Identity.GetUserId(),
                   "created new EUC application.",
                   Request.UserHostAddress);

                    //Enter the EUC Details only after the EUC application has been made successfully
                    //var parentEndUserCertificate = _context.EndUserCertificate.OrderByDescending(euc => euc.id).FirstOrDefault();
                    var parentEndUserCertificate = _context.EndUserCertificate.Where(euc => euc.applicationNo == applicationNumber).FirstOrDefault();

                    int endUserCertificateId = parentEndUserCertificate.id;

                    foreach (NewEUCApplicationViewModel i in itemlist)
                    {
                        if (i.Quantity != 0 && i.Weight != 0 && i.Cost != 0 && i.ItemDescription != null && i.itemCountryId != 0)
                        {
                            //loop through the array and insert value into database.
                            var Details = new EndUserCertificateDetails
                            {
                                applicationNo = applicationNumber,
                                HSCodeId = i.HSCode,
                                itemDescription = i.ItemDescription,
                                ItemCountryId = i.itemCountryId,
                                qty = i.Quantity,
                                unit = i.Weight,
                                cost = i.Cost,
                                EndUserCertificateId = endUserCertificateId
                            };

                            _context.EndUserCertificateDetails.Add(Details);
                            queryResult = _context.SaveChanges();

                            if (queryResult > 0)
                            {
                                //Document the entry in the audit trail
                                _auditTrail.CreateAuditTrail("User added new EUC item to application:" + i.ApplicationNumber + " . Email: " + User.Identity.GetUserName() + " Weight/Unit: " + i.Weight + " HS Code" + i.HSCode + " Quantity: " + i.Quantity,
                                   User.Identity.GetUserId(),
                                   "Added EUC item to EUC application.",
                                   Request.UserHostAddress);
                            }
                        }
                    }
                }

                //return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            //If the application number exists, update the certificate details instead of saving.
            else
            {
                var applicationId = applicationToCheck.id;
                var applicationToUpdate = _context.EndUserCertificate.Find(applicationId);

                applicationToUpdate.itemCat = Int32.Parse(ItemCat);
                applicationToUpdate.originCountry = country;
                applicationToUpdate.purposeOfUsage = PurposeOfUsage;
                applicationToUpdate.currencyOfPurchase = Int32.Parse(CurrencyOfPurchase);
                applicationToUpdate.portOfLanding = Int32.Parse(PortOfLanding);

                int queryResult = _context.SaveChanges();
                if (queryResult > 0)
                {
                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("User updated existing EUC application. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Application Number: " + user.ApplicationNumber,
                       User.Identity.GetUserId(),
                       "updated existing EUC application.",
                       Request.UserHostAddress);

                    //Enter the EUC Details only after the EUC application has been made successfully
                    //var parentEndUserCertificate = _context.EndUserCertificate.OrderByDescending(euc => euc.id).FirstOrDefault();
                    var parentEndUserCertificate = _context.EndUserCertificate.Where(euc => euc.applicationNo == applicationNumber).FirstOrDefault();

                    int endUserCertificateId = parentEndUserCertificate.id;

                    foreach (NewEUCApplicationViewModel i in itemlist)
                    {
                        if (i.Quantity != 0 && i.Weight != 0 && i.Cost != 0 && i.ItemDescription != null && i.itemCountryId != 0)
                        {
                            //loop through the array and insert value into database.
                            var Details = new EndUserCertificateDetails
                            {
                                applicationNo = applicationNumber,
                                HSCodeId = i.HSCode,
                                itemDescription = i.ItemDescription,
                                ItemCountryId = i.itemCountryId,
                                qty = i.Quantity,
                                unit = i.Weight,
                                cost = i.Cost,
                                EndUserCertificateId = endUserCertificateId
                            };

                            _context.EndUserCertificateDetails.Add(Details);
                            queryResult = _context.SaveChanges();

                            if (queryResult > 0)
                            {
                                //Document the entry in the audit trail
                                _auditTrail.CreateAuditTrail("User added new EUC item to application:" + i.ApplicationNumber + " . Email: " + User.Identity.GetUserName() + " Weight/Unit: " + i.Weight + " HS Code" + i.HSCode + " Quantity: " + i.Quantity,
                                   User.Identity.GetUserId(),
                                   "Added EUC item to EUC application.",
                                   Request.UserHostAddress);
                            }
                        }
                    }
                }
                //return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            return Json("Ok");
        }

        //Post Selected EUC Application Approval Status
        [HttpPost]
        public ActionResult SaveSelEUCApp(int[] approvedItems)
        {
            return View();
        }

        [Authorize(Roles = "Agency, Client User, ONSA Admin, ONSA Support, NSA, Exclusive User, ONSA PGSO")]
        public ActionResult AllEUCApplications()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();


            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User viewed all EUC applications page. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
               User.Identity.GetUserId(),
               "redirected to all EUC page.",
               Request.UserHostAddress);


            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;

            List<EndUserCertificate> eucApplications;
            int portalCategory = user.PortalUserCategoryId;
            switch (portalCategory)
            {
                //ONSA Support Users
                case 5:
                    {
                        eucApplications = _context.EndUserCertificate.Distinct().Where(euc => euc.UserID != null).OrderByDescending(date => date.dateCreated).ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        return View(new allEUCApplicationsViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }

                //Agency Users
                case 1:
                case 2:
                case 3:
                case 4:
                case 6:
                case 7:
                case 8:
                case 11:
                case 12:
                case 10:
                case 13:
                case 14:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 25:
                case 26:
                case 27:
                case 33:
                case 34:
                case 36:
                    {
                        eucApplications = _context.EndUserCertificate.Distinct().Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.UserID != null).OrderByDescending(date => date.dateCreated).ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        return View(new allEUCApplicationsViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
                //Exclusive Users
                case 29:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        return View(new allEUCApplicationsViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
                //Client Users
                case 28:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        return View(new allEUCApplicationsViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users,
                            Agencies = _context.PortalSubCategories.ToList()
                        });
                    }
                default:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        return View(new allEUCApplicationsViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
            }
        }

        [Authorize(Roles = "ONSA Support, NSA, Agency, ONSA PGSO")]
        public ActionResult forwardedEUCApplication()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User viewed all forwarded EUC applications page. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
               User.Identity.GetUserId(),
               "redirected to forwarded EUC page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            List<EndUserCertificate> eucApplications;
            int portalCategory = user.PortalUserCategoryId;
            //Get list of all agencies
            var agencies = _context.PortalSubCategories.ToList();
            switch (portalCategory)
            {
                //ONSA Support Users
                case 5:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        var allAgencies = agencies;
                        return View(new forwardedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users,
                            Agencies = allAgencies
                        });
                    }
                //Agency Users
                case 1:
                case 2:
                case 3:
                case 4:
                case 6:
                case 7:
                case 8:
                case 11:
                case 12:
                case 10:
                case 13:
                case 14:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 25:
                case 26:
                case 27:
                case 33:
                case 34:
                case 36:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyForwardedTo == user.PortalUserCategoryId && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        var allAgencies = agencies;
                        return View(new forwardedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users,
                            Agencies = allAgencies
                        });
                    }
                //Exclusive Users
                case 29:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        return View(new forwardedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
                //Client Users
                case 28:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        return View(new forwardedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
                default:
                    {
                        eucApplications = _context.EndUserCertificate.Where(a => a.EUCApprovalStatus == EUCApprovedStatus.Forwarded && a.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        var allAgencies = agencies;
                        return View(new forwardedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users,
                            Agencies = allAgencies
                        });
                    }
            }
        }

        [Authorize(Roles = "NSA")]
        public ActionResult AwaitingApproval()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("NSA visited awaiting approval page. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
               User.Identity.GetUserId(),
               "redirected NSA to awaiting approval EUC page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            List<EndUserCertificate> eucApplications;
            eucApplications = _context.EndUserCertificate.Where(a => a.EUCApprovalStatus == EUCApprovedStatus.Requiring_Approval && a.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
            var eucCategories = _context.EUCItemCategory.ToList();
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.ToList();
            return View(new AwaitingApprovalViewModel
            {
                user = user,
                role = roles,
                EndUserCertificate = eucApplications,
                EndUserCategories = eucCategories,
                UsersToQuery = users
            });
        }

        [Authorize(Roles = "Agency, Client User, ONSA Admin, ONSA Support, NSA, Exclusive User, ONSA PGSO")]
        public ActionResult pendingEUCApplication()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User visited awaiting pending EUC application page. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
               User.Identity.GetUserId(),
               "redirected user to pending EUC page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            List<EndUserCertificate> eucApplications;
            int portalCategory = user.PortalUserCategoryId;
            switch (portalCategory)
            {
                //ONSA Support Users
                case 5:
                    {
                        //eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending && euc.UserID != null).OrderByDescending(date => date.dateCreated).ToList();
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending && euc.UserID != null).OrderByDescending(date => date.dateCreated).ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        return View(new pendingEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
                //Agency Users
                case 1:
                case 2:
                case 3:
                case 4:
                case 6:
                case 7:
                case 8:
                case 11:
                case 12:
                case 10:
                case 13:
                case 14:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 25:
                case 26:
                case 27:
                case 33:
                case 34:
                case 36:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending && euc.AgencyId == user.PortalUserCategoryId && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.Where(applicant => applicant.IsBlackListed == false).ToList();
                        return View(new pendingEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
                //Exclusive Users
                case 29:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending && euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.Where(applicant => applicant.IsBlackListed == false).ToList();
                        return View(new pendingEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
                //Client Users
                case 28:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending && euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.Where(applicant => applicant.IsBlackListed == false).ToList();
                        return View(new pendingEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
                default:
                    {
                        eucApplications = _context.EndUserCertificate.Where(a => a.EUCApprovalStatus == EUCApprovedStatus.Pending && a.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.Where(applicant => applicant.IsBlackListed == false).ToList();
                        return View(new forwardedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
            }
        }
        [Authorize(Roles = "Agency, ONSA Admin, ONSA Support, NSA, ONSA PGSO")]
        public ActionResult applicantDetails(string id)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var eucApplications = _context.EndUserCertificate.Where(euc => euc.UserID == id).OrderByDescending(euc => euc.dateCreated).ToList();
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var eucCategories = _context.EUCItemCategory.ToList();
            var users = _context.Users.ToList();
            var meansOfId = _context.Identification.ToList();
            var countries = _context.CountryList.ToList();

            if (id == null)
            {
                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("User attempted to view applicant details with invalid applicant credentials page. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
                   User.Identity.GetUserId(),
                   "wrong credentials used to view applicant details.",
                   Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var applicant = _context.Users.Include(u => u.StateOfOrigin).Where(u => u.Id == id).FirstOrDefault();

            if (applicant == null)
            {
                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("User attempted to view applicant details but applicant does not exist. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
                   User.Identity.GetUserId(),
                   "Requested applicant does not exist.",
                   Request.UserHostAddress);

                return HttpNotFound();
            }
            var applicantuser = _context.Users.Where(u => u.Id == id).SingleOrDefault();
            if (applicantuser != null)
            {
                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("User viewed applicant details page. Email: " + applicantuser.Email + " Name: " + applicantuser.FirstName + " " + applicantuser.LastName + " Applicant Number: " + applicantuser.ApplicationNumber,
                   User.Identity.GetUserId(),
                   "Viewed applicant details page.",
                   Request.UserHostAddress);
            }
            //If the user has a corporate account, find his company and send to the view
            Company company = new Company();
            if (applicant.CategoryId == 2)
                company = _context.Company.Include(comp => comp.State).Where(comp => comp.OwnerId == applicant.Id).FirstOrDefault();

            //Check if the user has a company in the DB
            var doesCompanyExist = _context.Company.Where(userCompany => userCompany.OwnerId == applicant.Id).ToList().Count;
            //var viewModel = ViewMode
            return View(new applicantDetailsViewModel
            {
                user = user,
                role = roles,
                Applicant = applicant,
                Company = company,
                EndUserCertificate = eucApplications,
                EndUserCategories = eucCategories,
                UsersToQuery = users,
                MeansOfIdentification = meansOfId,
                Countries = countries,
                DoesCompanyExist = doesCompanyExist
            });
        }
        // POST: Student/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        [Authorize(Roles = "ONSA Admin, ONSA Support, NSA, ONSA PGSO")]
        public async Task<ActionResult> ApproveApplicant(string id, string reason)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            if (id == null)
            {
                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("User attempted to view approve an applicant without an ID. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
                   User.Identity.GetUserId(),
                   "Viewed applicant details page.",
                   Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var UserRoleManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_context));

            var applicantToUpdate = _context.Users.Find(id);

            applicantToUpdate.ApprovalStatus = ApprovedStatus.Approved;

            //Put agency definition here
            int portalCategory = applicantToUpdate.PortalUserCategoryId;
            switch (portalCategory)
            {
                //Agencies
                case 1:
                case 2:
                case 3:
                case 4:
                case 6:
                case 10:
                case 13:
                case 14:
                case 17:
                case 19:
                case 20:
                case 21:
                case 25:
                case 26:
                case 27:
                case 33:
                case 34:
                case 36:
                    {
                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("Added applicant to role: Agency User. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
                           User.Identity.GetUserId(),
                           "Added applicant to Information User role.",
                           Request.UserHostAddress);

                        UserRoleManager.AddToRole(applicantToUpdate.Id, "Agency");

                        var result = _context.SaveChanges();


                        //Send user email informing them their account has been approved
                        IdentityMessage EmailMessage = new IdentityMessage();

                        EmailMessage.Body = "Dear " + applicantToUpdate.FirstName + " " + applicantToUpdate.LastName + ". "
                                            + "<br/>"
                                            + "Your account has been approved successfully on the ONSA"
                                            + " End-User Certificate"
                                            + " portal."
                                            + "<br/>"
                                            + " You can now login to apply for an EUC certificate.";

                        EmailMessage.Destination = applicantToUpdate.Email;
                        EmailMessage.Subject = "Account Approved";
                        //Send email
                        EmailService emailService = new EmailService();
                        await emailService.SendAsync(EmailMessage);

                        //Send user verification SMS
                        IdentityMessage SmsMessage = new IdentityMessage();
                        //send accompanying SMS reminding user to verify their email
                        string smsBody = "Your account has been approved successfully on the EUC"
                                            + " portal."
                                            + " You can now login to apply.";

                        SmsMessage.Body = smsBody;
                        SmsMessage.Destination = applicantToUpdate.PhoneNumber;
                        SmsMessage.Subject = "Account Approved";
                        //Send SMS
                        SmsService smsSerice = new SmsService();
                        await smsSerice.SendAsync(SmsMessage);

                        break;
                    }
                case 7:
                case 8:
                case 11:
                case 12:
                case 18:
                    {
                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("Added applicant to role: Agency. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
                           User.Identity.GetUserId(),
                           "Added applicant to Agency role.",
                           Request.UserHostAddress);

                        UserRoleManager.AddToRole(applicantToUpdate.Id, "Agency");

                        var result = _context.SaveChanges();

                        //Send user email informing them their account has been approved
                        IdentityMessage EmailMessage = new IdentityMessage();

                        EmailMessage.Body = "Dear " + applicantToUpdate.FirstName + " " + applicantToUpdate.LastName + ". "
                                            + "<br/>"
                                            + "Your account has been approved successfully on the ONSA"
                                            + " End-User Certificate"
                                            + " portal."
                                            + "<br/>"
                                            + " You can now login to apply for an EUC certificate.";

                        EmailMessage.Destination = applicantToUpdate.Email;
                        EmailMessage.Subject = "Account Approved";
                        //Send email
                        EmailService emailService = new EmailService();
                        await emailService.SendAsync(EmailMessage);

                        //Send user verification SMS
                        IdentityMessage SmsMessage = new IdentityMessage();
                        //send accompanying SMS reminding user to verify their email
                        string smsBody = "Your account has been approved successfully on the"
                                            + " EUC portal."
                                            + " You can now login to apply.";

                        SmsMessage.Body = smsBody;
                        SmsMessage.Destination = applicantToUpdate.PhoneNumber;
                        SmsMessage.Subject = "Account Approved";
                        //Send SMS
                        SmsService smsSerice = new SmsService();
                        await smsSerice.SendAsync(SmsMessage);
                    }
                    break;

                //ONSA Support
                case 5:
                    {
                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("Added applicant to role: ONSA SUPPORT. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
                           User.Identity.GetUserId(),
                           "Added applicant to ONSA support role.",
                           Request.UserHostAddress);

                        UserRoleManager.AddToRole(applicantToUpdate.Id, "ONSA Support");

                        var result = _context.SaveChanges();

                        //Send user email informing them their account has been approved
                        IdentityMessage EmailMessage = new IdentityMessage();

                        EmailMessage.Body = "Dear " + applicantToUpdate.FirstName + " " + applicantToUpdate.LastName + ". "
                                            + "<br/>"
                                            + "Your account has been approved successfully on the ONSA"
                                            + " End-User Certificate"
                                            + " portal."
                                            + "<br/>"
                                            + " You can now login to apply for an EUC certificate.";

                        EmailMessage.Destination = applicantToUpdate.Email;
                        EmailMessage.Subject = "Account Approved";
                        //Send email
                        EmailService emailService = new EmailService();
                        await emailService.SendAsync(EmailMessage);

                        //Send user verification SMS
                        IdentityMessage SmsMessage = new IdentityMessage();
                        //send accompanying SMS reminding user to verify their email
                        string smsBody = "Your account has been approved on the"
                                            + " EUC portal."
                                            + " You can now login to apply.";

                        SmsMessage.Body = smsBody;
                        SmsMessage.Destination = applicantToUpdate.PhoneNumber;
                        SmsMessage.Subject = "Account Approved";
                        //Send SMS
                        SmsService smsSerice = new SmsService();
                        await smsSerice.SendAsync(SmsMessage);

                        break;
                    }
                case 16:
                    {
                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("Added applicant to role: Accreditor User. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
                           User.Identity.GetUserId(),
                           "Added applicant to NSA User role.",
                           Request.UserHostAddress);

                        UserRoleManager.AddToRole(applicantToUpdate.Id, "Agency");
                        var result = _context.SaveChanges();

                        //Send user email informing them their account has been approved
                        IdentityMessage EmailMessage = new IdentityMessage();

                        EmailMessage.Body = "Dear " + applicantToUpdate.FirstName + " " + applicantToUpdate.LastName + ". "
                                            + "<br/>"
                                            + "Your account has been approved successfully on the ONSA"
                                            + " End-User Certificate"
                                            + " portal."
                                            + "<br/>"
                                            + " You can now login to apply for an EUC certificate.";

                        EmailMessage.Destination = applicantToUpdate.Email;
                        EmailMessage.Subject = "Account Approved";
                        //Send email
                        EmailService emailService = new EmailService();
                        await emailService.SendAsync(EmailMessage);

                        //Send user verification SMS
                        IdentityMessage SmsMessage = new IdentityMessage();
                        //send accompanying SMS reminding user to verify their email
                        string smsBody = "Your account has been approved successfully on the"
                                            + " EUC portal."
                                            + " You can now login to apply.";

                        SmsMessage.Body = smsBody;
                        SmsMessage.Destination = applicantToUpdate.PhoneNumber;
                        SmsMessage.Subject = "Account Approved";
                        //Send SMS
                        SmsService smsSerice = new SmsService();
                        await smsSerice.SendAsync(SmsMessage);

                        break;
                    }
                case 22:
                    {
                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("Added applicant to role: Information User. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
                           User.Identity.GetUserId(),
                           "Added applicant to CLIENT USER role.",
                           Request.UserHostAddress);

                        UserRoleManager.AddToRole(applicantToUpdate.Id, "Agency");
                        var result = _context.SaveChanges();

                        //Send user email informing them their account has been approved
                        IdentityMessage EmailMessage = new IdentityMessage();

                        EmailMessage.Body = "Dear " + applicantToUpdate.FirstName + " " + applicantToUpdate.LastName + ". "
                                            + "<br/>"
                                            + "Your account has been approved successfully on the ONSA"
                                            + " End-User Certificate"
                                            + " portal."
                                            + "<br/>"
                                            + " You can now login to apply for an EUC certificate.";

                        EmailMessage.Destination = applicantToUpdate.Email;
                        EmailMessage.Subject = "Account Approved";
                        //Send email
                        EmailService emailService = new EmailService();
                        await emailService.SendAsync(EmailMessage);

                        //Send user verification SMS
                        IdentityMessage SmsMessage = new IdentityMessage();
                        //send accompanying SMS reminding user to verify their email
                        string smsBody = "Your account has been approved successfully on the"
                                            + " EUC portal."
                                            + " You can now login to apply.";

                        SmsMessage.Body = smsBody;
                        SmsMessage.Destination = applicantToUpdate.PhoneNumber;
                        SmsMessage.Subject = "Account Approved";
                        //Send SMS
                        SmsService smsSerice = new SmsService();
                        await smsSerice.SendAsync(SmsMessage);

                        break;
                    }
                case 24:
                    {
                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("Added applicant to role: ONSA Support. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
                           User.Identity.GetUserId(),
                           "Added applicant to CLIENT USER role.",
                           Request.UserHostAddress);

                        UserRoleManager.AddToRole(applicantToUpdate.Id, "ONSA User");
                        var result = _context.SaveChanges();

                        //Send user email informing them their account has been approved
                        IdentityMessage EmailMessage = new IdentityMessage();

                        EmailMessage.Body = "Dear " + applicantToUpdate.FirstName + " " + applicantToUpdate.LastName + ". "
                                            + "<br/>"
                                            + "Your account has been approved successfully on the ONSA"
                                            + " End-User Certificate"
                                            + " portal."
                                            + "<br/>"
                                            + " You can now login to apply for an EUC certificate.";

                        EmailMessage.Destination = applicantToUpdate.Email;
                        EmailMessage.Subject = "Account Approved";
                        //Send email
                        EmailService emailService = new EmailService();
                        await emailService.SendAsync(EmailMessage);

                        //Send user verification SMS
                        IdentityMessage SmsMessage = new IdentityMessage();
                        //send accompanying SMS reminding user to verify their email
                        string smsBody = "Your account has been approved successfully on the"
                                            + " EUC portal."
                                            + " You can now login to apply.";

                        SmsMessage.Body = smsBody;
                        SmsMessage.Destination = applicantToUpdate.PhoneNumber;
                        SmsMessage.Subject = "Account Approved";
                        //Send SMS
                        SmsService smsSerice = new SmsService();
                        await smsSerice.SendAsync(SmsMessage);

                        break;
                    }
                //Client Users
                case 28:
                    {
                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("Added applicant to role: CLIENT USER. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
                           User.Identity.GetUserId(),
                           "Added applicant to CLIENT USER role.",
                           Request.UserHostAddress);

                        UserRoleManager.AddToRole(applicantToUpdate.Id, "Client User");
                        var result = _context.SaveChanges();

                        //Send user email informing them their account has been approved
                        IdentityMessage EmailMessage = new IdentityMessage();

                        EmailMessage.Body = "Dear " + applicantToUpdate.FirstName + " " + applicantToUpdate.LastName + ". "
                                            + "<br/>"
                                            + "Your account has been approved successfully on the ONSA"
                                            + " End-User Certificate"
                                            + " portal."
                                            + "<br/>"
                                            + " You can now login to apply for an EUC certificate.";

                        EmailMessage.Destination = applicantToUpdate.Email;
                        EmailMessage.Subject = "Account Approved";
                        //Send email
                        EmailService emailService = new EmailService();
                        await emailService.SendAsync(EmailMessage);

                        //Send user verification SMS
                        IdentityMessage SmsMessage = new IdentityMessage();
                        //send accompanying SMS reminding user to verify their email
                        string smsBody = "Your account has been approved successfully on the"
                                            + " EUC portal."
                                            + " You can now login to apply.";

                        SmsMessage.Body = smsBody;
                        SmsMessage.Destination = applicantToUpdate.PhoneNumber;
                        SmsMessage.Subject = "Account Approved";
                        //Send SMS
                        SmsService smsSerice = new SmsService();
                        await smsSerice.SendAsync(SmsMessage);

                        break;
                    }
                //Exclusive Users
                case 29:
                    {
                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("Added applicant to role: Exclusive User. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
                           User.Identity.GetUserId(),
                           "Added applicant to EXCLUSIVE USER role.",
                           Request.UserHostAddress);

                        UserRoleManager.AddToRole(applicantToUpdate.Id, "Exclusive User");
                        var result = _context.SaveChanges();

                        //Send user email informing them their account has been approved
                        IdentityMessage EmailMessage = new IdentityMessage();

                        EmailMessage.Body = "Dear " + applicantToUpdate.FirstName + " " + applicantToUpdate.LastName + ". "
                                            + "<br/>"
                                            + "Your account has been approved successfully on the ONSA"
                                            + " End-User Certificate"
                                            + " portal."
                                            + "<br/>"
                                            + " You can now login to apply for an EUC certificate.";

                        EmailMessage.Destination = applicantToUpdate.Email;
                        EmailMessage.Subject = "Account Approved";
                        //Send email
                        EmailService emailService = new EmailService();
                        await emailService.SendAsync(EmailMessage);

                        //Send user verification SMS
                        IdentityMessage SmsMessage = new IdentityMessage();
                        //send accompanying SMS reminding user to verify their email
                        string smsBody = "Your account has been approved successfully on the"
                                            + " EUC portal."
                                            + " You can now login to apply.";

                        SmsMessage.Body = smsBody;
                        SmsMessage.Destination = applicantToUpdate.PhoneNumber;
                        SmsMessage.Subject = "Account Approved";
                        //Send SMS
                        SmsService smsSerice = new SmsService();
                        await smsSerice.SendAsync(SmsMessage);

                        break;
                    }
                default:
                    {
                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("Added applicant to role: CLIENT USER. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
                           User.Identity.GetUserId(),
                           "Added applicant to CLIENT USER role.",
                           Request.UserHostAddress);

                        UserRoleManager.AddToRole(applicantToUpdate.Id, "Client User");
                        _context.SaveChanges();
                        break;
                    }

            }
            //var addUserToClient = UserRoleManager.AddToRole(applicantToUpdate.Id, "Client User");

            _context.SaveChanges();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Sent application approved email to: " + applicantToUpdate.UserName + " Name: " + applicantToUpdate.FirstName + " " + applicantToUpdate.LastName + " Applicant Number: " + applicantToUpdate.ApplicationNumber,
               User.Identity.GetUserId(),
               "Sent application approved email.",
               Request.UserHostAddress);

            return RedirectToAction("AllPendingSignUpRequests");
        }

        [Authorize(Roles = "ONSA Admin, ONSA Support, NSA")]
        public async Task<ActionResult> RejectApplicant(string id, string reason)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            if (id == null)
            {

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("User attempted to reject an applicant without an ID. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Applicant Number: " + user.ApplicationNumber,
                   User.Identity.GetUserId(),
                   "Attempted to reject an applicant but failed.",
                   Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var UserRoleManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_context));

            //Reject the applicant
            var applicantToUpdate = _context.Users.Find(id);
            var userIdentification = applicantToUpdate.Id;

            //Send email and sms with reject reason before deleting the user
            IdentityMessage EmailMessage = new IdentityMessage();

            EmailMessage.Body = "Dear " + applicantToUpdate.FirstName + " " + applicantToUpdate.LastName + ". "
                                + "<br/>"
                                + "Your account on the ONSA End-User Certificate portal has been rejected.<br>"
                                + " Reason for rejection: " + reason;

            EmailMessage.Destination = applicantToUpdate.Email;
            EmailMessage.Subject = "Account Creation Rejected";
            //Send email
            EmailService emailService = new EmailService();
            await emailService.SendAsync(EmailMessage);

            //Send user verification SMS
            IdentityMessage SmsMessage = new IdentityMessage();
            //send accompanying SMS reminding user to verify their email
            string smsBody = "Your account has been rejected."
                                + " Reason for rejection: " + reason;

            SmsMessage.Body = smsBody;
            SmsMessage.Destination = applicantToUpdate.PhoneNumber;
            SmsMessage.Subject = "Account Creation Rejected";
            //Send SMS
            SmsService smsSerice = new SmsService();
            await smsSerice.SendAsync(SmsMessage);

            var companiesToDelete = _context.Company.Where(company => company.OwnerId == applicantToUpdate.Id).ToList();
            //Delete every company associated with the user
            if (companiesToDelete != null)
            {
                foreach (var company in companiesToDelete)
                {
                    _context.Company.Remove(company);
                }
            }

            //Remove the user from the role specified
            var userRole = await UserManager.RemoveFromRoleAsync(userIdentification, "Client User");

            //Finally, delete the user
            _context.Users.Remove(applicantToUpdate);

            _context.SaveChanges();

            //Code to set the user details as rejected i.e. the same as blacklisting
            #region Reject user instead of deleting
            //applicantToUpdate.ApprovalStatus = ApprovedStatus.Rejected;

            //applicantToUpdate.ApplicationRejectReason = reason;

            //int result = _context.SaveChanges();

            //if (result > 0)
            //{
            //    //Document the entry in the audit trail
            //    _auditTrail.CreateAuditTrail("User application rejected. Rejected applicant number: " + applicantToUpdate.ApplicationNumber + " Name: " + applicantToUpdate.FirstName + " " + applicantToUpdate.LastName + " Reason: " + reason,
            //       User.Identity.GetUserId(),
            //       "Attempted to reject an applicant but failed.",
            //       Request.UserHostAddress);

            //    //Send user email informing them their account has been approved
            //    IdentityMessage EmailMessage = new IdentityMessage();

            //    EmailMessage.Body = "Dear " + applicantToUpdate.FirstName + " " + applicantToUpdate.LastName + ". "
            //                        + "<br/>"
            //                        + "Your account on the ONSA End-User Certificate portal has been rejected.<br>"
            //                        + " Reason for rejection: " + reason;

            //    EmailMessage.Destination = applicantToUpdate.Email;
            //    EmailMessage.Subject = "Application Rejected";
            //    //Send email
            //    EmailService emailService = new EmailService();
            //    await emailService.SendAsync(EmailMessage);

            //    //Send user verification SMS
            //    IdentityMessage SmsMessage = new IdentityMessage();
            //    //send accompanying SMS reminding user to verify their email
            //    string smsBody = "Your account has been rejected."
            //                        + " Reason for rejection: " + reason;

            //    SmsMessage.Body = smsBody;
            //    SmsMessage.Destination = applicantToUpdate.PhoneNumber;
            //    SmsMessage.Subject = "Application Rejected";
            //    //Send SMS
            //    SmsService smsSerice = new SmsService();
            //    await smsSerice.SendAsync(SmsMessage);
            //}

            ////Document the entry in the audit trail
            //_auditTrail.CreateAuditTrail("Send application rejected email to user. Rejected applicant number: " + applicantToUpdate.ApplicationNumber + " Name: " + applicantToUpdate.FirstName + " " + applicantToUpdate.LastName + " Reason: " + reason,
            //   User.Identity.GetUserId(),
            //   "Sent applicant rejected mail.",
            //   Request.UserHostAddress);

            //TempData["Success"] = "Applicant Rejected Successfully!";
            #endregion

            #region delete the user, company and delete the role

            #endregion

            return RedirectToAction("AllPendingSignUpRequests");
        }

        [Authorize(Roles = "ONSA Admin, ONSA Support, NSA, Client User, Exclusive User, ONSA PGSO")]
        public ActionResult rejectedEUCApplication()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;

            List<EndUserCertificate> eucApplications;
            int portalCategory = user.PortalUserCategoryId;
            switch (portalCategory)
            {
                //ONSA Support Users
                case 5:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed rejected EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited rejected EUC applications page.",
                           Request.UserHostAddress);

                        return View(new rejectedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
                //Agency Users
                case 1:
                case 2:
                case 3:
                case 4:
                case 6:
                case 7:
                case 8:
                case 11:
                case 12:
                case 10:
                case 13:
                case 14:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 25:
                case 26:
                case 27:
                case 33:
                case 34:
                case 36:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected && euc.AgencyId == user.PortalUserCategoryId && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed rejected EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited rejected EUC applications page.",
                           Request.UserHostAddress);

                        return View(new rejectedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
                //Exclusive Users
                case 29:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected && euc.UserID == user.Id && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed rejected EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited rejected EUC applications page.",
                           Request.UserHostAddress);

                        return View(new rejectedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
                //Client Users
                case 28:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected && euc.UserID == user.Id && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed rejected EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited rejected EUC applications page.",
                           Request.UserHostAddress);

                        return View(new rejectedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
                default:
                    {
                        eucApplications = _context.EndUserCertificate.Where(a => a.EUCApprovalStatus == EUCApprovedStatus.Rejected && a.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed rejected EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited rejected EUC applications page.",
                           Request.UserHostAddress);

                        return View(new rejectedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
            }
        }
        [Authorize(Roles = "Client User, ONSA Support, NSA, Exclusive User, ONSA PGSO")]
        public ActionResult approvedEUCApplication()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;

            List<EndUserCertificate> eucApplications;
            int portalCategory = user.PortalUserCategoryId;
            switch (portalCategory)
            {
                //ONSA Support Users
                case 5:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed approved EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited approved EUC applications page.",
                           Request.UserHostAddress);

                        return View(new approvedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
                //Agency Users
                case 1:
                case 2:
                case 3:
                case 4:
                case 6:
                case 7:
                case 8:
                case 11:
                case 12:
                case 10:
                case 13:
                case 14:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 25:
                case 26:
                case 27:
                case 33:
                case 34:
                case 36:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.AgencyId == user.PortalUserCategoryId && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed approved EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited approved EUC applications page.",
                           Request.UserHostAddress);

                        return View(new approvedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
                //Exclusive Users
                case 29:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed approved EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited approved EUC applications page.",
                           Request.UserHostAddress);

                        return View(new approvedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
                //Client Users
                case 28:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed approved EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited approved EUC applications page.",
                           Request.UserHostAddress);

                        return View(new approvedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
                default:
                    {
                        eucApplications = _context.EndUserCertificate.Where(a => a.EUCApprovalStatus == EUCApprovedStatus.Approved && a.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed approved EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited approved EUC applications page.",
                           Request.UserHostAddress);

                        return View(new approvedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users
                        });
                    }
            }
        }
        [Authorize(Roles = "Agency, ONSA Support, NSA, ONSA PGSO")]
        public ActionResult endorsedEUCApplication()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;

            List<EndUserCertificate> eucApplications;
            int portalCategory = user.PortalUserCategoryId;
            switch (portalCategory)
            {
                //ONSA Support Users
                case 5:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        //var endorsedEucApplications = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "ENDORSED").OrderByDescending(date => date.activityDate).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        var userType = _context.Users.Where(u => u.Id == userId).Single();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed endorsed EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited endorsed EUC applications page.",
                           Request.UserHostAddress);

                        return View(new endorsedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users,
                            UserAgency = userType
                        });
                    }
                //Agency Users
                case 1:
                case 2:
                case 3:
                case 4:
                case 6:
                case 7:
                case 8:
                case 11:
                case 12:
                case 10:
                case 13:
                case 14:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 25:
                case 26:
                case 27:
                case 33:
                case 34:
                case 36:
                    {
                        //eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed && euc.AgencyForwardedFrom == user.PortalUserCategoryId && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var endorsedEucApplications = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "ENDORSED" && euc.SourceAgency == user.PortalUserCategoryId).OrderByDescending(date => date.activityDate).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        var userType = _context.Users.Where(u => u.Id == userId).Single();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed endorsed EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited endorsed EUC applications page.",
                           Request.UserHostAddress);

                        return View(new endorsedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EUCWorkflow = endorsedEucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users,
                            UserAgency = userType
                        });
                    }
                default:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        //var endorsedEucApplications = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "ENDORSED").OrderByDescending(date => date.activityDate).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        var userType = _context.Users.Where(u => u.Id == userId).Single();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed endorsed EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited endorsed EUC applications page.",
                           Request.UserHostAddress);

                        return View(new endorsedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users,
                            UserAgency = userType
                        });
                    }
            }
        }
        [Authorize(Roles = "Agency, ONSA Support, ONSA PGSO, NSA")]
        public ActionResult disendorsedEUCApplication()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;

            List<EndUserCertificate> eucApplications;
            int portalCategory = user.PortalUserCategoryId;
            switch (portalCategory)
            {
                //ONSA Support Users
                case 5:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        //var endorsedEucApplications = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "DISENDORSED").OrderByDescending(date => date.activityDate).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        var userType = _context.Users.Where(u => u.Id == userId).Single();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed dis-endorsed EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited dis-endorsed EUC applications page.",
                           Request.UserHostAddress);

                        return View(new disendorsedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users,
                            UserAgency = userType
                        });
                    }
                //Agency Users
                case 1:
                case 2:
                case 3:
                case 4:
                case 6:
                case 7:
                case 8:
                case 11:
                case 12:
                case 10:
                case 13:
                case 14:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 25:
                case 26:
                case 27:
                case 33:
                case 34:
                case 36:
                    {
                        //eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed && euc.AgencyForwardedFrom == user.PortalUserCategoryId && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        var disendorsedEucApplications = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "DISENDORSED" && euc.SourceAgency == user.PortalUserCategoryId).OrderByDescending(date => date.activityDate).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        var userType = _context.Users.Where(u => u.Id == userId).Single();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed dis-endorsed EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited dis-endorsed EUC applications page.",
                           Request.UserHostAddress);

                        return View(new disendorsedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            WorkFlow = disendorsedEucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users,
                            UserAgency = userType
                        });
                    }
                default:
                    {
                        eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                        //var endorsedEucApplications = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "DISENDORSED").OrderByDescending(date => date.activityDate).Distinct().ToList();
                        var eucCategories = _context.EUCItemCategory.ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();
                        var userType = _context.Users.Where(u => u.Id == userId).Single();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed dis-endorsed EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited dis-endorsed EUC applications page.",
                           Request.UserHostAddress);

                        return View(new disendorsedEUCApplicationViewModel
                        {
                            user = user,
                            role = roles,
                            EndUserCertificate = eucApplications,
                            EndUserCategories = eucCategories,
                            UsersToQuery = users,
                            UserAgency = userType
                        });
                    }
            }
        }
        [Authorize(Roles = "Agency, Client User, ONSA Support, ONSA PGSO, NSA, Customs User, Exclusive User")]
        public ActionResult allCert()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var UploadedCertificates = _context.UploadedEUCCertificate.Where(c => c.UploadUrl != null).ToList();

            List<EUCCertificates> eucCertificates;
            int portalCategory = user.PortalUserCategoryId;
            switch (portalCategory)
            {
                //ONSA Support Users
                case 5:
                    {
                        eucCertificates = _context.EUCCertificates.Where(s => s.CertStatus != CertificateStatus.Pending).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all EUC applications page.",
                           Request.UserHostAddress);

                        return View(new allCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users,
                            upload = UploadedCertificates
                        });
                    }
                //Agency Users
                case 1:
                case 2:
                case 3:
                case 4:
                case 6:
                case 7:
                case 8:
                case 11:
                case 12:
                case 10:
                case 13:
                case 14:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 25:
                case 26:
                case 27:
                case 33:
                case 34:
                case 36:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all EUC applications page.",
                           Request.UserHostAddress);

                        return View(new allCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                //Exclusive Users
                case 29:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.UserId == user.Id && euc.CertStatus != CertificateStatus.Pending).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all EUC applications page.",
                           Request.UserHostAddress);

                        return View(new allCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                //Client Users
                case 28:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.UserId == user.Id && euc.CertStatus != CertificateStatus.Pending).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all EUC applications page.",
                           Request.UserHostAddress);

                        return View(new allCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                default:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus != CertificateStatus.Pending).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all EUC applications page.",
                           Request.UserHostAddress);

                        return View(new allCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
            }
        }

        //List item for custom dashboard
        [Authorize(Roles = "Agency, Client User, ONSA Support, ONSA PGSO, NSA, Customs User, Exclusive User")]
        public ActionResult ValidCert()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;

            var UploadedCertificates = _context.UploadedEUCCertificate.Where(c => c.UploadUrl != null).ToList();
            var eucCertificates = _context.EUCCertificates.ToList();
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User viewed all Uploaded Certificate page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Visited all Uploaded Certificates page.",
               Request.UserHostAddress);

            return View(new UploadCertificateViewModel
            {
                user = user,
                role = roles,
                UploadedCertList = UploadedCertificates,
                UsersToQuery = users,
                EUCCertificates = eucCertificates
            });
        }
        [Authorize(Roles = "Agency, ONSA Support, ONSA PGSO, NSA, Client User, Exclusive User")]
        public ActionResult disputedCert()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;

            List<EUCCertificates> eucCertificates;
            int portalCategory = user.PortalUserCategoryId;
            switch (portalCategory)
            {
                //ONSA Support Users
                case 5:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Disputed).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all disputed EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all disputed EUC applications page.",
                           Request.UserHostAddress);

                        return View(new disputeCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                //Agency Users
                case 1:
                case 2:
                case 3:
                case 4:
                case 6:
                case 7:
                case 8:
                case 11:
                case 12:
                case 10:
                case 13:
                case 14:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 25:
                case 26:
                case 27:
                case 33:
                case 34:
                case 36:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.AgencyId == user.PortalUserCategoryId).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all disputed EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all disputed EUC applications page.",
                           Request.UserHostAddress);

                        return View(new disputeCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                //Exclusive Users
                case 29:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.UserId == user.Id).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all disputed EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all disputed EUC applications page.",
                           Request.UserHostAddress);

                        return View(new disputeCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                //Client Users
                case 28:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.UserId == user.Id).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all disputed EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all disputed EUC applications page.",
                           Request.UserHostAddress);

                        return View(new disputeCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                default:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Disputed).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all disputed EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all disputed EUC applications page.",
                           Request.UserHostAddress);

                        return View(new disputeCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
            }
        }
        [Authorize(Roles = "Agency, ONSA Support, ONSA PGSO, NSA")]
        public ActionResult utiliedCert()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;

            List<EUCCertificates> eucCertificates;
            int portalCategory = user.PortalUserCategoryId;
            switch (portalCategory)
            {
                //ONSA Support Users
                case 5:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Utilized).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all utilised EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all utilised EUC applications page.",
                           Request.UserHostAddress);

                        return View(new utilizedCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                //Agency Users
                case 1:
                case 2:
                case 3:
                case 4:
                case 6:
                case 7:
                case 8:
                case 11:
                case 12:
                case 10:
                case 13:
                case 14:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 25:
                case 26:
                case 27:
                case 33:
                case 34:
                case 36:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Utilized && euc.AgencyId == user.PortalUserCategoryId).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all utilised EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all utilised EUC applications page.",
                           Request.UserHostAddress);

                        return View(new utilizedCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                //Exclusive Users
                case 29:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Utilized && euc.UserId == user.Id).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all utilised EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all utilised EUC applications page.",
                           Request.UserHostAddress);

                        return View(new utilizedCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                //Client Users
                case 28:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Utilized && euc.UserId == user.Id).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all utilised EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all utilised EUC applications page.",
                           Request.UserHostAddress);

                        return View(new utilizedCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                default:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Utilized).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all utilised EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all utilised EUC applications page.",
                           Request.UserHostAddress);

                        return View(new utilizedCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
            }
        }
        [Authorize(Roles = "ONSA Support, ONSA PGSO, NSA")]
        public ActionResult expiredCert()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;

            List<EUCCertificates> eucCertificates;
            int portalCategory = user.PortalUserCategoryId;
            switch (portalCategory)
            {
                //ONSA Support Users
                case 5:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => DateTime.Now > euc.ExpiryDate).OrderByDescending(date => date.ExpiryDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all expired EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all expired EUC applications page.",
                           Request.UserHostAddress);

                        return View(new expiredCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                //Agency Users
                case 1:
                case 2:
                case 3:
                case 4:
                case 6:
                case 7:
                case 8:
                case 11:
                case 12:
                case 10:
                case 13:
                case 14:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 25:
                case 26:
                case 27:
                case 33:
                case 34:
                case 36:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => DateTime.Now > euc.ExpiryDate && euc.AgencyId == user.PortalUserCategoryId).OrderByDescending(date => date.ExpiryDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all expired EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all expired EUC applications page.",
                           Request.UserHostAddress);

                        return View(new expiredCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                //Exclusive Users
                case 29:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => DateTime.Now > euc.ExpiryDate && euc.UserId == user.Id).OrderByDescending(date => date.ExpiryDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all expired EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all expired EUC applications page.",
                           Request.UserHostAddress);

                        return View(new expiredCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                //Client Users
                case 28:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => DateTime.Now > euc.ExpiryDate && euc.UserId == user.Id).OrderByDescending(date => date.ExpiryDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all expired EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all expired EUC applications page.",
                           Request.UserHostAddress);

                        return View(new expiredCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                default:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => DateTime.Now > euc.ExpiryDate).OrderByDescending(date => date.ExpiryDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all expired EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all expired EUC applications page.",
                           Request.UserHostAddress);

                        return View(new expiredCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
            }
        }
        [Authorize(Roles = "ONSA Support, ONSA PGSO, NSA")]
        public ActionResult recalledCert()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;

            List<EUCCertificates> eucCertificates;
            int portalCategory = user.PortalUserCategoryId;
            switch (portalCategory)
            {
                //ONSA Support Users
                case 5:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Recalled).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all recalled EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all recalled EUC applications page.",
                           Request.UserHostAddress);

                        return View(new recalledCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                //Agency Users
                case 1:
                case 2:
                case 3:
                case 4:
                case 6:
                case 7:
                case 8:
                case 11:
                case 12:
                case 10:
                case 13:
                case 14:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 25:
                case 26:
                case 27:
                case 33:
                case 34:
                case 36:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Recalled && euc.AgencyId == user.PortalUserCategoryId).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all recalled EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all recalled EUC applications page.",
                           Request.UserHostAddress);

                        return View(new recalledCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                //Exclusive Users
                case 29:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Recalled && euc.UserId == user.Id).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all recalled EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all recalled EUC applications page.",
                           Request.UserHostAddress);

                        return View(new recalledCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                //Client Users
                case 28:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Recalled && euc.UserId == user.Id).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all recalled EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all recalled EUC applications page.",
                           Request.UserHostAddress);

                        return View(new recalledCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
                default:
                    {
                        eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Recalled).OrderByDescending(date => date.IssuanceDate).ToList();
                        var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                        var users = _context.Users.ToList();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("User viewed all recalled EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                           User.Identity.GetUserId(),
                           "Visited all recalled EUC applications page.",
                           Request.UserHostAddress);

                        return View(new recalledCertViewModel
                        {
                            user = user,
                            role = roles,
                            EUCCertificates = eucCertificates,
                            UsersToQuery = users
                        });
                    }
            }
        }

        [Authorize(Roles = "Agency, Client User, ONSA Support, ONSA PGSO, Exclusive User")]
        public ActionResult pendingRequest()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.Where(a => a.ApprovalStatus == ApprovedStatus.Pending).OrderByDescending(date => date.RequestedDate).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User viewed all pending user application requests: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Visited all pending user applications page.",
               Request.UserHostAddress);

            return View(new DashboardViewModel
            {
                user = user,
                role = roles,
                userList = users
            });
        }
        [Authorize(Roles = "Agency, Client User, ONSA Support, ONSA PGSO, Exclusive User")]
        public ActionResult completedRequest()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.Where(a => a.ApprovalStatus == ApprovedStatus.Approved).OrderByDescending(date => date.RequestedDate).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User viewed all completed user application requests: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Visited all completed user applications page.",
               Request.UserHostAddress);

            return View(new DashboardViewModel
            {
                user = user,
                role = roles,
                userList = users
            });
        }
        [Authorize(Roles = "Agency, Client User, ONSA Support, ONSA PGSO, Exclusive User")]
        public ActionResult rejectedRequest()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.Where(a => a.ApprovalStatus == ApprovedStatus.Rejected).OrderByDescending(date => date.RequestedDate).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User viewed all rejected user application requests: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Visited all rejected user applications page.",
               Request.UserHostAddress);

            return View(new DashboardViewModel
            {
                user = user,
                role = roles,
                userList = users
            });
        }
        [Authorize(Roles = "Agency, ONSA Admin, ONSA Support")]
        public ActionResult allRequest()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User viewed all application requests: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Visited all user applications page.",
               Request.UserHostAddress);

            return View(new DashboardViewModel
            {
                user = user,
                role = roles,
                userList = users
            });
        }
        //Get
        [Authorize(Roles = "Agency, ONSA Admin, ONSA Support, ONSA PGSO, NSA")]
        public ActionResult CreatePortal(string id, int? CategoryId)
        {
            if (CategoryId != null)
            {
                //Get ID of the currently logged in user
                var userId = User.Identity.GetUserId();
                //Create user object based off user ID
                var user = _context.Users.Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                var states = _context.States.ToList();
                var country = _context.CountryList.ToList();

                var portalCategories = _context.PortalCategories.Where(p => p.Id == CategoryId).ToList();
                var portalSubCategories = _context.PortalSubCategories.Where(p => p.PortalCategoryId == CategoryId).ToList();
                PortalCategories selectedCategory = _context.PortalCategories.Single(p => p.Id == CategoryId);

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("User visited create portal user page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                   User.Identity.GetUserId(),
                   "Visited create portal user page.",
                   Request.UserHostAddress);

                return View(new CreatePortalUserViewModel()
                {
                    CategoryName = selectedCategory.Name,
                    SelectedCategory = CategoryId,
                    user = user,
                    role = roles,
                    Categories = portalCategories,
                    States = states,
                    SubCategories = portalSubCategories,
                    MeansOfIdentifications = _context.Identification.ToList(),
                    Country = country
                });
            }
            else
            {
                //Get ID of the currently logged in user
                var userId = User.Identity.GetUserId();
                //Create user object based off user ID
                var user = _context.Users.Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                var portalCategories = _context.PortalCategories.ToList();
                var portalSubCategories = _context.PortalSubCategories.ToList();
                var states = _context.States.ToList();
                var country = _context.CountryList.ToList();

                return View(new CreatePortalUserViewModel()
                {
                    user = user,
                    role = roles,
                    States = states,
                    Categories = portalCategories,
                    SubCategories = portalSubCategories,
                    MeansOfIdentifications = _context.Identification.ToList(),
                    Country = country
                });
            }
        }
        // POST: /Account/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> createPortal(CreatePortalUserViewModel model)
        {
            ImageBuilder builder = new ImageBuilder();

            if (ModelState.IsValid /*&& model.passportUpload != null && model.IdUpload != null */)
            {
                #region upload passport file
                var extension = Path.GetExtension(model.passportUpload.FileName);

                var fileNameWithExtension = Path.GetFileName(model.passportUpload.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(model.passportUpload.FileName);

                string image = "/images/profile/" + model.Email + "/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), model.Email);

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + model.Email), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                model.passportUpload.SaveAs(imageUrl);

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Uploaded user passport on server. Path: " + path,
                   User.Identity.GetUserId(),
                   "Uploaded user passport.",
                   Request.UserHostAddress);
                #endregion

                #region
                var idExtension = Path.GetExtension(model.IdUpload.FileName);

                var idFileNameWithExtension = Path.GetFileName(model.IdUpload.FileName);
                var idFileNameWithoutExtension = Path.GetFileNameWithoutExtension(model.IdUpload.FileName);

                string idImage = "/images/profile/ID/" + model.Email + "/" + idFileNameWithExtension;
                string idPath = System.IO.Path.Combine(Server.MapPath("~/images/profile/ID"), model.Email);

                string idImageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/ID/" + model.Email), idFileNameWithExtension);

                var idDirectory = Directory.CreateDirectory(idPath);

                model.IdUpload.SaveAs(idImageUrl);

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Uploaded means of ID on server. Path: " + path,
                   User.Identity.GetUserId(),
                   "Uploaded means of ID.",
                   Request.UserHostAddress);
                #endregion

                //Select User Category
                CategoryList UserCategory = new CategoryList();
                if (model.SubCategory == 29 || model.SubCategory == 32)
                {
                    UserCategory = CategoryList.Exclusive;
                }
                else if (model.SubCategory == 28)
                {
                    UserCategory = CategoryList.Individual;
                }
                else
                {
                    UserCategory = CategoryList.Corporate;
                }

                #region Create User Applicant Number
                string applicantNumber = ApplicationNumberGenerator.
                    ApplicationNumberGenerator.
                    GenerateApplicationNumber();
                #endregion

                //Set a variable to check if the user can verify EUCs
                bool canVerifyEUCs;
                var subCategory = model.SubCategory;
                if (subCategory == 32)
                {
                    canVerifyEUCs = true;
                }
                else
                {
                    canVerifyEUCs = false;
                }


                var user = new ApplicationUser
                {
                    CategoryId = 1/*Individual Account Type*/,
                    CategoryList = UserCategory,
                    Identification = model.Identification,
                    UserName = model.Email,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    OtherName = model.OtherName,
                    LastName = model.LastName,
                    Gender = model.Gender,
                    PassportUrl = image,
                    MaritalStatus = model.MaritalStatus,
                    AddressLine1 = model.AddressLine1,
                    AddressLine2 = model.AddressLine2,
                    PhoneNumber = model.PhoneNumber,
                    StateOfOriginId = model.State,
                    City = model.City,
                    Country = model.countryId,
                    TIN = model.TIN,
                    MeansOfIdentificationId = model.MeansOfIdentification,
                    DriversLicenseNumber = model.LicenseNumber,
                    PlaceOfIssue = model.PlaceOfIssue,
                    LicenseDateOfIssuance = model.LicenseDateOfIssuance,
                    LicenseDateOfExpiry = model.LicenseDateOfExpiry,
                    NextOfKinFullname = model.NextOfKinFullname,
                    NextOfKinAddress = model.NextOfKinAddress,
                    NextOfKinRelationship = model.NextOfKinRelationship,
                    NextOfKinPhoneNumber = model.NextOfKinPhoneNumber,
                    DOB = model.DOB,
                    Approved = false,
                    UserType = UserType.PortalUser,
                    ApprovalStatus = ApprovedStatus.Pending,
                    IdUrl = idImage,
                    PortalUserCategoryId = model.SubCategory,
                    blacklistedDate = DateTime.Parse("1900-01-01 00:00:00.000"),
                    RequestedDate = DateTime.Now,
                    ApplicationNumber = applicantNumber,
                    OrganizationName = model.OrganizationName,
                    canVerifyEndUserCertificates = canVerifyEUCs
                    //Country = model.countryId
                };
                model.Password = "UKN2Uwuu!";

                var result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    //Create User Role from Admin View

                    string id = user.Id; // Your Identity column ID
                    var UserRoleManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(_context));

                    var applicantToUpdate = _context.Users.Find(id);

                    applicantToUpdate.ApprovalStatus = ApprovedStatus.Approved;
                    applicantToUpdate.EmailConfirmed = true;

                    _context.SaveChanges();

                    //Create Company Details for User
                    if (user.CategoryList == CategoryList.Exclusive)
                    {
                        var Company = new Company
                        {
                            Name = model.OrganizationName,
                            Email = user.Email,
                            Address = model.AddressLine1,
                            StateId = model.State,
                            PhoneNo = model.PhoneNumber,
                            OwnerId = user.Id,
                            DateOfIncorporation = DateTime.Now
                        };
                        _context.Company.Add(Company);
                        _context.SaveChanges();
                    }


                    //Put agency definition here
                    int userCat = applicantToUpdate.PortalUserCategoryId;
                    switch (userCat)
                    {
                        //Agencies
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 6:
                        case 10:
                        case 13:
                        case 14:
                        case 17:
                        case 19:
                        case 20:
                        case 21:
                        case 25:
                        case 26:
                        case 27:
                        case 30:
                        case 31:
                            {
                                UserRoleManager.AddToRole(applicantToUpdate.Id, "Agency");
                                _context.SaveChanges();

                                break;
                            }
                        case 7:
                        case 8:
                        case 11:
                        case 12:
                        case 18:
                            {
                                UserRoleManager.AddToRole(applicantToUpdate.Id, "Agency");
                                _context.SaveChanges();
                                break;
                            }


                        //ONSA Support
                        case 5:
                            {
                                UserRoleManager.AddToRole(applicantToUpdate.Id, "ONSA Support");
                                _context.SaveChanges();

                                break;
                            }
                        case 16:
                            {
                                UserRoleManager.AddToRole(applicantToUpdate.Id, "Agency");
                                _context.SaveChanges();

                                break;
                            }
                        case 22:
                            {
                                UserRoleManager.AddToRole(applicantToUpdate.Id, "Agency");
                                _context.SaveChanges();
                                break;
                            }
                        case 24:
                            {
                                UserRoleManager.AddToRole(applicantToUpdate.Id, "ONSA User");
                                _context.SaveChanges();

                                break;
                            }
                        //Client Users
                        case 28:
                            {
                                UserRoleManager.AddToRole(applicantToUpdate.Id, "Client User");
                                _context.SaveChanges();

                                break;
                            }
                        //Exclusive Users
                        case 29:
                            {
                                UserRoleManager.AddToRole(applicantToUpdate.Id, "Exclusive User");
                                _context.SaveChanges();

                                break;
                            }
                        //Certificate Verififcation Users
                        case 32:
                            {
                                UserRoleManager.AddToRole(applicantToUpdate.Id, "Certificate Verification User");
                                _context.SaveChanges();

                                break;
                            }
                        case 33:
                            {
                                UserRoleManager.AddToRole(applicantToUpdate.Id, "Agency");
                                _context.SaveChanges();

                                break;
                            }
                        case 34:
                            {
                                UserRoleManager.AddToRole(applicantToUpdate.Id, "Agency");
                                _context.SaveChanges();

                                break;
                            }
                        case 36:
                            {
                                UserRoleManager.AddToRole(applicantToUpdate.Id, "Agency");
                                _context.SaveChanges();

                                break;
                            }
                        default:
                            {

                                UserRoleManager.AddToRole(applicantToUpdate.Id, "Client User");
                                _context.SaveChanges();
                                break;
                            }
                    }
                    _context.SaveChanges();

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Created portal user. Created User Application Name: " + model.FirstName + " " + model.LastName,
                       User.Identity.GetUserId(),
                       "Created portal user.",
                       Request.UserHostAddress);

                    //Comment line below to disable auto sign in after registration
                    //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    //Send user email informing them their account has been approved
                    IdentityMessage EmailMessage = new IdentityMessage();

                    EmailMessage.Body = "Dear " + applicantToUpdate.FirstName + " " + applicantToUpdate.LastName + ". "
                                        + "<br/>"
                                        + "Your account has been created successfully on the ONSA"
                                        + " End-User Certificate"
                                        + " portal."
                                        + "<br/>"
                                        + " Kindly Login with the Username: " + model.Email + " and Password: " + model.Password;

                    EmailMessage.Destination = applicantToUpdate.Email;
                    EmailMessage.Subject = "Account Created on EUC Portal";
                    //Send email
                    EmailService emailService = new EmailService();
                    await emailService.SendAsync(EmailMessage);

                    //Send SMS Confirmation After Successful Registration
                    var smsConfirmationCode = GenerateConfirmationTokenForSMSAsync(user.Id);

                    IdentityMessage SmsMessage = new IdentityMessage();
                    //send accompanying SMS reminding user to verify their email
                    string smsBody = "This is to confirm that your account has been created on the ONSA EUC portal."
                                       + " Kindly Login with the Username: " + model.Email + " and Password: " + model.Password;

                    SmsMessage.Body = smsBody;
                    SmsMessage.Destination = user.PhoneNumber;
                    SmsMessage.Subject = "Account Created on EUC Portal";

                    SmsService smsSerice = new SmsService();
                    await smsSerice.SendAsync(SmsMessage);

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Send SMS confirmation to user after successful registration. Phone Number: " + model.PhoneNumber + " Applicant Number: " + applicantNumber + ". E-mail: " + model.Email,
                       User.Identity.GetUserId(),
                       "SMS confirmation sent.",
                       Request.UserHostAddress);

                    TempData["Success"] = "User Successfully Created!";
                    return RedirectToAction("MainDashboard", "Admin");
                }
                else
                {
                    AddErrors(result);
                    //Get ID of the currently logged in user
                    var userIdOnFailure = User.Identity.GetUserId();
                    //Create user object based off user ID
                    var userOnFailure = _context.Users.Where(u => u.Id == userIdOnFailure).Single();
                    var userIdentity = (ClaimsIdentity)User.Identity;
                    var claims = userIdentity.Claims;
                    var roleClaimType = userIdentity.RoleClaimType;
                    var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                    var states = _context.States.ToList();
                    var country = _context.CountryList.ToList();

                    //Get parent of 
                    //Get ID of parent category
                    var portalSubCategoryParentId = _context.PortalSubCategories.Where(p => p.Id == model.SubCategory).FirstOrDefault();
                    //User above ID to generate parent category
                    var portalCategory = _context.PortalCategories.Where(p => p.Id == portalSubCategoryParentId.PortalCategoryId).ToList();
                    //Use same ID to generate sub category ID
                    //Confusing, I know
                    var portalSubCategories = _context.PortalSubCategories.ToList();

                    model.user = userOnFailure;
                    model.role = roles;
                    model.Categories = portalCategory;
                    model.SubCategories = portalSubCategories;
                    model.States = states;
                    model.Country = country;
                    model.MeansOfIdentifications = _context.Identification.ToList();
                    // If we got this far, something failed, redisplay form
                    return View(model);
                }
            }
            return RedirectToAction("MainDashboard", "Admin");
        }

        [Authorize(Roles = "ONSA Admin")]
        public ActionResult EditUser(string id)
        {
            var user = _context.Users.Where(a => a.Id == id).FirstOrDefault();
            var country = _context.CountryList.ToList();
            var state = _context.States.ToList();
            var canVerifyEUCCertificate = user.canVerifyEndUserCertificates;

            var model = new CreatePortalUserViewModel
            {
                Email = user.Email,
                FirstName = user.FirstName,
                OtherName = user.OtherName,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                State = user.StateOfOriginId,
                City = user.City,
                countryId = user.Country,
                TIN = user.TIN,
                DOB = user.DOB,
                Country = country,
                States = state,
                CanVerifyEUC = user.canVerifyEndUserCertificates
            };
            model.UserId = id;
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited edit User page. Edited User Name: " + user.UserName,
               User.Identity.GetUserId(),
               "Edited User Record",
               Request.UserHostAddress);

            return View("EditUser", model);
        }

        [Authorize(Roles = "ONSA Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUser(CreatePortalUserViewModel model)
        {
            string Id = model.UserId;
            string image = null;
            var PortalUser = _context.Users.Where(u => u.Id == Id).FirstOrDefault();

            #region upload passport file
            if (model.passportUpload != null)
            {
                var extension = Path.GetExtension(model.passportUpload.FileName);

                var fileNameWithExtension = Path.GetFileName(model.passportUpload.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(model.passportUpload.FileName);

                image = "/images/profile/" + model.Email + "/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), model.Email);

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + model.Email), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                model.passportUpload.SaveAs(imageUrl);
                PortalUser.PassportUrl = image;
                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Uploaded user passport on server. Path: " + path,
                   User.Identity.GetUserId(),
                   "Uploaded user passport.",
                   Request.UserHostAddress);
            }
            #endregion

            PortalUser.LastName = model.LastName;
            PortalUser.FirstName = model.FirstName;
            PortalUser.OtherName = model.OtherName;
            PortalUser.PhoneNo = model.PhoneNumber;
            PortalUser.StateOfOriginId = model.State;
            PortalUser.City = model.City;
            PortalUser.TIN = model.TIN;
            PortalUser.DOB = model.DOB;
            PortalUser.Email = model.Email;
            PortalUser.UserName = model.Email;
            PortalUser.PassportUrl = image;
            PortalUser.canVerifyEndUserCertificates = model.CanVerifyEUC;
            if (model.Password != null)
            {
                PortalUser.PasswordHash = UserManager.PasswordHasher.HashPassword(model.Password);
            }
            _context.SaveChanges();
            TempData["Success"] = "Updated Successfully!";

            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            model.Country = _context.CountryList.ToList();
            model.States = _context.States.ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Updated User record. Edited User Name: " + PortalUser.UserName,
               User.Identity.GetUserId(),
               "Edited User Record",
               Request.UserHostAddress);

            return RedirectToAction("ManageAllUsers", "Admin");
        }

        [Authorize(Roles = "Agency, ONSA Admin, ONSA Support, ONSA PGSO, NSA")]
        public ActionResult allPortalUsers()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Viewed all portal users page. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Viewed portal user.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.Where(a => a.UserType == UserType.PortalUser).ToList();
            return View(new DashboardViewModel
            {
                user = user,
                role = roles,
                userList = users
            });
        }
        [Authorize(Roles = "Agency, ONSA Admin, ONSA Support, ONSA PGSO, NSA")]
        public ActionResult analytic()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Viewed analytics page. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Viewed analytics page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var state = _context.States.ToList();
            var agency = _context.PortalSubCategories.ToList();
            var country = _context.CountryList.ToList();
            var EUCItemCat = _context.EUCItemCategory.ToList();
            var ports = _context.PortsOfLanding.ToList();
            return View(new AnalyticsViewModel
            {
                user = user,
                role = roles,
                State = state,
                AgencyList = agency,
                Country = country,
                ItemCat = EUCItemCat,
                PortOfLaden = ports
            });
        }
        //Generate Analytics
        [Authorize(Roles = "ONSA Support, ONSA PGSO, NSA, ONSA Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult analytic(AnalyticsViewModel model)
        {
            var stream = new MemoryStream();

            if (Request.Form["applicationReport"] != null)
            {
                //Generate Application Reports Preview 
                if (model.RptType == ReportType.All)
                {
                    rptEUCAppDetailsAll report = new rptEUCAppDetailsAll();
                    report.Parameters["DATEFROM"].Value = model.DateFrom;
                    report.Parameters["DATETO"].Value = model.DateTo;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
                else if (model.RptType == ReportType.Applicant)
                {
                    rptEUCAppDetails report = new rptEUCAppDetails();
                    report.Parameters["DATEFROM"].Value = model.DateFrom;
                    report.Parameters["DATETO"].Value = model.DateTo;
                    report.Parameters["APPLICANTNO"].Value = model.ApplicantNo;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
                else if (model.RptType == ReportType.Approved)
                {
                    rptApprovedApp report = new rptApprovedApp();
                    report.Parameters["DATEFROM"].Value = model.DateFrom;
                    report.Parameters["DATETO"].Value = model.DateTo;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
                else if (model.RptType == ReportType.Forwarded)
                {
                    rptForwardedApp report = new rptForwardedApp();
                    report.Parameters["DATEFROM"].Value = model.DateFrom;
                    report.Parameters["DATETO"].Value = model.DateTo;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
                else if (model.RptType == ReportType.Rejected)
                {
                    rptRejectedApp report = new rptRejectedApp();
                    report.Parameters["DATEFROM"].Value = model.DateFrom;
                    report.Parameters["DATETO"].Value = model.DateTo;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
                else if (model.RptType == ReportType.Disendorsed)
                {
                    rptDisendorsedApp report = new rptDisendorsedApp();
                    report.Parameters["DATEFROM"].Value = model.DateFrom;
                    report.Parameters["DATETO"].Value = model.DateTo;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
                else if (model.RptType == ReportType.Item_Category)
                {

                    rptEUCAppDetailsCategory report = new rptEUCAppDetailsCategory();
                    report.Parameters["DATEFROM"].Value = model.DateFrom;
                    report.Parameters["DATETO"].Value = model.DateTo;
                    report.Parameters["ITEMCATEGORY"].Value = model.ItemCatID;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
                else if (model.RptType == ReportType.Detailed_Item_Category)
                {

                    rptCategoryDetails report = new rptCategoryDetails();
                    report.Parameters["DATEFROM"].Value = model.DateFrom;
                    report.Parameters["DATETO"].Value = model.DateTo;
                    report.Parameters["ITEMCATEGORY"].Value = model.ItemCatID;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
                else if (model.RptType == ReportType.Ports)
                {

                    rptEUCAppDetailsPort report = new rptEUCAppDetailsPort();
                    report.Parameters["DATEFROM"].Value = model.DateFrom;
                    report.Parameters["DATETO"].Value = model.DateTo;
                    report.Parameters["PORT"].Value = model.PortID;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
                else if (model.RptType == ReportType.Request_Country)
                {
                    rptEUCAppDetailsCountry report = new rptEUCAppDetailsCountry();
                    report.Parameters["DATEFROM"].Value = model.DateFrom;
                    report.Parameters["DATETO"].Value = model.DateTo;
                    report.Parameters["IMPORTCOUNTRY"].Value = model.CountryID;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
            }
            else if (Request.Form["certificateReport"] != null)
            {
                //Generate Application Reports Preview 
                if (model.CertRptType == CertReportType.All)
                {
                    rptCertificates report = new rptCertificates();
                    report.Parameters["DATEFROM"].Value = model.CertDateFrom;
                    report.Parameters["DATETO"].Value = model.CertDateTo;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
                else if (model.CertRptType == CertReportType.Application_Number)
                {
                    rptCertificatesAppNo report = new rptCertificatesAppNo();
                    report.Parameters["DATEFROM"].Value = model.CertDateFrom;
                    report.Parameters["DATETO"].Value = model.CertDateTo;
                    report.Parameters["APPLICATIONNO"].Value = model.ApplicationNumber;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
                else if (model.CertRptType == CertReportType.Certificate_Status)
                {
                    rptCertificatesStatus report = new rptCertificatesStatus();
                    report.Parameters["DATEFROM"].Value = model.CertDateFrom;
                    report.Parameters["DATETO"].Value = model.CertDateTo;
                    report.Parameters["CERTSTATUS"].Value = model.CertStatus;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
                else if (model.CertRptType == CertReportType.Expired_Certificates)
                {
                    rptCertificatesStatusExpired report = new rptCertificatesStatusExpired();
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
            }
            else if (Request.Form["userReport"] != null)
            {
                //Generate Application Reports Preview 
                if (model.UserRptType == UserReportType.All)
                {
                    rptUser report = new rptUser();
                    report.Parameters["DATEFROM"].Value = model.UserDateFrom;
                    report.Parameters["DATETO"].Value = model.UserDateTo;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
                else if (model.UserRptType == UserReportType.Approval_Status)
                {
                    rptUserStatus report = new rptUserStatus();
                    report.Parameters["DATEFROM"].Value = model.UserDateFrom;
                    report.Parameters["DATETO"].Value = model.UserDateTo;
                    report.Parameters["STATUS"].Value = model.UserApprovedStatus;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
                else if (model.UserRptType == UserReportType.User_Agency)
                {
                    rptUserAgency report = new rptUserAgency();
                    report.Parameters["DATEFROM"].Value = model.UserDateFrom;
                    report.Parameters["DATETO"].Value = model.UserDateTo;
                    report.Parameters["AGENCY"].Value = model.AgencyID;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
            }
            else if (Request.Form["chartReport"] != null)
            {
                //Generate Application Reports Preview 
                if (model.ChartRptType == ChartRptType.Applications)
                {
                    rptEUCChart report = new rptEUCChart();
                    report.Parameters["DateFrom"].Value = model.ChartDateFrom;
                    report.Parameters["DateTo"].Value = model.ChartDateTo;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
                else if (model.ChartRptType == ChartRptType.Certificates)
                {
                    rptEUCChart report = new rptEUCChart();
                    report.Parameters["DateFrom"].Value = model.ChartDateFrom;
                    report.Parameters["DateTo"].Value = model.ChartDateTo;
                    stream = new MemoryStream();
                    report.ExportToPdf(stream);
                }
            }
            //var cd = new System.Net.Mime.ContentDisposition
            //{
            //    FileName = "filename.pdf",
            //    Inline = false,
            //};
            //Response.AppendHeader("Content-Disposition", cd.ToString());
            return File(stream.GetBuffer(), "application/pdf");

            model.State = _context.States.ToList();
            model.AgencyList = _context.PortalSubCategories.ToList();
            model.Country = _context.CountryList.ToList();
            model.ItemCat = _context.EUCItemCategory.ToList();
            model.PortOfLaden = _context.PortsOfLanding.ToList();
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(model);
        }
        [Authorize(Roles = "ONSA PGSO, NSA")]
        public ActionResult trackApplication()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Viewed track application page. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Viewed track applications page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            //var eucAppList = _context.EUCWorkFlow.Where(e => e.applicationNumber == eucappNo).ToList();
            return View(new TrackEUCAppViewModel
            {
                user = user,
                role = roles
            });
        }
        [HttpPost]
        public ActionResult trackApplication(string eucappNo)
        {

            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Requested application through track applications page. Application Number: " + eucappNo + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Tracked application.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var eucAppList = _context.EUCWorkFlow.Where(e => e.applicationNumber.Contains(eucappNo)).OrderByDescending(date => date.activityDate).ToList();
            var Users = _context.Users.ToList();
            var AgencyList = _context.PortalSubCategories.ToList();

            return View(new TrackEUCAppViewModel
            {
                user = user,
                role = roles,
                EUCAppList = eucAppList,
                UserList = Users,
                AgencyList = AgencyList
            });
        }
        [Authorize(Roles = "ONSA Admin")]
        public ActionResult generalSettings()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Viewed general settings page. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Viewed general settings page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(new DashboardViewModel
            {
                user = user,
                role = roles
            });
        }

        [Authorize(Roles = "ONSA Support, ONSA PGSO, NSA")]
        public ActionResult blacklistedApplicant()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Viewed blacklisted applicants page. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Viewed blacklisted applicants page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var blacklistedApplicant = _context.Users.Where(b => b.IsBlackListed == true).OrderByDescending(b => b.blacklistedDate).ToList();
            var userList = _context.Users.ToList();
            return View(new BlacklistViewModel
            {
                user = user,
                role = roles,
                BlackListedApplicant = blacklistedApplicant
                //UserList = userList
            });
        }

        [Authorize(Roles = "ONSA Support, ONSA PGSO, NSA")]
        public ActionResult blacklistApplicant()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Viewed blacklist applicant page. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Viewed blacklist applicant page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(new BlacklistViewModel
            {
                user = user,
                role = roles
            });
        }
        //Applicant number = user ID
        [Authorize(Roles = "ONSA Support, ONSA PGSO, NSA")]
        public ActionResult BlacklistApplicantDetails(string ApplicantNo)
        {
            //Check is Applicant Application Number exist
            var appNo = _context.Users.Where(a => a.ApplicationNumber == ApplicantNo).FirstOrDefault();
            if (appNo != null)
            {
                var viewModel = new BlacklistViewModel
                {
                    ApplicantID = appNo.Id,
                    ApplicantDetail = appNo
                };

                var userId = User.Identity.GetUserId();
                viewModel.user = _context.Users.Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                viewModel.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Blacklisted applicant. Blacklisted Applicant Number: " + ApplicantNo + " User Name: " + appNo.FirstName + " " + appNo.LastName,
                   User.Identity.GetUserId(),
                   "Viewed track applications page.",
                   Request.UserHostAddress);

                return View("BlacklistApplicantDetails", viewModel);

            }
            else if (appNo == null)
            {

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("User attempted to blacklist and applicant with an applicant number that doesn't exist. Application Number: " + appNo,
                   User.Identity.GetUserId(),
                   "Blacklist attempt failed.",
                   Request.UserHostAddress);

                ViewBag.Message = "Please provide a valid Applicant Number.. The number provided does not exist";

                //Get ID of the currently logged in user
                var userId = User.Identity.GetUserId();
                //Create user object based off user ID
                var user = _context.Users.Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                var viewModel = new BlacklistViewModel
                {
                    user = user,
                    role = roles
                };
                return View("blacklistApplicant", viewModel);
            }
            return View();
        }

        [Authorize(Roles = "ONSA Support, ONSA PGSO, NSA")]
        public ActionResult BlacklistDetails(int? blacklistId, string userAppNo)
        {
            if (blacklistId == null)
            {
                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Attempted to view blacklist details without inputing a blacklist ID",
                   User.Identity.GetUserId(),
                   "Invalid blacklist details attempt.",
                   Request.UserHostAddress);

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var blacklistDetails = _context.BlackListApplicant.Where(a => a.Id == blacklistId).FirstOrDefault();
            var Users = _context.Users.ToList();
            var UserList = _context.Users.Where(u => u.Id == userAppNo).FirstOrDefault();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("View blacklist details page. Blacklisted Applicant's Number: " + UserList.ApplicationNumber,
               User.Identity.GetUserId(),
               "Viewed blacklist details page.",
               Request.UserHostAddress);

            var viewModel = new BlacklistViewModel
            {
                BlacklistDetails = blacklistDetails,
                AllUsers = Users,
                UserList = UserList
            };
            var userId = User.Identity.GetUserId();
            viewModel.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            viewModel.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View("BlacklistDetails", viewModel);
        }

        [Authorize(Roles = "ONSA Support, ONSA PGSO, NSA")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SaveBlacklistReason(BlacklistViewModel model)
        {
            var blackListAction = "BLACKLISTED";
            var BlackListerID = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                var blackListApplicant = new BlackListApplicant
                {
                    BlacklistDate = DateTime.Now,
                    ApplicantId = model.ApplicantID,
                    action = blackListAction,
                    BlacklistedBy = BlackListerID,
                    BlacklistReason = model.BlacklistReason
                };

                var user = _context.Users.Where(u => u.Id == model.ApplicantID).FirstOrDefault();
                if (user.IsBlackListed == false)
                {
                    _context.BlackListApplicant.Add(blackListApplicant);
                    _context.SaveChanges();

                    //Update User Table with Blacklist Status
                    user.IsBlackListed = true;
                    user.blacklistedDate = DateTime.Now;
                    user.blacklistedBy = User.Identity.GetUserId();
                    user.blacklistID = blackListApplicant.Id;
                    var result = _context.SaveChanges();
                    TempData["Success"] = "Blacklisted Successfully!";

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Blacklisted a user. Blacklisted User's Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
                       User.Identity.GetUserId(),
                       "Blacklisted user.",
                       Request.UserHostAddress);

                    //Notify use of blacklist by SMS and Email if the blacklist details have been saved in the DB
                    if (result > 0)
                    {
                        ApplicantNotification applicantNotification = new ApplicantNotification();
                        await applicantNotification.NotifyBlacklistedApplicant(user, model.BlacklistReason);
                    }
                }
                else if (user.IsBlackListed == true)
                {
                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Attempted to blacklist a user who had already been blacklisted. User's Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
                       User.Identity.GetUserId(),
                       "Failed to blacklist a user.",
                       Request.UserHostAddress);

                    TempData["Unsuccessul"] = "This Applicant has already been blacklisted!..";
                }
                else
                {
                    _context.BlackListApplicant.Add(blackListApplicant);
                    _context.SaveChanges();

                    //Update User Table with Blacklist Status
                    user.IsBlackListed = true;
                    user.blacklistedDate = DateTime.Now;
                    user.blacklistedBy = User.Identity.GetUserId();
                    user.blacklistID = blackListApplicant.Id;
                    _context.SaveChanges();

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Blacklisted a user. Blacklisted User's Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
                       User.Identity.GetUserId(),
                       "Blacklisted user.",
                       Request.UserHostAddress);

                    TempData["Success"] = "Blacklisted Successfully!";
                }

                return RedirectToAction("blacklistApplicant", "Admin");
            }
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(model);
        }

        [Authorize(Roles = "ONSA Support, ONSA PGSO, NSA")]
        public ActionResult RemoveFromBlacklist(string appID)
        {
            var blackListedApp = _context.Users.Where(a => a.Id == appID).FirstOrDefault();

            var viewModel = new BlacklistViewModel
            {
                ApplicantID = appID,
                ApplicantDetail = blackListedApp
            };
            var userId = User.Identity.GetUserId();
            viewModel.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            viewModel.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Viewed remove from blacklist page.",
               User.Identity.GetUserId(),
               "Visited remove from blacklist page.",
               Request.UserHostAddress);

            return View("RemoveFromBlacklist", viewModel);
        }

        [Authorize(Roles = "ONSA Support, ONSA PGSO, NSA")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveFromBlacklist(BlacklistViewModel model)
        {
            var blackListAction = "REMOVE FROM BLACKLIST";
            var BlackListerID = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                var blackListApplicant = new BlackListApplicant
                {
                    BlacklistDate = DateTime.Now,
                    ApplicantId = model.ApplicantID,
                    action = blackListAction,
                    BlacklistedBy = BlackListerID,
                    BlacklistReason = model.BlacklistReason
                };

                var user = _context.Users.Where(u => u.Id == model.ApplicantID).FirstOrDefault();
                if (user.IsBlackListed == true)
                {
                    _context.BlackListApplicant.Add(blackListApplicant);
                    _context.SaveChanges();

                    //Update User Table with Blacklist Status
                    user.IsBlackListed = false;
                    user.blacklistedDate = DateTime.Now;
                    user.blacklistedBy = User.Identity.GetUserId();
                    user.blacklistID = blackListApplicant.Id;
                    _context.SaveChanges();
                    TempData["Success"] = "REMOVE FROM BLACKLIST SUCCESSFULLY!";

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Un-blacklisted user. Un-blacklisted User Applicant Number: " + user.ApplicationNumber + "Name: " + user.FirstName + " " + user.LastName,
                       User.Identity.GetUserId(),
                       "USER REMOVED FROM BLACKLIST",
                       Request.UserHostAddress);
                }
                else if (user.IsBlackListed == false)
                {
                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Attempted to un-blacklisted user who was not in the blacklist. Applicant number: " + user.ApplicationNumber + "Name: " + user.FirstName + " " + user.LastName,
                       User.Identity.GetUserId(),
                       "Failed un-blacklist attempt.",
                       Request.UserHostAddress);

                    TempData["Unsuccessul"] = "This Applicant is not blacklisted, you can only unblacklist an Applicant that has been blacklisted!";
                }
                else
                {
                    _context.BlackListApplicant.Add(blackListApplicant);
                    _context.SaveChanges();

                    //Update User Table with Blacklist Status
                    user.IsBlackListed = false;
                    user.blacklistedDate = DateTime.Now;
                    user.blacklistedBy = User.Identity.GetUserId();
                    user.blacklistID = blackListApplicant.Id;
                    _context.SaveChanges();
                    TempData["Success"] = "UN-Blacklisted Successfully!";

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Un-blacklisted user. Un-blacklisted User Applicant Number: " + user.ApplicationNumber + "Name: " + user.FirstName + " " + user.LastName,
                       User.Identity.GetUserId(),
                       "Un-blacklisted user.",
                       Request.UserHostAddress);
                }

                return RedirectToAction("AllBlacklistedApplicant", "Admin");
            }
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(model);
        }

        [Authorize(Roles = "ONSA Support, ONSA PGSO, NSA")]
        public ActionResult blacklistHistory(string appID)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            //Get Blacklist History list
            var historyList = _context.BlackListApplicant.OrderByDescending(b => b.BlacklistDate).ToList();
            //Get User List
            var Users = _context.Users.ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited blacklist history page. Visiting User's Application Number: " + user.ApplicationNumber + "Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Visited blacklist history.",
               Request.UserHostAddress);

            return View(new BlacklistViewModel
            {
                user = user,
                role = roles,
                BlacklistHistory = historyList
                //UserList = Users
            });
        }

        [Authorize(Roles = "ONSA Support, ONSA PGSO, NSA")]
        public ActionResult ViewApplicantBlacklistHistory(string appID)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            //Get Blacklist History list
            var historyList = _context.BlackListApplicant.Where(h => h.ApplicantId == appID).ToList();
            //Get User List
            var Users = _context.Users.Where(u => u.Id == appID).FirstOrDefault();
            var userList = _context.Users.ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Viewed applicant blacklist history. Applicant Number: " + appID,
               User.Identity.GetUserId(),
               "Viewed blacklist history.",
               Request.UserHostAddress);

            return View(new BlacklistViewModel
            {
                user = user,
                role = roles,
                BlacklistHistory = historyList,
                UserList = Users,
                AllUsers = userList
            });
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        [Authorize(Roles = "ONSA Admin")]
        public ActionResult agencyManagement()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var agencyList = _context.PortalSubCategories.ToList();
            var agencyDesk = _context.AgencyDesk.ToList();
            var agencyType = _context.PortalCategories.ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Viewed agency management page. Visitor's Applicant Number: " + user.ApplicationNumber,
               User.Identity.GetUserId(),
               "Viewed agency management page.",
               Request.UserHostAddress);

            return View(new AgencyDataViewModel
            {
                user = user,
                role = roles,
                AgencyList = agencyList,
                AgencyDesk = agencyDesk,
                AgencyType = agencyType,
                agency_Type = agencyType
            });
        }

        //Save Agency Details to Database
        [Authorize(Roles = "ONSA Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult agencyManagement(AgencyDataViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Request.Form["submitCreateAgency"] != null)
                {
                    var Agency = new PortalSubCategories
                    {
                        Name = model.agency_Name,
                        agencyEmail = model.agency_Email,
                        agencyPhone = model.agency_Phone,
                        PortalCategoryId = model.agency_TypeId,
                        dateCreated = DateTime.Now,
                        contactName = model.contact_Name
                    };
                    _context.PortalSubCategories.Add(Agency);
                    _context.SaveChanges();
                    TempData["Success"] = "Added Successfully!";

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Added new agency. Agency Name: " + Agency.Name,
                       User.Identity.GetUserId(),
                       "Added new agency.",
                       Request.UserHostAddress);

                    return RedirectToAction("agencyManagement", "Admin");
                }
                else if (Request.Form["submitAgencyDesk"] != null)
                {
                    var AgencyDesk = new AgencyDesk
                    {
                        agencyName = model.agency_NameId,
                        deskName = model.desk_Name,
                        status = "Active"
                    };

                    _context.AgencyDesk.Add(AgencyDesk);
                    _context.SaveChanges();
                    TempData["Success"] = "Added Successfully!";

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Added new agency desk. " + AgencyDesk.agencyName,
                       User.Identity.GetUserId(),
                       "Added new agency desk.",
                       Request.UserHostAddress);

                    return RedirectToAction("agencyManagement", "Admin");
                }
            }
            model.agency_Type = _context.PortalCategories.ToList();
            model.AgencyList = _context.PortalSubCategories.ToList();
            model.AgencyType = _context.PortalCategories.ToList();
            model.AgencyDesk = _context.AgencyDesk.ToList();
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(model);
        }

        [Authorize(Roles = "ONSA Admin")]
        public ActionResult EditAgency(int Id)
        {
            var agency = _context.PortalSubCategories.Where(a => a.Id == Id).FirstOrDefault();

            var viewModel = new AgencyDataViewModel
            {
                Id = Id,
                agency_Name = agency.Name,
                agency_Email = agency.agencyEmail,
                agency_Phone = agency.agencyPhone,
                agency_TypeId = agency.PortalCategoryId,
                contact_Name = agency.contactName
            };
            viewModel.agency_Type = _context.PortalCategories.ToList();
            viewModel.AgencyList = _context.PortalSubCategories.ToList();
            viewModel.AgencyType = _context.PortalCategories.ToList();
            viewModel.AgencyDesk = _context.AgencyDesk.ToList();
            var userId = User.Identity.GetUserId();
            viewModel.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            viewModel.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited edit agency page. Agency Name: " + agency.Name,
               User.Identity.GetUserId(),
               "Visited edit agency page.",
               Request.UserHostAddress);

            return View("EditAgency", viewModel);
        }

        [Authorize(Roles = "ONSA Admin")]
        [HttpPost]
        public ActionResult EditAgency(AgencyDataViewModel model)
        {
            int Id = model.Id;
            var agency = _context.PortalSubCategories.Where(a => a.Id == Id).FirstOrDefault();

            agency.Name = model.agency_Name;
            agency.agencyEmail = model.agency_Email;
            agency.agencyPhone = model.agency_Phone;
            agency.PortalCategoryId = model.agency_TypeId;
            agency.dateCreated = DateTime.Now;
            agency.contactName = model.contact_Name;
            _context.SaveChanges();
            TempData["Success"] = "Updated Successfully!";

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Edited agency details. Agency Name: " + agency.Name,
               User.Identity.GetUserId(),
               "Edited agency details.",
               Request.UserHostAddress);

            return RedirectToAction("agencyManagement", "Admin");
        }
        //Edit Agency Desk
        [Authorize(Roles = "ONSA Admin")]
        public ActionResult EditAgencyDesk(int Id)
        {
            var agencyDesk = _context.AgencyDesk.Where(a => a.Id == Id).FirstOrDefault();

            var viewModel = new AgencyDataViewModel
            {
                DeskId = Id,
                agency_NameId = agencyDesk.agencyName,
                desk_Name = agencyDesk.deskName
            };
            viewModel.agency_Type = _context.PortalCategories.ToList();
            viewModel.AgencyList = _context.PortalSubCategories.ToList();
            viewModel.AgencyType = _context.PortalCategories.ToList();
            viewModel.AgencyDesk = _context.AgencyDesk.ToList();
            var userId = User.Identity.GetUserId();
            viewModel.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            viewModel.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited agency desk.",
               User.Identity.GetUserId(),
               "Visit agency desk.",
               Request.UserHostAddress);

            return View("EditAgencyDesk", viewModel);
        }

        [Authorize(Roles = "ONSA Admin")]
        [HttpPost]
        public ActionResult EditAgencyDesk(AgencyDataViewModel model)
        {
            int Id = model.Id;
            var agencyDesk = _context.AgencyDesk.Where(a => a.Id == Id).FirstOrDefault();

            agencyDesk.agencyName = model.agency_NameId;
            agencyDesk.deskName = model.desk_Name;
            _context.SaveChanges();
            TempData["Success"] = "Updated Successfully!";

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Edit agency desk. Agency Desk Name: " + agencyDesk.deskName,
               User.Identity.GetUserId(),
               "Edited agency desk.",
               Request.UserHostAddress);

            return RedirectToAction("agencyManagement", "Admin");
        }
        //Delete Agency Desk
        public ActionResult DeleteAgencyDesk(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AgencyDesk agencyDesk = _context.AgencyDesk.Find(id);
            if (agencyDesk == null)
            {
                return HttpNotFound();
            }
            AgencyDataViewModel viewModel = new AgencyDataViewModel();
            var userId = User.Identity.GetUserId();
            viewModel.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            viewModel.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("View delete agency desk. Agency Desk Name: " + agencyDesk.deskName,
               User.Identity.GetUserId(),
               "Viewed delete agency desk.",
               Request.UserHostAddress);

            return View(agencyDesk);
        }

        // POST: PersonalDetails/Delete/5
        [HttpPost, ActionName("DeleteAgencyDesk")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AgencyDesk agencyDesk = _context.AgencyDesk.Find(id);
            _context.AgencyDesk.Remove(agencyDesk);
            _context.SaveChanges();
            TempData["Success"] = "Deleted Successfully!";

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Deleted agency desk. Agency Desk Name: " + agencyDesk.deskName,
               User.Identity.GetUserId(),
               "Deleted agency desk.",
               Request.UserHostAddress);

            return RedirectToAction("agencyManagement", "Admin");
        }

        [Authorize(Roles = "ONSA Admin")]
        public ActionResult hsCodeMgt()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var hscode_List = _context.HSCodes.ToList();
            var itemCat_List = _context.EUCItemCategory.ToList();
            var EUCitemCat_List = _context.EUCItemCategory.ToList();
            var HsCodeUserCat_List = _context.HSCodeUserCat.ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited HS Code Management page.",
               User.Identity.GetUserId(),
               "Visited HS code management page.",
               Request.UserHostAddress);

            return View(new HSCodeViewModel
            {
                user = user,
                role = roles,
                HSCodeList = hscode_List,
                itemCatList = itemCat_List,
                EUCItemCat = EUCitemCat_List,
                HsCodeUserCategory = HsCodeUserCat_List
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult hsCodeMgt(HSCodeViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Request.Form["submitCreateItemCat"] != null)
                {
                    var EUCItemCategory = new EUCItemCategory
                    {
                        categoryName = model.itemCatName,
                        userTypeAllowed = model.SelApplicantType
                    };
                    _context.EUCItemCategory.Add(EUCItemCategory);
                    _context.SaveChanges();

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Added new HS Code. Code: " + model.HSCodeName,
                       User.Identity.GetUserId(),
                       "Added HS Code successfully.",
                       Request.UserHostAddress);

                    TempData["Success"] = "Added Successfully!";
                    return RedirectToAction("hsCodeMgt", "Admin");
                }
                else if (Request.Form["submitCreateItemHSCode"] != null)
                {
                    var HSCode = new HSCode
                    {
                        itemCatName = model.itemCat,
                        itemSubCatName = model.itemSubCatHSCode,
                        Name = model.itemSubCatName
                    };

                    _context.HSCodes.Add(HSCode);
                    _context.SaveChanges();
                    TempData["Success"] = "Added Successfully!";

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Added new HS Code. Code: " + model.HSCodeName,
                       User.Identity.GetUserId(),
                       "Added HS Code successfully.",
                       Request.UserHostAddress);

                    return RedirectToAction("hsCodeMgt", "Admin");
                }
            }
            model.HSCodeList = _context.HSCodes.ToList();
            model.itemCatList = _context.EUCItemCategory.ToList();
            model.EUCItemCat = _context.EUCItemCategory.ToList();
            model.HsCodeUserCategory = _context.HSCodeUserCat.ToList();
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(model);
        }

        [Authorize(Roles = "ONSA Admin")]
        public ActionResult EditItemCat(int Id)
        {
            var itemCat = _context.EUCItemCategory.Where(a => a.id == Id).FirstOrDefault();

            var viewModel = new HSCodeViewModel
            {
                Id = Id,
                itemCatName = itemCat.categoryName,
                SelApplicantType = itemCat.userTypeAllowed
            };
            viewModel.HSCodeList = _context.HSCodes.ToList();
            viewModel.itemCatList = _context.EUCItemCategory.ToList();
            viewModel.EUCItemCat = _context.EUCItemCategory.ToList();
            viewModel.HsCodeUserCategory = _context.HSCodeUserCat.ToList();
            var userId = User.Identity.GetUserId();
            viewModel.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            viewModel.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited edit item category page.",
               User.Identity.GetUserId(),
               "Visited item category page.",
               Request.UserHostAddress);

            return View("EditItemCat", viewModel);
        }

        [Authorize(Roles = "ONSA Admin")]
        [HttpPost]
        public ActionResult EditItemCat(HSCodeViewModel model)
        {
            int Id = model.Id;
            var itemCat = _context.EUCItemCategory.Where(a => a.id == Id).FirstOrDefault();

            itemCat.categoryName = model.itemCatName;
            itemCat.userTypeAllowed = model.SelApplicantType;
            _context.SaveChanges();
            TempData["Success"] = "Updated Successfully!";

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Edited item category.",
               User.Identity.GetUserId(),
               "Edited item category.",
               Request.UserHostAddress);

            return RedirectToAction("hsCodeMgt", "Admin");
        }
        //Edit HS Code
        [Authorize(Roles = "ONSA Admin")]
        public ActionResult EditHSCode(int Id)
        {
            var hsCode = _context.HSCodes.Where(a => a.Id == Id).FirstOrDefault();

            var viewModel = new HSCodeViewModel
            {
                CodeId = Id,
                itemCat = hsCode.itemCatName,
                itemSubCatName = hsCode.itemSubCatName,
                itemSubCatHSCode = hsCode.Name
            };
            viewModel.HSCodeList = _context.HSCodes.ToList();
            viewModel.itemCatList = _context.EUCItemCategory.ToList();
            viewModel.EUCItemCat = _context.EUCItemCategory.ToList();
            viewModel.HsCodeUserCategory = _context.HSCodeUserCat.ToList();
            var userId = User.Identity.GetUserId();
            viewModel.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            viewModel.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited edit HS Code page.",
               User.Identity.GetUserId(),
               "Visited edit HS Code page.",
               Request.UserHostAddress);

            return View("EditHSCode", viewModel);
        }

        [Authorize(Roles = "ONSA Admin")]
        [HttpPost]
        public ActionResult EditHSCode(HSCodeViewModel model)
        {
            int Id = model.Id;
            var hsCode = _context.HSCodes.Where(a => a.Id == Id).FirstOrDefault();

            hsCode.itemCatName = model.itemCat;
            hsCode.itemSubCatName = model.itemSubCatName;
            hsCode.Name = model.itemSubCatHSCode;

            _context.SaveChanges();
            TempData["Success"] = "Updated Successfully!";

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Edited HS Code.",
               User.Identity.GetUserId(),
               "Edited HS Code.",
               Request.UserHostAddress);

            return RedirectToAction("hsCodeMgt", "Admin");
        }

        [Authorize(Roles = "ONSA Admin")]
        public ActionResult currencyMgt()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var currency_List = _context.Currencies.ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User visited the currency management page.",
               User.Identity.GetUserId(),
               "Visited currency management page.",
               Request.UserHostAddress);

            return View(new CurrencyMgtViewModel
            {
                user = user,
                role = roles,
                CurrencyList = currency_List
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult currencyMgt(CurrencyMgtViewModel model)
        {
            var user = _context.Users.Find(User.Identity.GetUserId());

            if (ModelState.IsValid)
            {
                var Currency = new Currency
                {
                    Name = model.currencyName,
                    Symbol = model.currencySymbol
                };
                _context.Currencies.Add(Currency);
                _context.SaveChanges();
                TempData["Success"] = "Added Successfully!";

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Successfully added currency through currency management page. Username: " + user.Email + " Currency Name: " + model.currencyName,
                   User.Identity.GetUserId(),
                   "Added currency.",
                   Request.UserHostAddress);

                return RedirectToAction("currencyMgt", "Admin");
            }
            model.CurrencyList = _context.Currencies.ToList();
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Failed to add currency through currency management page. Username: " + user.Email + " Currency Name: " + model.currencyName,
               User.Identity.GetUserId(),
               "Failed to add currency.",
               Request.UserHostAddress);

            return View(model);
        }

        [Authorize(Roles = "ONSA Admin")]
        public ActionResult portMgt()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var stateList = _context.States.ToList();
            var landingPorts = _context.PortsOfLanding.ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited port management page.",
               User.Identity.GetUserId(),
               "Visited port management page.",
               Request.UserHostAddress);

            return View(new PortMgtViewModel
            {
                user = user,
                role = roles,
                State = stateList,
                PortOfLanding = landingPorts
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult portMgt(PortMgtViewModel model)
        {
            var user = _context.Users.Find(User.Identity.GetUserId());

            if (ModelState.IsValid)
            {
                var PortOfLanding = new PortOfLanding
                {
                    Name = model.PortCode,
                    portState = model.StateId
                };
                _context.PortsOfLanding.Add(PortOfLanding);
                _context.SaveChanges();
                TempData["Success"] = "Added Successfully!";

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Successfully created a port of landing on port management page. Username: " + user.Email + " Port Code: " + model.PortCode,
                   User.Identity.GetUserId(),
                   "Successfully added port of landing.",
                   Request.UserHostAddress);

                return RedirectToAction("portMgt", "Admin");
            }
            model.PortOfLanding = _context.PortsOfLanding.ToList();
            model.State = _context.States.ToList();
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Failed to add port of landing through port management page. Username: " + user.Email + " Port Code: " + model.PortCode,
               User.Identity.GetUserId(),
               "Failed to add port of landing.",
               Request.UserHostAddress);

            return View(model);
        }

        [Authorize(Roles = "ONSA Admin")]
        public ActionResult EditPort(int Id)
        {
            var port = _context.PortsOfLanding.Where(a => a.Id == Id).FirstOrDefault();

            var viewModel = new PortMgtViewModel
            {
                Id = Id,
                PortCode = port.Name,
                StateId = port.portState
            };
            viewModel.PortOfLanding = _context.PortsOfLanding.ToList();
            viewModel.State = _context.States.ToList();
            var userId = User.Identity.GetUserId();
            viewModel.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            viewModel.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited edit port of landing page.",
               User.Identity.GetUserId(),
               "Visited edit port of landing page.",
               Request.UserHostAddress);

            return View("EditPort", viewModel);
        }

        [Authorize(Roles = "ONSA Admin")]
        [HttpPost]
        public ActionResult EditPort(PortMgtViewModel model)
        {
            var user = _context.Users.Find(User.Identity.GetUserId());

            int Id = model.Id;
            var port = _context.PortsOfLanding.Where(a => a.Id == Id).FirstOrDefault();

            port.Name = model.PortCode;
            port.portState = model.StateId;
            _context.SaveChanges();
            TempData["Success"] = "Updated Successfully!";

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Successfully edited a port of landing. Username: " + user.Email + " Port Code: " + model.PortCode,
               User.Identity.GetUserId(),
               "Successfully added port of landing.",
               Request.UserHostAddress);

            return RedirectToAction("portMgt", "Admin");
        }

        [Authorize(Roles = "ONSA Admin")]
        public ActionResult applicationAttachType()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited 'application attachment type' page.",
               User.Identity.GetUserId(),
               "Visited 'application attachment type' page.",
               Request.UserHostAddress);

            return View(new DashboardViewModel
            {
                user = user,
                role = roles
            });
        }

        [Authorize(Roles = "ONSA Admin, NSA")]
        public ActionResult auditTrail()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var auditTrail = _context.AuditTrail.ToList();
            var userList = _context.Users.ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited 'audit trail' page.",
               User.Identity.GetUserId(),
               "Visited 'audit trail' page.",
               Request.UserHostAddress);

            return View(new AuditTrailViewModel
            {
                user = user,
                role = roles,
                AuditTrails = auditTrail,
                UserList = userList
            });
        }

        [Authorize(Roles = "ONSA Admin, ONSA Support")]
        public ActionResult userManagement(string id, int? CategoryId)
        {
            if (CategoryId != null)
            {
                //Get ID of the currently logged in user
                var userId = User.Identity.GetUserId();
                //Create user object based off user ID
                var user = _context.Users.Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

                var portalCategories = _context.PortalCategories.Where(p => p.Id == CategoryId).ToList();
                var portalSubCategories = _context.PortalSubCategories.Where(p => p.PortalCategoryId == CategoryId).ToList();
                PortalCategories selectedCategory = _context.PortalCategories.Single(p => p.Id == CategoryId);
                var portalUserList = _context.Users.OrderByDescending(u => u.RequestedDate).ToList();

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Visited 'user management' page.",
                   User.Identity.GetUserId(),
                   "Visited 'user management' page.",
                   Request.UserHostAddress);

                return View(new CreatePortalUserViewModel()
                {
                    CategoryName = selectedCategory.Name,
                    SelectedCategory = CategoryId,
                    user = user,
                    role = roles,
                    Categories = portalCategories,
                    SubCategories = portalSubCategories,
                    MeansOfIdentifications = _context.Identification.ToList(),
                    UserList = portalUserList
                });
            }
            else
            {
                //Get ID of the currently logged in user
                var userId = User.Identity.GetUserId();
                //Create user object based off user ID
                var user = _context.Users.Where(u => u.Id == userId).Single();
                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                var portalCategories = _context.PortalCategories.ToList();
                var portalSubCategories = _context.PortalSubCategories.ToList();
                var portalUserList = _context.Users.OrderByDescending(u => u.RequestedDate).ToList();

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Visited 'user management' page.",
                   User.Identity.GetUserId(),
                   "Visited 'user management' page.",
                   Request.UserHostAddress);

                return View(new CreatePortalUserViewModel()
                {
                    user = user,
                    role = roles,
                    Categories = portalCategories,
                    SubCategories = portalSubCategories,
                    MeansOfIdentifications = _context.Identification.ToList(),
                    UserList = portalUserList
                });
            }
        }
        private async Task<string> SendEmailConfirmationAsync(string userId, string subject)
        {
            await UserManager.SendEmailAsync(userId, subject,
               "Your account on the ONSA End-User Certificate portal has been approved.<br/>"
               + " You can now login to your account and make your application.");
            return subject;
        }
        private void SendSmsConfirmation(string recipient, string message)
        {
            //SMS Sending
            string sender = "ONSA ECIMS";

            string url = "https://www.MultiTexter.com/tools/geturl/Sms.php?username=ocedache@gmail.com&password=Dest1nee&sender="
                            + sender + "&message=" + message +
                            "&flash=0&sendtime=2009-10- 18%2006:30&listname=friends&recipients="
                            + recipient;

            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
            webReq.Method = "GET";
            HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();
            Stream answer = webResponse.GetResponseStream();
            StreamReader _recievedAnswer = new StreamReader(answer);
        }
        [Authorize(Roles = "Agency, Client User, ONSA Admin, ONSA Support, ONSA PGSO, NSA, Exclusive User")]
        public ActionResult EUCApplicationDetails(string appID, int? ID)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var applicant = _context.Users.Where(u => u != null).ToList();
            var company = _context.Company.ToList();
            if (appID == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var eucAppDetails = _context.EndUserCertificate.Where(appD => appD.applicationNo == appID || appD.id == ID).ToList();
            var eucAppAttach = _context.EUCApplicationAttachments.Where(appD => appD.applicationNo == appID).ToList();
            var eucAppItemDetails = _context.EndUserCertificateDetails.Where(appD => appD.applicationNo == appID).ToList();
            var HSCodeList = _context.HSCodes.ToList();
            var itemDescripList = _context.EUCItemCategory.ToList();
            var portOfLanding = _context.PortsOfLanding.ToList();
            var currencies = _context.Currencies.ToList();
            var portalUserCat = _context.PortalCategories.ToList();
            var portalUserSubCat = _context.PortalSubCategories.ToList();
            var DeskName = _context.AgencyDesk.ToList();
            var countries = _context.CountryList.ToList();

            var attachments = _context.EUCApplicationAttachments.Where(appD => appD.applicationNo == appID).ToList();
            //Get Total of cost for EUC Application Item
            ViewBag.Total = eucAppItemDetails.Sum(x => x.cost);
            //Get App Flags and Count total Flags
            int flagCounts = _context.EUCApplicationFlags.Where(o => o.applicationNo == appID).Count();
            var flagList = _context.EUCApplicationFlags.Where(appD => appD.applicationNo == appID).OrderByDescending(date => date.FlagDate).ToList();
            //Get App Comment and Count total Comments
            int commentCount = _context.EUCApplicationComments.Where(o => o.applicationNo == appID).Count();
            var commentList = _context.EUCApplicationComments.Where(appD => appD.applicationNo == appID).OrderByDescending(date => date.FlagDate).ToList();
            //Get App Endorse and Count total Comments
            int endorseCount = _context.EUCApplicationEndorsers.Where(o => o.applicationNo == appID).Count();
            var endorseList = _context.EUCApplicationEndorsers.Where(appD => appD.applicationNo == appID).OrderByDescending(date => date.EndorsementDate).ToList();
            //Get App Reject and Count total Comments
            int rejectCount = _context.EUCRejectComments.Where(o => o.EUCAppID == appID).Count();
            var rejectList = _context.EUCRejectComments.Where(appD => appD.EUCAppID == appID).OrderByDescending(date => date.RejectDate).ToList();
            //Get App Dis-Endorse and Count total Comments
            int disendorseCount = _context.DisendorsedEUCAppComments.Where(o => o.EUCAppID == appID).Count();
            var disendorseList = _context.DisendorsedEUCAppComments.Where(appD => appD.EUCAppID == appID).OrderByDescending(date => date.DisendorsedDate).ToList();

            var EUCOrgAppFor = _context.EUCOrganisations.Where(o => o.ApplicationNumber == appID).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited 'EUC application details' page.",
               User.Identity.GetUserId(),
               "Visited 'EUC application details' page.",
               Request.UserHostAddress);

            return View(new EUCApplicationDetailsViewModel
            {
                user = user,
                role = roles,
                Applicant = applicant,
                Company = company,
                EUCAppDetails = eucAppDetails,
                EUCAppAttach = eucAppAttach,
                EUCAppItemDetails = eucAppItemDetails,
                hsCode = HSCodeList,
                itemCategory = itemDescripList,
                landingPort = portOfLanding,
                Currencies = currencies,
                supportingDocument = attachments,
                FlagCount = flagCounts,
                FlagList = flagList,
                CommentCount = commentCount,
                CommentList = commentList,
                EndorseCount = endorseCount,
                EndorseList = endorseList,
                PortalCat = portalUserCat,
                PortalSubCat = portalUserSubCat,
                EUCOrgList = EUCOrgAppFor,
                AgencyDesk = DeskName,
                Countries = countries,
                rejectCount = rejectCount,
                rejectList = rejectList,
                disendorse = disendorseCount,
                disendorseList = disendorseList
            });
        }

        [Authorize(Roles = "Agency, Client User, ONSA Admin, ONSA Support, ONSA PGSO, NSA, Customs User, Exclusive User")]
        public ActionResult EUCCertificateDetails(string certID)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var eucCertDetail = _context.EUCCertificates.Where(euc => euc.CertNo == certID).ToList();
            var disputeDetail = _context.DisputeEUCCertificates.Where(dispute => dispute.CertNo == certID).ToList();
            var userLIst = _context.Users.ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited 'EUC certificate details' page.",
               User.Identity.GetUserId(),
               "Visited 'EUC certificate details' page.",
               Request.UserHostAddress);

            return View(new EUCCertDetailsViewModel
            {
                user = user,
                role = roles,
                EUCCertificates = eucCertDetail,
                disputeList = disputeDetail,
                UsersToQuery = userLIst
            });
        }

        //Validate supporting documents using ajax
        public ActionResult ValidateSupportingDocument(string document)
        {
            var user = _context.Users.Find(User.Identity.GetUserId());

            //find the corresponding attachment and set it as valid
            var applicationToValidate = _context.EUCAttachmentValidations.Where(eucAttachment => eucAttachment.DocumentName == document).FirstOrDefault();
            if (applicationToValidate != null)
            {
                applicationToValidate.isValidated = true;
                _context.SaveChanges();

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Validated supporting documents of an EUC application. User Validating: " + user.UserName + " Document Name: " + document,
                   User.Identity.GetUserId(),
                   "Validated EUC supporting documents.",
                   Request.UserHostAddress);
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        //Un-Validate supporting documents using ajax
        public ActionResult UnValidateSupportingDocument(string document)
        {
            var user = _context.Users.Find(User.Identity.GetUserId());

            //find the corresponding attachment and set it as valid
            var applicationToValidate = _context.EUCAttachmentValidations.Where(eucAttachment => eucAttachment.DocumentName == document).FirstOrDefault();
            if (applicationToValidate != null)
            {
                applicationToValidate.isValidated = false;
                _context.SaveChanges();

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Un-Validated supporting documents of an EUC application. User Validating: " + user.UserName + " Document Name: " + document,
                   User.Identity.GetUserId(),
                   "Un-Validated EUC supporting documents.",
                   Request.UserHostAddress);
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        private async Task<string> SendEmailConfirmation(string userID, string subject, bool confirmed, string reason)
        {
            //string code = await UserManager.SendEmailAsync(userID, subject, message);
            //var callbackUrl = Url.Action("ConfirmEmail", "Account",
            //   new { userId = userID, code = code }, protocol: Request.Url.Scheme);
            if (confirmed == true)
            {
                await UserManager.SendEmailAsync(userID, subject, "This email is to confirm that your account has been approved on the ONSA"
                                        + " End-User Certificate"
                                        + " portal."
                                        + " <br/>"
                                        + " An SMS has also been sent notifying you of the approval."
                                        + " <br/>"
                                        + " Please visit www.euc.nsa.gov.ng or click this link to login: <a href=\"" + "www.euc.nsa.gov.ng" + "\">here</a>");


                return "Account has been approved.";
            }
            else if (confirmed == false)
            {
                await UserManager.SendEmailAsync(userID, subject, "This email is to inform you that your account has been disapproved on the ONSA"
                                        + " End-User Certificate"
                                        + " portal."
                                        + " <br/>"
                                        + " An SMS has also been sent notifying you of this."
                                        + " <br/>"
                                        + " Your account creation request was rejected for the following reason: " + reason
                                        + " <br/>");

                return "Account has been rejected";
            }
            else
            {
                return "";
            }
        }
        //GET
        public ActionResult ForwardApplication(string AppID)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var ONSAAgency = _context.PortalSubCategories.Where(a => a.Id == 5 || a.Id == 45).ToList();
            var agencyList = _context.PortalSubCategories.Where(a => a.Id != 5).Where(a => a.Id != 45).ToList().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name });

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited forward application page",
               User.Identity.GetUserId(),
               "Visited forward application page",
               Request.UserHostAddress);

            return View(new ForwardApplicationViewModel
            {
                user = user,
                role = roles,
                Agency = agencyList,
                GetONSAAgencies = ONSAAgency,
                EUCID = AppID
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForwardApplication(ForwardApplicationViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Agency = _context.PortalSubCategories.ToList().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name });
                var user = _context.Users.Find(User.Identity.GetUserId());
                var eucApplication = _context.EndUserCertificate.Where(euc => euc.applicationNo == model.EUCID).FirstOrDefault();

                model.Agency = _context.PortalSubCategories.ToList().Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name });
                if (Request.Form["forwardToAgency"] != null)
                {
                    List<SelectListItem> selectedItems = model.Agency.Where(p => model.SelectedAgency.Contains(int.Parse(p.Value))).ToList();
                    var Application = _context.EndUserCertificate.Where(a => a.applicationNo == model.EUCID).FirstOrDefault();
                    foreach (var selectedItem in selectedItems)
                    {
                        //Check if a comment already exists on the application
                        if (Application.EUCApprovalStatus != EUCApprovedStatus.Forwarded)
                        {
                            if (selectedItem.Selected == false)
                            {
                                var ForwardApp = new ForwardEUCAppComment
                                {
                                    ForwardDate = DateTime.Now,
                                    EUCAppID = model.EUCID,
                                    ForwardedBy = user.PortalUserCategoryId.ToString(),
                                    AgencyAssignedTo = selectedItem.Value,
                                    Comment = model.ForwardComment
                                };
                                _context.ForwardEUCAppComment.Add(ForwardApp);
                                _context.SaveChanges();

                                //Update EUC Table with Application Status
                                Application.EUCApprovalStatus = EUCApprovedStatus.Forwarded;
                                Application.AgencyForwardedFrom = user.PortalUserCategoryId;
                                Application.dateForwarded = DateTime.Now;
                                Application.AgencyForwardedTo = Convert.ToInt32(selectedItem.Value);
                                var result = _context.SaveChanges();

                                if (result > 0)
                                {
                                    //Update EUC workflow to reflect a change to the application
                                    Controllers.EUCWorkflowController.EUCWorkflow eucWorkflow = new Controllers.EUCWorkflowController.EUCWorkflow();
                                    eucWorkflow.UpdateEUCWorkflow(DateTime.Now, model.EUCID, user.Id, Application.UserID, "FORWARDED", "PENDING", user.PortalUserCategoryId, 1, Convert.ToInt32(selectedItem.Value));

                                    int agencyCategoryId = Convert.ToInt32(selectedItem.Value);
                                    //Get every user belonging to this agency
                                    var agencyUserList = _context.Users.Where(agencyUsers => agencyUsers.PortalUserCategoryId == agencyCategoryId).ToList();
                                    //Send email notification to every user the selected item's agencies
                                    ForwardEUCNotification forwarder = new ForwardEUCNotification();
                                    await forwarder.SendForwardedEUCEmailToAgency(agencyUserList, model.EUCID, User.Identity.GetUserId(), Request.UserHostAddress);
                                }

                                TempData["Success"] = "This application has been forwarded to the respective agencies for further action!";

                                //Document the entry in the audit trail
                                _auditTrail.CreateAuditTrail("Forwarded EUC. From: " + user.UserName + ". Application Number: " + eucApplication.applicationNo + " Agency Assigned To: " + selectedItem.Text,
                                   User.Identity.GetUserId(),
                                   "Forwarded application page",
                                   Request.UserHostAddress);
                                return RedirectToAction("ForwardedEndUserCertificateApplication", "Admin");
                            }
                        }
                        else
                        {
                            TempData["Unsuccessul"] = "This application has already been forwarded to the respective agencies for further Action!..";

                            //Document the entry in the audit trail
                            _auditTrail.CreateAuditTrail("Forwarded EUC application to Agency. From: " + user.UserName + ". Application Number: " + eucApplication.applicationNo + " Agency Assigned To: " + model.ONSAAgID.ToString(),
                               User.Identity.GetUserId(),
                               "Forwarded application page",
                               Request.UserHostAddress);

                            return RedirectToAction("ForwardedEndUserCertificateApplication", "Admin");
                        }

                    }

                }
                else if (Request.Form["forwardToOnsa"] != null)
                {
                    var Application = _context.EndUserCertificate.Where(a => a.applicationNo == model.EUCID).FirstOrDefault();
                    if (Application.EUCApprovalStatus != EUCApprovedStatus.Requiring_Approval)
                    {
                        var ForwardApp = new ForwardEUCAppComment
                        {
                            ForwardDate = DateTime.Now,
                            EUCAppID = model.EUCID,
                            ForwardedBy = user.PortalUserCategoryId.ToString(),
                            AgencyAssignedTo = model.ONSAAgID.ToString(),
                            Comment = model.ForwardComment
                        };

                        _context.ForwardEUCAppComment.Add(ForwardApp);
                        _context.SaveChanges();

                        //Update EUC Table with Application Status
                        Application.EUCApprovalStatus = EUCApprovedStatus.Requiring_Approval;
                        Application.AgencyForwardedFrom = user.PortalUserCategoryId;
                        Application.AgencyForwardedTo = model.ONSAAgID;
                        var result = _context.SaveChanges();

                        if (result > 0)
                        {
                            Controllers.EUCWorkflowController.EUCWorkflow eucWorkflow = new Controllers.EUCWorkflowController.EUCWorkflow();
                            eucWorkflow.UpdateEUCWorkflow(DateTime.Now, model.EUCID, user.Id, Application.UserID, "FORWARDED", "PENDING", user.PortalUserCategoryId, 1, model.ONSAAgID);
                        }

                        TempData["Success"] = "This Application has been Forwarded to NSA for Approval!..";

                        //Send email to NSA informing him of new EUC application
                        string roleName = "NSA";

                        ////Generate a list of all users in ONSA Support role
                        //var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

                        ////Find the role to search for(ONSA Support)
                        //var role = await roleManager.FindByNameAsync(roleName);

                        //var users = new List<ApplicationUser>();
                        //foreach (var applicant in UserManager.Users.ToList())
                        //{
                        //    if (await UserManager.IsInRoleAsync(applicant.Id, role.Name))
                        //    {
                        //        users.Add(applicant);
                        //    }
                        //}
                        GroupUsersByRole.GroupUsersByRole GroupUsers = new GroupUsersByRole.GroupUsersByRole();
                        var users = GroupUsers.GetUsersInRole(roleName);

                        //Call method to send SMS and E-Mail notifications
                        NSANotification nsaSupportContacter = new NSANotification();
                        await nsaSupportContacter.NotifyNSAOfNewEUCApplication(users, Application.applicationNo);

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("Forwarded EUC application to NSA. From: " + user.UserName + ". Application Number: " + eucApplication.applicationNo + " Agency Assigned To: " + model.ONSAAgID.ToString(),
                           User.Identity.GetUserId(),
                           "Forwarded application page",
                           Request.UserHostAddress);

                        return RedirectToAction("ForwardedEndUserCertificateApplication", "Admin");
                    }
                    else
                    {
                        TempData["Unsuccessul"] = "This Application has already been Forwarded to NSA for Approval..";

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("Forwarded EUC application to ONSA. From: " + user.UserName + ". Application Number: " + eucApplication.applicationNo + " Agency Assigned To: " + model.ONSAAgID.ToString(),
                           User.Identity.GetUserId(),
                           "Forwarded application page",
                           Request.UserHostAddress);

                        return RedirectToAction("ForwardedEndUserCertificateApplication", "Admin");
                    }
                }
            }
            model.GetONSAAgencies = _context.PortalSubCategories.Where(a => a.Name == "ONSA").ToList();
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(model);
        }
        //GET
        public ActionResult ApproveApplication(string AppID)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var eucList = _context.EndUserCertificate.Where(e => e.applicationNo == AppID).ToList();

            //to-do
            var endorseDesk = _context.EUCAppEndorsersDesk.Where(d => d.AppId == AppID).ToList();
            var agencyList = _context.PortalSubCategories.ToList();
            var agencyDesk = _context.AgencyDesk.ToList();
            var agencyCat = _context.PortalCategories.ToList();
            var commentList = _context.ForwardEUCAppComment.Where(appD => appD.EUCAppID == AppID).ToList();
            var applicant = _context.Users.ToList();
            var portalUserCat = _context.PortalCategories.ToList();
            var portalUserSubCat = _context.PortalSubCategories.ToList();


            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited approve application page: User: " + user.UserName,
               User.Identity.GetUserId(),
               "Visited approve application page",
               Request.UserHostAddress);
            return View(new ApproveApplicationViewModel
            {
                user = user,
                role = roles,
                EUCDetails = eucList,
                EndorsementDesk = endorseDesk,
                Agency = agencyList,
                AgencyCat = agencyCat,
                AgencyDesk = agencyDesk,
                EUCID = AppID,
                CommentList = commentList,
                Applicant = applicant,
                PortalCat = portalUserCat
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ApproveApplicationWithToken(ApproveApplicationViewModel model)
        {
            try
            {                
                var userIdNumber = User.Identity.GetUserId();
                var nsaUser = _context.Users.Find(userIdNumber);

                DateTime CurrentDate = DateTime.Now;
                var Application = _context.EndUserCertificate.Where(a => a.applicationNo == model.EUCID).FirstOrDefault();
                var EUCUser = _context.Users.Where(a => a.Id == Application.UserID).FirstOrDefault();
                var WorkFlow = _context.EUCWorkFlow.Where(a => a.applicationNumber == model.EUCID && a.handledStatus == "FORWARDED").FirstOrDefault();

                if (string.IsNullOrEmpty(model.TokenCode))
                {
                    TempData["Unsuccessul"] = "Please enter the Authentication Code generated from your authenticating device before you can proceed";
                    return RedirectToAction("ApproveApplication", "Admin", new { AppID = Application.applicationNo });
                }
                else
                {
                    //Validate NSA's token google authenticator app
                    GoogleAuthenticator.GoogleAuthenticatorController googleAuthenticator = new GoogleAuthenticator.GoogleAuthenticatorController();
                    bool isTokenValid = googleAuthenticator.IsTwoFactorAuthentic(model.TokenCode, nsaUser.Id);

                    if (isTokenValid == false)
                    {
                        TempData["Unsuccessul"] = "The authentication code provided is incorrect.";
                        return RedirectToAction("ApproveApplication", "Admin", new { AppID = Application.applicationNo });
                    }
                }
                if (Application.EUCApprovalStatus != EUCApprovedStatus.Approved)
                {
                    var ApproveApp = new ApproveEUCApp
                    {
                        AproveDate = DateTime.Now,
                        AppID = model.EUCID,
                        ApprovedBy = User.Identity.GetUserId(),
                        ApproveToken = ""
                    };
                    _context.ApproveEUCApp.Add(ApproveApp);
                    var result = _context.SaveChanges();
                    if (result > 0)
                    {
                        //Send token code to NSA via sms and text
                        string roleName = "NSA";

                        //Generate a list of all users in ONSA Support role
                        //Find the role to search for(ONSA Support)
                        GroupUsersByRole.GroupUsersByRole groupUsersByRole = new GroupUsersByRole.GroupUsersByRole();
                        var nsaUsers = groupUsersByRole.GetUsersInRole(roleName);

                        //Call method to send SMS and E-Mail notifications
                        NSANotification nsaNotification = new NSANotification();
                        await nsaNotification.SendToken(nsaUsers, Application.applicationNo);

                        //Generate token code
                        TokenCodeGenerator.TokenCodeGenerator tokenCodeGenerator = new TokenCodeGenerator.TokenCodeGenerator();
                        //10 digit token code
                        string tokenCode = TokenCodeGenerator.TokenCodeGenerator.GenerateToken(10);

                        //Save token code in database for verification
                        var EUCApplication = _context.EndUserCertificate.Where(euc => euc.applicationNo == Application.applicationNo).FirstOrDefault();
                        EUCApplication.TokenCode = tokenCode;
                        //Update Token code for the EUC Application on the secondary table
                        ApproveApp.ApproveToken = tokenCode;
                        //Update EUC Table with Application Status
                        Application.EUCApprovalStatus = EUCApprovedStatus.Approved;
                        Application.certificateStatus = CertificateStatus.Pending;
                        Application.AgencyForwardedFrom = nsaUser.PortalUserCategoryId;
                        //DIFFERENTIATE BETWEEN NSA AND .NSA USERS
                        Application.AgencyForwardedTo = 5;
                        Application.dateForwarded = DateTime.Now;
                        Application.ApprovedBy = userIdNumber;
                        //Update application with token code
                        _context.SaveChanges();
                        //Create Certificate for the EUC Application
                        var EUCCert = new EUCCertificates
                        {
                            CertNo = ApplicationNumberGenerator.ApplicationNumberGenerator.GenerateCertificateNumber(),
                            UserId = EUCUser.Id,
                            ExpiryDate = CurrentDate.AddYears(1),
                            IssuanceDate = DateTime.Now,
                            CertStatus = CertificateStatus.Pending,
                            AgencyId = EUCUser.PortalUserCategoryId,
                            applicationNo = model.EUCID
                        };
                        _context.EUCCertificates.Add(EUCCert);

                        //Create the certificate
                        var certificateResult = _context.SaveChanges();


                        ////Make entry into workflow table
                        //var WorkflowEntry = new EUCWorkFlow()
                        //{
                        //    actionReciepient = Application.UserID,
                        //    actionSource = userIdNumber,
                        //    activityDate = DateTime.Now,
                        //    handledStatus = "APPROVED",
                        //    currentStatus = "APPROVED",
                        //    SourceAgency = EUCUser.PortalUserCategoryId,
                        //    isWorkedOn = 1,
                        //    AgencyWorkedOn = EUCUser.PortalUserCategoryId,
                        //    applicationNumber = model.EUCID
                        //};
                        //_context.EUCWorkFlow.Add(WorkflowEntry);
                        //certificateResult = _context.SaveChanges();

                        //Notify the user that the certificate has been approved via SMS and Email
                        if (certificateResult > 0)
                        {
                            //Update EUC Workflow Table with Application worked on Status
                            model.workedOn = 1;
                            WorkFlow.isWorkedOn = model.workedOn;

                            _context.SaveChanges();

                            EUCWorkflowController.EUCWorkflow eucWorkflow = new EUCWorkflowController.EUCWorkflow();
                            eucWorkflow.UpdateEUCWorkflow(DateTime.Now, model.EUCID, nsaUser.Id, "End User", "AWAITING APPROVAL", "APPROVED", nsaUser.PortalUserCategoryId, 1, nsaUser.PortalUserCategoryId);

                            ApplicantNotification notifyApplicant = new ApplicantNotification();
                            await notifyApplicant.NotifyApplicantOfApprovedCertificate(EUCUser, Application.applicationNo, Application.TokenCode);

                            ////Send the collection code to ONSA support via email and sms
                            //GroupUsersByRole.GroupUsersByRole groupUsersByRole = new GroupUsersByRole.GroupUsersByRole();
                            //var onsaSupportUsers = groupUsersByRole.GetUsersInRole("ONSA Support");

                            //ONSASupportNotification onsaSupportNotification = new ONSASupportNotification();
                            //await onsaSupportNotification.NotifyONSASupportOfApprovedCertificate(onsaSupportUsers, Application.applicationNo, Application.TokenCode);
                        }

                        TempData["Success"] = "Approved Successfully!";
                        return RedirectToAction("EndUserCertificatesRequiringApproval", "Admin");
                    }
                }
                else if (Application.TokenCode != model.TokenCode)
                {
                    TempData["Unsuccessul"] = "The authentication code provided is incorrect.";
                    return RedirectToAction("ApproveApplication", "Admin", new { AppID = Application.applicationNo });
                }
                else
                {
                    TempData["Unsuccessul"] = "This application has already been approved, awaiting EUC issuance!..";
                    return RedirectToAction("ApproveApplication", "Admin", new { AppID = Application.applicationNo });
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            return RedirectToAction("EndUserCertificatesRequiringApproval", "Admin");

        }

        public ActionResult RejectApplication(string AppID)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited reject application page: User: " + user.UserName,
               User.Identity.GetUserId(),
               "Visited reject application page",
               Request.UserHostAddress);

            return View(new RejectApplicationViewModel
            {
                user = user,
                role = roles,
                EUCID = AppID
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RejectApplication(RejectApplicationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var Application = _context.EndUserCertificate.Where(a => a.applicationNo == model.EUCID).FirstOrDefault();
                if (Application.EUCApprovalStatus != EUCApprovedStatus.Rejected)
                {
                    var userIdNumber = User.Identity.GetUserId();
                    var user = _context.Users.Find(userIdNumber);

                    var RejectApp = new EUCRejectComments
                    {
                        RejectDate = DateTime.Now,
                        EUCAppID = model.EUCID,
                        RejectedBy = user.PortalUserCategoryId.ToString(),
                        RejectComment = model.RejectComment
                    };
                    _context.EUCRejectComments.Add(RejectApp);
                    _context.SaveChanges();
                    //Update EUC Table with Application Status
                    Application.EUCApprovalStatus = EUCApprovedStatus.Rejected;
                    Application.AgencyForwardedFrom = user.PortalUserCategoryId;
                    Application.AgencyForwardedTo = 5;
                    Application.dateForwarded = DateTime.Now;
                    var result = _context.SaveChanges();
                    //Enter reject workflow entry
                    //Enter reject workflow entry
                    var eucWorkFlow = new EUCWorkFlow
                    {
                        activityDate = DateTime.Now,
                        applicationNumber = Application.applicationNo,
                        actionSource = "",
                        actionReciepient = User.Identity.GetUserId(),
                        handledStatus = "REJECTED",
                        currentStatus = "REJECTED"
                    };
                    _context.EUCWorkFlow.Add(eucWorkFlow);
                    int workflowEntry = _context.SaveChanges();

                    if (result > 0)
                    {
                        //Get the user who made the application
                        var userWhoMadeTheApplication = _context.Users.Find(Application.UserID);
                        //Send the user Email and SMS notifying them of the failed application
                        RejectEUCNotification rejecter = new RejectEUCNotification();
                        await rejecter.RejectApplicantEUCApplication(userWhoMadeTheApplication, model.EUCID, model.RejectComment, User.Identity.GetUserId(), Request.UserHostAddress);
                        //make entry into euc workflow table
                        Controllers.EUCWorkflowController.EUCWorkflow eucWorkflow = new Controllers.EUCWorkflowController.EUCWorkflow();
                        eucWorkflow.UpdateEUCWorkflow(DateTime.Now, model.EUCID, user.Id, Application.UserID, "ENDORSED", "REJECTED", Application.AgencyForwardedTo, 1, user.PortalUserCategoryId);
                    }

                    TempData["Success"] = "Rejected Successfully!";
                    return RedirectToAction("RejectedEndUserCertificateApplication", "Admin");
                }
                else
                {
                    TempData["Unsuccessul"] = "This application has already been rejected!..";
                    return RedirectToAction("RejectedEndUserCertificateApplication", "Admin");
                }
            }
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(model);
        }
        public ActionResult EndorseApplication(string AppID)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Step 1: Retrieve the agency(portalSubCategory) the user belongs to
            var userAgency = _context.PortalSubCategories.Where(portalCategory => portalCategory.Id == user.PortalUserCategoryId).FirstOrDefault();
            //Step 2: Retrieve all desks under that agency for display on the view
            var agencyDesks = _context.AgencyDesk.Where(agencyDesk => agencyDesk.agencyName == userAgency.Id).ToList();
            //Step 3: Return the list of desks to the user
            string agencyName = userAgency.Name;
            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited endorse application page: User: " + user.UserName,
               User.Identity.GetUserId(),
               "Visited endorse application page",
               Request.UserHostAddress);

            return View(new EndorseApplicationViewModel
            {
                user = user,
                role = roles,
                agencyDesks = agencyDesks,
                agencyName = agencyName,
                EUCID = AppID
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EndorseApplication(EndorseApplicationViewModel model)
        {
            var user = _context.Users.Find(User.Identity.GetUserId());
            var userAgency = _context.PortalSubCategories.Where(a => a.Id == user.PortalUserCategoryId).SingleOrDefault();

            if (ModelState.IsValid)
            {
                //Check for the Application number
                var Application = _context.EndUserCertificate.Where(a => a.applicationNo == model.EUCID).FirstOrDefault();
                var WorkFlow = _context.EUCWorkFlow.Where(a => a.applicationNumber == model.EUCID && a.handledStatus == "FORWARDED").FirstOrDefault();
                //Check if Application has already been endorsed
                if (Application.EUCApprovalStatus != EUCApprovedStatus.Endorsed)// If Application is not Endorsed, save a fresh endorse comment
                {
                    var EndorseApp = new EUCApplicationEndorsers
                    {
                        EndorsementDate = DateTime.Now,
                        applicationNo = model.EUCID,
                        userFrom = user.Id,
                        EndorseComment = model.EndorseComment,
                        EndorsementDesk = model.EndorseDesk,
                        AgencyName = user.PortalUserCategoryId,
                        AgencyType = userAgency.Id
                    };
                    _context.EUCApplicationEndorsers.Add(EndorseApp);
                    _context.SaveChanges();
                    //Update EUC Table with Application Status
                    Application.EUCApprovalStatus = EUCApprovedStatus.Endorsed;
                    Application.AgencyForwardedFrom = user.PortalUserCategoryId;
                    Application.AgencyForwardedTo = 5;
                    Application.dateForwarded = DateTime.Now;
                    var result = _context.SaveChanges();

                    //Create new EUC Workflow entry
                    if (result > 0)
                    {
                        //Update EUC Workflow Table with Application worked on Status
                        model.workedOn = 1;
                        WorkFlow.isWorkedOn = model.workedOn;
                        _context.SaveChanges();

                        Controllers.EUCWorkflowController.EUCWorkflow eucWorkflow = new Controllers.EUCWorkflowController.EUCWorkflow();
                        eucWorkflow.UpdateEUCWorkflow(DateTime.Now, model.EUCID, user.Id, Application.UserID, "ENDORSED", "FORWARDED", user.PortalUserCategoryId, 1, Application.AgencyForwardedTo);
                    }
                    //Inform support that an application has been endorsed
                    string roleName = "ONSA Support";

                    //Generate a list of all users in ONSA Support role
                    var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

                    //Find the role to search for(ONSA Support)
                    //var role = await roleManager.FindByNameAsync(roleName);
                    GroupUsersByRole.GroupUsersByRole groupUsers = new GroupUsersByRole.GroupUsersByRole();
                    var users = groupUsers.GetUsersInRole(roleName);
                    //    = new List<User>();
                    //foreach (var applicant in UserManager.Users.ToList())
                    //{
                    //    if (await UserManager.IsInRoleAsync(applicant.Id, role.Name))
                    //    {
                    //        users.Add(applicant);
                    //    }
                    //}
                    ////Get name of processing agency
                    var agency = _context.PortalSubCategories.Find(Application.AgencyForwardedFrom);

                    //Call method to send SMS and E-Mail notifications
                    ONSASupportNotification onsaSupportContacter = new ONSASupportNotification();
                    await onsaSupportContacter.NotifyONSASupportOfEndorsedEUCApplication(users, Application.applicationNo, agency.Name);

                    TempData["Success"] = "Endorsed Successfully!";

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Endorsed EUC application. User who endorsed: " + user.UserName + ". Application endorsed: " + Application.applicationNo + ". Comment: " + model.EndorseComment,
                       User.Identity.GetUserId(),
                       "Endorsed EUC application.",
                       Request.UserHostAddress);

                    return RedirectToAction("EndorsedEndUserCertificateApplication", "Admin");
                }
                else//else dont save and notify user that application has already been endorsed
                {
                    TempData["Unsuccessul"] = "This application has already been endorsed!..";

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Attempted to endorse EUC application but the application has already been endorsed. User who attempted to endorse: " + user.UserName + ". Application number: " + Application.applicationNo,
                       User.Identity.GetUserId(),
                       "Attempted to endorse an already endorsed EUC application.",
                       Request.UserHostAddress);

                    return RedirectToAction("EndorsedEndUserCertificateApplication", "Admin");
                }
            }
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            return View("EndorsedEndUserCertificateApplication", "Admin");
        }

        public ActionResult DisendorseApplication(string AppID)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited dis-endorse application page: User: " + user.UserName,
               User.Identity.GetUserId(),
               "Visited dis-endorse approve application page",
               Request.UserHostAddress);

            return View(new DisendorseApplicationViewModel
            {
                user = user,
                role = roles,
                EUCID = AppID
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DisendorseApplication(DisendorseApplicationViewModel model)
        {
            var user = _context.Users.Find(User.Identity.GetUserId());
            var application = _context.EndUserCertificate.Where(euc => euc.applicationNo == model.EUCID).FirstOrDefault();

            if (ModelState.IsValid)
            {
                var Application = _context.EndUserCertificate.Where(a => a.applicationNo == model.EUCID).FirstOrDefault();
                var WorkFlow = _context.EUCWorkFlow.Where(a => a.applicationNumber == model.EUCID && a.handledStatus == "FORWARDED").FirstOrDefault();
                if (Application.EUCApprovalStatus != EUCApprovedStatus.Disendorsed)
                {
                    //Add Disendorse comment to disendorse table
                    var DisendorseApp = new DisendorsedEUCAppComments
                    {
                        DisendorsedDate = DateTime.Now,
                        EUCAppID = model.EUCID,
                        DisendorsedBy = user.PortalUserCategoryId.ToString(),
                        DisendorsedComment = model.DisendorseComment
                    };
                    _context.DisendorsedEUCAppComments.Add(DisendorseApp);
                    _context.SaveChanges();
                    //Update EUC Table with Application Status
                    Application.EUCApprovalStatus = EUCApprovedStatus.Disendorsed;
                    Application.AgencyForwardedFrom = user.PortalUserCategoryId;
                    Application.AgencyForwardedTo = 5;
                    Application.dateForwarded = DateTime.Now;
                    var result = _context.SaveChanges();

                    //Create new EUC Workflow entry
                    if (result > 0)
                    {
                        //Update EUC Workflow Table with Application worked on Status
                        model.workedOn = 1;
                        WorkFlow.isWorkedOn = model.workedOn;
                        _context.SaveChanges();

                        Controllers.EUCWorkflowController.EUCWorkflow eucWorkflow = new Controllers.EUCWorkflowController.EUCWorkflow();
                        eucWorkflow.UpdateEUCWorkflow(DateTime.Now, model.EUCID, user.Id, Application.UserID, "DISENDORSED", "FORWARDED", user.PortalUserCategoryId, 1, Application.AgencyForwardedTo);
                    }
                    TempData["Success"] = "Disendorsed Successfully!";

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Dis-endorsed EUC application. User who dis-endorsed: " + user.UserName + ". Application number: " + application.applicationNo + ". Comment: " + model.DisendorseComment,
                       User.Identity.GetUserId(),
                       "Dis-endorsed EUC application.",
                       Request.UserHostAddress);

                    //Inform support that an application has been endorsed
                    string roleName = "ONSA Support";

                    //Generate a list of all users in ONSA Support role
                    var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

                    //Find the role to search for(ONSA Support)
                    var role = await roleManager.FindByNameAsync(roleName);

                    //Agency of approving user
                    var agencyName = _context.PortalSubCategories.Find(user.PortalUserCategoryId).Name;

                    GroupUsersByRole.GroupUsersByRole groupUsers = new GroupUsersByRole.GroupUsersByRole();
                    var users = groupUsers.GetUsersInRole(roleName);

                    //    = new List<ApplicationUser>();
                    //foreach (var applicant in UserManager.Users.ToList())
                    //{
                    //    if (await UserManager.IsInRoleAsync(applicant.Id, role.Name))
                    //    {
                    //        users.Add(applicant);
                    //    }
                    //}
                    //Get name of processing agency
                    var agency = _context.PortalSubCategories.Find(Application.AgencyId);

                    //Notify ONSA Support of disendorsed application
                    ONSASupportNotification notifySupportOfDisendorsedApplication = new ONSASupportNotification();
                    await notifySupportOfDisendorsedApplication.NotifyONSASupportOfDisEndorsedEUCApplication(users, application.applicationNo, agencyName);

                    return RedirectToAction("DisendorsedEndUserCertificateApplication", "Admin");
                }
                else
                {
                    TempData["Unsuccessul"] = "This application has already been disendorsed!..";

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Attempted to dis-endorse EUC application but the application has already been dis-endorsed. User who attempted to dis-endorse: " + user.UserName + ". Application number: " + application.applicationNo,
                       User.Identity.GetUserId(),
                       "Attempted to dis-endorse an already dis-endorsed EUC application.",
                       Request.UserHostAddress);

                    return RedirectToAction("DisendorsedEndUserCertificateApplication", "Admin");
                }
            }
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(model);
        }
        //Forward an application to an agency desk for action
        public ActionResult SaveEndorsersDesk(int Id, string appId, int endorsementDesk)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Check if the agency aexists
            var checkAgencyDesk = _context.EUCAppEndorsersDesk.Where(desk => desk.AgencyId == Id && desk.AppId == appId && desk.EndorsementDesk == endorsementDesk).FirstOrDefault();
            if (checkAgencyDesk == null)
            {
                //Save endorsement desk in DB
                var endorsersDesk = new EUCAppEndorsersDesk
                {
                    AgencyId = Id,
                    AppId = appId,
                    EndorsementDesk = endorsementDesk,
                    EndorsedBy = userId
                };
                _context.EUCAppEndorsersDesk.Add(endorsersDesk);
                int result = _context.SaveChanges();
                if (result > 0)
                {
                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("User saved endorsers desk to EUC application: User: " + user.UserName + " Appication ID: " + appId,
                       User.Identity.GetUserId(),
                       "Saved endorsers desk.",
                       Request.UserHostAddress);
                }
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        //Remove a forwarded application from an agency's desk
        public ActionResult UnSaveEndorsersDesk(int Id, string appId, int endorsementDesk)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            //Get ID of the agency desk in use
            var endorsedesk = _context.AgencyDesk.Where(endorsementdesk => endorsementdesk.Id == endorsementDesk).FirstOrDefault();

            //Check if the agency aexists
            var checkAgencyDesk = _context.EUCAppEndorsersDesk.Where(desk => desk.AgencyId == Id && desk.AppId == appId && desk.EndorsementDesk == endorsementDesk).FirstOrDefault();
            if (checkAgencyDesk != null)
            {
                _context.EUCAppEndorsersDesk.Remove(checkAgencyDesk);
                int result = _context.SaveChanges();
                if (result > 0)
                {
                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("User removed endorsers desk from EUC application: User: " + user.UserName + " Appication Number: " + appId + " Endorsement Desk Removed: " + endorsedesk.deskName,
                       User.Identity.GetUserId(),
                       "Removed endorsers desk.",
                       Request.UserHostAddress);
                }
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        private async Task<string> SendEmailConfirmationTokenAsync(string userID, string subject, string message)
        {
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(userID);
            var callbackUrl = Url.Action("ConfirmEmail", "Account",
               new { userId = userID, code = code }, protocol: Request.Url.Scheme);
            await UserManager.SendEmailAsync(userID, subject, message);

            return callbackUrl;
        }
        private async Task<string> SendSmsConfirmationTokenAsync(string userID, string subject)
        {
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(userID);
            var callbackUrl = Url.Action("ConfirmEmail", "Account",
               new { userId = userID, code = code }, protocol: Request.Url.Scheme);
            await UserManager.SendSmsAsync(userID, "Please confirm your account by clicking <a href=\""
                                                    + callbackUrl + "\">here</a>");

            return callbackUrl;
        }

        [Authorize(Roles = "Agency, ONSA Admin, ONSA Support, ONSA PGSO, NSA")]
        public ActionResult DownloadCert(string AppID)
        {
            IssueCertificateViewModel model = new IssueCertificateViewModel();
            model.EUCID = AppID;

            var stream = new MemoryStream();

            //Get certificate record for this ID
            var EUCCert = _context.EUCCertificates.Where(a => a.applicationNo == model.EUCID).FirstOrDefault();
            var EUCApplication = _context.EndUserCertificate.Where(a => a.applicationNo == model.EUCID).FirstOrDefault();
            //Get EUC User for Application
            var EUCUser = _context.Users.Where(u => u.Id == EUCApplication.UserID).FirstOrDefault();
            if (EUCCert != null)
            {
                //Check which currency type is application based on to generate report accordingly
                if (EUCApplication.currencyOfPurchase == 3)
                {
                    //Check if EUC application is made for Agency or Individual
                    if (EUCUser.CategoryList == CategoryList.Individual)
                    {
                        if (EUCApplication.IsOrganisation == false)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateNGNInd report = new PrintCertificateNGNInd();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                        else if (EUCApplication.IsOrganisation == true)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateNGNOrg report = new PrintCertificateNGNOrg();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                    }
                    else if (EUCUser.CategoryList == CategoryList.Corporate || EUCUser.CategoryList == CategoryList.Exclusive)
                    {
                        if (EUCApplication.IsOrganisation == false)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateNGN report = new PrintCertificateNGN();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                        else if (EUCApplication.IsOrganisation == true)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateNGNOrg report = new PrintCertificateNGNOrg();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                    }

                }
                else if (EUCApplication.currencyOfPurchase == 1)
                {
                    if (EUCUser.CategoryList == CategoryList.Individual)
                    {
                        if (EUCApplication.IsOrganisation == false)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateUSDInd report = new PrintCertificateUSDInd();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                        else if (EUCApplication.IsOrganisation == true)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateOrg report = new PrintCertificateOrg();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                    }
                    else if (EUCUser.CategoryList == CategoryList.Corporate || EUCUser.CategoryList == CategoryList.Exclusive)
                    {
                        if (EUCApplication.IsOrganisation == false)
                        {
                            //Generate Certificate Preview 
                            PrintCertificate report = new PrintCertificate();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                        else if (EUCApplication.IsOrganisation == true)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateOrg report = new PrintCertificateOrg();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                    }

                }
                else if (EUCApplication.currencyOfPurchase == 2)
                {
                    if (EUCUser.CategoryList == CategoryList.Individual)
                    {
                        if (EUCApplication.IsOrganisation == false)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateGBPInd report = new PrintCertificateGBPInd();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                        else if (EUCApplication.IsOrganisation == true)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateGBPOrg report = new PrintCertificateGBPOrg();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                    }
                    else if (EUCUser.CategoryList == CategoryList.Corporate || EUCUser.CategoryList == CategoryList.Exclusive)
                    {
                        if (EUCApplication.IsOrganisation == false)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateGBP report = new PrintCertificateGBP();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                        else if (EUCApplication.IsOrganisation == true)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateGBPOrg report = new PrintCertificateGBPOrg();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                    }

                }
                else if (EUCApplication.currencyOfPurchase == 12)
                {
                    if (EUCUser.CategoryList == CategoryList.Individual)
                    {
                        if (EUCApplication.IsOrganisation == false)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateEURInd report = new PrintCertificateEURInd();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                        else if (EUCApplication.IsOrganisation == true)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateEUROrg report = new PrintCertificateEUROrg();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }

                    }
                    else if (EUCUser.CategoryList == CategoryList.Corporate || EUCUser.CategoryList == CategoryList.Exclusive)
                    {
                        if (EUCApplication.IsOrganisation == false)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateEUR report = new PrintCertificateEUR();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                        else if (EUCApplication.IsOrganisation == true)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateEUROrg report = new PrintCertificateEUROrg();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }

                    }

                }
                else
                {
                    if (EUCUser.CategoryList == CategoryList.Individual)
                    {
                        if (EUCApplication.IsOrganisation == false)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateIndGen report = new PrintCertificateIndGen();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                        else if (EUCApplication.IsOrganisation == true)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateOrgGen report = new PrintCertificateOrgGen();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                    }
                    else if (EUCUser.CategoryList == CategoryList.Corporate || EUCUser.CategoryList == CategoryList.Exclusive)
                    {
                        if (EUCApplication.IsOrganisation == false)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateGen report = new PrintCertificateGen();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                        else if (EUCApplication.IsOrganisation == true)
                        {
                            //Generate Certificate Preview 
                            PrintCertificateOrgGen report = new PrintCertificateOrgGen();
                            report.Parameters["SELAPPNO"].Value = AppID;
                            report.Parameters["SELAPPNO"].Visible = false;
                            stream = new MemoryStream();
                            report.ExportToPdf(stream);
                            //return File(stream.GetBuffer(), "application/pdf");
                        }
                    }
                }
            }

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("EUC Certificate Printed for Application Number: " + AppID + "",
               User.Identity.GetUserId(),
               "Printed EUC Certificate.",
               Request.UserHostAddress);

            return File(stream.GetBuffer(), "application/pdf");
        }

        [Authorize(Roles = "Agency, ONSA Admin, ONSA PGSO, NSA, ONSA Support")]
        public ActionResult CollectCertificate(string CertNo)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Viewed collect certificate page. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Viewed collect certificate page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(new CollectCertificateViewModel
            {
                user = user,
                role = roles,
                CertNumber = CertNo
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CollectCertificate(CollectCertificateViewModel model)
        {
            DateTime CurrentDate = DateTime.Now;
            var stream = new MemoryStream();
            var userId = User.Identity.GetUserId();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;

            var user = User.Identity.GetUserId();
            var EUCCert = _context.EUCCertificates.Where(a => a.applicationNo == model.CertNumber).FirstOrDefault();
            //Get certificate record for this ID
            var EUCApplication = _context.EndUserCertificate.Where(a => a.applicationNo == model.CertNumber).FirstOrDefault();
            //Get EUC User for Application
            var EUCUser = _context.Users.Where(u => u.Id == EUCApplication.UserID).FirstOrDefault();
            //Create certificate workflow entry
            //Create NSA approval entry based on NSA's approval date/time in the workflow table
            var NSAApprovalWorkflowEntry = _context.EUCWorkFlow.Where(workflow => workflow.applicationNumber == EUCApplication.applicationNo && workflow.currentStatus == "APPROVED").FirstOrDefault();

            //Enter NSA approval entry into the certificate workflow view
            var CertificateWorkflow = new CertificateWorkflow()
            {
                ActionTaken = "Application Approved. Application number: " + EUCApplication.applicationNo,
                CertificateId = EUCCert.id,
                DateOfAction = NSAApprovalWorkflowEntry.activityDate,
                UserId = NSAApprovalWorkflowEntry.actionSource
            };
            _context.CertificateWorkflow.Add(CertificateWorkflow);
            int result = _context.SaveChanges();

            //Enter token delivery entry into the certificate workflow view
            CertificateWorkflow = new CertificateWorkflow()
            {
                ActionTaken = "Collection token code sent to end-user. End-User: " + EUCUser.FirstName + " " + EUCUser.LastName,
                CertificateId = EUCCert.id,
                DateOfAction = NSAApprovalWorkflowEntry.activityDate,
                UserId = NSAApprovalWorkflowEntry.actionSource
            };
            _context.CertificateWorkflow.Add(CertificateWorkflow);
            result = _context.SaveChanges();

            CertificateWorkflow = new CertificateWorkflow()
            {
                ActionTaken = "Issued certificate to end-user. Certificate number: " + EUCCert.CertNo + " Applicant Name: " +
                EUCUser.FirstName + " " + EUCUser.LastName + " Application Number: " + EUCApplication.applicationNo,
                CertificateId = EUCCert.id,
                DateOfAction = DateTime.Now,
                UserId = user
            };
            _context.CertificateWorkflow.Add(CertificateWorkflow);
            result = _context.SaveChanges();

            //Compare token collected to user against EUC Token
            var CheckToken = _context.EndUserCertificate.Where(token => token.TokenCode == model.CollectionToken).FirstOrDefault();
            if (ModelState.IsValid)
            {
                if (CheckToken == null)
                {
                    TempData["Wrong_Token"] = "Invalid Token Provided! Please Enter a different Token or ensure Token is typed correctly!";
                    model.user = _context.Users.Where(u => u.Id == userId).Single();
                    model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                    return View(model);
                }
                else
                {
                    if (EUCCert != null)
                    {
                        //Check which currency type is application based on to generate report accordingly
                        if (EUCApplication.currencyOfPurchase == 3)
                        {
                            //Update EUC Certificate Sttus
                            EUCCert.CertNo = model.NewCertNumber;
                            EUCCert.CertStatus = CertificateStatus.Issued;
                            EUCCert.ExpiryDate = CurrentDate.AddYears(1);
                            EUCCert.IssuanceDate = DateTime.Now;
                            _context.SaveChanges();

                            TempData["Success"] = "Certificate Collected Successfully!";
                            //Check if EUC application is made for Agency or Individual
                            if (EUCUser.CategoryList == CategoryList.Individual)
                            {
                                if (EUCApplication.IsOrganisation == false)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateNGNInd report = new PrintCertificateNGNInd();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                                else if (EUCApplication.IsOrganisation == true)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateNGNOrg report = new PrintCertificateNGNOrg();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                            }
                            else if (EUCUser.CategoryList == CategoryList.Corporate || EUCUser.CategoryList == CategoryList.Exclusive)
                            {
                                if (EUCApplication.IsOrganisation == false)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateNGN report = new PrintCertificateNGN();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                                else if (EUCApplication.IsOrganisation == true)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateNGNOrg report = new PrintCertificateNGNOrg();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                            }
                        }
                        else if (EUCApplication.currencyOfPurchase == 1)
                        {
                            //Update EUC Certificate Sttus
                            EUCCert.CertNo = model.NewCertNumber;
                            EUCCert.CertStatus = CertificateStatus.Issued;
                            EUCCert.IssuanceDate = DateTime.Now;
                            EUCCert.ExpiryDate = CurrentDate.AddYears(1);
                            _context.SaveChanges();

                            TempData["Success"] = "Certificate Collected Successfully!";

                            if (EUCUser.CategoryList == CategoryList.Individual)
                            {
                                if (EUCApplication.IsOrganisation == false)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateUSDInd report = new PrintCertificateUSDInd();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                                else if (EUCApplication.IsOrganisation == true)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateOrg report = new PrintCertificateOrg();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                            }
                            else if (EUCUser.CategoryList == CategoryList.Corporate || EUCUser.CategoryList == CategoryList.Exclusive)
                            {
                                if (EUCApplication.IsOrganisation == false)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificate report = new PrintCertificate();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                                else if (EUCApplication.IsOrganisation == true)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateOrg report = new PrintCertificateOrg();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                            }
                        }
                        else if (EUCApplication.currencyOfPurchase == 2)
                        {
                            //Update EUC Certificate Sttus
                            EUCCert.CertNo = model.NewCertNumber;
                            EUCCert.CertStatus = CertificateStatus.Issued;
                            EUCCert.IssuanceDate = DateTime.Now;
                            EUCCert.ExpiryDate = CurrentDate.AddYears(1);
                            _context.SaveChanges();

                            TempData["Success"] = "Certificate Collected Successfully!";

                            if (EUCUser.CategoryList == CategoryList.Individual)
                            {
                                if (EUCApplication.IsOrganisation == false)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateGBPInd report = new PrintCertificateGBPInd();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                                else if (EUCApplication.IsOrganisation == true)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateGBPOrg report = new PrintCertificateGBPOrg();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                            }
                            else if (EUCUser.CategoryList == CategoryList.Corporate || EUCUser.CategoryList == CategoryList.Exclusive)
                            {
                                if (EUCApplication.IsOrganisation == false)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateGBP report = new PrintCertificateGBP();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                                else if (EUCApplication.IsOrganisation == true)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateGBPOrg report = new PrintCertificateGBPOrg();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                            }

                        }
                        else if (EUCApplication.currencyOfPurchase == 12)
                        {
                            //Update EUC Certificate Sttus
                            EUCCert.CertNo = model.NewCertNumber;
                            EUCCert.CertStatus = CertificateStatus.Issued;
                            EUCCert.IssuanceDate = DateTime.Now;
                            EUCCert.ExpiryDate = CurrentDate.AddYears(1);
                            _context.SaveChanges();

                            TempData["Success"] = "Certificate Collected Successfully!";

                            if (EUCUser.CategoryList == CategoryList.Individual)
                            {
                                if (EUCApplication.IsOrganisation == false)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateEURInd report = new PrintCertificateEURInd();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                                else if (EUCApplication.IsOrganisation == true)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateEUROrg report = new PrintCertificateEUROrg();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }

                            }
                            else if (EUCUser.CategoryList == CategoryList.Corporate || EUCUser.CategoryList == CategoryList.Exclusive)
                            {
                                if (EUCApplication.IsOrganisation == false)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateEUR report = new PrintCertificateEUR();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                                else if (EUCApplication.IsOrganisation == true)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateEUROrg report = new PrintCertificateEUROrg();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }

                            }

                        }
                        else
                        {
                            //Update EUC Certificate Sttus
                            EUCCert.CertNo = model.NewCertNumber;
                            EUCCert.CertStatus = CertificateStatus.Issued;
                            EUCCert.IssuanceDate = DateTime.Now;
                            EUCCert.ExpiryDate = CurrentDate.AddYears(1);
                            _context.SaveChanges();

                            TempData["Success"] = "Certificate Collected Successfully!";

                            if (EUCUser.CategoryList == CategoryList.Individual)
                            {
                                if (EUCApplication.IsOrganisation == false)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateIndGen report = new PrintCertificateIndGen();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                                else if (EUCApplication.IsOrganisation == true)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateOrgGen report = new PrintCertificateOrgGen();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                            }
                            else if (EUCUser.CategoryList == CategoryList.Corporate || EUCUser.CategoryList == CategoryList.Exclusive)
                            {

                                if (EUCApplication.IsOrganisation == false)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateGen report = new PrintCertificateGen();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                                else if (EUCApplication.IsOrganisation == true)
                                {
                                    //Generate Certificate Preview 
                                    PrintCertificateOrgGen report = new PrintCertificateOrgGen();
                                    report.Parameters["SELAPPNO"].Value = model.CertNumber;
                                    report.Parameters["SELAPPNO"].Visible = false;
                                    stream = new MemoryStream();
                                    report.ExportToPdf(stream);
                                    //return File(stream.GetBuffer(), "application/pdf");
                                }
                            }
                        }
                        //Save Certificate Collect record to database
                        var CollectCert = new CollectEUCCert
                        {
                            CertNo = model.NewCertNumber,
                            CollectToken = model.CollectionToken,
                            UserID = user
                        };
                        _context.CollectEUCCert.Add(CollectCert);
                        _context.SaveChanges();

                    }

                    //var cd = new System.Net.Mime.ContentDisposition
                    //{
                    //    FileName = "filename.pdf",
                    //    Inline = false,
                    //};
                    //Response.AppendHeader("Content-Disposition", cd.ToString());
                    return File(stream.GetBuffer(), "application/pdf");

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("EUC Certificate Printed for Application Number: " + model.CertNumber + "",
                       User.Identity.GetUserId(),
                       "Printed EUC Certificate.",
                       Request.UserHostAddress);
                }
                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Successfully Issued Certificate to End User",
                   User.Identity.GetUserId(),
                   "Successfully Issued Certificate.",
                   Request.UserHostAddress);

                return RedirectToAction("AllEndUserCertificates", "Admin");
            }

            model.user = _context.Users.Where(u => u.Id == userId).Single();
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            return View(model);
        }

        [Authorize(Roles = "NSA, ONSA PGSO, ONSA Support")]
        public ActionResult UploadCertificate(string CertNo, string EUCUserID)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Viewed Uploaded Certificate page. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Viewed Uploaded Certificate page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(new UploadCertificateViewModel
            {
                user = user,
                role = roles,
                CertNumber = CertNo,
                EUCUserID = EUCUserID
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UploadCertificate(UploadCertificateViewModel model)
        {
            var user = _context.Users.Where(u => u.Id == model.EUCUserID).FirstOrDefault();


            if (ModelState.IsValid)
            {
                //---------Certificate upload------------------
                var uploadCert = model.UploadedCert;
                if (uploadCert != null)
                {
                    var extension = Path.GetExtension(uploadCert.FileName);

                    var fileNameWithExtension = Path.GetFileName(uploadCert.FileName);
                    var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(uploadCert.FileName);

                    string image = "/images/profile/" + user.Email + "/EUC Certificates/" + fileNameWithExtension;
                    string path = Path.Combine(Server.MapPath("~/images/profile"), user.Email + "/EUC Certificates/");

                    string imageUrl = Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/EUC Certificates/"), fileNameWithExtension);

                    var directory = Directory.CreateDirectory(path);

                    uploadCert.SaveAs(imageUrl);

                    //Create a validation entry in th database on Upload EUC Certificate
                    var UploadedEUCCert = new UploadedEUCCertificate
                    {
                        CertNo = model.CertNumber,
                        UploadUrl = image,
                        UserID = model.EUCUserID
                    };
                    _context.UploadedEUCCertificate.Add(UploadedEUCCert);
                    var result = _context.SaveChanges();
                    //If the certificate is uploaded successfully send an email and SMS to customs
                    if (result > 0)
                    {
                        //string roleName = "Customs User";

                        //Mail support if the user confirms their email account
                        //var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

                        ////Find the role to search for(ONSA Support)
                        //var role = await roleManager.FindByNameAsync(roleName);

                        //GroupUsersByRole.GroupUsersByRole groupUsers = new GroupUsersByRole.GroupUsersByRole();
                        //try
                        //{
                        //    var users = groupUsers.GetUsersInRole(roleName);
                        //    //Notify customs users of newly uploaded certificate
                        //    CustomsNotificationController.UploadedCertificateNotification notifyCustoms = new CustomsNotificationController.UploadedCertificateNotification();

                        //    await notifyCustoms.SendForwardedEUCEmailToAgency(users, model.CertNumber, user.Id, Request.UserHostAddress);
                        //}
                        //catch (Exception ex)
                        //{
                        //    ViewBag.NotificationException = "No Users In Customs Role";
                        //    return RedirectToAction("AllEndUserCertificates", "Admin");
                        //}
                        //Update End-User Certificate Application table with certificate status as collected.
                        var EucCert = _context.EUCCertificates.Where(cert => cert.CertNo == model.CertNumber).FirstOrDefault();
                        EucCert.CertStatus = CertificateStatus.Certificate_Collected;
                        _context.SaveChanges();

                        //Update EUC table with certificate status as collected.
                        var EucApp = _context.EndUserCertificate.Where(app => app.applicationNo == EucCert.applicationNo).FirstOrDefault();
                        EucApp.certificateStatus = CertificateStatus.Certificate_Collected;
                        _context.SaveChanges();

                        //var users = new List<ApplicationUser>();
                        //foreach (var applicant in UserManager.Users.ToList())
                        //{
                        //    if (await UserManager.IsInRoleAsync(applicant.Id, role.Name))
                        //    {
                        //        users.Add(applicant);
                        //    }
                        //}
                    }

                    TempData["Success"] = "Certificate Uploaded Successfully for End User!";

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Successfully Uploaded Certificate for End User",
                       User.Identity.GetUserId(),
                       "Successfully Uploaded Certificate.",
                       Request.UserHostAddress);

                }

                return RedirectToAction("AllEndUserCertificates", "Admin");
            }

            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            return View(model);
        }

        [Authorize(Roles = "Customs User")]
        public ActionResult DisputeCertificate(string CertID)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Opened Dispute Certificate Page. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Viewed Dispute Certificate page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(new DisputeCertificateViewModel
            {
                user = user,
                role = roles,
                CERTID = CertID
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DisputeCertificate(DisputeCertificateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var CertificateDetails = _context.EUCCertificates.Where(cert => cert.CertNo == model.CERTID).FirstOrDefault();
                var DisputeCert = new DisputeEUCCertificates
                {
                    DisputeDate = DateTime.Now,
                    CertNo = model.CERTID,
                    DisputeBy = User.Identity.GetUserId(),
                    DisputeDetails = model.DisputeComment,
                    DisputeStatus = CertificateStatus.Disputed
                };
                _context.DisputeEUCCertificates.Add(DisputeCert);
                _context.SaveChanges();
                //Update EUC Table with Certificate Status
                CertificateDetails.CertStatus = CertificateStatus.Disputed;
                var result = _context.SaveChanges();

                TempData["Success"] = "Disputed Successfully!";
                return RedirectToAction("AllEndUserCertificates", "Admin");

            }
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(model);
        }

        //Generate account confirmation link/token for SMS
        private async Task<string> GenerateConfirmationTokenForSMSAsync(string userID)
        {
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(userID);
            var callbackUrl = Url.Action("ConfirmEmail", "Account",
               new { userId = userID, code = code }, protocol: Request.Url.Scheme);

            return callbackUrl;
        }
        private async Task<string> SendEmailConfirmationTokenAsync(string userID, string subject)
        {
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(userID);
            var callbackUrl = Url.Action("ConfirmEmail", "Account",
               new { userId = userID, code = code }, protocol: Request.Url.Scheme);
            await UserManager.SendEmailAsync(userID, subject, "This is to confirm that your account has been created on the ONSA"
                                        + " End-User Certificate"
                                        + " portal."
                                        + " <br/>"
                                        + " An E-mail and SMS will be sent notifying you once your account has been approved or disapproved."
                                        + " <br/>"
                                        + " Please click this link to confirm your E-mail: <a href=\"" + callbackUrl + "\">here</a>"
                                        + " <br/>"
                                        + " NOTE: Only accounts with confirmed emails will be considered for approval.");

            return callbackUrl;
        }
        [Authorize(Roles = "Agency, ONSA Admin, ONSA Support, ONSA PGSO")]
        //Pagination for sign up requests
        public ActionResult AllUserSignUpRequests(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.Where(u => u.EmailConfirmed == true).OrderByDescending(date => date.RequestedDate).ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            IPagedList<ApplicationUser> usersToShow = db.Users.Where(u => u.EmailConfirmed == true).OrderByDescending
                                (euc => euc.RequestedDate).ToPagedList(pageIndex, pageSize);

            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;

            if (SearchTerm != "" && sortOrder == null)
            {
                usersToShow = db.Users
                    .Where(userToShow => userToShow.EmailConfirmed == true && userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);

                return View(new AllUserSignUpRequestsViewModel
                {
                    user = user,
                    role = roles,
                    usersToShow = usersToShow,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm
                });
            }

            switch (sortOrder)
            {
                case "ApplicantType":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.EmailConfirmed == true && userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.CategoryId).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.EmailConfirmed == true && userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.CategoryId).ToPagedList(pageIndex, pageSize);
                    break;
                case "DateCreated":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.EmailConfirmed == true && userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.EmailConfirmed == true && userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);
                    break;
                case "ApplicationNo":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.EmailConfirmed == true && userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.EmailConfirmed == true && userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                    break;
                case "LastName":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.EmailConfirmed == true && userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.LastName).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.EmailConfirmed == true && userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.LastName).ToPagedList(pageIndex, pageSize);
                    break;
                case "FirstName":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.EmailConfirmed == true && userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.FirstName).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.EmailConfirmed == true && userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.FirstName).ToPagedList(pageIndex, pageSize);
                    break;
                case "Email":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.EmailConfirmed == true && userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.Email).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.EmailConfirmed == true && userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.Email).ToPagedList(pageIndex, pageSize);
                    break;
                case "Status":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.EmailConfirmed == true && userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.ApprovalStatus).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.EmailConfirmed == true && userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.ApprovalStatus).ToPagedList(pageIndex, pageSize);
                    break;
                case "Default":
                    usersToShow = db.Users
                    .Where(userToShow => userToShow.EmailConfirmed == true && userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);
                    break;
            }
            ViewBag.CurrentSort = sortOrder;
            return View(new AllUserSignUpRequestsViewModel
            {
                user = user,
                role = roles,
                //userList = users,
                usersToShow = usersToShow,
                PageSize = pageSize,
                sortOrder = sortOrder,
                searchTerm = SearchTerm
            });
        }
        [Authorize(Roles = "Agency, ONSA Admin, ONSA Support, ONSA PGSO")]
        //Pagination for sign up requests
        public ActionResult AllPendingSignUpRequests(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.Where(euc => euc.ApprovalStatus == ApprovedStatus.Pending && euc.EmailConfirmed == true).OrderByDescending(date => date.RequestedDate).ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            IPagedList<ApplicationUser> usersToShow = db.Users.Where(euc => euc.ApprovalStatus == ApprovedStatus.Pending && euc.EmailConfirmed == true).OrderByDescending
                                (euc => euc.RequestedDate).ToPagedList(pageIndex, pageSize);

            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;

            if (SearchTerm != "" && sortOrder == null)
            {
                usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.FirstName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.Email.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.ApplicationNumber.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true)
                    .OrderByDescending(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);

                ViewBag.CurrentSort = sortOrder;
                return View(new AllUserSignUpRequestsViewModel
                {
                    user = user,
                    role = roles,
                    usersToShow = usersToShow,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm
                });
            }

            switch (sortOrder)
            {
                case "ApplicantType":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.FirstName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.Email.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.ApplicationNumber.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true)
                    .OrderByDescending(userToShow => userToShow.CategoryId).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.FirstName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.Email.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.ApplicationNumber.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true)
                    .OrderBy(userToShow => userToShow.CategoryId).ToPagedList(pageIndex, pageSize);
                    break;
                case "DateCreated":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.FirstName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.Email.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.ApplicationNumber.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true)
                    .OrderByDescending(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.FirstName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.Email.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.ApplicationNumber.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true)
                    .OrderBy(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);
                    break;
                case "ApplicationNo":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.FirstName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.Email.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.ApplicationNumber.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true)
                    .OrderByDescending(userToShow => userToShow.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.FirstName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.Email.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.ApplicationNumber.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true)
                    .OrderBy(userToShow => userToShow.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                    break;
                case "LastName":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.FirstName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.Email.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.ApplicationNumber.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true)
                    .OrderByDescending(userToShow => userToShow.LastName).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.FirstName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.Email.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.ApplicationNumber.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true)
                    .OrderBy(userToShow => userToShow.LastName).ToPagedList(pageIndex, pageSize);
                    break;
                case "FirstName":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.FirstName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.Email.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.ApplicationNumber.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true)
                    .OrderByDescending(userToShow => userToShow.FirstName).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.FirstName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.Email.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.ApplicationNumber.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true)
                    .OrderBy(userToShow => userToShow.FirstName).ToPagedList(pageIndex, pageSize);
                    break;
                case "Email":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.FirstName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.Email.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.ApplicationNumber.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true)
                    .OrderByDescending(userToShow => userToShow.Email).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.FirstName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.Email.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.ApplicationNumber.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true)
                    .OrderBy(userToShow => userToShow.Email).ToPagedList(pageIndex, pageSize);
                    break;
                case "Default":
                    usersToShow = usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.FirstName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.Email.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true
                    || userToShow.ApplicationNumber.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Pending && userToShow.EmailConfirmed == true)
                    .OrderByDescending(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);
                    break;
            }
            ViewBag.CurrentSort = sortOrder;
            return View(new AllUserSignUpRequestsViewModel
            {
                user = user,
                role = roles,
                //userList = users,
                usersToShow = usersToShow,
                PageSize = pageSize,
                sortOrder = sortOrder,
                searchTerm = SearchTerm
            });
        }
        [Authorize(Roles = "Agency, ONSA Admin, ONSA Support, ONSA PGSO")]
        //Pagination for sign up requests
        public ActionResult AllApprovedSignUpRequests(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.Where(euc => euc.ApprovalStatus == ApprovedStatus.Approved).OrderByDescending(date => date.RequestedDate).ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            IPagedList<ApplicationUser> usersToShow = db.Users.Where(euc => euc.ApprovalStatus == ApprovedStatus.Approved).OrderByDescending
                                (euc => euc.RequestedDate).ToPagedList(pageIndex, pageSize);

            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;

            if (SearchTerm != "" && sortOrder == null)
            {
                usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Approved
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);

                ViewBag.CurrentSort = sortOrder;
                return View(new AllUserSignUpRequestsViewModel
                {
                    user = user,
                    role = roles,
                    usersToShow = usersToShow,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm
                });
            }

            switch (sortOrder)
            {
                case "ApplicantType":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Approved
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.CategoryId).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Approved
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.CategoryId).ToPagedList(pageIndex, pageSize);
                    break;
                case "DateCreated":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Approved
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Approved
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);
                    break;
                case "ApplicationNo":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Approved
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Approved
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                    break;
                case "LastName":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Approved
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.LastName).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Approved
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.LastName).ToPagedList(pageIndex, pageSize);
                    break;
                case "FirstName":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Approved
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.FirstName).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Approved
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.FirstName).ToPagedList(pageIndex, pageSize);
                    break;
                case "Email":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Approved
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.Email).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Approved
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.Email).ToPagedList(pageIndex, pageSize);
                    break;
                case "Default":
                    usersToShow = usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Approved
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);
                    break;
            }
            ViewBag.CurrentSort = sortOrder;
            return View(new AllUserSignUpRequestsViewModel
            {
                user = user,
                role = roles,
                //userList = users,
                usersToShow = usersToShow,
                PageSize = pageSize,
                sortOrder = sortOrder,
                searchTerm = SearchTerm
            });
        }
        [Authorize(Roles = "Agency, ONSA Admin, ONSA Support, ONSA PGSO")]
        //Pagination for sign up requests
        public ActionResult AllRejectedSignUpRequests(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.Where(euc => euc.ApprovalStatus == ApprovedStatus.Rejected).OrderByDescending(date => date.RequestedDate).ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            IPagedList<ApplicationUser> usersToShow = db.Users.Where(euc => euc.ApprovalStatus == ApprovedStatus.Rejected).OrderByDescending
                                (euc => euc.RequestedDate).ToPagedList(pageIndex, pageSize);

            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;

            if (SearchTerm != "" && sortOrder == null)
            {
                usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Rejected
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);
                ViewBag.CurrentSort = sortOrder;
                return View(new AllUserSignUpRequestsViewModel
                {
                    user = user,
                    role = roles,
                    usersToShow = usersToShow,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm
                });
            }

            switch (sortOrder)
            {
                case "ApplicantType":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Rejected
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.CategoryId).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Rejected
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.CategoryId).ToPagedList(pageIndex, pageSize);
                    break;
                case "DateCreated":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Rejected
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Rejected
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);
                    break;
                case "ApplicationNo":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Rejected
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Rejected
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                    break;
                case "LastName":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Rejected
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.LastName).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Rejected
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.LastName).ToPagedList(pageIndex, pageSize);
                    break;
                case "FirstName":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Rejected
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.FirstName).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Rejected
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.FirstName).ToPagedList(pageIndex, pageSize);
                    break;
                case "Email":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Rejected
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.Email).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Rejected
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.Email).ToPagedList(pageIndex, pageSize);
                    break;
                case "Default":
                    usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm) && userToShow.ApprovalStatus == ApprovedStatus.Rejected
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);
                    break;
            }
            ViewBag.CurrentSort = sortOrder;
            return View(new AllUserSignUpRequestsViewModel
            {
                user = user,
                role = roles,
                //userList = users,
                usersToShow = usersToShow,
                PageSize = pageSize,
                sortOrder = sortOrder,
                searchTerm = SearchTerm
            });
        }
        public ActionResult Add()
        {
            return View();
        }
        [Authorize(Roles = "Agency, Client User, ONSA PGSO, Customs User, NSA, Exclusive User, ONSA Support")]
        //Pagination for EUC Applications
        // GET: /Employee/  
        public ActionResult AllEndUserCertificateApplication(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var itemCategories = _context.EUCItemCategory.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            IPagedList<EndUserCertificate> certificates = db.EndUserCertificate.Include(euc => euc.User)
                .Include(euc => euc.itemCategory)
                .Include(euc => euc.AgencyForwarded)
                .OrderByDescending
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
            #region support
            //Support User
            if (user.PortalUserCategoryId == 5 || user.PortalUserCategoryId == 45)
            {
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EndUserCertificate
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                    ViewBag.CurrentSort = sortOrder;
                    return View(new EndUserCertificatesViewModel
                    {
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        role = roles,
                        user = user,
                        itemCategories = itemCategories
                    });
                }

                switch (sortOrder)
                {
                    case "DateCreated":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending
                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy
                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending
                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy
                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending
                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy
                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ItemCategories":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending
                                    (euc => euc.itemCat).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy
                                    (euc => euc.itemCat).ToPagedList(pageIndex, pageSize);
                        break;
                    case "Status":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy
                                    (euc => euc.EUCApprovalStatus).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending
                                    (euc => euc.EUCApprovalStatus).ToPagedList(pageIndex, pageSize);
                        break;
                    case "Default":
                        certificates = db.EndUserCertificate
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                }
                ViewBag.CurrentSort = sortOrder;
                sortOrder = CurrentSort;
                return View(new EndUserCertificatesViewModel
                {
                    user = user,
                    role = roles,
                    //userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories
                });
            }
            #endregion
            #region agencies
            else if (user.PortalUserCategoryId == 1 || user.PortalUserCategoryId == 2 || user.PortalUserCategoryId == 3
                || user.PortalUserCategoryId == 4 || user.PortalUserCategoryId == 6 || user.PortalUserCategoryId == 7
                || user.PortalUserCategoryId == 8 || user.PortalUserCategoryId == 11 || user.PortalUserCategoryId == 12
                || user.PortalUserCategoryId == 10 || user.PortalUserCategoryId == 13 || user.PortalUserCategoryId == 14
                || user.PortalUserCategoryId == 16 || user.PortalUserCategoryId == 17 || user.PortalUserCategoryId == 18
                || user.PortalUserCategoryId == 19 || user.PortalUserCategoryId == 20 || user.PortalUserCategoryId == 21
                || user.PortalUserCategoryId == 25 || user.PortalUserCategoryId == 26 || user.PortalUserCategoryId == 27
                || user.PortalUserCategoryId == 30 || user.PortalUserCategoryId == 31 || user.PortalUserCategoryId == 38
                || user.PortalUserCategoryId == 34 || user.PortalUserCategoryId == 36 || user.PortalUserCategoryId == 37
                || user.PortalUserCategoryId == 39 || user.PortalUserCategoryId == 40 || user.PortalUserCategoryId == 41
                || user.PortalUserCategoryId == 42 || user.PortalUserCategoryId == 43 || user.PortalUserCategoryId == 44
                || user.PortalUserCategoryId == 46 || user.PortalUserCategoryId == 47)

            {

                certificates = db.EndUserCertificate.Include(euc => euc.User).Where(euc => euc.AgencyId == user.PortalUserCategoryId && userId != null)
                            .OrderBy(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                //eucApplications = _context.EndUserCertificate.Distinct()
                //.Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.UserID != null)
                //.OrderByDescending(date => date.dateCreated).ToList();
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "")
                {
                    //eucCertificates = _context.EUCCertificates
                    //.Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                    //.OrderByDescending(date => date.IssuanceDate).ToList();
                    certificates = db.EndUserCertificate.Distinct()
                        .Where(euc => euc.AgencyId == user.PortalUserCategoryId && userId != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                    ViewBag.CurrentSort = sortOrder;
                    return View(new EndUserCertificatesViewModel
                    {
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        role = roles,
                        user = user,
                        itemCategories = itemCategories
                    });
                }

                switch (sortOrder)
                {
                    case "DateCreated":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Distinct()
                        .Where(euc => euc.AgencyId == user.PortalUserCategoryId && userId != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Distinct()
                        .Where(euc => euc.AgencyId == user.PortalUserCategoryId && userId != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderBy
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Distinct()
                        .Where(euc => euc.AgencyId == user.PortalUserCategoryId && userId != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Distinct()
                        .Where(euc => euc.AgencyId == user.PortalUserCategoryId && userId != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderBy
                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Distinct()
                        .Where(euc => euc.AgencyId == user.PortalUserCategoryId && userId != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Distinct()
                        .Where(euc => euc.AgencyId == user.PortalUserCategoryId && userId != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderBy
                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Distinct()
                        .Where(euc => euc.AgencyId == user.PortalUserCategoryId && userId != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Distinct()
                        .Where(euc => euc.AgencyId == user.PortalUserCategoryId && userId != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderBy
                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        break;
                    case "Default":
                        certificates = db.EndUserCertificate.Distinct()
                        .Where(euc => euc.AgencyId == user.PortalUserCategoryId && userId != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                }
                ViewBag.CurrentSort = sortOrder;
                return View(new EndUserCertificatesViewModel
                {
                    user = user,
                    role = roles,
                    //userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories
                });
            }
            #endregion
            #region exclusive users
            //Exclusive User
            else if (user.PortalUserCategoryId == 29)
            {
                certificates = db.EndUserCertificate.Distinct()
                        .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                        && euc.UserID == user.Id
                        && userId != null).Include(euc => euc.User)
                        .OrderByDescending(euc => euc.dateCreated)
                        .ToPagedList(pageIndex, pageSize);

                //eucApplications = _context.EndUserCertificate.Distinct()
                //.Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.UserID != null)
                //.OrderByDescending(date => date.dateCreated).ToList();
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "")
                {
                    //eucCertificates = _context.EUCCertificates
                    //.Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                    //.OrderByDescending(date => date.IssuanceDate).ToList();
                    certificates = db.EndUserCertificate.Distinct()
                        .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                        && euc.UserID == user.Id
                        && userId != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                    ViewBag.CurrentSort = sortOrder;
                    return View(new EndUserCertificatesViewModel
                    {
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        role = roles,
                        user = user,
                        itemCategories = itemCategories
                    });
                }

                switch (sortOrder)
                {
                    case "DateCreated":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id
                                && userId != null)
                                .OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id
                                && userId != null)
                                .OrderBy(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id
                                && userId != null)
                                .OrderByDescending(euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id
                                && userId != null)
                                .OrderBy(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id
                                && userId != null)
                                .OrderByDescending(euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Where(euc => euc.AgencyId == user.PortalUserCategoryId
                            && euc.UserID == user.Id
                            && userId != null)
                                .OrderBy(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id
                                && userId != null)
                                .OrderByDescending(euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id
                                && userId != null)
                                .OrderBy(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "Default":
                        certificates = db.EndUserCertificate.Where(euc => euc.AgencyId == user.PortalUserCategoryId
                        && euc.UserID == user.Id
                        && userId != null)
                        .OrderBy(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                }
                ViewBag.CurrentSort = sortOrder;
                return View(new EndUserCertificatesViewModel
                {
                    user = user,
                    role = roles,
                    //userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories
                });
            }
            #endregion
            #region client user
            //Exclusive User
            else if (user.PortalUserCategoryId == 28)
            {
                certificates = db.EndUserCertificate.Distinct()
                        .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                        && euc.UserID == user.Id
                        && userId != null)
                        .OrderByDescending(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                //eucApplications = _context.EndUserCertificate.Distinct()
                //.Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.UserID != null)
                //.OrderByDescending(date => date.dateCreated).ToList();
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "")
                {
                    //eucCertificates = _context.EUCCertificates
                    //.Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                    //.OrderByDescending(date => date.IssuanceDate).ToList();
                    certificates = db.EndUserCertificate.Distinct()
                        .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                        && euc.UserID == user.Id
                        && userId != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                    ViewBag.CurrentSort = sortOrder;
                    return View(new EndUserCertificatesViewModel
                    {
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        role = roles,
                        user = user,
                        itemCategories = itemCategories
                    });
                }

                switch (sortOrder)
                {
                    case "DateCreated":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id
                                && userId != null)
                                .OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id
                                && userId != null)
                                .OrderBy(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id
                                && userId != null)
                                .OrderByDescending(euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id
                                && userId != null)
                                .OrderBy(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id
                                && userId != null)
                                .OrderByDescending(euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Where(euc => euc.AgencyId == user.PortalUserCategoryId
                            && euc.UserID == user.Id
                            && userId != null)
                                .OrderBy(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id
                                && userId != null)
                                .OrderByDescending(euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id
                                && userId != null)
                                .OrderBy(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "Default":
                        certificates = db.EndUserCertificate.Where(euc => euc.AgencyId == user.PortalUserCategoryId
                        && euc.UserID == user.Id
                        && userId != null)
                        .OrderBy(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                }
                ViewBag.CurrentSort = sortOrder;
                return View(new EndUserCertificatesViewModel
                {
                    user = user,
                    role = roles,
                    //userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories
                });
            }
            #endregion
            else
            {
                return View();
            }
        }

        [Authorize(Roles = "Agency, Client User, ONSA Support, ONSA PGSO, NSA, Customs User, Exclusive User")]
        public ActionResult PendingEndUserCertificateApplication(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var itemCategories = _context.EUCItemCategory.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            IPagedList<EndUserCertificate> certificates = db.EndUserCertificate.Include(euc => euc.User)
                                                            .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending).Distinct()
                                                                .OrderByDescending(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
            #region support
            if (user.PortalUserCategoryId == 5 || user.PortalUserCategoryId == 45)
            {
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                    return View(new EndUserCertificatesViewModel
                    {
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        role = roles,
                        user = user,
                        itemCategories = itemCategories
                    });
                }

                switch (sortOrder)
                {
                    case "DateCreated":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderBy
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderBy
                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderBy
                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderBy
                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ItemCategories":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.itemCat).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderBy
                                    (euc => euc.itemCat).ToPagedList(pageIndex, pageSize);
                        break;
                    case "Status":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.EUCApprovalStatus).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderBy
                                    (euc => euc.EUCApprovalStatus).ToPagedList(pageIndex, pageSize);
                        break;
                    case "Default":
                        certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                }
                ViewBag.CurrentSort = sortOrder;
                return View(new EndUserCertificatesViewModel
                {
                    user = user,
                    role = roles,
                    //userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories
                });
            }
            #endregion
            #region agencies
            else if (user.PortalUserCategoryId == 1 || user.PortalUserCategoryId == 2 || user.PortalUserCategoryId == 3
                || user.PortalUserCategoryId == 4 || user.PortalUserCategoryId == 6 || user.PortalUserCategoryId == 7
                || user.PortalUserCategoryId == 8 || user.PortalUserCategoryId == 11 || user.PortalUserCategoryId == 12
                || user.PortalUserCategoryId == 10 || user.PortalUserCategoryId == 13 || user.PortalUserCategoryId == 14
                || user.PortalUserCategoryId == 16
                || user.PortalUserCategoryId == 17 || user.PortalUserCategoryId == 18 || user.PortalUserCategoryId == 19
                || user.PortalUserCategoryId == 20 || user.PortalUserCategoryId == 21 || user.PortalUserCategoryId == 25
                || user.PortalUserCategoryId == 26 || user.PortalUserCategoryId == 27 || user.PortalUserCategoryId == 38
                || user.PortalUserCategoryId == 39 || user.PortalUserCategoryId == 40 || user.PortalUserCategoryId == 41
                || user.PortalUserCategoryId == 42 || user.PortalUserCategoryId == 43 || user.PortalUserCategoryId == 44
                || user.PortalUserCategoryId == 46 || user.PortalUserCategoryId == 47)
            {
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "")
                {
                    certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending && euc.AgencyId == user.PortalUserCategoryId && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)).Distinct()
                        .Include(euc => euc.User).OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                    ViewBag.CurrentSort = sortOrder;
                    return View(new EndUserCertificatesViewModel
                    {
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        role = roles,
                        user = user,
                        itemCategories = itemCategories
                    });
                }

                switch (sortOrder)
                {
                    case "DateCreated":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending && euc.UserID != null
                                && euc.AgencyId == user.PortalUserCategoryId).Distinct().OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending && euc.UserID != null
                                && euc.AgencyId == user.PortalUserCategoryId).Distinct().OrderBy
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending && euc.UserID != null
                                && euc.AgencyId == user.PortalUserCategoryId).Distinct().OrderByDescending
                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending && euc.UserID != null
                                && euc.AgencyId == user.PortalUserCategoryId).Distinct().OrderBy
                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending && euc.UserID != null
                                && euc.AgencyId == user.PortalUserCategoryId).Distinct().OrderByDescending
                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending && euc.UserID != null
                                && euc.AgencyId == user.PortalUserCategoryId).Distinct().OrderBy
                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending && euc.UserID != null
                                && euc.AgencyId == user.PortalUserCategoryId).Distinct().OrderByDescending
                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending && euc.UserID != null
                                && euc.AgencyId == user.PortalUserCategoryId).Distinct().OrderBy
                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        break;
                    case "Default":
                        certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Pending && euc.UserID != null
                                && euc.AgencyId == user.PortalUserCategoryId).Distinct().OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                }
                ViewBag.CurrentSort = sortOrder;
                return View(new EndUserCertificatesViewModel
                {
                    user = user,
                    role = roles,
                    //userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories
                });
            }
            #endregion
            #region exclusive users
            else if (user.PortalUserCategoryId == 29 || user.PortalUserCategoryId == 28)
            {
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "")
                {
                    certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus != EUCApprovedStatus.Certificate_Issued
                        && euc.EUCApprovalStatus != EUCApprovedStatus.Rejected
                        && euc.AgencyId == user.PortalUserCategoryId
                        && euc.UserID == user.Id)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                    ViewBag.CurrentSort = sortOrder;
                    return View(new EndUserCertificatesViewModel
                    {
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        role = roles,
                        user = user,
                        itemCategories = itemCategories
                    });
                }

                certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus != EUCApprovedStatus.Certificate_Issued
                                && euc.EUCApprovalStatus != EUCApprovedStatus.Rejected
                                && euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id)
                                .Distinct().OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                switch (sortOrder)
                {
                    case "DateCreated":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus != EUCApprovedStatus.Certificate_Issued
                                && euc.EUCApprovalStatus != EUCApprovedStatus.Rejected
                                && euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id).Distinct().OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus != EUCApprovedStatus.Certificate_Issued
                                && euc.EUCApprovalStatus != EUCApprovedStatus.Rejected
                                && euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id).Distinct().OrderBy
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus != EUCApprovedStatus.Certificate_Issued
                                && euc.EUCApprovalStatus != EUCApprovedStatus.Rejected
                                && euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id).Distinct().OrderByDescending
                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus != EUCApprovedStatus.Certificate_Issued
                                && euc.EUCApprovalStatus != EUCApprovedStatus.Rejected
                                && euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id).Distinct().OrderBy
                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus != EUCApprovedStatus.Certificate_Issued
                                && euc.EUCApprovalStatus != EUCApprovedStatus.Rejected
                                && euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id).Distinct().OrderByDescending
                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus != EUCApprovedStatus.Certificate_Issued
                                && euc.EUCApprovalStatus != EUCApprovedStatus.Rejected
                                && euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id).Distinct().OrderBy
                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus != EUCApprovedStatus.Certificate_Issued
                                && euc.EUCApprovalStatus != EUCApprovedStatus.Rejected
                                && euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id).Distinct().OrderByDescending
                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus != EUCApprovedStatus.Certificate_Issued
                                && euc.EUCApprovalStatus != EUCApprovedStatus.Rejected
                                && euc.AgencyId == user.PortalUserCategoryId
                                && euc.UserID == user.Id).Distinct().OrderBy
                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        break;
                    case "Default":
                        certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus != EUCApprovedStatus.Certificate_Issued
                                && euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id).Distinct().OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                }
                ViewBag.CurrentSort = sortOrder;
                return View(new EndUserCertificatesViewModel
                {
                    user = user,
                    role = roles,
                    //userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories
                });
            }
            #endregion
            else
            {
                return View();
            }
        }
        [Authorize(Roles = "Agency, ONSA Admin, ONSA Support, ONSA PGSO, NSA")]
        public ActionResult ForwardedEndUserCertificateApplication(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var agencies = _context.PortalSubCategories.ToList();
            var itemCategories = _context.EUCItemCategory.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            IPagedList<EndUserCertificate> certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.UserID != null).Include(euc => euc.User).Include(euc => euc.AgencyForwarded).Distinct().OrderByDescending
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

            #region support
            if (user.PortalUserCategoryId == 5 || user.PortalUserCategoryId == 45)
            {
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                    return View(new EndUserCertificatesViewModel
                    {
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        role = roles,
                        user = user,
                        itemCategories = itemCategories,
                        Agencies = agencies
                    });
                }

                switch (sortOrder)
                {
                    case "DateCreated":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct()
                        .Include(euc => euc.AgencyForwarded).OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct()
                        .Include(euc => euc.AgencyForwarded).OrderBy
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct()
                        .Include(euc => euc.AgencyForwarded).OrderByDescending
                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct()
                        .Include(euc => euc.AgencyForwarded).OrderBy
                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct()
                        .Include(euc => euc.AgencyForwarded).OrderByDescending
                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct()
                        .Include(euc => euc.AgencyForwarded).OrderBy
                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct()
                        .Include(euc => euc.AgencyForwarded).OrderByDescending
                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct()
                        .Include(euc => euc.AgencyForwarded).OrderBy
                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ItemCategories":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.UserID != null)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct()
                        .Include(euc => euc.AgencyForwarded).OrderByDescending
                                    (euc => euc.itemCat).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.UserID != null)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct()
                        .Include(euc => euc.AgencyForwarded).OrderBy
                                    (euc => euc.itemCat).ToPagedList(pageIndex, pageSize);
                        break;
                    case "Status":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.UserID != null)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct()
                        .Include(euc => euc.AgencyForwarded).OrderByDescending
                                    (euc => euc.EUCApprovalStatus).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.UserID != null)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct()
                        .Include(euc => euc.AgencyForwarded).OrderBy
                                    (euc => euc.EUCApprovalStatus).ToPagedList(pageIndex, pageSize);
                        break;
                    case "Default":
                        certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct()
                        .Include(euc => euc.AgencyForwarded).OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                }
                ViewBag.CurrentSort = sortOrder;
                return View(new EndUserCertificatesViewModel
                {
                    user = user,
                    role = roles,
                    //userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories,
                    Agencies = agencies
                });
            }
            #endregion
            #region agencies
            else if (user.PortalUserCategoryId == 1 || user.PortalUserCategoryId == 2 || user.PortalUserCategoryId == 3
                || user.PortalUserCategoryId == 4 || user.PortalUserCategoryId == 6 || user.PortalUserCategoryId == 7
                || user.PortalUserCategoryId == 8 || user.PortalUserCategoryId == 11 || user.PortalUserCategoryId == 12
                || user.PortalUserCategoryId == 10 || user.PortalUserCategoryId == 13 || user.PortalUserCategoryId == 14
                || user.PortalUserCategoryId == 16 || user.PortalUserCategoryId == 17 || user.PortalUserCategoryId == 18
                || user.PortalUserCategoryId == 19 || user.PortalUserCategoryId == 20 || user.PortalUserCategoryId == 21
                || user.PortalUserCategoryId == 25 || user.PortalUserCategoryId == 26 || user.PortalUserCategoryId == 27
                || user.PortalUserCategoryId == 30 || user.PortalUserCategoryId == 31 || user.PortalUserCategoryId == 38
                || user.PortalUserCategoryId == 34 || user.PortalUserCategoryId == 36 || user.PortalUserCategoryId == 37
                || user.PortalUserCategoryId == 39 || user.PortalUserCategoryId == 40 || user.PortalUserCategoryId == 41
                || user.PortalUserCategoryId == 42 || user.PortalUserCategoryId == 43 || user.PortalUserCategoryId == 44
                || user.PortalUserCategoryId == 46 || user.PortalUserCategoryId == 47)
            {
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                certificates = db.EndUserCertificate.Include(euc => euc.User)
                                .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyForwardedTo == user.PortalUserCategoryId && euc.UserID != null
                                ).Distinct().OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyForwardedTo == user.PortalUserCategoryId && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                    return View(new EndUserCertificatesViewModel
                    {
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        role = roles,
                        user = user,
                        itemCategories = itemCategories,
                        Agencies = agencies
                    });
                }

                switch (sortOrder)
                {
                    case "DateCreated":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyForwardedTo == user.PortalUserCategoryId && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyForwardedTo == user.PortalUserCategoryId && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderBy
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyForwardedTo == user.PortalUserCategoryId && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyForwardedTo == user.PortalUserCategoryId && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderBy
                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyForwardedTo == user.PortalUserCategoryId && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyForwardedTo == user.PortalUserCategoryId && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderBy
                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyForwardedTo == user.PortalUserCategoryId && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyForwardedTo == user.PortalUserCategoryId && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderBy
                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        break;
                    case "Default":
                        certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyForwardedTo == user.PortalUserCategoryId && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                }
                ViewBag.CurrentSort = sortOrder;
                return View(new EndUserCertificatesViewModel
                {
                    user = user,
                    role = roles,
                    //userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories,
                    Agencies = agencies
                });
            }
            #endregion
            #region exclusive users
            else if (user.PortalUserCategoryId == 29 || user.PortalUserCategoryId == 28)
            {
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                    return View(new EndUserCertificatesViewModel
                    {
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        role = roles,
                        user = user,
                        itemCategories = itemCategories,
                        Agencies = agencies
                    });
                }

                switch (sortOrder)
                {
                    case "DateCreated":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderBy
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        break;
                    case "Default":
                        certificates = db.EndUserCertificate
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Forwarded && euc.AgencyId == user.PortalUserCategoryId && euc.UserID == user.Id && euc.UserID != null)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .Include(euc => euc.User).Distinct().OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                }
                ViewBag.CurrentSort = sortOrder;
                return View(new EndUserCertificatesViewModel
                {
                    user = user,
                    role = roles,
                    //userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories,
                    Agencies = agencies
                });
            }
            #endregion
            else
            {
                return View();
            }
        }
        [Authorize(Roles = "Agency, Client User, ONSA PGSO, NSA, Exclusive User")]

        public ActionResult ApprovedEndUserCertificateApplication(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)

        {

            //Plumbing code for shared view

            //Get ID of the currently logged in user

            var userId = User.Identity.GetUserId();

            //Create user object based off user ID

            var user = _context.Users.Where(u => u.Id == userId).Single();

            var userIdentity = (ClaimsIdentity)User.Identity;

            var claims = userIdentity.Claims;

            var roleClaimType = userIdentity.RoleClaimType;

            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();

            var agencies = _context.PortalSubCategories.ToList();

            var itemCategories = _context.EUCItemCategory.ToList();

            //Redirect SA if portal sub category is 45
            if (user.PortalUserCategoryId == 45)
            {
                return RedirectToAction("SAsEucsApproved");
            }

            ApplicationDbContext db = new ApplicationDbContext();

            int pageSize = 10;



            if (PageSize != null)

                pageSize = PageSize.GetValueOrDefault();



            int pageIndex = 1;

            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;



            //ViewBag.CurrentSort = sortOrder;

            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;



            IPagedList<EndUserCertificate> certificates;



            if (user.PortalUserCategoryId == 5)
            #region
            {

                certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued).Include(euc => euc.User).Include(euc => euc.AgencyForwarded).OrderByDescending
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);



                //Parse search term

                string SearchTerm = "";

                if (searchTerm == null)

                    SearchTerm = "";

                else if (searchTerm != null)

                    SearchTerm = searchTerm;



                if (SearchTerm != "" && sortOrder == null)

                {

                    certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);



                    return View(new EndUserCertificatesViewModel

                    {

                        certificatesToShow = certificates,

                        PageSize = pageSize,

                        sortOrder = sortOrder,

                        searchTerm = SearchTerm,

                        Agencies = agencies,

                        user = user,

                        role = roles,

                        itemCategories = itemCategories

                    });

                }



                switch (sortOrder)

                {

                    case "DateCreated":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy

                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                        break;

                    case "DateForwarded":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.dateForwarded).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy

                                    (euc => euc.dateForwarded).ToPagedList(pageIndex, pageSize);

                        break;

                    case "ApplicationNo":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy

                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);

                        break;

                    case "ApplicantNo":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy

                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);

                        break;

                    case "ApplicantName":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy

                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);

                        break;

                    case "ItemCategories":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.itemCat).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate

                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy

                                    (euc => euc.itemCat).ToPagedList(pageIndex, pageSize);

                        break;

                    case "Status":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate

                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.EUCApprovalStatus).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate

                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy

                                    (euc => euc.EUCApprovalStatus).ToPagedList(pageIndex, pageSize);

                        break;

                    case "Default":

                        certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                        break;

                }

                ViewBag.CurrentSort = sortOrder;

                return View(new EndUserCertificatesViewModel

                {

                    user = user,

                    role = roles,

                    //userList = users,

                    certificatesToShow = certificates,

                    PageSize = pageSize,

                    sortOrder = sortOrder,

                    searchTerm = SearchTerm,

                    Agencies = agencies,

                    itemCategories = itemCategories

                });

            }
            #endregion
            else if (user.PortalUserCategoryId == 45)
            {

                certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved
                                || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)
                                .Where(euc => euc.AgencyForwardedTo == 45)
                                .Include(euc => euc.User)
                                .Include(euc => euc.AgencyForwarded)
                                .OrderByDescending(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);



                //Parse search term

                string SearchTerm = "";

                if (searchTerm == null)

                    SearchTerm = "";

                else if (searchTerm != null)

                    SearchTerm = searchTerm;



                if (SearchTerm != "" && sortOrder == null)

                {

                    certificates = db.EndUserCertificate
                                    .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)
                                    .Where(euc => euc.AgencyForwardedTo == 45)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);



                    return View(new EndUserCertificatesViewModel

                    {

                        certificatesToShow = certificates,

                        PageSize = pageSize,

                        sortOrder = sortOrder,

                        searchTerm = SearchTerm,

                        Agencies = agencies,

                        user = user,

                        role = roles,

                        itemCategories = itemCategories

                    });

                }



                switch (sortOrder)

                {

                    case "DateCreated":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)
                        .Where(euc => euc.AgencyForwardedTo == 45)
                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)
                        .Where(euc => euc.AgencyForwardedTo == 45)
                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy

                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                        break;

                    case "DateForwarded":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)
                        .Where(euc => euc.AgencyForwardedTo == 45)
                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.dateForwarded).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy

                                    (euc => euc.dateForwarded).ToPagedList(pageIndex, pageSize);

                        break;

                    case "ApplicationNo":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)
                        .Where(euc => euc.AgencyForwardedTo == 45)
                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)
                           .Where(euc => euc.AgencyForwardedTo == 45)
                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy

                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);

                        break;

                    case "ApplicantNo":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)
                           .Where(euc => euc.AgencyForwardedTo == 45)
                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)
                           .Where(euc => euc.AgencyForwardedTo == 45)
                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy

                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);

                        break;

                    case "ApplicantName":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)
                         .Where(euc => euc.AgencyForwardedTo == 45)
                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)
                         .Where(euc => euc.AgencyForwardedTo == 45)
                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy

                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);

                        break;

                    case "ItemCategories":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)
                         .Where(euc => euc.AgencyForwardedTo == 45)
                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.itemCat).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate
                        .Where(euc => euc.AgencyForwardedTo == 45)
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy

                                    (euc => euc.itemCat).ToPagedList(pageIndex, pageSize);

                        break;

                    case "Status":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate
                           .Where(euc => euc.AgencyForwardedTo == 45)
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.EUCApprovalStatus).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate
                           .Where(euc => euc.AgencyForwardedTo == 45)
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy

                                    (euc => euc.EUCApprovalStatus).ToPagedList(pageIndex, pageSize);

                        break;

                    case "Default":

                        certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved || euc.EUCApprovalStatus == EUCApprovedStatus.Certificate_Issued)
                           .Where(euc => euc.AgencyForwardedTo == 45)
                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm)

                        || euc.AgencyForwarded.Name.Contains(SearchTerm))

                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending

                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                        break;

                }

                ViewBag.CurrentSort = sortOrder;

                return View(new EndUserCertificatesViewModel

                {

                    user = user,

                    role = roles,

                    //userList = users,

                    certificatesToShow = certificates,

                    PageSize = pageSize,

                    sortOrder = sortOrder,

                    searchTerm = SearchTerm,

                    Agencies = agencies,

                    itemCategories = itemCategories

                });

            }
            else
            {

                certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id).Include(euc => euc.User).OrderByDescending

                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                //certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved).Include(euc => euc.User).OrderByDescending

                //            (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);



                //Parse search term

                string SearchTerm = "";

                if (searchTerm == null)

                    SearchTerm = "";

                else if (searchTerm != null)

                    SearchTerm = searchTerm;



                if (SearchTerm != "" && sortOrder == null)

                {

                    certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id).Include(euc => euc.User)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm))

                        .Include(euc => euc.User).OrderByDescending

                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);



                    return View(new EndUserCertificatesViewModel

                    {

                        certificatesToShow = certificates,

                        PageSize = pageSize,

                        sortOrder = sortOrder,

                        searchTerm = SearchTerm,

                        Agencies = agencies,

                        user = user,

                        role = roles,

                        itemCategories = itemCategories

                    });

                }



                switch (sortOrder)

                {

                    case "DateCreated":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id).Include(euc => euc.User)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm))

                        .Include(euc => euc.User).OrderByDescending

                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id).Include(euc => euc.User)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm))

                        .Include(euc => euc.User).OrderBy

                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                        break;

                    case "ApplicationNo":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id).Include(euc => euc.User)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm))

                        .Include(euc => euc.User).OrderByDescending

                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id).Include(euc => euc.User)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm))

                        .Include(euc => euc.User).OrderBy

                                    (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);

                        break;

                    case "ApplicantNo":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id).Include(euc => euc.User)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm))

                        .Include(euc => euc.User).OrderByDescending

                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id).Include(euc => euc.User)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm))

                        .Include(euc => euc.User).OrderBy

                                    (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);

                        break;

                    case "ApplicantName":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id).Include(euc => euc.User)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm))

                        .Include(euc => euc.User).OrderByDescending

                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id).Include(euc => euc.User)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm))

                        .Include(euc => euc.User).OrderBy

                                    (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);

                        break;

                    case "ItemCategories":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id).Include(euc => euc.User)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm))

                        .Include(euc => euc.User).OrderByDescending

                                    (euc => euc.itemCat).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id).Include(euc => euc.User)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm))

                        .Include(euc => euc.User).OrderBy

                                    (euc => euc.itemCat).ToPagedList(pageIndex, pageSize);

                        break;

                    case "Status":

                        if (sortOrder.Equals(CurrentSort))

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id).Include(euc => euc.User)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm))

                        .Include(euc => euc.User).OrderByDescending

                                    (euc => euc.EUCApprovalStatus).ToPagedList(pageIndex, pageSize);

                        else

                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id).Include(euc => euc.User)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm))

                        .Include(euc => euc.User).OrderBy

                                    (euc => euc.EUCApprovalStatus).ToPagedList(pageIndex, pageSize);

                        break;

                    case "Default":

                        certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.UserID == user.Id).Include(euc => euc.User)

                        .Where(

                           euc => euc.User.LastName.Contains(SearchTerm)

                        || euc.User.FirstName.Contains(SearchTerm)

                        || euc.User.Email.Contains(SearchTerm)

                        || euc.applicationNo.Contains(SearchTerm)

                        || euc.User.ApplicationNumber.Contains(SearchTerm))

                        .Include(euc => euc.User).OrderByDescending

                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                        break;

                }

                ViewBag.CurrentSort = sortOrder;

                return View(new EndUserCertificatesViewModel

                {

                    user = user,

                    role = roles,

                    //userList = users,

                    certificatesToShow = certificates,

                    PageSize = pageSize,

                    sortOrder = sortOrder,

                    searchTerm = SearchTerm,

                    Agencies = agencies,

                    itemCategories = itemCategories

                });

            }

        }
        [Authorize(Roles = "Agency, Client User, ONSA Support, ONSA PGSO, NSA, Exclusive User")]
        public ActionResult RejectedEndUserCertificateApplication(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var agencies = _context.PortalSubCategories.ToList();
            var itemCategories = _context.EUCItemCategory.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            IPagedList<EndUserCertificate> certificates;

            if (user.PortalUserCategoryId == 5 || user.PortalUserCategoryId == 45)
                certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected).Include(euc => euc.User).OrderByDescending
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
            else
                certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected && euc.UserID == user.Id).Include(euc => euc.User).OrderByDescending
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;

            if (SearchTerm != "" && sortOrder == null)
            {
                certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected)
                    .Where(
                       euc => euc.User.LastName.Contains(SearchTerm)
                    || euc.User.FirstName.Contains(SearchTerm)
                    || euc.User.Email.Contains(SearchTerm)
                    || euc.applicationNo.Contains(SearchTerm)
                    || euc.User.ApplicationNumber.Contains(SearchTerm))
                    .Include(euc => euc.User).OrderByDescending
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                return View(new EndUserCertificatesViewModel
                {
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    Agencies = agencies,
                    user = user,
                    role = roles,
                    itemCategories = itemCategories
                });
            }

            switch (sortOrder)
            {
                case "DateCreated":
                    if (sortOrder.Equals(CurrentSort))
                        certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected)
                    .Where(
                       euc => euc.User.LastName.Contains(SearchTerm)
                    || euc.User.FirstName.Contains(SearchTerm)
                    || euc.User.Email.Contains(SearchTerm)
                    || euc.applicationNo.Contains(SearchTerm)
                    || euc.User.ApplicationNumber.Contains(SearchTerm))
                    .Include(euc => euc.User).OrderByDescending
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                    else
                        certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected)
                    .Where(
                       euc => euc.User.LastName.Contains(SearchTerm)
                    || euc.User.FirstName.Contains(SearchTerm)
                    || euc.User.Email.Contains(SearchTerm)
                    || euc.applicationNo.Contains(SearchTerm)
                    || euc.User.ApplicationNumber.Contains(SearchTerm))
                    .Include(euc => euc.User).OrderBy
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                    break;
                case "ApplicationNo":
                    if (sortOrder.Equals(CurrentSort))
                        certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected)
                    .Where(
                       euc => euc.User.LastName.Contains(SearchTerm)
                    || euc.User.FirstName.Contains(SearchTerm)
                    || euc.User.Email.Contains(SearchTerm)
                    || euc.applicationNo.Contains(SearchTerm)
                    || euc.User.ApplicationNumber.Contains(SearchTerm))
                    .Include(euc => euc.User).OrderByDescending
                                (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                    else
                        certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected)
                    .Where(
                       euc => euc.User.LastName.Contains(SearchTerm)
                    || euc.User.FirstName.Contains(SearchTerm)
                    || euc.User.Email.Contains(SearchTerm)
                    || euc.applicationNo.Contains(SearchTerm)
                    || euc.User.ApplicationNumber.Contains(SearchTerm))
                    .Include(euc => euc.User).OrderBy
                                (euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                    break;
                case "ApplicantNo":
                    if (sortOrder.Equals(CurrentSort))
                        certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected)
                    .Where(
                       euc => euc.User.LastName.Contains(SearchTerm)
                    || euc.User.FirstName.Contains(SearchTerm)
                    || euc.User.Email.Contains(SearchTerm)
                    || euc.applicationNo.Contains(SearchTerm)
                    || euc.User.ApplicationNumber.Contains(SearchTerm))
                    .Include(euc => euc.User).OrderByDescending
                                (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                    else
                        certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected)
                    .Where(
                       euc => euc.User.LastName.Contains(SearchTerm)
                    || euc.User.FirstName.Contains(SearchTerm)
                    || euc.User.Email.Contains(SearchTerm)
                    || euc.applicationNo.Contains(SearchTerm)
                    || euc.User.ApplicationNumber.Contains(SearchTerm))
                    .Include(euc => euc.User).OrderBy
                                (euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                    break;
                case "ApplicantName":
                    if (sortOrder.Equals(CurrentSort))
                        certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected)
                    .Where(
                       euc => euc.User.LastName.Contains(SearchTerm)
                    || euc.User.FirstName.Contains(SearchTerm)
                    || euc.User.Email.Contains(SearchTerm)
                    || euc.applicationNo.Contains(SearchTerm)
                    || euc.User.ApplicationNumber.Contains(SearchTerm))
                    .Include(euc => euc.User).OrderByDescending
                                (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                    else
                        certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected)
                    .Where(
                       euc => euc.User.LastName.Contains(SearchTerm)
                    || euc.User.FirstName.Contains(SearchTerm)
                    || euc.User.Email.Contains(SearchTerm)
                    || euc.applicationNo.Contains(SearchTerm)
                    || euc.User.ApplicationNumber.Contains(SearchTerm))
                    .Include(euc => euc.User).OrderByDescending
                                (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                    break;
                case "ItemCategories":
                    if (sortOrder.Equals(CurrentSort))
                        certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected)
                    .Where(
                       euc => euc.User.LastName.Contains(SearchTerm)
                    || euc.User.FirstName.Contains(SearchTerm)
                    || euc.User.Email.Contains(SearchTerm)
                    || euc.applicationNo.Contains(SearchTerm)
                    || euc.User.ApplicationNumber.Contains(SearchTerm))
                    .Include(euc => euc.User).OrderByDescending
                                (euc => euc.itemCat).ToPagedList(pageIndex, pageSize);
                    else
                        certificates = db.EndUserCertificate
                    .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected)
                    .Where(
                       euc => euc.User.LastName.Contains(SearchTerm)
                    || euc.User.FirstName.Contains(SearchTerm)
                    || euc.User.Email.Contains(SearchTerm)
                    || euc.applicationNo.Contains(SearchTerm)
                    || euc.User.ApplicationNumber.Contains(SearchTerm))
                    .Include(euc => euc.User).OrderBy
                                (euc => euc.itemCat).ToPagedList(pageIndex, pageSize);
                    break;
                case "Status":
                    if (sortOrder.Equals(CurrentSort))
                        certificates = db.EndUserCertificate
                    .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected)
                    .Where(
                       euc => euc.User.LastName.Contains(SearchTerm)
                    || euc.User.FirstName.Contains(SearchTerm)
                    || euc.User.Email.Contains(SearchTerm)
                    || euc.applicationNo.Contains(SearchTerm)
                    || euc.User.ApplicationNumber.Contains(SearchTerm))
                    .Include(euc => euc.User).OrderByDescending
                                (euc => euc.EUCApprovalStatus).ToPagedList(pageIndex, pageSize);
                    else
                        certificates = db.EndUserCertificate
                    .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected)
                    .Where(
                       euc => euc.User.LastName.Contains(SearchTerm)
                    || euc.User.FirstName.Contains(SearchTerm)
                    || euc.User.Email.Contains(SearchTerm)
                    || euc.applicationNo.Contains(SearchTerm)
                    || euc.User.ApplicationNumber.Contains(SearchTerm))
                    .Include(euc => euc.User).OrderBy
                                (euc => euc.EUCApprovalStatus).ToPagedList(pageIndex, pageSize);
                    break;
                case "Default":
                    certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Rejected)
                    .Where(
                       euc => euc.User.LastName.Contains(SearchTerm)
                    || euc.User.FirstName.Contains(SearchTerm)
                    || euc.User.Email.Contains(SearchTerm)
                    || euc.applicationNo.Contains(SearchTerm)
                    || euc.User.ApplicationNumber.Contains(SearchTerm))
                    .Include(euc => euc.User).OrderByDescending
                                (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                    break;
            }
            ViewBag.CurrentSort = sortOrder;
            return View(new EndUserCertificatesViewModel
            {
                user = user,
                role = roles,
                //userList = users,
                certificatesToShow = certificates,
                PageSize = pageSize,
                sortOrder = sortOrder,
                searchTerm = SearchTerm,
                Agencies = agencies,
                itemCategories = itemCategories
            });
        }

        [Authorize(Roles = "Agency, ONSA Support, ONSA PGSO, NSA")]
        public ActionResult EndorsedEndUserCertificateApplication(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var agencies = _context.PortalSubCategories.ToList();
            var endorsedEucApplications = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "ENDORSED" && euc.SourceAgency == user.PortalUserCategoryId).OrderByDescending(date => date.activityDate).Distinct().ToList();
            var usersToQuery = _context.Users.ToList();
            var itemCategories = _context.EUCItemCategory.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;

            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;
            #region support users
            //Return from end-user certificate table if logged in as onsa support
            if (user.PortalUserCategoryId == 5 || user.PortalUserCategoryId == 45)
            {
                IPagedList<EndUserCertificate> certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed).Include(euc => euc.User).Include(euc => euc.AgencyForwarded).OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Include(euc => euc.User).Include(euc => euc.AgencyForwarded)
                        .OrderByDescending(euc => euc.dateCreated)
                        .ToPagedList(pageIndex, pageSize);

                    return View(new EndUserCertificatesViewModel
                    {
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        Agencies = agencies,
                        user = user,
                        role = roles,
                        EUCWorkFlow = endorsedEucApplications,
                        UsersToQuery = usersToQuery,
                        itemCategories = itemCategories
                    });
                }

                switch (sortOrder)
                {
                    case "DateCreated":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Include(euc => euc.User).Include(euc => euc.AgencyForwarded)
                        .OrderByDescending(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Include(euc => euc.User).Include(euc => euc.AgencyForwarded)
                        .OrderBy(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Include(euc => euc.User).Include(euc => euc.AgencyForwarded)
                        .OrderByDescending(euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Include(euc => euc.User).Include(euc => euc.AgencyForwarded)
                        .OrderBy(euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Include(euc => euc.User).Include(euc => euc.AgencyForwarded)
                        .OrderByDescending(euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Include(euc => euc.User).Include(euc => euc.AgencyForwarded)
                        .OrderBy(euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Include(euc => euc.User).Include(euc => euc.AgencyForwarded)
                        .OrderByDescending(euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Include(euc => euc.User).Include(euc => euc.AgencyForwarded)
                        .OrderBy(euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ItemCategories":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Include(euc => euc.User).Include(euc => euc.AgencyForwarded)
                        .OrderByDescending(euc => euc.itemCat).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Include(euc => euc.User).Include(euc => euc.AgencyForwarded)
                        .OrderBy(euc => euc.itemCat).ToPagedList(pageIndex, pageSize);
                        break;
                    case "Default":
                        certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed)
                        .Include(euc => euc.User).Include(euc => euc.AgencyForwarded)
                        .OrderByDescending(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                }
                ViewBag.CurrentSort = sortOrder;
                return View(new EndUserCertificatesViewModel
                {
                    user = user,
                    role = roles,
                    //userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    Agencies = agencies,
                    UsersToQuery = usersToQuery,
                    itemCategories = itemCategories
                });
            }
            #endregion
            #region agencies
            //Return from EUC workflow table if logged in as agency
            else if (user.PortalUserCategoryId == 1 || user.PortalUserCategoryId == 2 || user.PortalUserCategoryId == 3
                || user.PortalUserCategoryId == 4 || user.PortalUserCategoryId == 6 || user.PortalUserCategoryId == 7
                || user.PortalUserCategoryId == 8 || user.PortalUserCategoryId == 11 || user.PortalUserCategoryId == 12
                || user.PortalUserCategoryId == 10 || user.PortalUserCategoryId == 13 || user.PortalUserCategoryId == 14
                || user.PortalUserCategoryId == 16
                || user.PortalUserCategoryId == 17 || user.PortalUserCategoryId == 18 || user.PortalUserCategoryId == 19
                || user.PortalUserCategoryId == 20 || user.PortalUserCategoryId == 21 || user.PortalUserCategoryId == 25
                || user.PortalUserCategoryId == 26 || user.PortalUserCategoryId == 27
                || user.PortalUserCategoryId == 30 || user.PortalUserCategoryId == 31 || user.PortalUserCategoryId == 38
                || user.PortalUserCategoryId == 34 || user.PortalUserCategoryId == 36 || user.PortalUserCategoryId == 37
                || user.PortalUserCategoryId == 39 || user.PortalUserCategoryId == 40 || user.PortalUserCategoryId == 41
                || user.PortalUserCategoryId == 42 || user.PortalUserCategoryId == 43 || user.PortalUserCategoryId == 44
                || user.PortalUserCategoryId == 46 || user.PortalUserCategoryId == 47)
            {
                if (SearchTerm != "" && sortOrder == null)
                {
                    var certificates = _context.EUCWorkFlow.Where(euc => euc.SourceAgency == user.PortalUserCategoryId
                                       && euc.handledStatus == "ENDORSED").Include(euc => euc.Source)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm)
                                        || euc.Source.LastName.Contains(SearchTerm)
                                        || euc.Source.Email.Contains(SearchTerm)
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm)
                                        || euc.applicationNumber.Contains(SearchTerm))
                                        .OrderByDescending(euc => euc.activityDate)
                                        .ToPagedList(pageIndex, pageSize);
                    //_context.EUCWorkFlow.Include(euc => euc.Source)
                    //.Where(euc => euc.Source.FirstName.Contains(SearchTerm)
                    //|| euc.Source.LastName.Contains(SearchTerm)
                    //|| euc.Source.Email.Contains(SearchTerm)
                    //|| euc.Source.ApplicationNumber.Contains(SearchTerm)
                    //|| euc.applicationNumber.Contains(SearchTerm))
                    //.Where(euc => euc.handledStatus == "ENDORSED"
                    //&& euc.SourceAgency == user.PortalUserCategoryId)
                    //.Distinct().OrderByDescending(date => date.activityDate)
                    //.ToPagedList(pageIndex, pageSize);

                    return View(new EndUserCertificatesViewModel
                    {
                        applicationsFromWorflowToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        Agencies = agencies,
                        user = user,
                        role = roles,
                        EUCWorkFlow = endorsedEucApplications,
                        UsersToQuery = usersToQuery,
                        itemCategories = itemCategories
                    });
                }

                //eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed && euc.AgencyForwardedFrom == user.PortalUserCategoryId && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                var endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Include(euc => euc.Source).Where(euc => euc.handledStatus == "ENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED")
                                        .Distinct().OrderByDescending(date => date.activityDate)
                                        .ToPagedList(pageIndex, pageSize);

                var eucCategories = _context.EUCItemCategory.ToList();
                roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                users = _context.Users.ToList();
                var userType = _context.Users.Where(u => u.Id == userId).Single();

                switch (sortOrder)
                {
                    case "DateCreated":
                        if (sortOrder.Equals(CurrentSort))
                            endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Include(euc => euc.Source).Where(euc => euc.handledStatus == "ENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED")
                                        .Distinct().OrderByDescending(euc => euc.activityDate)
                                        .ToPagedList(pageIndex, pageSize);
                        else
                            endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Include(euc => euc.Source).Where(euc => euc.handledStatus == "ENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED")
                                        .Distinct().OrderBy(euc => euc.activityDate)
                                        .ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNo":
                        if (sortOrder.Equals(CurrentSort))
                            endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Include(euc => euc.Source).Where(euc => euc.handledStatus == "ENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED")
                                        .Distinct().OrderByDescending(euc => euc.applicationNumber)
                                        .ToPagedList(pageIndex, pageSize);
                        else
                            endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Include(euc => euc.Source).Where(euc => euc.handledStatus == "ENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED")
                                        .Distinct().OrderByDescending(euc => euc.applicationNumber)
                                        .ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantNo":
                        if (sortOrder.Equals(CurrentSort))
                            endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Include(euc => euc.Source).Where(euc => euc.handledStatus == "ENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED")
                                        .Distinct().OrderByDescending(euc => euc.Source.ApplicationNumber)
                                        .ToPagedList(pageIndex, pageSize);
                        else
                            endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Include(euc => euc.Source).Where(euc => euc.handledStatus == "ENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED")
                                        .Distinct().OrderBy(euc => euc.Source.ApplicationNumber)
                                        .ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Include(euc => euc.Source).Where(euc => euc.handledStatus == "ENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED")
                                        .Distinct().OrderByDescending(euc => euc.Source.LastName)
                                        .ToPagedList(pageIndex, pageSize);
                        else
                            endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Include(euc => euc.Source).Where(euc => euc.handledStatus == "ENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED")
                                        .Distinct().OrderByDescending(euc => euc.Source.LastName)
                                        .ToPagedList(pageIndex, pageSize);
                        break;
                    case "Default":
                        endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Include(euc => euc.Source).Where(euc => euc.handledStatus == "ENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "ENDORSED")
                                        .Distinct().OrderByDescending(euc => euc.activityDate)
                                        .ToPagedList(pageIndex, pageSize);
                        break;
                }

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("User viewed endorsed EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                   User.Identity.GetUserId(),
                   "Visited endorsed EUC applications page.",
                   Request.UserHostAddress);
                //Set sort value in view bag 
                if (CurrentSort == null)
                    ViewBag.CurrentSort = "DateCreated";
                else
                    ViewBag.CurrentSort = sortOrder;

                return View(new EndUserCertificatesViewModel
                {
                    user = user,
                    role = roles,
                    //userList = users,
                    applicationsFromWorflowToShow = endorsedEucApplicationsFromWorkflow,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    Agencies = agencies,
                    UsersToQuery = usersToQuery,
                    itemCategories = itemCategories
                });
            }
            #endregion
            else
            {
                return View();
            }
        }
        [Authorize(Roles = "Agency, ONSA Support, ONSA PGSO, NSA")]
        public ActionResult DisendorsedEndUserCertificateApplication(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var agencies = _context.PortalSubCategories.ToList();
            var endorsedEucApplications = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "DISENDORSED" && euc.SourceAgency == user.PortalUserCategoryId).OrderByDescending(date => date.activityDate).Distinct().ToList();
            var usersToQuery = _context.Users.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;

            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;

            //Return from end-user certificate table if logged in as onsa support
            if (user.PortalUserCategoryId == 5 || user.PortalUserCategoryId == 45)
            {
                IPagedList<EndUserCertificate> certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed).Include(euc => euc.User).Include(euc => euc.AgencyForwarded).OrderByDescending
                                    (euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                    return View(new EndUserCertificatesViewModel
                    {
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        Agencies = agencies,
                        user = user,
                        role = roles,
                        EUCWorkFlow = endorsedEucApplications,
                        UsersToQuery = usersToQuery
                    });
                }

                switch (sortOrder)
                {
                    case "DateCreated":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending(euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy(euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending(euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy(euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending(euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy(euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ItemCategories":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending(euc => euc.itemCat).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderBy(euc => euc.itemCat).ToPagedList(pageIndex, pageSize);
                        break;
                    case "Default":
                        certificates = db.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed)
                        .Where(
                           euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Disendorsed
                        || euc.AgencyForwarded.Name.Contains(SearchTerm))
                        .Include(euc => euc.User)
                        .Include(euc => euc.AgencyForwarded)
                        .OrderByDescending(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                }
                return View(new EndUserCertificatesViewModel
                {
                    user = user,
                    role = roles,
                    //userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    Agencies = agencies,
                    UsersToQuery = usersToQuery
                });
            }
            //Return from EUC workflow table if logged in as agency
            else if (user.PortalUserCategoryId == 1 || user.PortalUserCategoryId == 2 || user.PortalUserCategoryId == 3
                || user.PortalUserCategoryId == 4 || user.PortalUserCategoryId == 6 || user.PortalUserCategoryId == 7
                || user.PortalUserCategoryId == 8 || user.PortalUserCategoryId == 11 || user.PortalUserCategoryId == 12
                || user.PortalUserCategoryId == 10 || user.PortalUserCategoryId == 13 || user.PortalUserCategoryId == 14
                || user.PortalUserCategoryId == 16
                || user.PortalUserCategoryId == 17 || user.PortalUserCategoryId == 18 || user.PortalUserCategoryId == 19
                || user.PortalUserCategoryId == 20 || user.PortalUserCategoryId == 21 || user.PortalUserCategoryId == 25
                || user.PortalUserCategoryId == 26 || user.PortalUserCategoryId == 27
                || user.PortalUserCategoryId == 30 || user.PortalUserCategoryId == 31 || user.PortalUserCategoryId == 38
                || user.PortalUserCategoryId == 34 || user.PortalUserCategoryId == 36 || user.PortalUserCategoryId == 37
                || user.PortalUserCategoryId == 39 || user.PortalUserCategoryId == 40 || user.PortalUserCategoryId == 41
                || user.PortalUserCategoryId == 42 || user.PortalUserCategoryId == 43 || user.PortalUserCategoryId == 44
                || user.PortalUserCategoryId == 46 || user.PortalUserCategoryId == 47)
            {
                if (SearchTerm != "" && sortOrder == null)
                {
                    var certificates = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "DISENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Include(euc => euc.Source)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED")
                                        .Distinct().OrderByDescending(date => date.activityDate)
                                        .ToPagedList(pageIndex, pageSize);

                    return View(new EndUserCertificatesViewModel
                    {
                        applicationsFromWorflowToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        Agencies = agencies,
                        user = user,
                        role = roles,
                        EUCWorkFlow = endorsedEucApplications,
                        UsersToQuery = usersToQuery
                    });
                }

                //eucApplications = _context.EndUserCertificate.Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Endorsed && euc.AgencyForwardedFrom == user.PortalUserCategoryId && euc.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();
                var endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "DISENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Include(euc => euc.Source)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED")
                                        .Distinct().OrderByDescending(euc => euc.activityDate)
                                        .ToPagedList(pageIndex, pageSize);

                var eucCategories = _context.EUCItemCategory.ToList();
                roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
                users = _context.Users.ToList();
                var userType = _context.Users.Where(u => u.Id == userId).Single();

                switch (sortOrder)
                {
                    case "DateCreated":
                        if (sortOrder.Equals(CurrentSort))
                            endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "DISENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Include(euc => euc.Source)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED")
                                        .Distinct().OrderByDescending(euc => euc.activityDate)
                                        .ToPagedList(pageIndex, pageSize);
                        else
                            endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "DISENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Include(euc => euc.Source)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED")
                                        .Distinct().OrderBy(euc => euc.activityDate)
                                        .ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNo":
                        if (sortOrder.Equals(CurrentSort))
                            endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "DISENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Include(euc => euc.Source)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED")
                                        .Distinct().OrderByDescending(euc => euc.applicationNumber)
                                        .ToPagedList(pageIndex, pageSize);
                        else
                            endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "DISENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Include(euc => euc.Source)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED")
                                        .Distinct().OrderBy(euc => euc.applicationNumber)
                                        .ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantNo":
                        if (sortOrder.Equals(CurrentSort))
                            endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "DISENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Include(euc => euc.Source)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED")
                                        .Distinct().OrderByDescending(euc => euc.Source.ApplicationNumber)
                                        .ToPagedList(pageIndex, pageSize);
                        else
                            endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "DISENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Include(euc => euc.Source)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED")
                                        .Distinct().OrderBy(euc => euc.Source.ApplicationNumber)
                                        .ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "DISENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Include(euc => euc.Source)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED")
                                        .Distinct().OrderByDescending(euc => euc.Source.LastName)
                                        .ToPagedList(pageIndex, pageSize);
                        else
                            endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "DISENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Include(euc => euc.Source)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED")
                                        .Distinct().OrderByDescending(euc => euc.Source.LastName)
                                        .ToPagedList(pageIndex, pageSize);
                        break;
                    case "Default":
                        endorsedEucApplicationsFromWorkflow = _context.EUCWorkFlow.Where(euc => euc.handledStatus == "DISENDORSED"
                                        && euc.SourceAgency == user.PortalUserCategoryId)
                                        .Include(euc => euc.Source)
                                        .Where(euc => euc.Source.FirstName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.LastName.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.Email.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.Source.ApplicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED"
                                        || euc.applicationNumber.Contains(SearchTerm) && euc.handledStatus == "DISENDORSED")
                                        .Distinct().OrderByDescending(euc => euc.activityDate)
                                        .ToPagedList(pageIndex, pageSize);
                        break;
                }

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("User viewed disendorsed EUC applications page: " + user.ApplicationNumber + " Name: " + user.FirstName + " " + user.LastName,
                   User.Identity.GetUserId(),
                   "Visited disendorsed EUC applications page.",
                   Request.UserHostAddress);
                //Set sort value in view bag 
                if (CurrentSort == null)
                    ViewBag.CurrentSort = "DateCreated";
                else
                    ViewBag.CurrentSort = sortOrder;

                return View(new EndUserCertificatesViewModel
                {
                    user = user,
                    role = roles,
                    //userList = users,
                    applicationsFromWorflowToShow = endorsedEucApplicationsFromWorkflow,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    Agencies = agencies,
                    UsersToQuery = usersToQuery
                });
            }
            else
            {
                return View();
            }
        }
        [Authorize(Roles = "NSA")]
        public ActionResult EndUserCertificatesRequiringApproval(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var agencies = _context.PortalSubCategories.ToList();
            //eucApplications = _context.EndUserCertificate.Where(a => a.EUCApprovalStatus == EUCApprovedStatus.Requiring_Approval && a.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();

            //Check user portal category-5 for NSA-45-for SA
            //If it is the SA, redirect him to a view where he sees only applications which have been forwarded to him
            if (user.PortalUserCategoryId == 45)
            {
                return RedirectToAction("SAsEucsRequiringApproval");
            }

            var applicationsAwaitingApproal = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                        .Requiring_Approval && a.UserID != null)
                                        .OrderByDescending(euc => euc.dateCreated).Distinct().ToList();


            var usersToQuery = _context.Users.ToList();
            var itemCategories = _context.EUCItemCategory.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;

            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;

            IPagedList<EndUserCertificate> certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                        .Requiring_Approval && a.UserID != null).Distinct()
                                        .OrderByDescending(euc => euc.dateCreated)
                                        .ToPagedList(pageIndex, pageSize);


            //Return from end-user certificate table if logged in as NSA
            if (user.PortalUserCategoryId == 5)
            {
                if (SearchTerm != "")
                {
                    certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                            .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                            .Requiring_Approval && a.UserID != null)
                           .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                    return View(new EndUserCertificatesViewModel
                    {
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        Agencies = agencies,
                        user = user,
                        role = roles,
                        UsersToQuery = usersToQuery,
                        itemCategories = itemCategories
                    });
                }
            }
            //if user is signed in as NSA AKA 5
            else
            {
                certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                        .Requiring_Approval
                                        && a.UserID != null
                                        && a.User.PortalUserCategoryId == 5).Distinct()
                                        .OrderByDescending(euc => euc.dateCreated)
                                        .ToPagedList(pageIndex, pageSize);

                if (SearchTerm != "")
                {
                    certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                            .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                            .Requiring_Approval && a.UserID != null)
                           .Where(euc => euc.User.LastName.Contains(SearchTerm)
                           && euc.User.PortalUserCategoryId == 5
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm))
                        .Include(euc => euc.User).OrderByDescending(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                    return View(new EndUserCertificatesViewModel
                    {
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        Agencies = agencies,
                        user = user,
                        role = roles,
                        UsersToQuery = usersToQuery,
                        itemCategories = itemCategories
                    });
                }
            }
            //ViewBag.CurrentSort = sortOrder;
            //CurrentSort = sortOrder;

            switch (sortOrder)
            {
                case "DateCreated":
                    if (sortOrder.Equals(CurrentSort))
                        if (user.PortalUserCategoryId == 5)
                        {
                            certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                        .Requiring_Approval && a.UserID != null).Distinct()
                                        .OrderByDescending(euc => euc.dateCreated)
                                        .ToPagedList(pageIndex, pageSize);
                        }
                        else
                            certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                            .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                            .Requiring_Approval && a.UserID != null).Distinct()
                                            .OrderBy(euc => euc.dateCreated)
                                            .ToPagedList(pageIndex, pageSize);

                    if (user.PortalUserCategoryId == 45)
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                            .Where(a => a.AgencyForwardedTo == 45)
                                    .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                    .Requiring_Approval && a.UserID != null).Distinct()
                                    .OrderByDescending(euc => euc.dateCreated)
                                    .ToPagedList(pageIndex, pageSize);
                    }
                    else
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                       .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                       .Requiring_Approval && a.UserID != null).Distinct()
                                       .OrderBy(euc => euc.dateCreated)
                                       .ToPagedList(pageIndex, pageSize);
                    break;

                case "ApplicationNo":
                    if (sortOrder.Equals(CurrentSort))
                        if (user.PortalUserCategoryId == 5)
                        {
                            certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                        .Requiring_Approval && a.UserID != null).Distinct()
                                        .OrderByDescending(euc => euc.dateCreated)
                                        .ToPagedList(pageIndex, pageSize);
                        }
                        else
                        {
                            certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                            .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                            .Requiring_Approval && a.UserID != null).Distinct()
                                            .OrderBy(euc => euc.dateCreated)
                                            .ToPagedList(pageIndex, pageSize);
                        }

                    if (user.PortalUserCategoryId == 45)
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                            .Where(a => a.AgencyForwardedTo == 45)
                                    .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                    .Requiring_Approval && a.UserID != null).Distinct()
                                    .OrderByDescending(euc => euc.dateCreated)
                                    .ToPagedList(pageIndex, pageSize);
                    }
                    else
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                       .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                       .Requiring_Approval && a.UserID != null).Distinct()
                                       .OrderBy(euc => euc.dateCreated)
                                       .ToPagedList(pageIndex, pageSize);
                    }
                    break;
                case "ApplicantNo":
                    if (sortOrder.Equals(CurrentSort))
                        if (user.PortalUserCategoryId == 5)
                        {
                            certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                        .Requiring_Approval && a.UserID != null).Distinct()
                                        .OrderByDescending(euc => euc.dateCreated)
                                        .ToPagedList(pageIndex, pageSize);
                        }
                        else
                        {
                            certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                            .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                            .Requiring_Approval && a.UserID != null).Distinct()
                                            .OrderBy(euc => euc.dateCreated)
                                            .ToPagedList(pageIndex, pageSize);
                        }

                    if (user.PortalUserCategoryId == 45)
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                            .Where(a => a.AgencyForwardedTo == 45)
                                    .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                    .Requiring_Approval && a.UserID != null).Distinct()
                                    .OrderByDescending(euc => euc.dateCreated)
                                    .ToPagedList(pageIndex, pageSize);
                    }
                    else
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                       .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                       .Requiring_Approval && a.UserID != null).Distinct()
                                       .OrderBy(euc => euc.dateCreated)
                                       .ToPagedList(pageIndex, pageSize);
                    }
                    break;
                case "ApplicantName":
                    if (sortOrder.Equals(CurrentSort))
                        if (user.PortalUserCategoryId == 5)
                        {
                            certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                        .Requiring_Approval && a.UserID != null).Distinct()
                                        .OrderByDescending(euc => euc.dateCreated)
                                        .ToPagedList(pageIndex, pageSize);
                        }
                        else
                        {
                            certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                            .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                            .Requiring_Approval && a.UserID != null).Distinct()
                                            .OrderBy(euc => euc.dateCreated)
                                            .ToPagedList(pageIndex, pageSize);
                        }

                    if (user.PortalUserCategoryId == 45)
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                            .Where(a => a.AgencyForwardedTo == 45)
                                    .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                    .Requiring_Approval && a.UserID != null).Distinct()
                                    .OrderByDescending(euc => euc.dateCreated)
                                    .ToPagedList(pageIndex, pageSize);
                    }
                    else
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                       .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                       .Requiring_Approval && a.UserID != null).Distinct()
                                       .OrderBy(euc => euc.dateCreated)
                                       .ToPagedList(pageIndex, pageSize);
                    }
                    break;
                case "Default":
                    certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                        .Requiring_Approval && a.UserID != null && a.User.PortalUserCategoryId == 45).Distinct()
                                        .OrderByDescending(euc => euc.dateCreated)
                                        .ToPagedList(pageIndex, pageSize);
                    break;
            }
            //ViewBag.CurrentSort = sortOrder;
            return View(new EndUserCertificatesViewModel
            {
                user = user,
                role = roles,
                //userList = users,
                certificatesToShow = certificates,
                PageSize = pageSize,
                sortOrder = sortOrder,
                searchTerm = SearchTerm,
                Agencies = agencies,
                UsersToQuery = usersToQuery,
                itemCategories = itemCategories
            });

        }

        //FOR SA TO APPROVE AND VIEW APPLICATIONS FROM
        [Authorize(Roles = "NSA")]
        public ActionResult SAsEucsRequiringApproval(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var agencies = _context.PortalSubCategories.ToList();
            //eucApplications = _context.EndUserCertificate.Where(a => a.EUCApprovalStatus == EUCApprovedStatus.Requiring_Approval && a.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();

            var applicationsAwaitingApproal = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(euc => euc.AgencyForwardedTo == 45)
                                        .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                        .Requiring_Approval
                                        && a.UserID != null)
                                        .OrderByDescending(euc => euc.dateCreated).Distinct().ToList();


            var usersToQuery = _context.Users.ToList();
            var itemCategories = _context.EUCItemCategory.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;

            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;

            IPagedList<EndUserCertificate> certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(euc => euc.AgencyForwardedTo == 45)
                                        .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                        .Requiring_Approval
                                        && a.UserID != null).Distinct()
                                        .OrderByDescending(euc => euc.dateCreated)
                                        .ToPagedList(pageIndex, pageSize);


            //Return from end-user certificate table
            if (SearchTerm != "")
            {
                certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(euc => euc.AgencyForwardedTo == 45)
                                        .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                        .Requiring_Approval && a.UserID != null)
                       .Where(euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Requiring_Approval
                    || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Requiring_Approval
                    || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Requiring_Approval
                    || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Requiring_Approval
                    || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Requiring_Approval)
                    .Include(euc => euc.User).OrderByDescending(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                return View(new EndUserCertificatesViewModel
                {
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    Agencies = agencies,
                    user = user,
                    role = roles,
                    UsersToQuery = usersToQuery,
                    itemCategories = itemCategories
                });
            }


            //ViewBag.CurrentSort = sortOrder;
            //CurrentSort = sortOrder;

            switch (sortOrder)
            {
                case "DateCreated":
                    if (sortOrder.Equals(CurrentSort))
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                    .Where(euc => euc.AgencyForwardedTo == 45)
                                    .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                    .Requiring_Approval && a.UserID != null).Distinct()
                                    .OrderByDescending(euc => euc.dateCreated)
                                    .ToPagedList(pageIndex, pageSize);
                    }
                    else
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(euc => euc.AgencyForwardedTo == 45)
                                       .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                       .Requiring_Approval && a.UserID != null).Distinct()
                                       .OrderBy(euc => euc.dateCreated)
                                       .ToPagedList(pageIndex, pageSize);
                    }
                    break;

                case "ApplicationNo":
                    if (sortOrder.Equals(CurrentSort))
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                            .Where(a => a.AgencyForwardedTo == 45)
                                    .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                    .Requiring_Approval && a.UserID != null).Distinct()
                                    .OrderByDescending(euc => euc.applicationNo)
                                    .ToPagedList(pageIndex, pageSize);
                    }
                    else
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                             .Where(a => a.AgencyForwardedTo == 45)
                                       .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                       .Requiring_Approval && a.UserID != null).Distinct()
                                       .OrderBy(euc => euc.applicationNo)
                                       .ToPagedList(pageIndex, pageSize);
                    }
                    break;
                case "ApplicantNo":
                    if (sortOrder.Equals(CurrentSort))
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                            .Where(a => a.AgencyForwardedTo == 45)
                                    .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                    .Requiring_Approval && a.UserID != null).Distinct()
                                    .OrderByDescending(euc => euc.User.ApplicationNumber)
                                    .ToPagedList(pageIndex, pageSize);
                    }
                    else
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                             .Where(a => a.AgencyForwardedTo == 45)
                                       .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                       .Requiring_Approval && a.UserID != null).Distinct()
                                       .OrderBy(euc => euc.User.ApplicationNumber)
                                       .ToPagedList(pageIndex, pageSize);
                    }
                    break;
                case "ApplicantName":
                    if (sortOrder.Equals(CurrentSort))
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                            .Where(a => a.AgencyForwardedTo == 45)
                                    .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                    .Requiring_Approval && a.UserID != null).Distinct()
                                    .OrderByDescending(euc => euc.User.LastName)
                                    .ToPagedList(pageIndex, pageSize);
                    }
                    else
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(a => a.AgencyForwardedTo == 45)
                                       .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                       .Requiring_Approval && a.UserID != null).Distinct()
                                       .OrderBy(euc => euc.User.LastName)
                                       .ToPagedList(pageIndex, pageSize);
                    }
                    break;
                case "Default":
                    certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                         .Where(a => a.AgencyForwardedTo == 45)
                                        .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                        .Requiring_Approval && a.UserID != null).Distinct()
                                        .OrderByDescending(euc => euc.dateCreated)
                                        .ToPagedList(pageIndex, pageSize);
                    break;
            }
            //ViewBag.CurrentSort = sortOrder;
            return View(new EndUserCertificatesViewModel
            {
                user = user,
                role = roles,
                //userList = users,
                certificatesToShow = certificates,
                PageSize = pageSize,
                sortOrder = sortOrder,
                searchTerm = SearchTerm,
                Agencies = agencies,
                UsersToQuery = usersToQuery,
                itemCategories = itemCategories
            });
        }

        //FOR SA TO APPROVE AND VIEW APPLICATIONS FROM
        [Authorize(Roles = "NSA")]
        public ActionResult SAsEucsApproved(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var agencies = _context.PortalSubCategories.ToList();
            //eucApplications = _context.EndUserCertificate.Where(a => a.EUCApprovalStatus == EUCApprovedStatus.Requiring_Approval && a.UserID != null).OrderByDescending(date => date.dateCreated).Distinct().ToList();

            var applicationsAwaitingApproal = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(euc => euc.AgencyForwardedFrom == 45)
                                        .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                        .Approved
                                        && a.UserID != null)
                                        .OrderByDescending(euc => euc.dateCreated).Distinct().ToList();


            var usersToQuery = _context.Users.ToList();
            var itemCategories = _context.EUCItemCategory.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;

            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;

            IPagedList<EndUserCertificate> certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(euc => euc.AgencyForwardedFrom == 45)
                                        .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                        .Approved
                                        && a.UserID != null).Distinct()
                                        .OrderByDescending(euc => euc.dateCreated)
                                        .ToPagedList(pageIndex, pageSize);


            //Return from end-user certificate table
            if (SearchTerm != "")
            {
                certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(euc => euc.AgencyForwardedFrom == 45)
                                        .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                        .Approved && a.UserID != null)
                       .Where(euc => euc.User.LastName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Requiring_Approval
                    || euc.User.FirstName.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Requiring_Approval
                    || euc.User.Email.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Requiring_Approval
                    || euc.User.ApplicationNumber.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Requiring_Approval
                    || euc.applicationNo.Contains(SearchTerm) && euc.EUCApprovalStatus == EUCApprovedStatus.Requiring_Approval)
                    .Include(euc => euc.User).OrderByDescending(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);

                return View(new EndUserCertificatesViewModel
                {
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    Agencies = agencies,
                    user = user,
                    role = roles,
                    UsersToQuery = usersToQuery,
                    itemCategories = itemCategories
                });
            }


            //ViewBag.CurrentSort = sortOrder;
            //CurrentSort = sortOrder;

            switch (sortOrder)
            {
                case "DateCreated":
                    if (sortOrder.Equals(CurrentSort))
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                    .Where(euc => euc.AgencyForwardedFrom == 45)
                                    .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                    .Approved && a.UserID != null).Distinct()
                                    .OrderByDescending(euc => euc.dateCreated)
                                    .ToPagedList(pageIndex, pageSize);
                    }
                    else
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(euc => euc.AgencyForwardedFrom == 45)
                                       .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                       .Approved && a.UserID != null).Distinct()
                                       .OrderBy(euc => euc.dateCreated)
                                       .ToPagedList(pageIndex, pageSize);
                    }
                    break;

                case "ApplicationNo":
                    if (sortOrder.Equals(CurrentSort))
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                            .Where(a => a.AgencyForwardedFrom == 45)
                                    .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                    .Approved && a.UserID != null).Distinct()
                                    .OrderByDescending(euc => euc.applicationNo)
                                    .ToPagedList(pageIndex, pageSize);
                    }
                    else
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                             .Where(a => a.AgencyForwardedFrom == 45)
                                       .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                       .Approved && a.UserID != null).Distinct()
                                       .OrderBy(euc => euc.applicationNo)
                                       .ToPagedList(pageIndex, pageSize);
                    }
                    break;
                case "ApplicantNo":
                    if (sortOrder.Equals(CurrentSort))
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                            .Where(a => a.AgencyForwardedFrom == 45)
                                    .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                    .Approved && a.UserID != null).Distinct()
                                    .OrderByDescending(euc => euc.User.ApplicationNumber)
                                    .ToPagedList(pageIndex, pageSize);
                    }
                    else
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                             .Where(a => a.AgencyForwardedFrom == 45)
                                       .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                       .Approved && a.UserID != null).Distinct()
                                       .OrderBy(euc => euc.User.ApplicationNumber)
                                       .ToPagedList(pageIndex, pageSize);
                    }
                    break;
                case "ApplicantName":
                    if (sortOrder.Equals(CurrentSort))
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                            .Where(a => a.AgencyForwardedFrom == 45)
                                    .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                    .Approved && a.UserID != null).Distinct()
                                    .OrderByDescending(euc => euc.User.LastName)
                                    .ToPagedList(pageIndex, pageSize);
                    }
                    else
                    {
                        certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                        .Where(a => a.AgencyForwardedFrom == 45)
                                       .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                       .Approved && a.UserID != null).Distinct()
                                       .OrderBy(euc => euc.User.LastName)
                                       .ToPagedList(pageIndex, pageSize);
                    }
                    break;
                case "Default":
                    certificates = _context.EndUserCertificate.Include(euc => euc.User)
                                         .Where(a => a.AgencyForwardedFrom == 45)
                                        .Where(a => a.EUCApprovalStatus == EUCApprovedStatus
                                        .Approved && a.UserID != null).Distinct()
                                        .OrderByDescending(euc => euc.dateCreated)
                                        .ToPagedList(pageIndex, pageSize);
                    break;
            }
            //ViewBag.CurrentSort = sortOrder;
            return View(new EndUserCertificatesViewModel
            {
                user = user,
                role = roles,
                //userList = users,
                certificatesToShow = certificates,
                PageSize = pageSize,
                sortOrder = sortOrder,
                searchTerm = SearchTerm,
                Agencies = agencies,
                UsersToQuery = usersToQuery,
                itemCategories = itemCategories
            });
        }

        public ActionResult ApproveComment(string AppID)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("NSA viewed Add comment page. Email: " + User.Identity.GetUserName() + " Name: " + user.FirstName + " " + user.LastName + " Application Number: " + user.ApplicationNumber,
               User.Identity.GetUserId(),
               "Add Comment Page",
               Request.UserHostAddress);

            return View(new ApproveCommentViewModel
            {
                EUCID = AppID,
                user = user,
                role = roles
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApproveComment(ApproveCommentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var ApproveComment = new EUCApplicationComments
                {
                    applicationNo = model.EUCID,
                    userFrom = User.Identity.GetUserId(),
                    userComment = model.Comment,
                    FlagDate = DateTime.Now.Date
                };
                _context.EUCApplicationComments.Add(ApproveComment);
                _context.SaveChanges();

                TempData["Success"] = "Comment Added Successfully!";
                return RedirectToAction("ApprovedEndUserCertificateApplication", "Admin");
            }
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(model);
        }

        //Pagination for Blacklist History
        [Authorize(Roles = "Agency, ONSA Admin, ONSA Support, ONSA PGSO, NSA")]
        public ActionResult BlacklistHistoryList(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var agencies = _context.PortalSubCategories.ToList();

            var historyList = _context.BlackListApplicant.OrderByDescending(b => b.BlacklistDate).ToList();
            var usersToQuery = _context.Users.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;

            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;

            //Return from end-user certificate table if logged in as onsa support
            if (user.PortalUserCategoryId == 5 || user.PortalUserCategoryId == 45)
            {
                IPagedList<BlackListApplicant> blacklistHistory = _context.BlackListApplicant.Include(s => s.Applicant)
                                                                .OrderByDescending(s => s.BlacklistDate)
                                                                .ToPagedList(pageIndex, pageSize);
                if (SearchTerm != "" && sortOrder == null)
                {
                    blacklistHistory = _context.BlackListApplicant
                                        .Where(blacklistedApplicants => blacklistedApplicants.Applicant.LastName.Contains(SearchTerm)
                                        || blacklistedApplicants.Applicant.ApplicationNumber.Contains(SearchTerm))
                                      .OrderByDescending(blacklistedApplicants => blacklistedApplicants.BlacklistDate)
                                      .ToPagedList(pageIndex, pageSize);

                    return View(new BlacklistedViewModel
                    {
                        BlacklistHistory = blacklistHistory,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        user = user,
                        role = roles,
                        UserList = usersToQuery
                    });
                }

                switch (sortOrder)
                {
                    case "BlackListDate":
                        if (sortOrder.Equals(CurrentSort))
                            blacklistHistory = _context.BlackListApplicant
                                        .Where(blacklistedApplicants => blacklistedApplicants.Applicant.LastName.Contains(SearchTerm)
                                        || blacklistedApplicants.Applicant.ApplicationNumber.Contains(SearchTerm))
                                      .OrderByDescending(blacklistedApplicants => blacklistedApplicants.BlacklistDate)
                                      .ToPagedList(pageIndex, pageSize);
                        else
                            blacklistHistory = _context.BlackListApplicant
                                        .Where(blacklistedApplicants => blacklistedApplicants.Applicant.LastName.Contains(SearchTerm)
                                        || blacklistedApplicants.Applicant.ApplicationNumber.Contains(SearchTerm))
                                      .OrderBy(blacklistedApplicants => blacklistedApplicants.BlacklistDate)
                                      .ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantNumber":
                        if (sortOrder.Equals(CurrentSort))
                            blacklistHistory = _context.BlackListApplicant
                                        .Where(blacklistedApplicants => blacklistedApplicants.Applicant.LastName.Contains(SearchTerm)
                                        || blacklistedApplicants.Applicant.ApplicationNumber.Contains(SearchTerm))
                                      .OrderByDescending(blacklistedApplicants => blacklistedApplicants.Applicant.ApplicationNumber)
                                      .ToPagedList(pageIndex, pageSize);
                        else
                            blacklistHistory = _context.BlackListApplicant
                                        .Where(blacklistedApplicants => blacklistedApplicants.Applicant.LastName.Contains(SearchTerm)
                                        || blacklistedApplicants.Applicant.ApplicationNumber.Contains(SearchTerm))
                                      .OrderBy(blacklistedApplicants => blacklistedApplicants.Applicant.ApplicationNumber)
                                      .ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            blacklistHistory = _context.BlackListApplicant
                                        .Where(blacklistedApplicants => blacklistedApplicants.Applicant.LastName.Contains(SearchTerm)
                                        || blacklistedApplicants.Applicant.ApplicationNumber.Contains(SearchTerm))
                                      .OrderByDescending(blacklistedApplicants => blacklistedApplicants.Applicant.LastName)
                                      .ToPagedList(pageIndex, pageSize);
                        else
                            blacklistHistory = _context.BlackListApplicant.Include(s => s.Applicant)
                                                                .OrderBy(s => s.Applicant.LastName)
                                                                .ToPagedList(pageIndex, pageSize);
                        break;
                    case "ActionPerformed":
                        if (sortOrder.Equals(CurrentSort))
                            blacklistHistory = _context.BlackListApplicant.Include(s => s.Applicant)
                                                                .OrderByDescending(s => s.action)
                                                                .ToPagedList(pageIndex, pageSize);
                        else
                            blacklistHistory = _context.BlackListApplicant.Include(s => s.Applicant)
                                                                .OrderBy(s => s.action)
                                                                .ToPagedList(pageIndex, pageSize);
                        break;
                    case "Default":
                        blacklistHistory = _context.BlackListApplicant.Include(s => s.Applicant)
                                                                .OrderByDescending(s => s.BlacklistDate)
                                                                .ToPagedList(pageIndex, pageSize);
                        break;

                }
                return View(new BlacklistedViewModel
                {
                    user = user,
                    role = roles,
                    BlacklistHistory = blacklistHistory,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    UserList = usersToQuery,
                });
            }
            else
            {
                return View("Index");
            }
        }

        [Authorize(Roles = "Agency, ONSA Admin, ONSA Support, ONSA PGSO, NSA")]
        //Pagination for sign up requests
        public ActionResult AllBlacklistedApplicant(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.blacklistedDate).ToList();
            var blacklistList = _context.BlackListApplicant.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            IPagedList<ApplicationUser> usersToShow = db.Users.Where(u => u.IsBlackListed == true).OrderByDescending
                                (euc => euc.blacklistedDate).ToPagedList(pageIndex, pageSize);

            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;

            if (SearchTerm != "" && sortOrder == null)
            {
                usersToShow = db.Users
                    .Where(userToShow => userToShow.IsBlackListed == true)
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.blacklistedDate).ToPagedList(pageIndex, pageSize);

                return View(new AllUserSignUpRequestsViewModel
                {
                    user = user,
                    role = roles,
                    usersToShow = usersToShow,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    users = users,
                    BlacklistedApplicants = blacklistList
                });
            }

            switch (sortOrder)
            {
                case "blacklistDate":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.IsBlackListed == true)
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.blacklistedDate).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.IsBlackListed == true)
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.blacklistedDate).ToPagedList(pageIndex, pageSize);
                    break;
                case "applicantType":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.IsBlackListed == true)
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.CategoryId).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.IsBlackListed == true)
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.CategoryId).ToPagedList(pageIndex, pageSize);
                    break;
                case "ApplicationNo":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.IsBlackListed == true)
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.IsBlackListed == true)
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                    break;
                case "ApplicantName":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.IsBlackListed == true)
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.LastName).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.IsBlackListed == true)
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.LastName).ToPagedList(pageIndex, pageSize);
                    break;
                case "BlacklistedBy":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.IsBlackListed == true)
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.blacklistedBy).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users
                    .Where(userToShow => userToShow.IsBlackListed == true)
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderBy(userToShow => userToShow.blacklistedBy).ToPagedList(pageIndex, pageSize);
                    break;
                case "Default":
                    usersToShow = db.Users
                    .Where(userToShow => userToShow.IsBlackListed == true)
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.blacklistedDate).ToPagedList(pageIndex, pageSize);
                    break;
            }
            ViewBag.CurrentSort = sortOrder;
            return View(new AllUserSignUpRequestsViewModel
            {
                user = user,
                role = roles,
                //userList = users,
                usersToShow = usersToShow,
                PageSize = pageSize,
                sortOrder = sortOrder,
                searchTerm = SearchTerm,
                users = users,
                BlacklistedApplicants = blacklistList
            });
        }

        [Authorize(Roles = "Agency, ONSA Admin, ONSA Support, ONSA PGSO, NSA")]
        //Pagination for sign up requests
        public ActionResult ManageAllUsers(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var portalSubCategories = _context.PortalSubCategories.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            IPagedList<ApplicationUser> usersToShow = db.Users.OrderByDescending
                                (euc => euc.RequestedDate).ToPagedList(pageIndex, pageSize);

            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;

            if (SearchTerm != "")
            {
                usersToShow = db.Users
                    .Where(userToShow => userToShow.LastName.Contains(SearchTerm)
                    || userToShow.FirstName.Contains(SearchTerm)
                    || userToShow.Email.Contains(SearchTerm)
                    || userToShow.ApplicationNumber.Contains(SearchTerm)
                    || userToShow.PhoneNumber.Contains(SearchTerm))
                    .OrderByDescending(userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);

                return View(new AllUserSignUpRequestsViewModel
                {
                    user = user,
                    role = roles,
                    usersToShow = usersToShow,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    UserAgency = portalSubCategories
                });
            }

            switch (sortOrder)
            {
                case "ApplicantType":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users.OrderByDescending
                                (userToShow => userToShow.CategoryId).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users.OrderBy
                                (userToShow => userToShow.CategoryId).ToPagedList(pageIndex, pageSize);
                    break;
                case "DateCreated":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users.OrderByDescending
                                (userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users.OrderBy
                                (userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);
                    break;
                case "ApplicationNo":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users.OrderByDescending
                                (userToShow => userToShow.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users.OrderBy
                                (userToShow => userToShow.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                    break;
                case "LastName":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users.OrderByDescending
                                (userToShow => userToShow.LastName).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users.OrderBy
                                (userToShow => userToShow.LastName).ToPagedList(pageIndex, pageSize);
                    break;
                case "FirstName":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users.OrderByDescending
                                (userToShow => userToShow.FirstName).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users.OrderBy
                                (userToShow => userToShow.LastName).ToPagedList(pageIndex, pageSize);
                    break;
                case "Email":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users.OrderByDescending
                                (userToShow => userToShow.Email).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users.OrderBy
                                (userToShow => userToShow.Email).ToPagedList(pageIndex, pageSize);
                    break;
                case "PhoneNumber":
                    if (sortOrder.Equals(CurrentSort))
                        usersToShow = db.Users.OrderByDescending
                                (userToShow => userToShow.PhoneNumber).ToPagedList(pageIndex, pageSize);
                    else
                        usersToShow = db.Users.OrderBy
                                (userToShow => userToShow.PhoneNumber).ToPagedList(pageIndex, pageSize);
                    break;
                case "Default":
                    usersToShow = usersToShow = db.Users.OrderByDescending
                                (userToShow => userToShow.RequestedDate).ToPagedList(pageIndex, pageSize);
                    break;
            }
            ViewBag.CurrentSort = sortOrder;
            return View(new AllUserSignUpRequestsViewModel
            {
                user = user,
                role = roles,
                //userList = users,
                usersToShow = usersToShow,
                PageSize = pageSize,
                sortOrder = sortOrder,
                searchTerm = SearchTerm,
                UserAgency = portalSubCategories
            });
        }

        [Authorize(Roles = "Agency, ONSA Admin, ONSA Support")]
        //Pagination for sign up requests
        public ActionResult AuditTrailsView(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.ToList();
            var auditTrail = _context.AuditTrail.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            IPagedList<AuditTrail> AuditTrails = db.AuditTrail.OrderByDescending
                                (euc => euc.DateOfActivity).ToPagedList(pageIndex, pageSize);

            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;

            if (SearchTerm != "")
            {
                AuditTrails = db.AuditTrail
                    .Where(itemToShow => itemToShow.ActionCarriedOut.Contains(SearchTerm)
                    || itemToShow.ActionDescription.Contains(SearchTerm)
                    || itemToShow.UserIPAddress.Contains(SearchTerm))
                    .OrderByDescending(itemToShow => itemToShow.DateOfActivity).ToPagedList(pageIndex, pageSize);

                return View(new AuditTrailViewModel
                {
                    user = user,
                    role = roles,
                    AuditTrailList = AuditTrails,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    UserList = users
                });
            }

            switch (sortOrder)
            {
                case "ActivityDate":
                    if (sortOrder.Equals(CurrentSort))
                        AuditTrails = db.AuditTrail.OrderByDescending
                                (itemToShow => itemToShow.DateOfActivity).ToPagedList(pageIndex, pageSize);
                    else
                        AuditTrails = db.AuditTrail.OrderBy
                                (itemToShow => itemToShow.DateOfActivity).ToPagedList(pageIndex, pageSize);
                    break;
                case "ActionCarriedOut":
                    if (sortOrder.Equals(CurrentSort))
                        AuditTrails = db.AuditTrail.OrderByDescending
                                (itemToShow => itemToShow.ActionCarriedOut).ToPagedList(pageIndex, pageSize);
                    else
                        AuditTrails = db.AuditTrail.OrderBy
                                (itemToShow => itemToShow.ActionCarriedOut).ToPagedList(pageIndex, pageSize);
                    break;
                case "ActionDescription":
                    if (sortOrder.Equals(CurrentSort))
                        AuditTrails = db.AuditTrail.OrderByDescending
                                (itemToShow => itemToShow.ActionDescription).ToPagedList(pageIndex, pageSize);
                    else
                        AuditTrails = db.AuditTrail.OrderBy
                                (itemToShow => itemToShow.ActionDescription).ToPagedList(pageIndex, pageSize);
                    break;
                case "UserIP":
                    if (sortOrder.Equals(CurrentSort))
                        AuditTrails = db.AuditTrail.OrderByDescending
                                (itemToShow => itemToShow.UserIPAddress).ToPagedList(pageIndex, pageSize);
                    else
                        AuditTrails = db.AuditTrail.OrderBy
                                (itemToShow => itemToShow.UserIPAddress).ToPagedList(pageIndex, pageSize);
                    break;
                case "Default":
                    AuditTrails = AuditTrails = db.AuditTrail.OrderByDescending
                                (itemToShow => itemToShow.DateOfActivity).ToPagedList(pageIndex, pageSize);
                    break;
            }
            ViewBag.CurrentSort = sortOrder;
            return View(new AuditTrailViewModel
            {
                user = user,
                role = roles,
                //userList = users,
                AuditTrailList = AuditTrails,
                PageSize = pageSize,
                sortOrder = sortOrder,
                searchTerm = SearchTerm,
                UserList = users
            });
        }
        [Authorize(Roles = "Agency, Client User, ONSA PGSO, Customs User, NSA, Exclusive User, ONSA Support")]
        //Pagination for EUC Applications
        // GET: /Employee/  
        public ActionResult AllEndUserCertificates(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var itemCategories = _context.EUCItemCategory.ToList();
            var uploadedCertificates = _context.UploadedEUCCertificate.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            IPagedList<EUCCertificates> certificates = db.EUCCertificates.Include(certificate => certificate.User).OrderByDescending
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
            #region support
            //SUPPORT USERS
            if (user.PortalUserCategoryId == 5)
            {
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                    .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                    .OrderByDescending(certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);

                    return View(new EUCViewModel
                    {
                        user = user,
                        role = roles,
                        userList = users,
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        itemCategories = itemCategories,
                        UploadedCertificates = uploadedCertificates
                    });
                }
                switch (sortOrder)
                {
                    case "ExpiryDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "IssuanceDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateNumber":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNumber":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.applicationNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.User.LastName).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateStatus":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        break;
                    default:
                        certificates = db.EUCCertificates.Include(certificate => certificate.User)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm)
                                    )
                                    .Distinct()
                            .OrderByDescending
                            (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        break;
                }
                return View(new EUCViewModel
                {
                    user = user,
                    role = roles,
                    userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories,
                    UploadedCertificates = uploadedCertificates
                });
            }
            else if (user.PortalUserCategoryId == 45)
            {
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EUCCertificates.Include(certificate => certificate.User)
                        .Where(certificate => certificate.User.PortalUserCategoryId == 45)
                                    .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                    .OrderByDescending(certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);

                    return View(new EUCViewModel
                    {
                        user = user,
                        role = roles,
                        userList = users,
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        itemCategories = itemCategories,
                        UploadedCertificates = uploadedCertificates
                    });
                }
                switch (sortOrder)
                {
                    case "ExpiryDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.User.PortalUserCategoryId == 45)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.User.PortalUserCategoryId == 45)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "IssuanceDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.User.PortalUserCategoryId == 45)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.User.PortalUserCategoryId == 45)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateNumber":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.User.PortalUserCategoryId == 45)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.User.PortalUserCategoryId == 45)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNumber":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.User.PortalUserCategoryId == 45)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.User.PortalUserCategoryId == 45)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.applicationNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.User.PortalUserCategoryId == 45)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.User.PortalUserCategoryId == 45)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.User.LastName).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateStatus":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.User.PortalUserCategoryId == 45)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.User.PortalUserCategoryId == 45)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        break;
                    default:
                        certificates = db.EUCCertificates.Include(certificate => certificate.User)
                            .Where(certificate => certificate.User.PortalUserCategoryId == 45)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm)
                                    )
                                    .Distinct()
                            .OrderByDescending
                            (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        break;
                }
                return View(new EUCViewModel
                {
                    user = user,
                    role = roles,
                    userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories,
                    UploadedCertificates = uploadedCertificates
                });
            }
            #endregion
            //AGENCY USERS
            else if (user.PortalUserCategoryId == 1 || user.PortalUserCategoryId == 2 || user.PortalUserCategoryId == 3
                || user.PortalUserCategoryId == 4 || user.PortalUserCategoryId == 6 || user.PortalUserCategoryId == 7
                || user.PortalUserCategoryId == 8 || user.PortalUserCategoryId == 11 || user.PortalUserCategoryId == 12
                || user.PortalUserCategoryId == 10 || user.PortalUserCategoryId == 13 || user.PortalUserCategoryId == 14
                || user.PortalUserCategoryId == 16
                || user.PortalUserCategoryId == 17 || user.PortalUserCategoryId == 18 || user.PortalUserCategoryId == 19
                || user.PortalUserCategoryId == 20 || user.PortalUserCategoryId == 21 || user.PortalUserCategoryId == 25
                || user.PortalUserCategoryId == 26 || user.PortalUserCategoryId == 27 || user.PortalUserCategoryId == 30
                || user.PortalUserCategoryId == 38 || user.PortalUserCategoryId == 34 || user.PortalUserCategoryId == 36
                || user.PortalUserCategoryId == 37 || user.PortalUserCategoryId == 39 || user.PortalUserCategoryId == 40
                || user.PortalUserCategoryId == 41 || user.PortalUserCategoryId == 42 || user.PortalUserCategoryId == 43
                || user.PortalUserCategoryId == 44 || user.PortalUserCategoryId == 46 || user.PortalUserCategoryId == 47)
            {
                string SearchTerm = "";

                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                    .Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                                    .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                    .OrderByDescending(certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);

                    return View(new EUCViewModel
                    {
                        user = user,
                        role = roles,
                        userList = users,
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        itemCategories = itemCategories,
                        UploadedCertificates = uploadedCertificates
                    });
                }
                switch (sortOrder)
                {
                    case "ExpiryDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "IssuanceDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateNumber":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNumber":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.applicationNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.User.LastName).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateStatus":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        break;
                    default:
                        certificates = db.EUCCertificates.Include(certificate => certificate.User)
                            .Where(euc => euc.AgencyId == user.PortalUserCategoryId && euc.CertStatus != CertificateStatus.Pending)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                            .OrderByDescending
                            (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                }
                return View(new EUCViewModel
                {
                    user = user,
                    role = roles,
                    userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories,
                    UploadedCertificates = uploadedCertificates
                });
            }
            //EXCLUSIVE
            if (user.PortalUserCategoryId == 28 || user.PortalUserCategoryId == 29)
            {
                string SearchTerm = "";

                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                    .Where(euc => euc.UserId == user.Id && euc.CertStatus != CertificateStatus.Pending)
                                    .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                    .OrderByDescending(certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);

                    return View(new EUCViewModel
                    {
                        user = user,
                        role = roles,
                        userList = users,
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        itemCategories = itemCategories,
                        UploadedCertificates = uploadedCertificates
                    });
                }
                switch (sortOrder)
                {
                    case "ExpiryDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(euc => euc.UserId == user.Id && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(euc => euc.UserId == user.Id && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "IssuanceDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(euc => euc.UserId == user.Id && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(euc => euc.UserId == user.Id && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateNumber":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(euc => euc.UserId == user.Id && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(euc => euc.UserId == user.Id && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicationNumber":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(euc => euc.UserId == user.Id && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(euc => euc.UserId == user.Id && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.applicationNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(euc => euc.UserId == user.Id && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(euc => euc.UserId == user.Id && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.User.LastName).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateStatus":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(euc => euc.UserId == user.Id && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(euc => euc.UserId == user.Id && euc.CertStatus != CertificateStatus.Pending)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        break;
                    default:
                        certificates = db.EUCCertificates
                            .Where(euc => euc.UserId == user.Id && euc.CertStatus != CertificateStatus.Pending)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                            .OrderByDescending
                            (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                }
                return View(new EUCViewModel
                {
                    user = user,
                    role = roles,
                    userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories,
                    UploadedCertificates = uploadedCertificates
                });
            }
            else
            {
                return View();
            }
        }
        [Authorize(Roles = "Agency, Client User, NSA, ONSA PGSO,Customs User, Exclusive User")]
        //Pagination for EUC Applications
        // GET: /Employee/  
        public ActionResult DisputedEndUserCertificates(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var itemCategories = _context.EUCItemCategory.ToList();
            var uploadedCertificates = _context.UploadedEUCCertificate.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            //eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Disputed).OrderByDescending(date => date.IssuanceDate).ToList();

            IPagedList<EUCCertificates> certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Disputed)
                                .OrderByDescending
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
            #region support
            //Support User
            if (user.PortalUserCategoryId == 5 || user.PortalUserCategoryId == 45)
            {
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                    .Where(certificate => certificate.CertStatus == CertificateStatus.Disputed)
                                    .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                    .OrderByDescending(certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);

                    return View(new EUCViewModel
                    {
                        user = user,
                        role = roles,
                        userList = users,
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        itemCategories = itemCategories,
                        UploadedCertificates = uploadedCertificates
                    });
                }

                switch (sortOrder)
                {
                    case "ExpiryDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Disputed)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Disputed)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "IssuanceDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Disputed)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Disputed)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateNumber":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Disputed)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Disputed)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateStatus":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Disputed)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Disputed)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        break;
                    default:
                        certificates = db.EUCCertificates
                            .Where(certificate => certificate.CertStatus == CertificateStatus.Disputed)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                            .OrderByDescending
                            (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                }
                return View(new EUCViewModel
                {
                    user = user,
                    role = roles,
                    userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories,
                    UploadedCertificates = uploadedCertificates
                });
            }
            else if (user.PortalUserCategoryId == 1 || user.PortalUserCategoryId == 2 || user.PortalUserCategoryId == 3
                || user.PortalUserCategoryId == 4 || user.PortalUserCategoryId == 6 || user.PortalUserCategoryId == 7
                || user.PortalUserCategoryId == 8 || user.PortalUserCategoryId == 11 || user.PortalUserCategoryId == 12
                || user.PortalUserCategoryId == 10 || user.PortalUserCategoryId == 13 || user.PortalUserCategoryId == 14
                || user.PortalUserCategoryId == 16
                || user.PortalUserCategoryId == 17 || user.PortalUserCategoryId == 18 || user.PortalUserCategoryId == 19
                || user.PortalUserCategoryId == 20 || user.PortalUserCategoryId == 21 || user.PortalUserCategoryId == 25
                || user.PortalUserCategoryId == 26 || user.PortalUserCategoryId == 27 || user.PortalUserCategoryId == 38
                || user.PortalUserCategoryId == 39 || user.PortalUserCategoryId == 40)
            {
                string SearchTerm = "";

                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                    .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.AgencyId == user.PortalUserCategoryId)
                                    .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                    .OrderByDescending(certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);

                    return View(new EUCViewModel
                    {
                        user = user,
                        role = roles,
                        userList = users,
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        itemCategories = itemCategories,
                        UploadedCertificates = uploadedCertificates
                    });
                }
                switch (sortOrder)
                {
                    case "ExpiryDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.AgencyId == user.PortalUserCategoryId)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.AgencyId == user.PortalUserCategoryId)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "IssuanceDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.AgencyId == user.PortalUserCategoryId)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.AgencyId == user.PortalUserCategoryId)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateNumber":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.AgencyId == user.PortalUserCategoryId)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.AgencyId == user.PortalUserCategoryId)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateStatus":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.AgencyId == user.PortalUserCategoryId)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.AgencyId == user.PortalUserCategoryId)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        break;
                    default:
                        certificates = db.EUCCertificates
                            .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.AgencyId == user.PortalUserCategoryId)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                            .OrderByDescending
                            (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                }
                return View(new EUCViewModel
                {
                    user = user,
                    role = roles,
                    userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories,
                    UploadedCertificates = uploadedCertificates
                });
            }
            else if (user.PortalUserCategoryId == 28 || user.PortalUserCategoryId == 29)
            {
                string SearchTerm = "";

                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                    .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.UserId == user.Id)
                                    .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                    .OrderByDescending(certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);

                    return View(new EUCViewModel
                    {
                        user = user,
                        role = roles,
                        userList = users,
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        itemCategories = itemCategories,
                        UploadedCertificates = uploadedCertificates
                    });
                }
                switch (sortOrder)
                {
                    case "ExpiryDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.UserId == user.Id)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.UserId == user.Id)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "IssuanceDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.UserId == user.Id)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.UserId == user.Id)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateNumber":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.UserId == user.Id)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.UserId == user.Id)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateStatus":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.UserId == user.Id)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.UserId == user.Id)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        break;
                    default:
                        certificates = db.EUCCertificates
                            .Where(euc => euc.CertStatus == CertificateStatus.Disputed && euc.UserId == user.Id)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                            .OrderByDescending
                            (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                }
                return View(new EUCViewModel
                {
                    user = user,
                    role = roles,
                    userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories,
                    UploadedCertificates = uploadedCertificates
                });
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            #endregion
        }
        [Authorize(Roles = "Agency, Client User, NSA, ONSA PGSO, Customs User, Exclusive User, NSA")]
        //Pagination for EUC Applications
        // GET: /Employee/  
        public ActionResult UtilizedEndUserCertificates(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var itemCategories = _context.EUCItemCategory.ToList();
            var uploadedCertificates = _context.UploadedEUCCertificate.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            //eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Disputed).OrderByDescending(date => date.IssuanceDate).ToList();

            IPagedList<EUCCertificates> certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Utilized)
                                .OrderByDescending
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
            #region support
            //Support User
            if (user.PortalUserCategoryId == 5 || user.PortalUserCategoryId == 45)
            {
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                    .Where(certificate => certificate.CertStatus == CertificateStatus.Utilized)
                                    .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                    .OrderByDescending(certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);

                    return View(new EUCViewModel
                    {
                        user = user,
                        role = roles,
                        userList = users,
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        itemCategories = itemCategories,
                        UploadedCertificates = uploadedCertificates
                    });
                }

                switch (sortOrder)
                {
                    case "ExpiryDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Utilized)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Utilized)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "IssuanceDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Utilized)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Utilized)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateNumber":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Utilized)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Utilized)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateStatus":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Utilized)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Utilized)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        break;
                    default:
                        certificates = db.EUCCertificates
                            .Where(certificate => certificate.CertStatus == CertificateStatus.Utilized)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                            .OrderByDescending
                            (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                }
                return View(new EUCViewModel
                {
                    user = user,
                    role = roles,
                    userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories,
                    UploadedCertificates = uploadedCertificates
                });
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            #endregion
        }
        [Authorize(Roles = "Agency, Client User, NSA, ONSA PGSO, Customs User, Exclusive User")]
        //Pagination for EUC Applications
        // GET: /Employee/  
        public ActionResult ExpiredEndUserCertificates(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var itemCategories = _context.EUCItemCategory.ToList();
            var uploadedCertificates = _context.UploadedEUCCertificate.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            //eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Disputed).OrderByDescending(date => date.IssuanceDate).ToList();

            IPagedList<EUCCertificates> certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => DateTime.Now > euc.ExpiryDate)
                                .OrderByDescending
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
            #region support
            //Support User
            if (user.PortalUserCategoryId == 5 || user.PortalUserCategoryId == 45)
            {
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                    .Where(euc => DateTime.Now > euc.ExpiryDate)
                                    .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                    .OrderByDescending(certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);

                    return View(new EUCViewModel
                    {
                        user = user,
                        role = roles,
                        userList = users,
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        itemCategories = itemCategories,
                        UploadedCertificates = uploadedCertificates
                    });
                }

                switch (sortOrder)
                {
                    case "ExpiryDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => DateTime.Now > euc.ExpiryDate)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => DateTime.Now > euc.ExpiryDate)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "IssuanceDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => DateTime.Now > euc.ExpiryDate)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => DateTime.Now > euc.ExpiryDate)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateNumber":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => DateTime.Now > euc.ExpiryDate)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => DateTime.Now > euc.ExpiryDate)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateStatus":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => DateTime.Now > euc.ExpiryDate)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(euc => DateTime.Now > euc.ExpiryDate)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        break;
                    default:
                        certificates = db.EUCCertificates.Include(certificate => certificate.User)
                            .Where(euc => DateTime.Now > euc.ExpiryDate)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                            .OrderByDescending
                            (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                }
                return View(new EUCViewModel
                {
                    user = user,
                    role = roles,
                    userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories,
                    UploadedCertificates = uploadedCertificates
                });
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            #endregion
        }
        [Authorize(Roles = "Agency, Client User, NSA, ONSA PGSO, Customs User, Exclusive User")]
        //Pagination for EUC Applications
        // GET: /Employee/  
        public ActionResult RecalledEndUserCertificates(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var itemCategories = _context.EUCItemCategory.ToList();
            var uploadedCertificates = _context.UploadedEUCCertificate.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            //eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Disputed).OrderByDescending(date => date.IssuanceDate).ToList();

            IPagedList<EUCCertificates> certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Recalled)
                                .OrderByDescending
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
            #region support
            //Support User
            if (user.PortalUserCategoryId == 5 || user.PortalUserCategoryId == 45)
            {
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                    .Where(certificate => certificate.CertStatus == CertificateStatus.Recalled)
                                    .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                    .OrderByDescending(certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);

                    return View(new EUCViewModel
                    {
                        user = user,
                        role = roles,
                        userList = users,
                        certificatesToShow = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        itemCategories = itemCategories,
                        UploadedCertificates = uploadedCertificates
                    });
                }

                switch (sortOrder)
                {
                    case "ExpiryDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Recalled)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Recalled)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "IssuanceDate":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Recalled)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Recalled)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateNumber":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Recalled)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Recalled)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                        break;
                    case "CertificateStatus":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Recalled)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EUCCertificates
                                .Where(certificate => certificate.CertStatus == CertificateStatus.Recalled)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                .OrderBy
                                (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                        break;
                    default:
                        certificates = db.EUCCertificates
                            .Where(certificate => certificate.CertStatus == CertificateStatus.Recalled)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                    || certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                            .OrderByDescending
                            (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                        break;
                }
                return View(new EUCViewModel
                {
                    user = user,
                    role = roles,
                    userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories,
                    UploadedCertificates = uploadedCertificates
                });
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            #endregion
        }
        [Authorize(Roles = "NSA, ONSA PGSO")]
        //Pagination for EUC Applications
        // GET: /Employee/  
        public ActionResult EndUserCertificatesPendingIssuance(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var itemCategories = _context.EUCItemCategory.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            //eucCertificates = _context.EUCCertificates.Where(euc => euc.CertStatus == CertificateStatus.Disputed).OrderByDescending(date => date.IssuanceDate).ToList();

            IPagedList<EndUserCertificate> certificates = db.EndUserCertificate.Include(certificate => certificate.User)
                                .Where(certificate => certificate.EUCApprovalStatus == EUCApprovedStatus.Approved && certificate.certificateStatus == CertificateStatus.Pending)
                                .OrderByDescending
                                (certificate => certificate.dateCreated).ToPagedList(pageIndex, pageSize);
            #region support
            //Support User
            if (user.PortalUserCategoryId == 5 || user.PortalUserCategoryId == 45)
            {
                //Parse search term
                string SearchTerm = "";
                if (searchTerm == null)
                    SearchTerm = "";
                else if (searchTerm != null)
                    SearchTerm = searchTerm;

                if (SearchTerm != "" && sortOrder == null)
                {
                    certificates = db.EndUserCertificate.Include(certificate => certificate.User)
                                    .Where(certificate => certificate.EUCApprovalStatus == EUCApprovedStatus.Approved && certificate.certificateStatus == CertificateStatus.Pending)
                                    .Where(certificate => certificate.applicationNo.Contains(SearchTerm)
                                    || certificate.User.LastName.Contains(SearchTerm)
                                    || certificate.User.FirstName.Contains(SearchTerm)
                                    || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                    || certificate.User.Email.Contains(SearchTerm))
                                    .OrderByDescending(certificate => certificate.dateCreated).ToPagedList(pageIndex, pageSize);

                    return View(new EUCViewModel
                    {
                        user = user,
                        role = roles,
                        userList = users,
                        eucapplication = certificates,
                        PageSize = pageSize,
                        sortOrder = sortOrder,
                        searchTerm = SearchTerm,
                        itemCategories = itemCategories
                    });
                }

                switch (sortOrder)

                {
                    case "DateCreated":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User).Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.certificateStatus == CertificateStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .OrderByDescending(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Include(euc => euc.User).Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.certificateStatus == CertificateStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .OrderBy(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;

                    case "ApplicationNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User).Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.certificateStatus == CertificateStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .OrderByDescending(euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Include(euc => euc.User).Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.certificateStatus == CertificateStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .OrderBy(euc => euc.applicationNo).ToPagedList(pageIndex, pageSize);
                        break;

                    case "ApplicantNo":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User).Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.certificateStatus == CertificateStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .OrderByDescending(euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Include(euc => euc.User).Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.certificateStatus == CertificateStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .OrderBy(euc => euc.User.ApplicationNumber).ToPagedList(pageIndex, pageSize);
                        break;

                    case "ApplicantName":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User).Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.certificateStatus == CertificateStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .OrderByDescending(euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        else
                            certificates = db.EndUserCertificate.Include(euc => euc.User).Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.certificateStatus == CertificateStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .OrderBy(euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                        break;

                    case "ItemCategories":
                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User).Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.certificateStatus == CertificateStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .OrderByDescending(euc => euc.itemCat).ToPagedList(pageIndex, pageSize);

                        else
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.certificateStatus == CertificateStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .OrderBy(euc => euc.itemCat).ToPagedList(pageIndex, pageSize);
                        break;

                    case "Status":

                        if (sortOrder.Equals(CurrentSort))
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.certificateStatus == CertificateStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .OrderByDescending(euc => euc.EUCApprovalStatus).ToPagedList(pageIndex, pageSize);

                        else
                            certificates = db.EndUserCertificate.Include(euc => euc.User)
                        .Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.certificateStatus == CertificateStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .OrderBy(euc => euc.EUCApprovalStatus).ToPagedList(pageIndex, pageSize);
                        break;

                    case "Default":
                        certificates = db.EndUserCertificate.Include(euc => euc.User).Where(euc => euc.EUCApprovalStatus == EUCApprovedStatus.Approved && euc.certificateStatus == CertificateStatus.Pending)
                        .Where(euc => euc.User.LastName.Contains(SearchTerm)
                        || euc.User.FirstName.Contains(SearchTerm)
                        || euc.User.Email.Contains(SearchTerm)
                        || euc.applicationNo.Contains(SearchTerm)
                        || euc.User.ApplicationNumber.Contains(SearchTerm))
                        .OrderByDescending(euc => euc.dateCreated).ToPagedList(pageIndex, pageSize);
                        break;
                }
                return View(new EUCViewModel
                {
                    user = user,
                    role = roles,
                    userList = users,
                    eucapplication = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories
                });
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            #endregion
        }

        [Authorize(Roles = "Agency, Client User, ONSA PGSO, Customs User, NSA, Exclusive User")]
        //Pagination for EUC Applications
        // GET: /Employee/  
        public ActionResult AllUploadedCertificate(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
            //Plumbing code for shared view
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var itemCategories = _context.EUCItemCategory.ToList();
            var uploadedCertificates = _context.UploadedEUCCertificate.ToList();

            ApplicationDbContext db = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;
            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            IPagedList<EUCCertificates> certificates = db.EUCCertificates.Include(certificate => certificate.User).OrderByDescending
                                (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;

            if (SearchTerm != "" && sortOrder == null)
            {
                certificates = db.EUCCertificates.Include(certificate => certificate.User)
                                .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                || certificate.applicationNo.Contains(SearchTerm)
                                || certificate.User.LastName.Contains(SearchTerm)
                                || certificate.User.FirstName.Contains(SearchTerm)
                                || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                || certificate.User.Email.Contains(SearchTerm))
                                .OrderByDescending(certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);

                return View(new EUCViewModel
                {
                    user = user,
                    role = roles,
                    userList = users,
                    certificatesToShow = certificates,
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    itemCategories = itemCategories,
                    UploadedCertificates = uploadedCertificates
                });
            }
            switch (sortOrder)
            {
                case "ExpiryDate":
                    if (sortOrder.Equals(CurrentSort))
                        certificates = db.EUCCertificates.Include(certificate => certificate.User)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                || certificate.applicationNo.Contains(SearchTerm)
                                || certificate.User.LastName.Contains(SearchTerm)
                                || certificate.User.FirstName.Contains(SearchTerm)
                                || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                || certificate.User.Email.Contains(SearchTerm))
                            .OrderByDescending
                            (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                    else
                        certificates = db.EUCCertificates.Include(certificate => certificate.User)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                || certificate.applicationNo.Contains(SearchTerm)
                                || certificate.User.LastName.Contains(SearchTerm)
                                || certificate.User.FirstName.Contains(SearchTerm)
                                || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                || certificate.User.Email.Contains(SearchTerm))
                            .OrderBy
                            (certificate => certificate.ExpiryDate).ToPagedList(pageIndex, pageSize);
                    break;
                case "IssuanceDate":
                    if (sortOrder.Equals(CurrentSort))
                        certificates = db.EUCCertificates.Include(certificate => certificate.User)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                || certificate.applicationNo.Contains(SearchTerm)
                                || certificate.User.LastName.Contains(SearchTerm)
                                || certificate.User.FirstName.Contains(SearchTerm)
                                || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                || certificate.User.Email.Contains(SearchTerm))
                            .OrderByDescending
                            (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                    else
                        certificates = db.EUCCertificates.Include(certificate => certificate.User)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                || certificate.applicationNo.Contains(SearchTerm)
                                || certificate.User.LastName.Contains(SearchTerm)
                                || certificate.User.FirstName.Contains(SearchTerm)
                                || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                || certificate.User.Email.Contains(SearchTerm))
                            .OrderBy
                            (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                    break;
                case "CertificateNumber":
                    if (sortOrder.Equals(CurrentSort))
                        certificates = db.EUCCertificates.Include(certificate => certificate.User)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                || certificate.applicationNo.Contains(SearchTerm)
                                || certificate.User.LastName.Contains(SearchTerm)
                                || certificate.User.FirstName.Contains(SearchTerm)
                                || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                || certificate.User.Email.Contains(SearchTerm))
                            .OrderByDescending
                            (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                    else
                        certificates = db.EUCCertificates.Include(certificate => certificate.User)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                || certificate.applicationNo.Contains(SearchTerm)
                                || certificate.User.LastName.Contains(SearchTerm)
                                || certificate.User.FirstName.Contains(SearchTerm)
                                || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                || certificate.User.Email.Contains(SearchTerm))
                            .OrderBy
                            (certificate => certificate.CertNo).ToPagedList(pageIndex, pageSize);
                    break;
                case "ApplicationNumber":
                    if (sortOrder.Equals(CurrentSort))
                        certificates = db.EUCCertificates.Include(certificate => certificate.User)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                || certificate.applicationNo.Contains(SearchTerm)
                                || certificate.User.LastName.Contains(SearchTerm)
                                || certificate.User.FirstName.Contains(SearchTerm)
                                || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                || certificate.User.Email.Contains(SearchTerm))
                            .OrderByDescending
                            (certificate => certificate.applicationNo).ToPagedList(pageIndex, pageSize);
                    else
                        certificates = db.EUCCertificates.Include(certificate => certificate.User)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                || certificate.applicationNo.Contains(SearchTerm)
                                || certificate.User.LastName.Contains(SearchTerm)
                                || certificate.User.FirstName.Contains(SearchTerm)
                                || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                || certificate.User.Email.Contains(SearchTerm))
                            .OrderBy
                            (certificate => certificate.applicationNo).ToPagedList(pageIndex, pageSize);
                    break;
                case "ApplicantName":
                    if (sortOrder.Equals(CurrentSort))
                        certificates = db.EUCCertificates.Include(certificate => certificate.User)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                || certificate.applicationNo.Contains(SearchTerm)
                                || certificate.User.LastName.Contains(SearchTerm)
                                || certificate.User.FirstName.Contains(SearchTerm)
                                || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                || certificate.User.Email.Contains(SearchTerm))
                            .OrderByDescending
                            (certificate => certificate.User.LastName).ToPagedList(pageIndex, pageSize);
                    else
                        certificates = db.EUCCertificates.Include(certificate => certificate.User)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                || certificate.applicationNo.Contains(SearchTerm)
                                || certificate.User.LastName.Contains(SearchTerm)
                                || certificate.User.FirstName.Contains(SearchTerm)
                                || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                || certificate.User.Email.Contains(SearchTerm))
                            .OrderBy
                            (certificate => certificate.User.LastName).ToPagedList(pageIndex, pageSize);
                    break;
                case "CertificateStatus":
                    if (sortOrder.Equals(CurrentSort))
                        certificates = db.EUCCertificates.Include(certificate => certificate.User)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                || certificate.applicationNo.Contains(SearchTerm)
                                || certificate.User.LastName.Contains(SearchTerm)
                                || certificate.User.FirstName.Contains(SearchTerm)
                                || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                || certificate.User.Email.Contains(SearchTerm))
                            .OrderByDescending
                            (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                    else
                        certificates = db.EUCCertificates.Include(certificate => certificate.User)
                            .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                || certificate.applicationNo.Contains(SearchTerm)
                                || certificate.User.LastName.Contains(SearchTerm)
                                || certificate.User.FirstName.Contains(SearchTerm)
                                || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                || certificate.User.Email.Contains(SearchTerm))
                            .OrderBy
                            (certificate => certificate.CertStatus).ToPagedList(pageIndex, pageSize);
                    break;
                default:
                    certificates = db.EUCCertificates.Include(certificate => certificate.User)
                        .Where(certificate => certificate.CertNo.Contains(SearchTerm)
                                || certificate.applicationNo.Contains(SearchTerm)
                                || certificate.User.LastName.Contains(SearchTerm)
                                || certificate.User.FirstName.Contains(SearchTerm)
                                || certificate.User.ApplicationNumber.Contains(SearchTerm)
                                || certificate.User.Email.Contains(SearchTerm)
                                )
                                .Distinct()
                        .OrderByDescending
                        (certificate => certificate.IssuanceDate).ToPagedList(pageIndex, pageSize);
                    break;
            }
            return View(new EUCViewModel
            {
                user = user,
                role = roles,
                userList = users,
                certificatesToShow = certificates,
                PageSize = pageSize,
                sortOrder = sortOrder,
                searchTerm = SearchTerm,
                itemCategories = itemCategories,
                UploadedCertificates = uploadedCertificates
            });
        }
        public ActionResult RecallCertificate(string AppID)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            string certificateNumber = "";

            //Use application number to get certificate number
            if (AppID != null)
            {
                certificateNumber = _context.EUCCertificates.Where(euc => euc.applicationNo == AppID).FirstOrDefault().CertNo;
            }


            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited Recall application page: User: " + user.UserName,
               User.Identity.GetUserId(),
               "Visited Recall application page",
               Request.UserHostAddress);

            return View(new RecallApplicationViewModel
            {
                user = user,
                role = roles,
                EUCID = AppID,
                CertificateNumber = certificateNumber
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RecallCertificate(RecallApplicationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var Certificate = _context.EUCCertificates.Where(a => a.applicationNo == model.EUCID).FirstOrDefault();
                var Application = _context.EndUserCertificate.Where(a => a.applicationNo == model.EUCID).FirstOrDefault();
                if (Certificate.CertStatus != CertificateStatus.Recalled)
                {
                    var userIdNumber = User.Identity.GetUserId();
                    var user = _context.Users.Find(userIdNumber);

                    var Comment = new EUCApplicationComments
                    {
                        FlagDate = DateTime.Now,
                        applicationNo = model.EUCID,
                        userFrom = user.PortalUserCategoryId.ToString(),
                        userComment = model.RecallComment
                    };
                    _context.EUCApplicationComments.Add(Comment);
                    _context.SaveChanges();
                    //Update EUC Table with Certificate Status
                    Certificate.CertStatus = CertificateStatus.Recalled;
                    var result = _context.SaveChanges();

                    if (result > 0)
                    {
                        //Update EUC Table with Application Status
                        Application.certificateStatus = CertificateStatus.Recalled;
                        Application.AgencyForwardedFrom = user.PortalUserCategoryId;
                        Application.AgencyForwardedTo = 5;
                        _context.SaveChanges();

                        //Get the user who made the application
                        var userWhoMadeTheApplication = _context.Users.Find(Application.UserID);

                        //Send the user Email and SMS notifying them that their certificate has been recalled
                        ApplicantNotification recallCertificateNotification = new ApplicantNotification();
                        await recallCertificateNotification.RecallEUCCertificate(userWhoMadeTheApplication, Certificate.CertNo, Certificate.applicationNo, model.RecallComment);

                        //Make entry into euc workflow table
                        Controllers.EUCWorkflowController.EUCWorkflow eucWorkflow = new Controllers.EUCWorkflowController.EUCWorkflow();
                        eucWorkflow.UpdateEUCWorkflow(DateTime.Now, model.EUCID, user.Id, Application.UserID, "ENDORSED", "RECALLED", Application.AgencyForwardedTo, 1, user.PortalUserCategoryId);
                    }

                    TempData["Success"] = "Recalled Successfully!";
                    return RedirectToAction("RecalledEndUserCertificates", "Admin");
                }
                else
                {
                    TempData["Unsuccessul"] = "This application has already been Recalled!..";
                    return RedirectToAction("RecalledEndUserCertificates", "Admin");
                }
            }
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(model);
        }

        public ActionResult ReIssueCertificate(string AppID)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            string certificateNumber = "";

            //Use application number to get certificate number
            if (AppID != null)
            {
                certificateNumber = _context.EUCCertificates.Where(euc => euc.applicationNo == AppID).FirstOrDefault().CertNo;
            }


            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Visited Re-Issue Certificate page: User: " + user.UserName,
               User.Identity.GetUserId(),
               "Visited Re-Issue Certificate page",
               Request.UserHostAddress);

            return View(new RecallApplicationViewModel
            {
                user = user,
                role = roles,
                EUCID = AppID,
                CertificateNumber = certificateNumber
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ReIssueCertificate(RecallApplicationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var Certificate = _context.EUCCertificates.Where(a => a.applicationNo == model.EUCID).FirstOrDefault();
                var Application = _context.EndUserCertificate.Where(a => a.applicationNo == model.EUCID).FirstOrDefault();
                if (Certificate.CertStatus != CertificateStatus.Certificate_Collected)
                {
                    var userIdNumber = User.Identity.GetUserId();
                    var user = _context.Users.Find(userIdNumber);

                    var Comment = new EUCApplicationComments
                    {
                        FlagDate = DateTime.Now,
                        applicationNo = model.EUCID,
                        userFrom = user.PortalUserCategoryId.ToString(),
                        userComment = model.RecallComment
                    };
                    _context.EUCApplicationComments.Add(Comment);
                    _context.SaveChanges();
                    //Update EUC Table with Certificate Status
                    Certificate.CertStatus = CertificateStatus.Certificate_Collected;
                    var result = _context.SaveChanges();

                    if (result > 0)
                    {
                        //Update EUC Table with Application Status
                        Application.certificateStatus = CertificateStatus.Certificate_Collected;
                        Application.AgencyForwardedFrom = user.PortalUserCategoryId;
                        Application.AgencyForwardedTo = 5;
                        _context.SaveChanges();

                        //Get the user who made the application
                        var userWhoMadeTheApplication = _context.Users.Find(Application.UserID);

                        //Send the user Email and SMS notifying them that their certificate has been recalled
                        ApplicantNotification reissuedCertificateNotification = new ApplicantNotification();
                        await reissuedCertificateNotification.ReIssuedCertificate(userWhoMadeTheApplication, Certificate.CertNo, Certificate.applicationNo, model.RecallComment);

                        //Make entry into euc workflow table
                        Controllers.EUCWorkflowController.EUCWorkflow eucWorkflow = new Controllers.EUCWorkflowController.EUCWorkflow();
                        eucWorkflow.UpdateEUCWorkflow(DateTime.Now, model.EUCID, user.Id, Application.UserID, "ENDORSED", "REISSUED", Application.AgencyForwardedTo, 1, user.PortalUserCategoryId);
                    }

                    TempData["Success"] = "Ressiued Successfully!";
                    return RedirectToAction("AllEndUserCertificates", "Admin");
                }
                else
                {
                    TempData["Unsuccessul"] = "This application has already been Recalled!..";
                    return RedirectToAction("AllEndUserCertificates", "Admin");
                }
            }
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(model);
        }
    }
}
