﻿using ecims.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ecims.Controllers.ApplicantNotificationController
{
    public class ApplicantNotification
    {
        public async Task<ActionResult> NotifyApplicantOfApprovedCertificate(ApplicationUser user, string applicationNumber, string tokenCode)
        {
            //Send user verification email            
            IdentityMessage EmailMessage = new IdentityMessage();

            EmailMessage.Body = "Dear " + user.FirstName + " " + user.LastName + ". "
                                + "<br/>"
                                + "Your EUC application has just been approved, and your certificate is ready for collection."
                                + "<br/>"
                                + "Application Number: " + applicationNumber + ". "
                                + "<br/>"
                                + "Kindly come to ONSA in Three Arms Zone, Abuja, with the following:"
                                + "<br/>"
                                + " 1. Valid means of identification(International Passport, Drivers License or National ID Card)."
                                + "<br/>"
                                + " 2. Letter of authority from your organisation."
                                + "<br/>"
                                + " 3. Printed copy of this email received containing the reference stated below."
                                + "<br/>"
                                + " Kindly come to the NSA's office with the reference below for collection."
                                + "<br/>"
                                + "Your collection reference is: <b>" + tokenCode + "</b>"
                                + "<br/>"
                                + " Collection time is 12:00 pm to 02:00 pm (Monday to Friday), except on public holidays."
                                + "<br/>"
                                + " Thank you.";
            EmailMessage.Destination = user.Email;
            EmailMessage.Subject = "EUC Application Approved: " + applicationNumber;


            EmailService emailService = new EmailService();
            await emailService.SendAsync(EmailMessage);

            //Send user verification SMS
            IdentityMessage SmsMessage = new IdentityMessage();
            //send accompanying SMS reminding user to verify their email
            string smsBody = "Dear " + user.FirstName + " " + user.LastName + ". \n"
                                + " Your EUC application has just been approved, and your certificate is ready for collection."
                                + " Application Number: " + applicationNumber + ". \n"
                                + " Kindly come to ONSA in Three Arms Zone, Abuja, with the following: \n"
                                + " 1. Valid means of identification(International Passport, Drivers License or National ID Card). \n"
                                + " 2. Letter of authority from your organisation. \n"
                                + " 3. Printed copy of the email received containing the reference stated below. \n"
                                + " Your collection reference is: " + tokenCode + "\n"
                                + " Collection time is 12:00 pm to 02:00 pm (Monday to Friday), except on public holidays. \n"
                                + " Thank you.";

            SmsMessage.Body = smsBody;
            SmsMessage.Destination = user.PhoneNumber;
            SmsMessage.Subject = "EUC Application Approved: " + applicationNumber;

            SmsService smsSerice = new SmsService();
            await smsSerice.SendAsync(SmsMessage);

            //Return server result of 'OK' if  messages are sent successfully
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        //Notify user that he has been blacklisted and the reason why
        public async Task<ActionResult> NotifyBlacklistedApplicant(ApplicationUser user, string blacklistReason)
        {
            //Send user verification email            
            IdentityMessage EmailMessage = new IdentityMessage();

            EmailMessage.Body = "Dear " + user.FirstName + " " + user.LastName + ". "
                                + "<br/>"
                                + "Your account on the EUC web portal has been blacklisted."
                                + "<br/>"
                                + "Reason: " + blacklistReason
                                + "<br/>"
                                + "If you have any enquiries, please call support on: +234 809 038 5718"
                                + "<br/>"
                                + " Thank you.";
            EmailMessage.Destination = user.Email;
            EmailMessage.Subject = "EUC Account Blacklisted";

            EmailService emailService = new EmailService();
            await emailService.SendAsync(EmailMessage);

            //Send user verification SMS
            IdentityMessage SmsMessage = new IdentityMessage();
            //send accompanying SMS reminding user to verify their email
            string smsBody = " Your account on the EUC web portal has been blacklisted."
                                + "Reason: " + blacklistReason
                                + "If you have any enquiries, please call support on: +234 809 038 5718"
                                + " Thank you.";

            SmsMessage.Body = smsBody;
            SmsMessage.Destination = user.PhoneNumber;
            SmsMessage.Subject = "EUC Account Blacklisted";

            SmsService smsSerice = new SmsService();
            await smsSerice.SendAsync(SmsMessage);

            //Return server result of 'OK' if  messages are sent successfully
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        //Notify user that he has been blacklisted and the reason why
        public async Task<ActionResult> NotifyDeBlacklistedApplicant(ApplicationUser user)
        {
            //Send user verification email            
            IdentityMessage EmailMessage = new IdentityMessage();

            EmailMessage.Body = "Dear " + user.FirstName + " " + user.LastName + ". "
                                + "<br/>"
                                + "Your account on the EUC web portal has been de-blacklisted."
                                + "<br/>"
                                + "If you have any enquiries, please call support on: +234 809 038 5718"
                                + "<br/>"
                                + " Thank you.";
            EmailMessage.Destination = user.Email;
            EmailMessage.Subject = "EUC Account De-Blacklisted";

            EmailService emailService = new EmailService();
            await emailService.SendAsync(EmailMessage);

            //Send user verification SMS
            IdentityMessage SmsMessage = new IdentityMessage();
            //send accompanying SMS reminding user to verify their email
            string smsBody = "Your account on the EUC web portal has been de-blacklisted."
                                + "If you have any enquiries, please call support on: +234 809 038 5718"
                                + " Thank you.";

            SmsMessage.Body = smsBody;
            SmsMessage.Destination = user.PhoneNumber;
            SmsMessage.Subject = "EUC Account De-Blacklisted";

            SmsService smsSerice = new SmsService();
            await smsSerice.SendAsync(SmsMessage);

            //Return server result of 'OK' if  messages are sent successfully
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        //Notify user that he has been blacklisted and the reason why
        public async Task<ActionResult> RecallEUCCertificate(ApplicationUser user, string certificateNumber, string applicationNumber, string recallReason)
        {
            //Send user verification email            
            IdentityMessage EmailMessage = new IdentityMessage();

            EmailMessage.Body = "Dear " + user.FirstName + " " + user.LastName + ". "
                                + "<br/>"
                                + "Your End-User Certificate with details:"
                                + "<br/>"
                                + "Certificate Number: " + certificateNumber 
                                + "<br/>" 
                                + "Application Number: " + applicationNumber
                                + "<br/>"
                                + "Has been recalled."
                                + "<br/>"
                                + "Reason for recall: " + recallReason;
            EmailMessage.Destination = user.Email;
            EmailMessage.Subject = "Recalled End-User Certificate.";

            EmailService emailService = new EmailService();
            await emailService.SendAsync(EmailMessage);

            //Send user verification SMS
            IdentityMessage SmsMessage = new IdentityMessage();
            //send accompanying SMS reminding user to verify their email
            string smsBody = "Your EUC has been recalled."
                                + "Certificate Number: " + certificateNumber + "; Application Number: " + applicationNumber + "; Reason for recall: " + recallReason;

            SmsMessage.Body = smsBody;
            SmsMessage.Destination = user.PhoneNumber;
            SmsMessage.Subject = "Your End-User Certificate Has Been Recalled.";

            SmsService smsSerice = new SmsService();
            await smsSerice.SendAsync(SmsMessage);

            //Return server result of 'OK' if  messages are sent successfully
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public async Task<ActionResult> ReIssuedCertificate(ApplicationUser user, string certificateNumber, string applicationNumber, string reissueReason)
        {
            //Send user verification email            
            IdentityMessage EmailMessage = new IdentityMessage();

            EmailMessage.Body = "Dear " + user.FirstName + " " + user.LastName + ". "
                                + "<br/>"
                                + "Your End-User Certificate with details:"
                                + "<br/>"
                                + "Certificate Number: " + certificateNumber
                                + "<br/>"
                                + "Application Number: " + applicationNumber
                                + "<br/>"
                                + "Has been Reissued."
                                + "<br/>"
                                + "Reason for Reissuance: " + reissueReason;
            EmailMessage.Destination = user.Email;
            EmailMessage.Subject = "Reissued End-User Certificate.";

            EmailService emailService = new EmailService();
            await emailService.SendAsync(EmailMessage);

            //Send user verification SMS
            IdentityMessage SmsMessage = new IdentityMessage();
            //send accompanying SMS reminding user to verify their email
            string smsBody = "Your EUC has been reissued."
                                + "Certificate Number: " + certificateNumber + "; Application Number: " + applicationNumber + "; Reason for Reissuance: " + reissueReason;

            SmsMessage.Body = smsBody;
            SmsMessage.Destination = user.PhoneNumber;
            SmsMessage.Subject = "Your End-User Certificate Has Been Reissued.";

            SmsService smsSerice = new SmsService();
            await smsSerice.SendAsync(SmsMessage);

            //Return server result of 'OK' if  messages are sent successfully
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        public async Task<ActionResult> SMSRejectedUsers(ApplicationUser user)
        {
            //Send user verification SMS
            IdentityMessage SmsMessage = new IdentityMessage();
            //send accompanying SMS reminding user to verify their email
            string smsBody = "Your EUC has been reissued."
                                + "Certificate Number: ";

            SmsMessage.Body = smsBody;
            SmsMessage.Destination = user.PhoneNumber;
            SmsMessage.Subject = "Your End-User Certificate Has Been Reissued.";

            SmsService smsSerice = new SmsService();
            await smsSerice.SendAsync(SmsMessage);

            //Return server result of 'OK' if  messages are sent successfully
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}