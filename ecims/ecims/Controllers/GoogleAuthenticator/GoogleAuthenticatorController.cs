﻿using ecims.Models;
using ecims.ViewModels.GoogleAuthenticator;
using Google.Authenticator;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ecims.Controllers.GoogleAuthenticator
{
    public class GoogleAuthenticatorController : Controller
    {
        private const string key = "qazqaz12345";

        ApplicationDbContext _context;
        private ApplicationUserManager _userManager;
        AuditTrailGenerator.AuditTrail _auditTrail = new AuditTrailGenerator.AuditTrail();
        
        //Class constructor
        public GoogleAuthenticatorController()
        {
            _context = new ApplicationDbContext();
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        // GET: Admin
        //Open dashboard page
        //[Authorize(Roles = "ONSA Support, NSA")]
        // GET: GoogleAuthenticator
        public async Task<ActionResult> BarCodeCreator()
        {
            //Only move forward if a user is signed in
            if (User.Identity.GetUserId() == null)
            {
                //Set a message to display to the user
                Session["Message"] = "Only authorized users are permitted to view this page";
                return View("Account/Login");
            }
            else
            {
                //Get ID of the currently logged in user
                var userId = User.Identity.GetUserId();
                //Create user object based off user ID
                var user = _context.Users.Where(u => u.Id == userId).Single();
                //Redirect any user who is not ONSA Support or NSA back to their dashboard
                if (!user.canVerifyEndUserCertificates)
                {
                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Attempted to visit Google Authenticator barcode page. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
                       User.Identity.GetUserId(),
                       "Visited Google Authenticator barcode page.",
                       Request.UserHostAddress);
                    //Google authenticator logic
                    Session["GoogleAuthenticatorMessage"] = "Invalid Request. This user is not authorized to view the requested page!";
                    return RedirectToAction("MainDashboard", "Admin");
                }
                if (user.isGoogleAuthenticatorActive)
                {
                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Attempted to visit Google Authenticator barcode page but already had Google Authentication active. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
                       User.Identity.GetUserId(),
                       "Visited Google Authenticator barcode page.",
                       Request.UserHostAddress);
                    //Google authenticator logic
                    Session["GoogleAuthenticatorMessage"] = "Invalid Request. User already has google authentication active on the account!";
                    return RedirectToAction("MainDashboard", "Admin");
                }
                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Visited Google Authenticator barcode page. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
                   User.Identity.GetUserId(),
                   "Visited Google Authenticator barcode page.",
                   Request.UserHostAddress);

                var userIdentity = (ClaimsIdentity)User.Identity;
                var claims = userIdentity.Claims;
                var roleClaimType = userIdentity.RoleClaimType;
                var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

                //Google authenticator logic
                Session["UserName"] = user.UserName;
                //Two Factor Authentication Setup
                TwoFactorAuthenticator TwoFacAuth = new TwoFactorAuthenticator();
                string UserUniqueKey = (user.UserName + key);
                Session["UserUniqueKey"] = UserUniqueKey;
                var setupInfo = TwoFacAuth.GenerateSetupCode("End User Certificate Authorization", user.UserName, UserUniqueKey, 300, 300);
                ViewBag.BarcodeImageUrl = setupInfo.QrCodeSetupImageUrl;
                ViewBag.SetupCode = setupInfo.ManualEntryKey;

                return View(new BarCodeCreatorViewModel
                {
                    user = user,
                    role = roles,
                });
            }
        }
        public ActionResult TwoFactorAuthenticate()
        {
            var token = Request["CodeDigit"];
            TwoFactorAuthenticator TwoFacAuth = new TwoFactorAuthenticator();
            string UserUniqueKey = Session["UserUniqueKey"].ToString();
            bool isValid = TwoFacAuth.ValidateTwoFactorPIN(UserUniqueKey, token);
            if (isValid)
            {
                Session["IsValidTwoFactorAuthentication"] = true;
                //Get ID of the currently logged in user
                var userId = User.Identity.GetUserId();
                //Create user object based off user ID
                var user = _context.Users.Where(u => u.Id == userId).Single();
                
                //Update Database to enable two fact authorization
                var userToEnableGoogleAuthorizationFor = _context.Users.Find(user.Id);
                userToEnableGoogleAuthorizationFor.isGoogleAuthenticatorActive = true;
                _context.SaveChanges();

                Session["GoogleAuthenticationSuccessfulMessage"] = "Google authentication has been activated successfully for this account!";

                //If the user is not a support user and can verify EUCs redirect them to the verify EUC portal
                if (user.PortalUserCategoryId != 5 && user.canVerifyEndUserCertificates)
                {
                    return RedirectToAction("Dashboard", "VerifyEUCPortal");
                }
                else
                {
                    return RedirectToAction("MainDashboard", "Admin");
                }
            }
            Session["GoogleAuthenticationUnsuccessfulMessage"] = "Google authentication activation failed for this account!. Please re-enter the generated token.";
            return RedirectToAction("BarCodeCreator", "GoogleAuthenticator");
        }
        //Check if a generated token code is authentic
        public bool IsTwoFactorAuthentic(string tokenCode, string userId)
        {
            //Get ID of the currently logged in user
            //var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Find(userId);

            var token = tokenCode;
            TwoFactorAuthenticator TwoFacAuth = new TwoFactorAuthenticator();
            string UserUniqueKey = (user.UserName + key); ;
            bool isValid = TwoFacAuth.ValidateTwoFactorPIN(UserUniqueKey, token);
            if (isValid)
            {
                bool IsValidTwoFactorAuthentication = true;

                return IsValidTwoFactorAuthentication;
            }
            return false;
        }
    }
}