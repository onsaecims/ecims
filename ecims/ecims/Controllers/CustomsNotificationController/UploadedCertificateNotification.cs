﻿using ecims.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.AspNet.Identity.Owin;
using System.Net;
using System.IO;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace ecims.Controllers.CustomsNotificationController
{
    public class UploadedCertificateNotification
    {
        private ApplicationUserManager _userManager;
        AuditTrailGenerator.AuditTrail _auditTrail = new AuditTrailGenerator.AuditTrail();

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //Send emails to every agency user informing them that a new application has been forwarded for their action
        public async Task<ActionResult> SendForwardedEUCEmailToAgency(List<ApplicationUser> customsUsers, string applicationNumber, string forwardingUserID, string forwardingUserIPAddress)
        {
            //Get every user belonging to this agency
            //var agencyUserList = _context.Users.Where(agencyUsers => agencyUsers.PortalUserCategoryId == portalSubCategoryID).ToList();
            //Message to be sent via SMS and email
            string emailMessage = "An End-User Certificate has been uploaded for your attention, " +
                            "<br/>" +
                            "Certificate Number: " + applicationNumber +
                            "<br/>" +
                            "Kindly find the uploaded certificate on your account after logging in." +
                            "<br/>" +
                            "Thank you.";
            //SMS Message
            string smsMessage = "An End-User Certificate has been uploaded for your attention, " +
                            "\n" +
                            "Certificate Number: " + applicationNumber +
                            "\n" +
                            "Kindly find the uploaded certificate on your account after logging in." +
                            "\n" +
                            "Thank you.";

            string subject = "New EUC Application Uploaded " + applicationNumber;

            //Check user list for null
            if (customsUsers != null)
            {
                foreach (var user in customsUsers)
                {
                    //Send user verification email
                    IdentityMessage EmailMessage = new IdentityMessage();

                    EmailMessage.Body = "Dear " + user.FirstName + " " + user.LastName + ". "
                                        + " An End-User Certificate has been uploaded for your attention, " +
                                        "<br/>" +
                                        "Certificate Number: " + applicationNumber +
                                        "<br/>" +
                                        "Kindly find the uploaded certificate on your account after logging in." +
                                        "<br/>" +
                                        "Thank you.";
                    ;
                    EmailMessage.Destination = user.Email;
                    EmailMessage.Subject = "New EUC Application Uploaded " + applicationNumber;


                    EmailService emailService = new EmailService();
                    await emailService.SendAsync(EmailMessage);
                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Email notification sent to customs for Application: " + applicationNumber + ". Email Sent to:" + user.Email + " Has been sent to " + user.LastName + " " + user.FirstName + " for approval/rejection.",
                       forwardingUserID,
                       "EUC Application Forwarded For Vetting",
                       forwardingUserIPAddress);

                    //Send SMS to agency user
                    //Send user verification SMS
                    IdentityMessage SmsMessage = new IdentityMessage();
                    //send accompanying SMS reminding user to verify their email
                    string smsBody = "Dear " + user.FirstName + " " + user.LastName + ". "
                                     + " An End-User Certificate has been uploaded for your attention, " +
                                     "\n" +
                                     "Certificate Number: " + applicationNumber +
                                     "\n" +
                                     "Kindly find the uploaded certificate on your account after logging in." +
                                     "\n" +
                                     "Thank you.";

                    SmsMessage.Body = smsBody;
                    SmsMessage.Destination = user.PhoneNumber;
                    SmsMessage.Subject = "New EUC Application Uploaded " + applicationNumber;

                    SmsService smsSerice = new SmsService();
                    await smsSerice.SendAsync(SmsMessage);

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("SMS notification sent to customs for Application: " + applicationNumber + ". Phone Number: " + user.PhoneNumber + "Has been sent to " + user.LastName + " " + user.FirstName + " for approval/rejection.",
                       forwardingUserID,
                       "SMS Notification Sent.",
                       forwardingUserIPAddress);
                }
                //Return server result of 'OK' if  messages are sent successfully
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            else
            {
                //Return server result of 'Bad Request' if  the user list is null
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }
    }
}