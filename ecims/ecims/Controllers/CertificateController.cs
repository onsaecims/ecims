﻿using ecims.Models;
using ecims.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace ecims.Controllers
{
    public class CertificateController : Controller
    {
        ApplicationDbContext _context;
        private ApplicationUserManager _userManager;
        AuditTrailGenerator.AuditTrail _auditTrail = new AuditTrailGenerator.AuditTrail();

        public CertificateController()
        {
            _context = new ApplicationDbContext();

        }

        // GET: Certificate
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        [Authorize(Roles = "ONSA Admin, ONSA PGSO, NSA")]
        //Search the certificate and mark as utilized
        public ActionResult SearchCertificate()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Viewed search certificate page. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Viewed search certificate page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            //var eucAppList = _context.EUCWorkFlow.Where(e => e.applicationNumber == eucappNo).ToList();
            return View(new SearchCertificateViewModel
            {
                user = user,
                role = roles
            });

        }
        [HttpPost]
        [Authorize]
        //Search the certificate and mark as utilized
        public ActionResult SearchCertificate(SearchCertificateViewModel model)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            if (String.IsNullOrEmpty(model.CertificateNumber))
            {
                TempData["Error"] = "Please enter a valid certificate number";
                //var eucAppList = _context.EUCWorkFlow.Where(e => e.applicationNumber == eucappNo).ToList();
                return View(new SearchCertificateViewModel
                {
                    user = user,
                    role = roles
                });
            }

            //Search the uploaded certificates
            var certificateNumber = model.CertificateNumber;

            var certificate = _context.UploadedEUCCertificate.Where(c => c.CertNo == certificateNumber).FirstOrDefault();

            if (certificate == null)
            {
                TempData["Error"] = "Please enter a valid certificate number";
                //var eucAppList = _context.EUCWorkFlow.Where(e => e.applicationNumber == eucappNo).ToList();
                return View(new SearchCertificateViewModel
                {
                    user = user,
                    role = roles
                });
            }

            //Mark the certificate as utilized
            var cert = _context.EUCCertificates.Where(c => c.CertNo == certificateNumber).FirstOrDefault();

            cert.CertStatus = CertificateStatus.Utilized;

            //Enter entry into the certificate workflow table
            var CertificateWorkflow = new CertificateWorkflow()
            {
                ActionTaken = "Utilized certificate. Certificate Number: " + certificateNumber,
                CertificateId = cert.id,
                DateOfAction = DateTime.Now,
                UserId = user.Id
            };
            _context.CertificateWorkflow.Add(CertificateWorkflow);
            int result = _context.SaveChanges();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Utilized certificate: Certificate Number: " + certificateNumber 
                + ". Verified by: " + user.FirstName + " " + user.LastName 
                + ". Application Number: " + cert.applicationNo,

               User.Identity.GetUserId(),
               "Utilized Certificate.",
               Request.UserHostAddress);

            result = _context.SaveChanges();

            return View(new SearchCertificateViewModel
            {
                user = user,
                role = roles,
                CertificateNumber = certificateNumber,
                Certificate = certificate
            });

        }
        //Mark the certificate as utilized
        public ActionResult UtilizeCertificate(string certificateNumber) 
        {
            var certificate = _context.EUCCertificates.Where(c => c.CertNo == certificateNumber).FirstOrDefault();

            certificate.CertStatus = CertificateStatus.Utilized;

            int result = _context.SaveChanges();

            var url = _context.UploadedEUCCertificate.Where(c => c.CertNo == certificateNumber).FirstOrDefault();

            //Return the certificate to the browser
            byte[] fileBytes = System.IO.File.ReadAllBytes(System.Web.Hosting.HostingEnvironment.MapPath(url.UploadUrl));
            string fileName = url.UploadUrl;
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }
    }
}