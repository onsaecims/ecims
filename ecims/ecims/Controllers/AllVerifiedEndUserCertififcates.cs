﻿using ecims.Models;
using ecims.ViewModels;
using Microsoft.AspNet.Identity;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace ecims.Controllers
{
    public class VerifiedEndUserCertififcatesController : Controller
    {
        ApplicationDbContext _context;
        private ApplicationUserManager _userManager;
        AuditTrailGenerator.AuditTrail _auditTrail = new AuditTrailGenerator.AuditTrail();

        public VerifiedEndUserCertififcatesController()
        {
            _context = new ApplicationDbContext();
        }

        [Authorize (Roles = "NSA")]
        public ActionResult AllVerifiedEndUserCertificates(string sortOrder, string CurrentSort, int? page, int? PageSize, string searchTerm)
        {
             //Plumbing code for shared view
             //Get ID of the currently logged in user
             var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var users = _context.Users.OrderByDescending(date => date.RequestedDate).ToList();
            var agencies = _context.PortalSubCategories.ToList();
            var itemCategories = _context.EUCItemCategory.ToList();
            var uploadedCertificates = _context.UploadedEUCCertificate.ToList();

            IPagedList<VerifiedCertificates> verifiedCertificates;

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User viewed all verified EUC applications page. Email: " 
                                        + User.Identity.GetUserName() + " Name: " 
                                        + user.FirstName + " " 
                                        + user.LastName + " Applicant Number: " 
                                        + user.ApplicationNumber,
                                        User.Identity.GetUserId(),
                                        "redirected to verified EUC page.",
                                        Request.UserHostAddress);

            //Parse search term
            string SearchTerm = "";
            if (searchTerm == null)
                SearchTerm = "";
            else if (searchTerm != null)
                SearchTerm = searchTerm;

            _context = new ApplicationDbContext();
            int pageSize = 10;

            if (PageSize != null)
                pageSize = PageSize.GetValueOrDefault();

            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;

            ViewBag.CurrentSort = sortOrder;

            if (SearchTerm != "" && sortOrder == null)
            {
                verifiedCertificates = _context.VerifiedCertificates
                    .Where(euc => euc.User.FirstName.Contains(searchTerm)
                    || euc.User.LastName.Contains(searchTerm)
                    || euc.User.Email.Contains(SearchTerm)
                    || euc.Certificate.CertNo.Contains(SearchTerm)
                    || euc.Certificate.applicationNo.Contains(SearchTerm)
                    || euc.User.ApplicationNumber.Contains(SearchTerm))
                    .Include(euc => euc.User)
                    .Include(euc => euc.Certificate)
                    .Distinct().OrderByDescending
                    (euc => euc.DateVerified).ToPagedList(pageIndex, pageSize);

                return View(new AllVerifiedEndUserCertififcatesViewModel()
                {
                    PageSize = pageSize,
                    sortOrder = sortOrder,
                    searchTerm = SearchTerm,
                    Agencies = agencies,
                    VerifiedEndUserCertificate = verifiedCertificates,
                    role = roles,
                    user = user,
                    UploadedCertificates = uploadedCertificates
                });
            }

            //sortOrder = String.IsNullOrEmpty(sortOrder) ? "Name" : sortOrder;
            verifiedCertificates = _context.VerifiedCertificates
                                   .Where(euc => euc.Id > 0)
                                   .Include(euc => euc.User)
                                   .Include(euc => euc.Certificate)
                                   .Distinct()
                                   .OrderByDescending
                                   (euc => euc.DateVerified).ToPagedList(pageIndex, pageSize);

            switch (sortOrder)
            {
                case "DateVerified":
                    if (sortOrder.Equals(CurrentSort))
                        verifiedCertificates = _context.VerifiedCertificates
                                                .Where(euc => euc.User.FirstName.Contains(searchTerm)
                                                || euc.User.LastName.Contains(searchTerm)
                                                || euc.User.Email.Contains(SearchTerm)
                                                || euc.Certificate.CertNo.Contains(SearchTerm)
                                                || euc.Certificate.applicationNo.Contains(SearchTerm)
                                                || euc.User.ApplicationNumber.Contains(SearchTerm))
                                                .Include(euc => euc.User)
                                                .Include(euc => euc.Certificate)
                                                .Distinct().OrderByDescending
                                                (euc => euc.DateVerified).ToPagedList(pageIndex, pageSize);
                    else
                        verifiedCertificates = _context.VerifiedCertificates
                                                .Where(euc => euc.User.FirstName.Contains(searchTerm)
                                                || euc.User.LastName.Contains(searchTerm)
                                                || euc.User.Email.Contains(SearchTerm)
                                                || euc.Certificate.CertNo.Contains(SearchTerm)
                                                || euc.Certificate.applicationNo.Contains(SearchTerm)
                                                || euc.User.ApplicationNumber.Contains(SearchTerm))
                                                .Include(euc => euc.User)
                                                .Include(euc => euc.Certificate)
                                                .Distinct().OrderBy
                                                (euc => euc.DateVerified).ToPagedList(pageIndex, pageSize);
                    break;
                case "CertificateNumber":
                    if (sortOrder.Equals(CurrentSort))
                        verifiedCertificates = _context.VerifiedCertificates
                                                .Where(euc => euc.User.FirstName.Contains(searchTerm)
                                                || euc.User.LastName.Contains(searchTerm)
                                                || euc.User.Email.Contains(SearchTerm)
                                                || euc.Certificate.CertNo.Contains(SearchTerm)
                                                || euc.Certificate.applicationNo.Contains(SearchTerm)
                                                || euc.User.ApplicationNumber.Contains(SearchTerm))
                                                .Include(euc => euc.User)
                                                .Include(euc => euc.Certificate)
                                                .Distinct().OrderByDescending
                                                (euc => euc.Certificate.CertNo).ToPagedList(pageIndex, pageSize);
                    else
                        verifiedCertificates = _context.VerifiedCertificates
                                                .Where(euc => euc.User.FirstName.Contains(searchTerm)
                                                || euc.User.LastName.Contains(searchTerm)
                                                || euc.User.Email.Contains(SearchTerm)
                                                || euc.Certificate.CertNo.Contains(SearchTerm)
                                                || euc.Certificate.applicationNo.Contains(SearchTerm)
                                                || euc.User.ApplicationNumber.Contains(SearchTerm))
                                                .Include(euc => euc.User)
                                                .Include(euc => euc.Certificate)
                                                .Distinct().OrderBy
                                                (euc => euc.Certificate.CertNo).ToPagedList(pageIndex, pageSize);
                    break;
                case "VerifiedBy":
                    if (sortOrder.Equals(CurrentSort))
                        verifiedCertificates = _context.VerifiedCertificates
                                                .Where(euc => euc.User.FirstName.Contains(searchTerm)
                                                || euc.User.LastName.Contains(searchTerm)
                                                || euc.User.Email.Contains(SearchTerm)
                                                || euc.Certificate.CertNo.Contains(SearchTerm)
                                                || euc.Certificate.applicationNo.Contains(SearchTerm)
                                                || euc.User.ApplicationNumber.Contains(SearchTerm))
                                                .Include(euc => euc.User)
                                                .Include(euc => euc.Certificate)
                                                .Distinct().OrderByDescending
                                                (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                    else
                        verifiedCertificates = _context.VerifiedCertificates
                                                .Where(euc => euc.User.FirstName.Contains(searchTerm)
                                                || euc.User.LastName.Contains(searchTerm)
                                                || euc.User.Email.Contains(SearchTerm)
                                                || euc.Certificate.CertNo.Contains(SearchTerm)
                                                || euc.Certificate.applicationNo.Contains(SearchTerm)
                                                || euc.User.ApplicationNumber.Contains(SearchTerm))
                                                .Include(euc => euc.User)
                                                .Include(euc => euc.Certificate)
                                                .Distinct().OrderBy
                                                (euc => euc.User.LastName).ToPagedList(pageIndex, pageSize);
                    break;
            }

                    return View(new AllVerifiedEndUserCertififcatesViewModel()
            {
                PageSize = pageSize,
                sortOrder = sortOrder,
                searchTerm = SearchTerm,
                Agencies = agencies,
                VerifiedEndUserCertificate = verifiedCertificates,
                role = roles,
                user = user,
                UploadedCertificates = uploadedCertificates 
            });
        }
    }
}