﻿using System.Security.Cryptography;
using System.Text;

namespace ecims.Controllers.TokenCodeGenerator
{
    public class TokenCodeGenerator
    {
        //Method to generate an 8 digit alpha-numberic token to accompany an EUC
        public static string GenerateToken(int maxSize)
        {
            //Set max length of token
            int tokenSize = 10;

            char[] chars = new char[62];
            chars =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[tokenSize];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(tokenSize);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }
    }
}