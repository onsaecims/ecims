﻿using ecims.ViewModels.UserDataViaExcel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ecims.Models;
using PagedList;

namespace ecims.Controllers.DirectoryAndDatabaseChangeController
{
    public class UserDataViaExcelController : Controller
    {
        const int RecordsPerPage = 5;
        ApplicationDbContext _context;

        // GET: UserDataViaExcel
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserDataViaExcel(UserDataViaExcelViewmodel model)


        {
            _context = new ApplicationDbContext();
            if (!string.IsNullOrEmpty(model.SearchButton) || model.Page.HasValue)
            {
                var users = _context.Users.Where(user => user.FirstName == model.FirstName || model.FirstName == null && user.LastName == model.LastName || model.FirstName == null).ToList();

                var pageIndex = model.Page ?? 1;
                model.SearchResults = users.ToPagedList(pageIndex, RecordsPerPage);
            }
            return View(model);
        }
    }
}