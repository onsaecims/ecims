﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecims.Controllers.DirectoryAndDatabaseChangeController
{
    public class ProgressLog : Controller
    {
        public void PassportUploadLogger(string filename, string path, string logFile)
        {
            //Name and path of our passport log
            string logFileName = "Passport Upload Log-" + DateTime.Now.Year +"-"+ DateTime.Now.Month +"-"+ DateTime.Now.Day +".txt";
            //string logPath = "~/FileUploadLogs/" + logFileName;
            //string logPath = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));
            //Check if the file log exists
            //If it doesnt, create and write to it
            try
            {
                logFile += "\\" + logFileName;

                if (!System.IO.File.Exists(logFile))
                {
                    //Write to the log
                    System.IO.File.WriteAllText(logFile, filename + " could not be found at " + path);
                }
                else if (System.IO.File.Exists(logFile))
                {
                    using (StreamWriter file = new StreamWriter(logFile, true))
                    {
                        file.WriteLine(DateTime.Now.ToString() + "-" + filename + " could not be found at " + path);
                    }
                }
            }
            //If it does, then append to it
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void MeansOfIDUploadLogger(string filename, string path, string logFile)
        {
            //Name and path of our passport log
            string logFileName = "Means Of Identification Upload Log-" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + ".txt";
            //string logPath = "~/FileUploadLogs/" + logFileName;
            //string logPath = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));
            //Check if the file log exists
            //If it doesnt, create and write to it
            try
            {
                logFile += "\\" + logFileName;

                if (!System.IO.File.Exists(logFile))
                {
                    //Write to the log
                    System.IO.File.WriteAllText(logFile, filename + " could not be found at " + path);
                }
                else if (System.IO.File.Exists(logFile))
                {
                    using (StreamWriter file = new StreamWriter(logFile, true))
                    {
                        file.WriteLine(DateTime.Now.ToString() + "-" + filename + " could not be found at " + path);
                    }
                }
            }
            //If it does, then append to it
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void UploadedCertificatesUploadLogger(string filename, string path, string logFile)
        {
            //Name and path of our passport log
            string logFileName = "Uploaded EUC Certificates Log-" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + ".txt";
            //string logPath = "~/FileUploadLogs/" + logFileName;
            //string logPath = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));
            //Check if the file log exists
            //If it doesnt, create and write to it
            try
            {
                logFile += "\\" + logFileName;

                if (!System.IO.File.Exists(logFile))
                {
                    //Write to the log
                    System.IO.File.WriteAllText(logFile, filename + " could not be found at " + path);
                }
                else if (System.IO.File.Exists(logFile))
                {
                    using (StreamWriter file = new StreamWriter(logFile, true))
                    {
                        file.WriteLine(DateTime.Now.ToString() + "-" + filename + " could not be found at " + path);
                    }
                }
            }
            //If it does, then append to it
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void UploadedCompanyLogoLogger(string filename, string path, string logFile)
        {
            //Name and path of our passport log
            string logFileName = "Uploaded Company Logo Log-" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + ".txt";
            //string logPath = "~/FileUploadLogs/" + logFileName;
            //string logPath = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));
            //Check if the file log exists
            //If it doesnt, create and write to it
            try
            {
                logFile += "\\" + logFileName;

                if (!System.IO.File.Exists(logFile))
                {
                    //Write to the log
                    System.IO.File.WriteAllText(logFile, filename + " could not be found at " + path);
                }
                else if (System.IO.File.Exists(logFile))
                {
                    using (StreamWriter file = new StreamWriter(logFile, true))
                    {
                        file.WriteLine(DateTime.Now.ToString() + "-" + filename + " could not be found at " + path);
                    }
                }
            }
            //If it does, then append to it
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void UploadedCompanyCACLogger(string filename, string path, string logFile)
        {
            //Name and path of our passport log
            string logFileName = "Uploaded Company CAC Log-" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + ".txt";
            //string logPath = "~/FileUploadLogs/" + logFileName;
            //string logPath = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));
            //Check if the file log exists
            //If it doesnt, create and write to it
            try
            {
                logFile += "\\" + logFileName;

                if (!System.IO.File.Exists(logFile))
                {
                    //Write to the log
                    System.IO.File.WriteAllText(logFile, filename + " could not be found at " + path);
                }
                else if (System.IO.File.Exists(logFile))
                {
                    using (StreamWriter file = new StreamWriter(logFile, true))
                    {
                        file.WriteLine(DateTime.Now.ToString() + "-" + filename + " could not be found at " + path);
                    }
                }
            }
            //If it does, then append to it
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}