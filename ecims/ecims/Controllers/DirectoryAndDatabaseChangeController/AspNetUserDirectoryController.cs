﻿using ecims.Controllers.DirectoryAndDatabaseChangeController;
using ecims.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ecims.Controllers.DirectoryController
{
    public class AspNetUserDirectoryController : Controller
    {
        ApplicationDbContext _context;

        public ActionResult DirectoryChange()
        {
            return View();
        }

        public AspNetUserDirectoryController()
        {
            _context = new ApplicationDbContext();
        }

        //Change user image directory to match the new system's naming convention
        public async Task<ActionResult> changeUserImageDirectory()
        {
            //User list in db
            var usersInDb = _context.Users.ToList();
            //Loop through every user in the database
            foreach (var user in usersInDb)
            {
                //Get passport column
                var passport = user.PassportUrl;

                if (passport == null)
                {
                    continue;
                }

                //String to match
                var matchingText = "/images/profile/" + user.Email;

                //If matching text exists in user record
                if (passport.Contains(matchingText))
                {
                    //Do nothing

                }
                //If it doesn't match
                else if (!passport.Contains(matchingText))
                {
                    //string currentPath = System.IO.Path.Combine(Server.MapPath("~/dev/ecims/webapps/resources/images/uploadedfiles"));
                    //string finalPath = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email);

                    //var directory = Directory.CreateDirectory(path);

                    //STEP 1: Get file path and create the directory in the database
                    string fileName = Path.GetFileName(passport);

                    //Sample save path-/images/profile/ocedache35@gmail.com/face.jpg
                    user.PassportUrl = "/images/profile/" + user.Email + "/" + fileName;

                    await _context.SaveChangesAsync();

                    //STEP 2: Move the files from the current location to the new one
                    //string oldPath = "~/images/importedFiles/" + fileName;
                    //string newPath = "~" + user.PassportUrl;
                    string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                    string newPath = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email);

                    string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email + "\\" + fileName);

                    try
                    {
                        if (!System.IO.File.Exists(oldPath))
                        {
                            //Check that the file doesn't exist
                            ProgressLog log = new ProgressLog();

                            string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                            log.PassportUploadLogger(fileName, oldPath, logFile);
                        }
                        //Ensure that the target path does now exist
                        else if (System.IO.File.Exists(oldPath))
                        {
                            //Create the path to save on server
                            var saveDirectory = Directory.CreateDirectory(newPath);

                            System.IO.File.Copy(oldPath, desinationPath, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                }
            }
            return RedirectToAction("Index", "Home");
        }
        //Change saved means of identifications to match directory structure
        public async Task<ActionResult> changeUserMeansOfIdentificationDirectory()
        {
            //User list in db
            var usersInDb = _context.Users.ToList();
            //Loop through every user in the database
            foreach (var user in usersInDb)
            {
                //Get means of identication column
                var meansOfIdentification = user.IdUrl;

                if (meansOfIdentification == null)
                {
                    continue;
                }

                //String to match
                var matchingText = "/images/profile/ID/" + user.Email;

                //If matching text exists in user record
                if (meansOfIdentification.Contains(matchingText))
                {
                    //Do nothing

                }
                //If it doesn't match
                else if (!meansOfIdentification.Contains(matchingText))
                {
                    //STEP 1: Get file path and create the directory in the database
                    string fileName = Path.GetFileName(meansOfIdentification);

                    //Sample save path-/images/profile/ocedache35@gmail.com/face.jpg
                    user.IdUrl = "/images/profile/ID/" + user.Email + "/" + fileName;

                    await _context.SaveChangesAsync();

                    //STEP 2: Move the files from the current location to the new one
                    string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                    string newPath = System.IO.Path.Combine(Server.MapPath("~/images/profile/ID/"), user.Email);

                    string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/profile/ID/"), user.Email + "\\" + fileName);

                    try
                    {
                        if (!System.IO.File.Exists(oldPath))
                        {
                            //Check that the file doesn't exist
                            ProgressLog log = new ProgressLog();

                            string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                            log.MeansOfIDUploadLogger(fileName, oldPath, logFile);
                        }
                        //Ensure that the target path does now exist
                        else if (System.IO.File.Exists(oldPath))
                        {
                            //Create the path to save on server
                            var saveDirectory = Directory.CreateDirectory(newPath);

                            System.IO.File.Copy(oldPath, desinationPath, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }
        //Change EUC certificate to match directory structure
        public async Task<ActionResult> changeUserUploadedCertificateDirectory()
        {
            //List of all certificates in db
            var uploadedCertificates = _context.UploadedEUCCertificate.ToList();
            //Loop through every user in the database
            foreach (var certificate in uploadedCertificates)
            {
                //Get means of identication column
                var uploadedCertificate = certificate.UploadUrl;

                if (uploadedCertificate == null)
                {
                    continue;
                }

                //Get the user that owns the certificate
                var user = _context.Users.Find(certificate.UserID);
                if (user == null)
                {
                    continue;
                }

                //String to match
                var matchingText = "/images/profile/" + user.Email + "/EUC Certificates/";

                //If matching text exists in user record
                if (uploadedCertificate.Contains(matchingText))
                {
                    //Do nothing

                }
                //If it doesn't match
                else if (!uploadedCertificate.Contains(matchingText))
                {
                    //STEP 1: Get file path and create the directory in the database
                    string fileName = Path.GetFileName(uploadedCertificate);

                    //Sample save path-/images/profile/zillaitor@gmail.com/EUC Certificates/CollectCertificate-01.pdf
                    certificate.UploadUrl = "/images/profile/" + user.Email + "/EUC Certificates/" + fileName;

                    await _context.SaveChangesAsync();

                    //STEP 2: Move the files from the current location to the new one
                    string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                    string newPath = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/EUC Certificates/"));

                    string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/EUC Certificates" + "\\" + fileName));

                    try
                    {
                        if (!System.IO.File.Exists(oldPath))
                        {
                            //Check that the file doesn't exist
                            ProgressLog log = new ProgressLog();

                            string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                            log.MeansOfIDUploadLogger(fileName, oldPath, logFile);
                        }
                        //Ensure that the target path does now exist
                        else if (System.IO.File.Exists(oldPath))
                        {
                            //Create the path to save on server
                            var saveDirectory = Directory.CreateDirectory(newPath);

                            System.IO.File.Copy(oldPath, desinationPath, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }
        //Change company logo to match directory structure
        public async Task<ActionResult> changeCompanyLogoDirectory()
        {
            //List of all certificates in db
            var companies = _context.Company.ToList();
            //Loop through every user in the database
            foreach (var company in companies)
            {
                //Get means of identication column
                var logo = company.Logo;

                if (logo == null)
                {
                    continue;
                }

                //Get the user that owns the certificate
                var user = _context.Users.Find(company.OwnerId);

                if (user == null)
                {
                    continue;
                }
                //String to match
                var matchingText = "/images/profile/" + user.Email + "/Company Documents/";

                //If matching text exists in user record
                if (logo.Contains(matchingText))
                {
                    //Do nothing
                }
                //If it doesn't match
                else if (!logo.Contains(matchingText))
                {
                    //STEP 1: Get file path and create the directory in the database
                    string fileName = Path.GetFileName(logo);

                    //Sample save path-/images/profile/zillaitor@gmail.com/EUC Certificates/CollectCertificate-01.pdf
                    company.Logo = "/images/profile/" + user.Email + "/Company Documents/" + fileName;

                    await _context.SaveChangesAsync();

                    //STEP 2: Move the files from the current location to the new one
                    string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                    string newPath = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Company Documents/"));

                    string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Company Documents" + "\\" + fileName));

                    try
                    {
                        if (!System.IO.File.Exists(oldPath))
                        {
                            //Check that the file doesn't exist
                            ProgressLog log = new ProgressLog();

                            string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                            log.UploadedCompanyLogoLogger(fileName, oldPath, logFile);
                        }
                        //Ensure that the target path does now exist
                        else if (System.IO.File.Exists(oldPath))
                        {
                            //Create the path to save on server
                            var saveDirectory = Directory.CreateDirectory(newPath);

                            System.IO.File.Copy(oldPath, desinationPath, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }
        //Change company logo to match directory structure
        public async Task<ActionResult> changeCompanyCACDirectory()
        {
            //List of all certificates in db
            var companies = _context.Company.ToList();
            //Loop through every user in the database
            foreach (var company in companies)
            {
                //Get means of identication column
                var cac = company.CertificateOfIncorporation;

                if (cac == null)
                {
                    continue;
                }

                //Get the user that owns the certificate
                var user = _context.Users.Find(company.OwnerId);
                if (user == null)
                {
                    continue;
                }
                //String to match
                var matchingText = "/images/profile/" + user.Email + "/Company Documents/";

                //If matching text exists in user record
                if (cac.Contains(matchingText))
                {
                    //Do nothing
                }
                //If it doesn't match
                else if (!cac.Contains(matchingText))
                {
                    //STEP 1: Get file path and create the directory in the database
                    string fileName = Path.GetFileName(cac);

                    //Sample save path-/images/profile/zillaitor@gmail.com/EUC Certificates/CollectCertificate-01.pdf
                    company.CertificateOfIncorporation = "/images/profile/" + user.Email + "/Company Documents/" + fileName;

                    await _context.SaveChangesAsync();

                    //STEP 2: Move the files from the current location to the new one
                    string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                    string newPath = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Company Documents/"));

                    string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + user.Email + "/Company Documents" + "\\" + fileName));

                    try
                    {
                        if (!System.IO.File.Exists(oldPath))
                        {
                            //Check that the file doesn't exist
                            ProgressLog log = new ProgressLog();

                            string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                            log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                        }
                        //Ensure that the target path does now exist
                        else if (System.IO.File.Exists(oldPath))
                        {
                            //Create the path to save on server
                            var saveDirectory = Directory.CreateDirectory(newPath);

                            System.IO.File.Copy(oldPath, desinationPath, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }
        //Set supporting documents from the old dtorage structure to the new one
        public async Task<ActionResult> changeEUCSupportingDocuments()
        {
            //Get every EUC attachment row in the DB
            var eucSupportingDocumentsRecords = _context.EUCApplicationAttachments.ToList();

            //Iterate through them
            foreach (var supportingDocumentRow in eucSupportingDocumentsRecords)
            {
                //Get application numer of the application
                var applicationNumber = supportingDocumentRow.applicationNo;

                if (applicationNumber != null)
                {
                    //Find the application
                    var application = _context.EndUserCertificate.Where(euc => euc.applicationNo == applicationNumber).FirstOrDefault();

                    #region CBN ATTACHMENTS
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.cbnApplication == null
                            || supportingDocumentRow.cbnApplication.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.cbnApplication);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/CBN Letter/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/CBN Letter" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.cbnApplication = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/CBN Letter/" + fileName;

                                    await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion
                    #region MIN AGRIC DOCUMENTS
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.minOfAgricApplication == null
                            || supportingDocumentRow.minOfAgricApplication.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.minOfAgricApplication);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/MIN AGRIC/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/MIN AGRIC" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.minOfAgricApplication = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/MIN AGRIC/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion                            
                    #region NCAA
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.NCCALetter == null
                            || supportingDocumentRow.NCCALetter.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.NCCALetter);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/NCAA LETTER/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/NCAA LETTER" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.NCCALetter = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/NCAA LETTER/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion                            
                    #region ARPIdentification
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.ARPIdentification == null
                            || supportingDocumentRow.ARPIdentification.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.ARPIdentification);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/ARP IDENTIFICATION/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/ARP IDENTIFICATION" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.ARPIdentification = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/ARP IDENTIFICATION/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion    
                    #region MEMO OF ASSOCIATION
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.memoOfAssociation == null
                            || supportingDocumentRow.memoOfAssociation.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.memoOfAssociation);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/MEMO OF ASSOCIATION/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/MEMO OF ASSOCIATION" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.memoOfAssociation = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/MEMO OF ASSOCIATION/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion       
                    #region BILL OF LADEN
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.bilOfLaden == null
                            || supportingDocumentRow.bilOfLaden.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.bilOfLaden);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/BILL OF LADEN/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/BILL OF LADEN" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.bilOfLaden = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/BILL OF LADEN/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region ANALYSIS CERTIFICATE
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.analysisCert == null
                            || supportingDocumentRow.analysisCert.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.analysisCert);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/ANALYSIS CERTIFICATE/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/ANALYSIS CERTIFICATE" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.analysisCert = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/ANALYSIS CERTIFICATE/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion    
                    #region INCORPORATION CERTIFICATE
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.incorporationCert == null
                            || supportingDocumentRow.incorporationCert.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.incorporationCert);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/INCORPORATION CERTIFICATE/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/INCORPORATION CERTIFICATE" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.incorporationCert = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/INCORPORATION CERTIFICATE/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region AC GAD 001
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.ac_gad001 == null
                            || supportingDocumentRow.ac_gad001.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.ac_gad001);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/AC GAD001/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/AC GAD001" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.ac_gad001 = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/AC GAD001/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region TAX CLEARANCE
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.taxClerance == null
                            || supportingDocumentRow.taxClerance.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.taxClerance);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/TAX CLEARANCE/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/TAX CLEARANCE" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.taxClerance = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/TAX CLEARANCE/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion   
                    #region PREVIOUS NETWORK DISTRIBUTION
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.previousNetworkDistribution == null
                            || supportingDocumentRow.previousNetworkDistribution.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.previousNetworkDistribution);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/PREVIOUS NETWORK DISTRIBUTION/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/PREVIOUS NETWORK DISTRIBUTION" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.previousNetworkDistribution = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/PREVIOUS NETWORK DISTRIBUTION/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region PRIVATE SECURITY REG
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.privateSecurityReg == null
                            || supportingDocumentRow.privateSecurityReg.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.privateSecurityReg);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/PRIVATE SECURITY REG/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/PRIVATE SECURITY REG" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.privateSecurityReg = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/PRIVATE SECURITY REG/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion 
                    #region CIT LICENSE
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.citLicense == null
                            || supportingDocumentRow.citLicense.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.citLicense);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/CIT LICENSE/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/CIT LICENSE" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.citLicense = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/CIT LICENSE/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region FIRS CERTIFICATE
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.firsCertificate == null
                            || supportingDocumentRow.firsCertificate.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.firsCertificate);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/FIRS CERTIFICATE/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/FIRS CERTIFICATE" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.firsCertificate = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/FIRS CERTIFICATE/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion   
                    #region FORM C07
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.formCO7 == null
                            || supportingDocumentRow.formCO7.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.formCO7);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/FORM C07/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/FORM C07" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.formCO7 = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/FORM C07/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion    
                    #region CBN FORM M
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.cbnFormM == null
                            || supportingDocumentRow.cbnFormM.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.cbnFormM);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/CBN FORM M/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/CBN FORM M" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.cbnFormM = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/CBN FORM M/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion   
                    #region IDENTITY CARD
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.identityCard == null
                            || supportingDocumentRow.identityCard.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.identityCard);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/IDENTITY CARD/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/IDENTITY CARD" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.identityCard = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/IDENTITY CARD/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion
                    #region LAST 1 YEAR EUCS
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.last1YearEUCS == null
                            || supportingDocumentRow.last1YearEUCS.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.last1YearEUCS);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/LAST 1 YEAR EUCS/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/LAST 1 YEAR EUCS" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.last1YearEUCS = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/LAST 1 YEAR EUCS/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion   
                    #region ACQUIRER INTRO LETTER
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.acquirerIntroLetter == null
                            || supportingDocumentRow.acquirerIntroLetter.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.acquirerIntroLetter);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/ACQUIRER INTRO LETTER/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/ACQUIRER INTRO LETTER" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.acquirerIntroLetter = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/ACQUIRER INTRO LETTER/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region MAGAZINE LICENSES
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.magazineLicenses == null
                            || supportingDocumentRow.magazineLicenses.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.magazineLicenses);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/MAGAZINE LICENSE/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/MAGAZINE LICENSE" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.magazineLicenses = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/MAGAZINE LICENSE/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion
                    #region MANUFACTURERS SAFETY
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.manufacturersSafety == null
                            || supportingDocumentRow.manufacturersSafety.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.manufacturersSafety);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/MANUFACTURERS SAFETY/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/MANUFACTURERS SAFETY" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.manufacturersSafety = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/MANUFACTURERS SAFETY/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region NAFDAC ATTACHMENT
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.NAFDACAttachment == null
                            || supportingDocumentRow.NAFDACAttachment.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.NAFDACAttachment);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/NAFDAC ATTACHMENT/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/NAFDAC ATTACHMENT" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.NAFDACAttachment = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/NAFDAC ATTACHMENT/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region NPF FIREWORKS PERMIT
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.NPFFIreworksPermit == null
                            || supportingDocumentRow.NPFFIreworksPermit.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.NPFFIreworksPermit);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/NPF FIREWORKS PERMIT/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/NPF FIREWORKS PERMIT" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.NPFFIreworksPermit = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/NPF FIREWORKS PERMIT/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion 
                    #region PACKING LIST
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.packingList == null
                            || supportingDocumentRow.packingList.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.packingList);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/PACKING LIST/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/PACKING LIST" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.packingList = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/PACKING LIST/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion 
                    #region IMPORT PERMIT FOR EXPLOSIVE
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.importPermitforExplosive == null
                            || supportingDocumentRow.importPermitforExplosive.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.importPermitforExplosive);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/IMPORT PERMIT FOR EXPLOSIVE/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/IMPORT PERMIT FOR EXPLOSIVE" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.importPermitforExplosive = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/IMPORT PERMIT FOR EXPLOSIVE/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion
                    #region PREV STORAGE ATTACHMENT
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.prevStorageAttachment == null
                            || supportingDocumentRow.prevStorageAttachment.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.prevStorageAttachment);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/PREV STORAGE ATTACHMENT/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/PREV STORAGE ATTACHMENT" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.prevStorageAttachment = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/PREV STORAGE ATTACHMENT/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion    
                    #region PROFORMA INVOICE
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.proformaInvoice == null
                            || supportingDocumentRow.proformaInvoice.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.proformaInvoice);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/PROFORMA INVOICE/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/PROFORMA INVOICE" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.proformaInvoice = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/PROFORMA INVOICE/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion 
                    #region QUALIFIED TECH OFFICER DETAILS
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.qualifiedTechOfficerDetails == null
                            || supportingDocumentRow.qualifiedTechOfficerDetails.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.qualifiedTechOfficerDetails);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/QUALIFIED TECH OFFICER DETAILS/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/QUALIFIED TECH OFFICER DETAILS" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.qualifiedTechOfficerDetails = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/QUALIFIED TECH OFFICER DETAILS/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region FFD REG
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.FFDReg == null
                            || supportingDocumentRow.FFDReg.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.FFDReg);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/FFD REG/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/FFD REG" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.FFDReg = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/FFD REG/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion 
                    #region SPECIFICATION OF GOODS
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.specificationOfGoods == null
                            || supportingDocumentRow.specificationOfGoods.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.specificationOfGoods);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/SPECIFICATION OF GOODS/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/SPECIFICATION OF GOODS" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.specificationOfGoods = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/SPECIFICATION OF GOODS/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion 
                    #region STORAGE FACILITY ATTACHMENT
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.storageFacilityAttachment == null
                            || supportingDocumentRow.storageFacilityAttachment.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.storageFacilityAttachment);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/STORAGE FACILITY ATTACHMENT/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/STORAGE FACILITY ATTACHMENT" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.storageFacilityAttachment = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/STORAGE FACILITY ATTACHMENT/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion     
                    #region TRANSIT DETAILS
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.transitDetails == null
                            || supportingDocumentRow.transitDetails.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.transitDetails);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/TRANSIT DETAILS/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/TRANSIT DETAILS" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.transitDetails = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/TRANSIT DETAILS/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion    
                    #region TRANSPORTER PARTICULARS
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.transporterParticulars == null
                            || supportingDocumentRow.transporterParticulars.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.transporterParticulars);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/TRANSPORTER PARTICULARS/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/TRANSPORTER PARTICULARS" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.transporterParticulars = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/TRANSPORTER PARTICULARS/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion   
                    #region VAT CERTIFICATE
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.vatCert == null
                            || supportingDocumentRow.vatCert.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.vatCert);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/VAT CERTIFICATE/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/VAT CERTIFICATE" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.vatCert = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/VAT CERTIFICATE/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion 
                    #region VENDOR ID DETAILS
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.vendorIDDetails == null
                            || supportingDocumentRow.vendorIDDetails.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.vendorIDDetails);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/VENDOR ID DETAILS/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/VENDOR ID DETAILS" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.vendorIDDetails = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/VENDOR ID DETAILS/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion    
                    #region OPERATION YEARS
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.operationYears == null
                            || supportingDocumentRow.operationYears.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.operationYears);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/OPERATION YEARS/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/OPERATION YEARS" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.operationYears = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/OPERATION YEARS/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion    
                    #region COMPANY DIRECTORS PROFILE
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.companyDirectorsProfile == null
                            || supportingDocumentRow.companyDirectorsProfile.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.companyDirectorsProfile);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/COMPANY DIRECTORS PROFILE/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/COMPANY DIRECTORS PROFILE" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.companyDirectorsProfile = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/COMPANY DIRECTORS PROFILE/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region STOCK BALANCE
                    try
                    {
                        //String to match
                        var matchingText = "/images/EUC Supporting Documents";
                        if (supportingDocumentRow.StockBalance == null
                            || supportingDocumentRow.StockBalance.Contains(matchingText))
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.StockBalance);

                            //STEP 2: Move the files from the current location to the new one
                            string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                            string newPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/STOCK BALANCE/"));

                            string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/STOCK BALANCE" + "\\" + fileName));

                            try
                            {
                                if (!System.IO.File.Exists(oldPath))
                                {
                                    //Check that the file doesn't exist
                                    ProgressLog log = new ProgressLog();

                                    //Log the missing file
                                    //string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                                    //log.UploadedCompanyCACLogger(fileName, oldPath, logFile);
                                }
                                //Ensure that the target path does now exist
                                else if (System.IO.File.Exists(oldPath))
                                {
                                    //Create the path to save on server
                                    var saveDirectory = Directory.CreateDirectory(newPath);

                                    System.IO.File.Copy(oldPath, desinationPath, true);

                                    //save the new path in the database
                                    supportingDocumentRow.StockBalance = "/images/EUC Supporting Documents/" + supportingDocumentRow.applicationNo + "/STOCK BALANCE/" + fileName;

                                    var result = await _context.SaveChangesAsync();
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
                }
            }
            return RedirectToAction("Index", "Home");
        }
        //Generate application numbers for all users with null values
        public async Task<ActionResult> changeOrganisationAwardContractDirectory()
        {
            //List of all certificates in db
            var companyAwardContracts = _context.EUCOrganisations.ToList();
            //Loop through every user in the database
            foreach (var awardcontract in companyAwardContracts)
            {
                //Get means of identication column
                var awardLetter = awardcontract.AwardContract;

                if (awardLetter == null)
                {
                    continue;
                }

                //String to match
                var matchingText = "/images/company award letters/" + awardcontract.ApplicationNumber;

                //If matching text exists in user record
                if (awardLetter.Contains(matchingText))
                {
                    //Do nothing

                }
                //If it doesn't match
                else if (!awardLetter.Contains(matchingText))
                {
                    //STEP 1: Get file path and create the directory in the database
                    string fileName = Path.GetFileName(awardLetter);

                    //Sample save path-/images/profile/zillaitor@gmail.com/EUC Certificates/CollectCertificate-01.pdf
                    awardcontract.AwardContract = "/images/Company Award Letters/" + awardcontract.ApplicationNumber + "/" + fileName;

                    await _context.SaveChangesAsync();

                    //STEP 2: Move the files from the current location to the new one
                    string oldPath = System.IO.Path.Combine(Server.MapPath("~/images/importedFiles") + "\\" + fileName);
                    string newPath = System.IO.Path.Combine(Server.MapPath("~/images/Company Award Letters/" + awardcontract.ApplicationNumber + "/"));

                    string desinationPath = System.IO.Path.Combine(Server.MapPath("~/images/Company Award Letters/" + awardcontract.ApplicationNumber + "\\" + fileName));

                    try
                    {
                        if (!System.IO.File.Exists(oldPath))
                        {
                            //Check that the file doesn't exist
                            ProgressLog log = new ProgressLog();

                            string logFile = System.IO.Path.Combine(Server.MapPath("~/FileUploadLogs"));

                            //log.MeansOfIDUploadLogger(fileName, oldPath, logFile);
                        }
                        //Ensure that the target path does now exist
                        else if (System.IO.File.Exists(oldPath))
                        {
                            //Create the path to save on server
                            var saveDirectory = Directory.CreateDirectory(newPath);

                            System.IO.File.Copy(oldPath, desinationPath, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }
        public ActionResult generateApplicationNumbers()
        {
            ApplicationNumberGenerator.ApplicationNumberGeneratorForNullApplicantsController appNumberGenerator = new ApplicationNumberGenerator.ApplicationNumberGeneratorForNullApplicantsController();

            //var usersInDb = _context.Users.ToList();
            try
            {
                appNumberGenerator.GenerateApplicationNumberForAplicantsWithNullValues();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return RedirectToAction("Index", "Home");
        }
        //RESET CODE BLOCK
        //Change user image directory to match the new system's naming convention
        public async Task<ActionResult> resetUserPassport()
        {
            //User list in db
            var usersInDb = _context.Users.ToList();
            //Loop through every user in the database
            foreach (var user in usersInDb)
            {
                //Get passport column
                var passport = user.PassportUrl;

                if (passport == null)
                {
                    continue;
                }

                //string currentPath = System.IO.Path.Combine(Server.MapPath("~/dev/ecims/webapps/resources/images/uploadedfiles"));
                //string finalPath = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email);

                //var directory = Directory.CreateDirectory(path);

                //STEP 1: Get file path and create the directory in the database
                string fileName = Path.GetFileName(passport);

                //Sample save path-/images/profile/ocedache35@gmail.com/face.jpg
                user.PassportUrl = fileName;

                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Index", "Home");
        }
        //Change user ID to match the new system's naming convention
        public async Task<ActionResult> resetUserID()
        {
            //User list in db
            var usersInDb = _context.Users.ToList();
            //Loop through every user in the database
            foreach (var user in usersInDb)
            {
                //Get passport column
                var ID = user.IdUrl;

                if (ID == null)
                {
                    continue;
                }

                //string currentPath = System.IO.Path.Combine(Server.MapPath("~/dev/ecims/webapps/resources/images/uploadedfiles"));
                //string finalPath = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email);

                //var directory = Directory.CreateDirectory(path);

                //STEP 1: Get file path and create the directory in the database
                string fileName = Path.GetFileName(ID);

                //Sample save path-/images/profile/ocedache35@gmail.com/face.jpg
                user.IdUrl = fileName;

                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Index", "Home");
        }
        public async Task<ActionResult> resetUploadedCertificateDirectory()
        {
            //User list in db
            var uploadedCertificates = _context.UploadedEUCCertificate.ToList();
            //Loop through every user in the database
            foreach (var user in uploadedCertificates)
            {
                //Get passport column
                var file = user.UploadUrl;

                if (file == null)
                {
                    continue;
                }

                //string currentPath = System.IO.Path.Combine(Server.MapPath("~/dev/ecims/webapps/resources/images/uploadedfiles"));
                //string finalPath = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email);

                //var directory = Directory.CreateDirectory(path);

                //STEP 1: Get file path and create the directory in the database
                string fileName = Path.GetFileName(file);

                //Sample save path-/images/profile/ocedache35@gmail.com/face.jpg
                user.UploadUrl = fileName;

                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Index", "Home");
        }
        public async Task<ActionResult> resetCompanyLogoDirectory()
        {
            //User list in db
            var companies = _context.Company.ToList();
            //Loop through every user in the database
            foreach (var company in companies)
            {
                if (company.Logo == null)
                {
                    continue;
                }
                //Get passport column
                var file = company.Logo;

                if (file == null)
                {
                    continue;
                }

                //string currentPath = System.IO.Path.Combine(Server.MapPath("~/dev/ecims/webapps/resources/images/uploadedfiles"));
                //string finalPath = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email);

                //var directory = Directory.CreateDirectory(path);

                //STEP 1: Get file path and create the directory in the database
                string fileName = Path.GetFileName(file);

                //Sample save path-/images/profile/ocedache35@gmail.com/face.jpg
                company.Logo = fileName;

                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Index", "Home");
        }
        public async Task<ActionResult> resetCompanyCACDirectory()
        {
            //User list in db
            var companies = _context.Company.ToList();
            //Loop through every user in the database
            foreach (var company in companies)
            {
                if (company.CertificateOfIncorporation == null)
                {
                    continue;
                }
                //Get passport column
                var file = company.CertificateOfIncorporation;

                if (file == null)
                {
                    continue;
                }

                //string currentPath = System.IO.Path.Combine(Server.MapPath("~/dev/ecims/webapps/resources/images/uploadedfiles"));
                //string finalPath = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email);

                //var directory = Directory.CreateDirectory(path);

                //STEP 1: Get file path and create the directory in the database
                string fileName = Path.GetFileName(file);

                //Sample save path-/images/profile/ocedache35@gmail.com/face.jpg
                company.CertificateOfIncorporation = fileName;

                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Index", "Home");
        }
        public async Task<ActionResult> resetCompanyAwardLetterDirectory()
        {
            //User list in db
            var organizations = _context.EUCOrganisations.ToList();
            //Loop through every user in the database
            foreach (var company in organizations)
            {
                if (company.AwardContract == null)
                {
                    continue;
                }
                //Get passport column
                var file = company.AwardContract;

                if (file == null)
                {
                    continue;
                }

                //string currentPath = System.IO.Path.Combine(Server.MapPath("~/dev/ecims/webapps/resources/images/uploadedfiles"));
                //string finalPath = System.IO.Path.Combine(Server.MapPath("~/images/profile"), user.Email);

                //var directory = Directory.CreateDirectory(path);

                //STEP 1: Get file path and create the directory in the database
                string fileName = Path.GetFileName(file);

                //Sample save path-/images/profile/ocedache35@gmail.com/face.jpg
                company.AwardContract = fileName;

                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Index", "Home");
        }
        //Reset supporting documents block
        //Set supporting documents from the old dtorage structure to the new one
        public ActionResult resetEUCSupportingDocuments()
        {
            //Get every EUC attachment row in the DB
            var eucSupportingDocumentsRecords = _context.EUCApplicationAttachments.ToList();

            //Iterate through them
            foreach (var supportingDocumentRow in eucSupportingDocumentsRecords)
            {
                //Get application numer of the application
                var applicationNumber = supportingDocumentRow.applicationNo;

                if (applicationNumber != null)
                {
                    //Find the application
                    var application = _context.EndUserCertificate
                                    .Where(euc => euc.applicationNo == applicationNumber)
                                    .FirstOrDefault();

                    #region CBN ATTACHMENTS
                    try
                    {
                        if (supportingDocumentRow.cbnApplication == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.cbnApplication);
                            //save the new path in the database
                            supportingDocumentRow.cbnApplication = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion
                    #region MIN AGRIC DOCUMENTS
                    try
                    {
                        if (supportingDocumentRow.minOfAgricApplication == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.minOfAgricApplication);
                            //save the new path in the database
                            supportingDocumentRow.minOfAgricApplication = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion                            
                    #region NCAA
                    try
                    {
                        if (supportingDocumentRow.NCCALetter == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.NCCALetter);
                            //save the new path in the database
                            supportingDocumentRow.NCCALetter = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    #endregion                            
                    #region ARPIdentification
                    try
                    {
                        if (supportingDocumentRow.ARPIdentification == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.ARPIdentification);
                            //save the new path in the database
                            supportingDocumentRow.ARPIdentification = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion    
                    #region MEMO OF ASSOCIATION
                    try
                    {
                        if (supportingDocumentRow.memoOfAssociation == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.memoOfAssociation);
                            //save the new path in the database
                            supportingDocumentRow.memoOfAssociation = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion       
                    #region BILL OF LADEN
                    try
                    {
                        if (supportingDocumentRow.bilOfLaden == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.bilOfLaden);
                            //save the new path in the database
                            supportingDocumentRow.bilOfLaden = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region ANALYSIS CERTIFICATE
                    try
                    {
                        if (supportingDocumentRow.analysisCert == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.analysisCert);
                            //save the new path in the database
                            supportingDocumentRow.analysisCert = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion    
                    #region INCORPORATION CERTIFICATE
                    try
                    {
                        if (supportingDocumentRow.incorporationCert == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.incorporationCert);
                            //save the new path in the database
                            supportingDocumentRow.incorporationCert = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region AC GAD 001
                    try
                    {
                        if (supportingDocumentRow.ac_gad001 == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.ac_gad001);
                            //save the new path in the database
                            supportingDocumentRow.ac_gad001 = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region TAX CLEARANCE
                    try
                    {
                        if (supportingDocumentRow.taxClerance == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.taxClerance);
                            //save the new path in the database
                            supportingDocumentRow.taxClerance = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion   
                    #region PREVIOUS NETWORK DISTRIBUTION
                    try
                    {
                        if (supportingDocumentRow.previousNetworkDistribution == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.previousNetworkDistribution);
                            //save the new path in the database
                            supportingDocumentRow.previousNetworkDistribution = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region PRIVATE SECURITY REG
                    try
                    {
                        if (supportingDocumentRow.privateSecurityReg == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.privateSecurityReg);
                            //save the new path in the database
                            supportingDocumentRow.privateSecurityReg = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion 
                    #region CIT LICENSE
                    try
                    {
                        if (supportingDocumentRow.citLicense == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.citLicense);
                            //save the new path in the database
                            supportingDocumentRow.citLicense = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region FIRS CERTIFICATE
                    try
                    {
                        if (supportingDocumentRow.firsCertificate == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.firsCertificate);
                            //save the new path in the database
                            supportingDocumentRow.firsCertificate = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion   
                    #region FORM C07
                    try
                    {
                        if (supportingDocumentRow.formCO7 == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.formCO7);
                            //save the new path in the database
                            supportingDocumentRow.formCO7 = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion    
                    #region CBN FORM M
                    try
                    {
                        if (supportingDocumentRow.cbnFormM == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.cbnFormM);
                            //save the new path in the database
                            supportingDocumentRow.cbnFormM = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion   
                    #region IDENTITY CARD
                    try
                    {
                        if (supportingDocumentRow.identityCard == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.identityCard);
                            //save the new path in the database
                            supportingDocumentRow.identityCard = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion
                    #region LAST 1 YEAR EUCS
                    try
                    {
                        if (supportingDocumentRow.last1YearEUCS == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.last1YearEUCS);
                            //save the new path in the database
                            supportingDocumentRow.last1YearEUCS = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion   
                    #region ACQUIRER INTRO LETTER
                    try
                    {
                        if (supportingDocumentRow.acquirerIntroLetter == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.acquirerIntroLetter);
                            //save the new path in the database
                            supportingDocumentRow.acquirerIntroLetter = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region MAGAZINE LICENSES
                    try
                    {
                        if (supportingDocumentRow.magazineLicenses == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.magazineLicenses);
                            //save the new path in the database
                            supportingDocumentRow.magazineLicenses = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion
                    #region MANUFACTURERS SAFETY
                    try
                    {
                        if (supportingDocumentRow.manufacturersSafety == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.manufacturersSafety);
                            //save the new path in the database
                            supportingDocumentRow.manufacturersSafety = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region NAFDAC ATTACHMENT
                    try
                    {
                        if (supportingDocumentRow.NAFDACAttachment == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.NAFDACAttachment);
                            //save the new path in the database
                            supportingDocumentRow.NAFDACAttachment = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region NPF FIREWORKS PERMIT
                    try
                    {
                        if (supportingDocumentRow.NPFFIreworksPermit == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.NPFFIreworksPermit);
                            //save the new path in the database
                            supportingDocumentRow.NPFFIreworksPermit = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion 
                    #region PACKING LIST
                    try
                    {
                        if (supportingDocumentRow.packingList == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.packingList);
                            //save the new path in the database
                            supportingDocumentRow.packingList = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion 
                    #region IMPORT PERMIT FOR EXPLOSIVE
                    try
                    {
                        if (supportingDocumentRow.importPermitforExplosive == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.importPermitforExplosive);
                            //save the new path in the database
                            supportingDocumentRow.importPermitforExplosive = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion
                    #region PREV STORAGE ATTACHMENT
                    try
                    {
                        if (supportingDocumentRow.prevStorageAttachment == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.prevStorageAttachment);
                            //save the new path in the database
                            supportingDocumentRow.prevStorageAttachment = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion    
                    #region PROFORMA INVOICE
                    try
                    {
                        if (supportingDocumentRow.proformaInvoice == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.proformaInvoice);
                            //save the new path in the database
                            supportingDocumentRow.proformaInvoice = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion 
                    #region QUALIFIED TECH OFFICER DETAILS
                    try
                    {
                        if (supportingDocumentRow.qualifiedTechOfficerDetails == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.qualifiedTechOfficerDetails);
                            //save the new path in the database
                            supportingDocumentRow.qualifiedTechOfficerDetails = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region FFD REG
                    try
                    {
                        if (supportingDocumentRow.FFDReg == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.FFDReg);
                            //save the new path in the database
                            supportingDocumentRow.FFDReg = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion 
                    #region SPECIFICATION OF GOODS
                    try
                    {
                        if (supportingDocumentRow.specificationOfGoods == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.specificationOfGoods);
                            //save the new path in the database
                            supportingDocumentRow.specificationOfGoods = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion 
                    #region STORAGE FACILITY ATTACHMENT
                    try
                    {
                        if (supportingDocumentRow.storageFacilityAttachment == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.storageFacilityAttachment);
                            //save the new path in the database
                            supportingDocumentRow.storageFacilityAttachment = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion     
                    #region TRANSIT DETAILS
                    try
                    {
                        if (supportingDocumentRow.transitDetails == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.transitDetails);
                            //save the new path in the database
                            supportingDocumentRow.transitDetails = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion    
                    #region TRANSPORTER PARTICULARS
                    try
                    {
                        if (supportingDocumentRow.transporterParticulars == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.transporterParticulars);
                            //save the new path in the database
                            supportingDocumentRow.transporterParticulars = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion   
                    #region VAT CERTIFICATE
                    try
                    {
                        if (supportingDocumentRow.vatCert == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.vatCert);
                            //save the new path in the database
                            supportingDocumentRow.vatCert = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion 
                    #region VENDOR ID DETAILS
                    try
                    {
                        if (supportingDocumentRow.vendorIDDetails == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.vendorIDDetails);
                            //save the new path in the database
                            supportingDocumentRow.vendorIDDetails = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion    
                    #region OPERATION YEARS
                    try
                    {
                        if (supportingDocumentRow.operationYears == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.operationYears);
                            //save the new path in the database
                            supportingDocumentRow.operationYears = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion    
                    #region COMPANY DIRECTORS PROFILE
                    try
                    {
                        if (supportingDocumentRow.companyDirectorsProfile == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.companyDirectorsProfile);
                            //save the new path in the database
                            supportingDocumentRow.companyDirectorsProfile = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion  
                    #region STOCK BALANCE
                    try
                    {
                        if (supportingDocumentRow.StockBalance == null)
                        {
                            //Do nothing
                        }
                        else
                        {
                            //File to be moved and saved
                            string fileName = Path.GetFileName(supportingDocumentRow.StockBalance);
                            //save the new path in the database
                            supportingDocumentRow.StockBalance = fileName;

                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);
                    }
                    #endregion                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
                }
            }
            return RedirectToAction("Index", "Home");
        }
        //USER NOTIFICATION BLOCK
        public async Task<ActionResult> NotifyUsersOfUpgrade()
        {
            //Get database context
            ApplicationDbContext _context = new ApplicationDbContext();
            //Get the list of users to email & SMS
            var users = _context.Users.ToList();
            foreach (var user in users)
            {
                if (user.Email != null )
                {
                IdentityMessage EmailMessage = new IdentityMessage();

                EmailMessage.Body = "Dear " + user.FirstName + " " + user.LastName + ", "
                                    + "<br/>"
                                    + "The End User Certificate(EUC) Web Portal will be upgraded on Thursday, 12 April, 2018."
                                    + "<br/>"
                                    + "You will be required to follow these 4 steps before you can log in:"
                                    + "<br/>"
                                    + " 1. Click the login link on the homepage at www.euc.nsa.gov.ng"
                                    + "<br/>"
                                    + " 2. Enter your email address(the address this mail has been sent to)."
                                    + "<br/>"
                                    + " 3. You will receive a confirmation email. Click the link sent in that mail."
                                    + "<br/>"
                                    + " 4. Enter your password and click the submit button. Passwords must contain a capital letter, "
                                    + "a special character, a number and at least 8 characters in total."
                                    + "<br/>"
                                    + " Thank you for your patience."
                                    + "<br/>"
                                    + "Yours Faithfully,"
                                    + "<br/>"
                                    + "Office of the National Security Adviser.";
                EmailMessage.Destination = user.Email;
                EmailMessage.Subject = "End User Certificate Web Portal Upgrade Notice.";

                EmailService emailService = new EmailService();
                await emailService.SendAsync(EmailMessage);
                }
                if (user.PhoneNumber != null)
                {
                    //Send user verification SMS
                    IdentityMessage SmsMessage = new IdentityMessage();
                    //send accompanying SMS reminding user to verify their email
                    string smsBody = "Dear " + user.FirstName + " " + user.LastName + ", \n"
                                        + "The EUC Web Portal will be upgraded on 12 April, 2018.\n"
                                        + " To login, you will be required to:\n"
                                        + " 1. Click the login link at www.euc.nsa.gov.ng. \n"
                                        + " 2. Enter your email address(the address this mail has been sent to). \n"
                                        + " 3. Click the link sent to your email. \n"
                                        + " 4. Enter your password and click the submit button on the page you are redirected to."
                                        + " Passwords must contain a capital letter, a special character, a number and at least 8 characters in total. "
                                        + "\n"
                                        + " Thank you for your patience."
                                        + "\n"
                                        + "Yours Faithfully,"
                                        + "\n"
                                        + "Office of the National Security Adviser.";

                    SmsMessage.Body = smsBody;
                    SmsMessage.Destination = user.PhoneNumber;
                    SmsMessage.Subject = "End User Certificate Web Portal Upgrade Notice.";

                    SmsService smsSerice = new SmsService();
                    await smsSerice.SendAsync(SmsMessage);
                }
            }
            //Return server result of 'OK' if  messages are sent successfully
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        //Reset company logo
        //Change copany logo directory in DB
        public async Task<ActionResult> changeCompanyLogoDirectoryInDatabase()
        {
            //Company logo
            var companyLogos = _context.Company.Where(company => company.Logo != null).ToList();
            
            //Loop through every user in the database
            foreach (var company in companyLogos)
            {
                //Get company logo column
                var logo = company.Logo;

                //Get user who owns the company
                var owner = _context.Users.Where(ownerOfCompany => ownerOfCompany.Id == company.OwnerId).FirstOrDefault();
                if (owner == null)
                {
                    continue;
                }

                if (logo.Contains("/images/profile/"))
                {
                    continue;
                }
                else if (owner.Email == null)
                {
                    continue;
                }
                else
                {
                    var newLogo = "/images/profile/" + owner.Email + "/Company Documents/" + logo;

                    company.Logo = newLogo;

                    int result = _context.SaveChanges();
                }
            }
            return RedirectToAction("Index", "Home");
        }
        //SMS rejected users
        public async Task<ActionResult> SMSRejectedUsers()
        {
            var usersToReject = _context.Users.Where(u => u.ApprovalStatus == ApprovedStatus.Rejected && u.RequestedDate > DateTime.Parse("01/04/2018"));

            if (usersToReject != null)
            {
                foreach (var user in usersToReject)
                {

                }
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}