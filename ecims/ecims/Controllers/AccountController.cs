﻿using ecims.Controllers.ONSASupportNotificationController;
using ecims.Models;
using ecims.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ecims.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationDbContext _context;
        private ApplicationRoleManager _roleManager;
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        AuditTrailGenerator.AuditTrail _auditTrail = new AuditTrailGenerator.AuditTrail();

        public AccountController()
        {
            _context = new ApplicationDbContext();
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            RoleManager = roleManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            var userId = User.Identity.GetUserId();
            if (userId != null)
            {
                var user = _context.Users.Where(u => u.Id == userId).FirstOrDefault();
                if (user.PortalUserCategoryId == 32)
                {
                    //return RedirectToAction("Dashboard", "VerifyEUCPortal");//Close any session in progress
                    //Session.Abandon();
                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                    return RedirectToAction("Login", "Account");
                }
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            //Check if the user has a password before moving forward
            PasswordCheckController PasswordCheck = new PasswordCheckController();
            var passwordExists = PasswordCheck.CheckIfPasswordExists(model.Email);

            //If user password exists set a message in the view bag and redirect the user to the password reset page
            if (passwordExists == false)
            {
                TempData["PasswordCheckResult"] = "In order to serve all end-users better, and to improve data security on the EUC "
                                                + "platform, all users are required to reset their passwords. "
                                                + "Please use the form below to enter your e-mail and reset your password.";

                return RedirectToAction("ForgotPassword", "Account");
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            //var signedIn = Request.IsAuthenticated;

            //Check if user is blacklisted
            //var blacklistCheck = UserManager.FindByEmail(model.Email);
            //if (blacklistCheck.IsBlackListed == true)
            //{
            //    TempData["Blacklisted"] = "This account has been blacklisted from applying for EUC certificates. Kindly contact support for more information";
            //    return RedirectToAction("Login", "Account");
            //}

            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);

            var user = SignInManager.UserManager.Find(model.Email, model.Password);
            if (user != null)
            {
                if (!await UserManager.IsEmailConfirmedAsync(user.Id))
                {
                    //Add message to view
                    ModelState.AddModelError("", "Check your email and confirm your account registration for the vetting process to begin.");

                    //Send user verification email
                    var confirmationLink = GenerateConfirmationTokenAsync(user.Id);

                    IdentityMessage EmailMessage = new IdentityMessage();

                    EmailMessage.Body = "Dear " + user.FirstName + " " + user.LastName + ". "
                                        + "<br/>"
                                        + "You cannot log in because you have yet to verify your registered email on the ONSA"
                                        + " End-User Certificate"
                                        + " portal."
                                        + "<br/>"
                                        + " Kindly do so by clicking the link below to begin your vetting process"
                                        + "<br/>"
                                        + confirmationLink.Result;
                    ;
                    EmailMessage.Destination = user.Email;
                    EmailMessage.Subject = "Confirm your account-Resend";

                    EmailService emailService = new EmailService();
                    await emailService.SendAsync(EmailMessage);

                    //Send user verification SMS
                    //var smsConfirmationCode = GenerateConfirmationTokenForSMSAsync(user.Id);

                    //IdentityMessage SmsMessage = new IdentityMessage();
                    ////send accompanying SMS reminding user to verify their email
                    //string smsBody = "You are unable to login because your account has not been verified on the EUC web portal. Please confirm your email by clicking the link sent to your inbox.";

                    //SmsMessage.Body = smsBody;
                    //SmsMessage.Destination = user.PhoneNumber;
                    //SmsMessage.Subject = "Confirm your account-Resend";

                    //SmsService smsSerice = new SmsService();
                    ////SendSmsUsingXML();
                    //await smsSerice.SendAsync(SmsMessage);
                    ////smsSerice.SendSmsConfirmationSynchronously(SmsMessage);

                    ////SendSmsConfirmation(user.PhoneNumber, smsMessage);

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Applicant: " + user.ApplicationNumber + ". Name: " + user.LastName + " " + user.FirstName + " attempted to log in without confirming email.",
                       User.Identity.GetUserId(),
                       "Attempted to log in.",
                       Request.UserHostAddress);


                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                    return View(model);
                }
            }
            else if (user == null)
            {
                ModelState.AddModelError("", "Make sure your Email address and Password are correctly typed, otherwise create an account to sign in");

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("User account does not exist.",
                   User.Identity.GetUserId(),
                   "No account.",
                   Request.UserHostAddress);


                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                return View(model);
            }
            else
            {
                ModelState.AddModelError("", "Invalid username or password");

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Applicant: " + user.ApplicationNumber + ". Name: " + user.LastName + " " + user.FirstName + " attempted to log in with wrong credentials.",
                   User.Identity.GetUserId(),
                   "Wrong credentials.",
                   Request.UserHostAddress);

                return View(model);
            }


            var approvalStatus = user.ApprovalStatus;

            if (approvalStatus == ApprovedStatus.Pending)
            {
                //return View(model);
                ModelState.AddModelError("", "Your account registration vetting process is ongoing. A notification will be sent to you via SMS and email once your account has been approved or disapproved.");

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Applicant: " + user.ApplicationNumber + ". Name: " + user.LastName + " " + user.FirstName + " attempted to log in with an un-approved account.",
                   User.Identity.GetUserId(),
                   "Acount has not been approved.",
                   Request.UserHostAddress);

                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                return View(model);
            }
            if (approvalStatus == ApprovedStatus.Rejected)
            {
                //return View(model);
                ModelState.AddModelError("", "This account has been rejected and permanently banned from accessing the EUC portal.");

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Applicant: " + user.ApplicationNumber + ". Name: " + user.LastName + " " + user.FirstName + " attempted to log in with a rejected account.",
                   User.Identity.GetUserId(),
                   "Rejected account attempting to access the portal.",
                   Request.UserHostAddress);

                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                return View(model);
            }

            if (user.IsBlackListed == true)
            {
                TempData["Blacklisted"] = "This account has been blacklisted from applying for EUC certificates. Kindly call the support line +234-809-038-5718 for resolution regarding your blacklisted EUC account.";
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                return RedirectToAction("Login", "Account");
            }

            if (user.PortalUserCategoryId == 32)
            {
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                return RedirectToAction("Login", "Account");
            }

            switch (result)
            {
                case SignInStatus.Success:

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Applicant: " + user.ApplicationNumber + ". Name: " + user.LastName + " " + user.FirstName + " signed in successfully.",
                       User.Identity.GetUserId(),
                       "redirected to dashboard.",
                       Request.UserHostAddress);

                    return RedirectToAction("MainDashboard", "Admin");
                case SignInStatus.LockedOut:

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Applicant: " + user.ApplicationNumber + ". Name: " + user.LastName + " " + user.FirstName + " locked user out.",
                       User.Identity.GetUserId(),
                       "user locked out.",
                       Request.UserHostAddress);

                    return View("Lockout");
                case SignInStatus.RequiresVerification:

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Applicant: " + user.ApplicationNumber + ". Name: " + user.LastName + " " + user.FirstName + " sent user verification code because of un-verified account.",
                       User.Identity.GetUserId(),
                       "sent user verification code.",
                       Request.UserHostAddress);

                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("User attempting to log in has not been verified. Provider: " + provider + ". Return URL = " + returnUrl,
                   User.Identity.GetUserId(),
                   "unverified account login attempt.",
                   Request.UserHostAddress);
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register(string returnUrl)
        {
            if (Request.IsAuthenticated)
            {
                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("User is logged in and was redirected to the dashboard instead of register page. Username: " + User.Identity.GetUserName(),
                   User.Identity.GetUserId(),
                   "redirected to dashboard page.",
                   Request.UserHostAddress);

                return RedirectToAction("MainDashboard", "Admin");
            }

            var states = _context.States.ToList();
            var country = _context.CountryList.ToList();
            var meansOfIdentification = _context.Identification.ToList();


            var viewModel = new RegisterViewModel
            {
                Categories = _context.Categories.ToList(),
                MeansOfIdentifications = meansOfIdentification,
                States = states,
                CompanyStates = states,
                isRedirect = true,
                Country = country
            };

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Anonymous user accessed register page.",
               User.Identity.GetUserId(),
               "redirected to registration page.",
               Request.UserHostAddress);

            return View(viewModel);
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            ImageBuilder builder = new ImageBuilder();

            //var category = _context.Categories.Single(c => c.Id == model.Category);
            //var identification = _context.Identification.Single(i => i.Id == model.MeansOfIdentification);
            var allowedExtensions = new[] { ".jpeg", ".jpg", ".JPEG", ".png", ".PNG" };
            //Sort out date validation issue
            //if (!ModelState.IsValid
            //    && !ModelState.IsValidField("LicenseDateOfExpiry")
            //    && model.LicenseDateOfExpiry != null)
            ////|| !ModelState.IsValidField("LicenseDateOfIssuance")
            ////|| model.LicenseDateOfIssuance != null)
            //{
            //    foreach (var key in ModelState.Keys)
            //    {
            //        ModelState[key].Errors.Clear();
            //    }
            //}
            //model.LicenseDateOfIssuance = DateTime.Now;
            //model.LicenseDateOfExpiry = DateTime.Now;
            if (ModelState.IsValid && model.passportUpload != null && model.IdUpload != null)
            {
                //Check if the company being registered by corporate users already exists in the DB
                if (model.CategoryList == CategoryList.Corporate)
                {
                    if (model.CompanyName != null)
                    {
                        var companyName = model.CompanyName;

                        //Check if the company exists
                        var existingCompany = _context.Company.Where(company => company.Name == model.CompanyName).FirstOrDefault();
                        var existingCAC = _context.Company.Where(company => company.CACNo == model.CACNo).FirstOrDefault();

                        if (existingCompany != null || existingCAC != null)
                        {
                            TempData["CompanyExists"] = "Your request to create an account was denied, this is because the company attached to your registration already exists in our records." +
                                              "Note that creation of multiple accounts is strongly advised against " +
                                              "and users found creating multiple accounts will be permanently banned from accessing the EUC portal.";
                            return RedirectToAction("Login");
                        }
                    }
                }
                #region upload passport file
                var extension = Path.GetExtension(model.passportUpload.FileName);

                var fileNameWithExtension = Path.GetFileName(model.passportUpload.FileName);
                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(model.passportUpload.FileName);

                string image = "/images/profile/" + model.Email + "/" + fileNameWithExtension;
                string path = System.IO.Path.Combine(Server.MapPath("~/images/profile"), model.Email);

                string imageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + model.Email), fileNameWithExtension);

                var directory = Directory.CreateDirectory(path);

                model.passportUpload.SaveAs(imageUrl);
                #endregion

                #region
                var idExtension = Path.GetExtension(model.IdUpload.FileName);

                var idFileNameWithExtension = Path.GetFileName(model.IdUpload.FileName);
                var idFileNameWithoutExtension = Path.GetFileNameWithoutExtension(model.IdUpload.FileName);

                string idImage = "/images/profile/ID/" + model.Email + "/" + idFileNameWithExtension;
                string idPath = System.IO.Path.Combine(Server.MapPath("~/images/profile/ID"), model.Email);

                string idImageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/ID/" + model.Email), idFileNameWithExtension);

                var idDirectory = Directory.CreateDirectory(idPath);

                model.IdUpload.SaveAs(idImageUrl);

                int catId;
                int meansOfId;

                #endregion

                #region Set user category(Individual or Corporate)
                //Set image extension
                //builder.buildImage(directory, extension);
                if (model.CategoryList == CategoryList.Individual)
                    catId = 1;
                else
                    catId = 2;

                if (model.Identification == Identification.DriversLicense)
                    meansOfId = 1;
                else if (model.Identification == Identification.InternationalPassport)
                    meansOfId = 2;
                else if (model.Identification == Identification.NationalID)
                    meansOfId = 3;
                else if (model.Identification == Identification.PermanentVotersCard)
                    meansOfId = 4;
                else
                    meansOfId = 1;
                #endregion

                #region Create User Applicant Number
                string applicantNumber = ApplicationNumberGenerator.
                    ApplicationNumberGenerator.
                    GenerateApplicationNumber();
                #endregion

                #region Instantiate User Object
                var user = new ApplicationUser
                {
                    CategoryId = catId,
                    CategoryList = model.CategoryList,
                    Identification = model.Identification,
                    UserName = model.Email,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    OtherName = model.OtherName,
                    LastName = model.LastName,
                    Gender = model.Gender,
                    PassportUrl = image,
                    MaritalStatus = model.MaritalStatus,
                    AddressLine1 = model.AddressLine1,
                    AddressLine2 = model.AddressLine2,
                    PhoneNumber = "+234" + model.PhoneNumber,
                    StateOfOriginId = model.State,
                    State = model.ForeignStateOfOrigin,
                    City = model.City,
                    TIN = model.TIN,
                    MeansOfIdentificationId = meansOfId,
                    DriversLicenseNumber = model.LicenseNumber,
                    PlaceOfIssue = model.PlaceOfIssue,
                    LicenseDateOfIssuance = model.LicenseDateOfIssuance,
                    LicenseDateOfExpiry = model.LicenseDateOfExpiry,
                    NextOfKinFullname = model.NextOfKinFullname,
                    NextOfKinAddress = model.NextOfKinAddress,
                    NextOfKinRelationship = model.NextOfKinRelationship,
                    NextOfKinPhoneNumber = model.NextOfKinPhoneNumber,
                    DOB = model.DOB,
                    Approved = false,
                    UserType = UserType.ClientUser,
                    ApprovalStatus = ApprovedStatus.Pending,
                    IdUrl = idImage,
                    PortalUserCategoryId = 28,//Client User
                    ApplicationNumber = applicantNumber,
                    RequestedDate = DateTime.Now,
                    IsBlackListed = false,
                    Country = model.countryId
                    //blacklistedDate = DateTime.MinValue
                };
                #endregion

                var result = await UserManager.CreateAsync(user, model.Password);

                //Document the entry in the audit trail
                var auditTrail = new AuditTrail
                {
                    DateOfActivity = DateTime.Now,
                    ActionCarriedOut = "Registered new user. Name: " + model.FirstName + " " + model.LastName + " Applicant Number: " + applicantNumber + ". E-mail: " + model.Email,
                    CarriedOutBy = "Anonymous User",
                    ActionDescription = "Register new user",
                    UserIPAddress = Request.UserHostAddress
                };
                _context.AuditTrail.Add(auditTrail);
                _context.SaveChanges();

                if (result.Succeeded)
                {
                    #region Register company for corporate accounts
                    //Check company category
                    if (catId == 2)
                    {
                        //Generate company media and logo for upload
                        var logoFileExtension = Path.GetExtension(model.CompanyLogo.FileName);

                        var logoFileNameWithExtension = Path.GetFileName(model.CompanyLogo.FileName);
                        var logoFileNameWithoutExtension = Path.GetFileNameWithoutExtension(model.CompanyLogo.FileName);

                        string logoImage = "/images/profile/" + model.Email + "/Company Documents/" + logoFileNameWithExtension;
                        string logoPath = System.IO.Path.Combine(Server.MapPath("~/images/profile"), model.Email + "/Company Documents/");

                        string logoImageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + model.Email + "/Company Documents/"), logoFileNameWithExtension);

                        var logoDirectory = Directory.CreateDirectory(logoPath);

                        model.CompanyLogo.SaveAs(logoImageUrl);

                        //CAC documents
                        var cacFileExtension = Path.GetExtension(model.CertificateOfIncorporation.FileName);

                        var cacFileNameWithExtension = Path.GetFileName(model.CertificateOfIncorporation.FileName);
                        var cacFileNameWithoutExtension = Path.GetFileNameWithoutExtension(model.CertificateOfIncorporation.FileName);

                        string cacImage = "/images/profile/" + model.Email + "/Company Documents/" + cacFileNameWithExtension;
                        string cacPath = System.IO.Path.Combine(Server.MapPath("~/images/profile"), model.Email + "/Company Documents/");

                        string cacImageUrl = System.IO.Path.Combine(Server.MapPath("~/images/profile/" + model.Email + "/Company Documents/"), cacFileNameWithExtension);

                        var cacDirectory = Directory.CreateDirectory(cacPath);

                        model.CertificateOfIncorporation.SaveAs(cacImageUrl);

                        var userCompany = new Company
                        {
                            Name = model.CompanyName,
                            Address = model.CompanyAddress,
                            StateId = model.CompanyStateID,
                            PhoneNo = model.PhoneNumber,
                            Email = model.Email,
                            Website = model.CompanyWebsite,
                            CACNo = model.CACNo,
                            DateOfIncorporation = model.DateOfIncorporation,
                            OwnerId = user.Id,
                            Logo = logoImage,
                            CertificateOfIncorporation = cacImage
                        };
                        _context.Company.Add(userCompany);
                        _context.SaveChanges();

                        //Document the entry in the audit trail
                        _auditTrail.CreateAuditTrail("Registered new company. Company Name: " + model.CompanyName + " Applicant Number: " + applicantNumber + ". E-mail: " + model.Email,
                           User.Identity.GetUserId(),
                           "Company registered.",
                           Request.UserHostAddress);
                    }

                    #endregion

                    //Comment line below to disable auto sign in after registration
                    //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);                    

                    // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    string callbackUrl = await SendEmailConfirmationTokenAsync(user.Id, "Confirm your account");

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Sent email confirmation to user after successful registration. Applicant Number: " + applicantNumber + ". E-mail: " + model.Email,
                       User.Identity.GetUserId(),
                       "Email confirmation sent.",
                       Request.UserHostAddress);

                    ViewBag.Message = "Check your email and"
                                    + " confirm your account registration"
                                    + " for the vetting process to begin.";

                    //Send SMS Confirmation After Successful Registration
                    var smsConfirmationCode = GenerateConfirmationTokenForSMSAsync(user.Id);

                    IdentityMessage SmsMessage = new IdentityMessage();
                    //send accompanying SMS reminding user to verify their email
                    string smsBody = "This is to confirm that your account has been created on the EUC portal. An E-mail and SMS will be sent to you when your account is approved or disapproved. Click the verification link in your inbox to confirm your email.";

                    SmsMessage.Body = smsBody;
                    SmsMessage.Destination = user.PhoneNumber;
                    SmsMessage.Subject = "Confirm your account-Resend";

                    SmsService smsSerice = new SmsService();
                    await smsSerice.SendAsync(SmsMessage);

                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Send SMS confirmation to user after successful registration. Phone Number: " + model.PhoneNumber + " Applicant Number: " + applicantNumber + ". E-mail: " + model.Email,
                       User.Identity.GetUserId(),
                       "SMS confirmation sent.",
                       Request.UserHostAddress);

                    return View("Login");
                }
                AddErrors(result);
            }
            model.States = _context.States.ToList();
            model.MeansOfIdentifications = _context.Identification.ToList();
            model.CompanyStates = _context.States.ToList();
            model.Country = _context.CountryList.ToList();
            // If we got this far, something failed, redisplay form
            return View("Register", model);
        }

        //Controller to edit user details.
        //Was eventually scrapped because users are only allowed to change their passwords, and nothing else
        public ActionResult EditUser(string Id)
        { 
            var userId = User.Identity.GetUserId();
            var user = _context.Users.Single(u => u.Id == userId);
            var Country = _context.CountryList.ToList();
            var applicant = _context.Users.Where(u => u.Id == userId).FirstOrDefault();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User visited edit user view. Applicant Number: " + user.ApplicationNumber + ". E-mail: " + user.Email + " Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Visited edit user page.",
               Request.UserHostAddress);

            var viewModel = new UserViewModel
            {
                Category = user.CategoryId,
                Categories = _context.Categories.ToList(),
                Id = user.Id,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                OtherName = user.OtherName,
                Gender = user.Gender,
                MaritalStatus = user.MaritalStatus,
                AddressLine1 = user.AddressLine1,
                AddressLine2 = user.AddressLine2,
                PhoneNumber = user.PhoneNumber,
                //Country = user.Country,
                State = user.State,
                City = user.City,
                TIN = user.TIN,
                LicenseNumber = user.DriversLicenseNumber,
                PlaceOfIssue = user.PlaceOfIssue,
                LicenseDateOfIssuance = user.LicenseDateOfIssuance,
                LicenseDateOfExpiry = user.LicenseDateOfExpiry,
                NextOfKinFullname = user.NextOfKinFullname,
                NextOfKinAddress = user.NextOfKinAddress,
                NextOfKinRelationship = user.NextOfKinRelationship,
                NextOfKinPhoneNumber = user.NextOfKinPhoneNumber,
                DOB = user.DOB,
                PassportURL = user.PassportUrl,
                CountryList = Country,
                Applicant = applicant
            };

            return View("Profile", viewModel);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            //Mail every user in the support category if the user successfully confirms their email
            if (result.Succeeded)
            {
                //Find the user who confirmed his email
                var user = _context.Users.Find(userId);

                string roleName = "ONSA Support";

                //Mail support if the user confirms their email account
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));

                //Find the role to search for(ONSA Support)
                var role = await roleManager.FindByNameAsync(roleName);

                GroupUsersByRole.GroupUsersByRole groupUsers = new GroupUsersByRole.GroupUsersByRole();
                var users = groupUsers.GetUsersInRole(roleName);

                ONSASupportNotification onsaSupportContacter = new ONSASupportNotification();
                //Notify ONSA Support of new confirmed email
                await onsaSupportContacter.NotifyUsers(users, user);

                //Sign the user out
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            }

            //TO-DO

            //if (result.Succeeded)
            //{
            //    //Find the user who confirmed their account
            //    var user = _context.Users.Find(userId);
            //    //Get all support users in the system

            //    IdentityMessage EmailMessage = new IdentityMessage();

            //    EmailMessage.Body = "Dear " + user.FirstName + " " + user.LastName + ". "
            //                        + "<br/>"
            //                        + "You are unable to login because your account has not been verified on the ONSA"
            //                        + " End-User Certificate"
            //                        + " portal."
            //                        + "<br/>"
            //                        + " To verify your account, please access your registered email and click the link in the email which has just been sent."
            //                        + "<br/>"
            //                        + confirmationLink.Result;
            //    ;
            //    EmailMessage.Destination = user.Email;
            //    EmailMessage.Subject = "Confirm your account-Resend";


            //    EmailService emailService = new EmailService();
            //    await emailService.SendAsync(EmailMessage);

            //    //Send user verification SMS
            //    var smsConfirmationCode = GenerateConfirmationTokenForSMSAsync(user.Id);

            //    IdentityMessage SmsMessage = new IdentityMessage();
            //    //send accompanying SMS reminding user to verify their email
            //    string smsBody = "Dear " +user.FirstName + " " + user.LastName + ". "
            //                        + "You are unable to login because your account has not been verified on the ONSA"
            //                        + " End-User Certificate"
            //                        + " portal."
            //                        + " To verify your account, please access your registered email and click the link in the email which has just been sent. " + smsConfirmationCode.Result;

            //    SmsMessage.Body = smsBody;
            //    SmsMessage.Destination = user.PhoneNumber;
            //    SmsMessage.Subject = "Confirm your account-Resend";

            //    SmsService smsSerice = new SmsService();
            //    await smsSerice.SendAsync(SmsMessage);
            //}
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User visited the forgot password page.",
               User.Identity.GetUserId(),
               "Visited forgot password page.",
               Request.UserHostAddress);

            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                //For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                //Send an email with this link
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);

                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Send reset password link to user. Applicant Number: " + user.ApplicationNumber + ". E-mail: " + user.Email + " Name: " + user.FirstName + " " + user.LastName,
                   User.Identity.GetUserId(),
                   "Sent reset password mail.",
                   Request.UserHostAddress);

                await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\" style=\"background-color: #4CAF50; color: white; padding: 14px 25px; text-align: center; text-decoration: none; display: inline-block;\">Reset Password</a>");
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            var userId = User.Identity.GetUserId();
            //var user = _context.Users.Single(u => u.Id == userId);

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User visited forgot password confirmation page.",
               User.Identity.GetUserId(),
               "Sent reset password mail.",
               Request.UserHostAddress);

            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                //Document the entry in the audit trail
                _auditTrail.CreateAuditTrail("Failed password reset attempt. Email does not exist. E-mail: " + model.Email,
                   User.Identity.GetUserId(),
                   "Invalid password reset attempt.",
                   Request.UserHostAddress);

                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            if (user != null)
            {
                //Put a plus sign in any space field.
                var code = model.Code.Replace(" ", "+");
                var result = await UserManager.ResetPasswordAsync(user.Id, code, model.Password);
                //var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
                if (result.Succeeded)
                {
                    //Document the entry in the audit trail
                    _auditTrail.CreateAuditTrail("Successful password reset attempt. E-mail: " + model.Email,
                       User.Identity.GetUserId(),
                       "Password reset successfully.",
                       Request.UserHostAddress);

                    return RedirectToAction("ResetPasswordConfirmation", "Account");
                }
                AddErrors(result);
            }
                     
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            var userId = User.Identity.GetUserId();
            var user = _context.Users.Single(u => u.Id == userId);

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Sign user out. E-mail: " + user.Email,
               User.Identity.GetUserId(),
               "User signed out.",
               Request.UserHostAddress);

            Session.Contents.RemoveAll();
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            //return Redirect("Index", "Home");
            return Redirect("https://euc.nsa.gov.ng/");
        }

        //
        // POST: /Account/LogOff
        public ActionResult Logout()
        {
            Session.Contents.RemoveAll();
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            //return Redirect("Index", "Home");
            return Redirect("https://euc.nsa.gov.ng/");
        }

        //Send SMS using XML
        private async Task SendSmsUsingXML()
        {
            WebRequest req = null;
            WebResponse rsp = null;
            try
            {
                string fileName = Server.MapPath("~/App_Data/MultiTexterSMSFormat.xml");
                string uri = "https://www.multitexter.com/tools/xml/Sms.php";
                req = WebRequest.Create(uri);
                //req.Proxy = WebProxy.GetDefaultProxy(); // Enable if using proxy
                req.Method = "POST";        // Post method
                req.ContentType = "text/xml";     // content type
                req.Proxy = new WebProxy("202.79.27.50", 8080);
                // Wrap the request stream with a text-based writer
                StreamWriter writer = new StreamWriter(req.GetRequestStream());
                // Write the XML text into the stream
                writer.WriteLine(this.GetTextFromXMLFile(fileName));
                writer.Close();
                // Send the data to the webserver
                rsp = req.GetResponse(); //I am getting error over here
                StreamReader sr = new StreamReader(rsp.GetResponseStream());
                string result = sr.ReadToEnd();
                sr.Close();
                Response.Write(result);
            }
            catch (WebException webEx)
            {
                Response.Write(webEx.Message.ToString());
                Response.Write(webEx.StackTrace.ToString());
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message.ToString());
                Response.Write(ex.StackTrace.ToString());
            }
            finally
            {
                if (req != null) req.GetRequestStream().Close();
                if (rsp != null) rsp.GetResponseStream().Close();
            }
        }
        private string GetTextFromXMLFile(string file)
        {
            StreamReader reader = new StreamReader(file);
            string ret = reader.ReadToEnd();
            reader.Close();
            return ret;
        }
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }
        [Authorize(Roles = "ONSA Admin")]
        public ActionResult EditUserRec(string Id)
        {
            var userId = User.Identity.GetUserId();
            var user = _context.Users.Single(u => u.Id == userId);

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("User visited edit user view. Applicant Number: " + user.ApplicationNumber + ". E-mail: " + user.Email + " Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Visited edit user page.",
               Request.UserHostAddress);

            var viewModel = new UserViewModel
            {
                Category = user.CategoryId,
                Categories = _context.Categories.ToList(),
                Id = user.Id,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                OtherName = user.OtherName,
                Gender = user.Gender,
                MaritalStatus = user.MaritalStatus,
                AddressLine1 = user.AddressLine1,
                AddressLine2 = user.AddressLine2,
                PhoneNumber = user.PhoneNumber,
                //Country = user.Country,
                State = user.State,
                City = user.City,
                TIN = user.TIN,
                LicenseNumber = user.DriversLicenseNumber,
                PlaceOfIssue = user.PlaceOfIssue,
                LicenseDateOfIssuance = user.LicenseDateOfIssuance,
                LicenseDateOfExpiry = user.LicenseDateOfExpiry,
                NextOfKinFullname = user.NextOfKinFullname,
                NextOfKinAddress = user.NextOfKinAddress,
                NextOfKinRelationship = user.NextOfKinRelationship,
                NextOfKinPhoneNumber = user.NextOfKinPhoneNumber,
                DOB = user.DOB,
                PassportURL = user.PassportUrl
            };
            return View(viewModel);
        }
        
        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }
        private async Task<string> SendEmailConfirmationTokenAsync(string userID, string subject)
        {
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(userID);
            var callbackUrl = Url.Action("ConfirmEmail", "Account",
               new { userId = userID, code = code }, protocol: Request.Url.Scheme);
            await UserManager.SendEmailAsync(userID, subject, "This is to confirm that your account has been created on the ONSA"
                                        + " End-User Certificate"
                                        + " portal."
                                        + " <br/>"
                                        + " An E-mail and SMS will be sent notifying you once your account has been approved or disapproved."
                                        + " <br/>"
                                        + " Please click this link to confirm your E-mail: <a href=\"" + callbackUrl + "\" style=\"background-color: #4CAF50; color: white; padding: 14px 25px; text-align: center; text-decoration: none; display: inline-block;\">Confirm Email</a>"
                                        + " <br/>"
                                        + " NOTE: Only accounts with confirmed emails will be considered for vetting.");

            return callbackUrl;
        }
        //Generate account confirmation link/token for SMS
        private async Task<string> GenerateConfirmationTokenForSMSAsync(string userID)
        {
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(userID);
            var callbackUrl = Url.Action("ConfirmEmail", "Account",
               new { userId = userID, code = code }, protocol: Request.Url.Scheme);

            return callbackUrl;
        }
        //Generate account confirmation link/token for EMAIL
        private async Task<string> GenerateConfirmationTokenAsync(string userID)
        {
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(userID);
            var callbackUrl = Url.Action("ConfirmEmail", "Account",
               new { userId = userID, code = code }, protocol: Request.Url.Scheme);
            string clickCallbackUrl = "Please confirm your email by clicking <a href=\""
                                                    + callbackUrl + "\" style=\"background-color: #4CAF50; color: white; padding: 14px 25px; text-align: center; text-decoration: none; display: inline-block;\">Confirm Email</a>";

            return clickCallbackUrl;
        }
        private async Task<string> SendSmsConfirmationTokenAsync(string userID, string subject)
        {
            string code = await UserManager.GenerateEmailConfirmationTokenAsync(userID);
            var callbackUrl = Url.Action("ConfirmEmail", "Account",
               new { userId = userID, code = code }, protocol: Request.Url.Scheme);
            await UserManager.SendSmsAsync(userID, "Please confirm your email by clicking <a href=\""
                                                    + callbackUrl + "\" style=\"background-color: #4CAF50; color: white; padding: 14px 25px; text-align: center; text-decoration: none; display: inline-block;\">Confirm Email</a>");

            return callbackUrl;
        }

        private void SendSmsConfirmation(string recipient, string message)
        {
            //SMS Sending
            string sender = "ONSA ECIMS";

            string url = "https://www.MultiTexter.com/tools/geturl/Sms.php?username=ocedache@gmail.com&password=Omanchala1944&sender="
                            + sender + "&message=" + message +
                            "&flash=0&sendtime=2009-10- 18%2006:30&listname=friends&recipients="
                            + recipient;

            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(string.Format(url));
            webReq.Method = "GET";
            HttpWebResponse webResponse = (HttpWebResponse)webReq.GetResponse();
            Stream answer = webResponse.GetResponseStream();
            StreamReader _recievedAnswer = new StreamReader(answer);
        }
        #endregion
    }
}