﻿using ecims.Models;
using ecims.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ecims.Controllers
{
    public class SmsTestController : Controller
    {
        ApplicationDbContext _context;
        private ApplicationUserManager _userManager;

        public SmsTestController()
        {
            _context = new ApplicationDbContext();
        }
        // GET: SmsTest
        public ActionResult SmsTest(SmsTestViewModel model)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            return View(new SmsTestViewModel
            {
                user = user,
                role = roles
            });
        }
        // POST: SmsTest
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendSms(SmsTestViewModel model)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();

            //Send user verification SMS
            IdentityMessage SmsMessage = new IdentityMessage();
            //send accompanying SMS reminding user to verify their email
            string smsBody = model.Message;
            string phoneNumber = model.PhoneNumber;

            SmsMessage.Body = smsBody;
            SmsMessage.Destination = phoneNumber;
            SmsMessage.Subject = "Testing";
            
            //Send SMS
            SmsService smsSerice = new SmsService();
            await smsSerice.SendAsync(SmsMessage);



            return View(new SmsTestViewModel
            {
                user = user,
                role = roles
            });
        }
    }
}