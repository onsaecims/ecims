﻿using ecims.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ecims.Controllers.ONSASupportNotificationController
{
    public class ONSASupportNotification
    {
        //Send notification to every user in this role
        public void NotifyUsers(List<ApplicationUser> usersToNotify)
        {
            
        }

        public async Task NotifyUsers(List<ApplicationUser> users, ApplicationUser applicantToUpdate)
        {
            foreach (var user in users)
            {
                //Send user verification email            
                IdentityMessage EmailMessage = new IdentityMessage();

                EmailMessage.Body = "Dear " + user.FirstName + " " + user.LastName + ". "
                                    + "<br/>"
                                    + "A new user has just registered and confirmed their email on the ONSA End-User Certificate portal."
                                    + "<br/>"
                                    + "Applicant Details: First Name: " + applicantToUpdate.FirstName + " Last Name: " + applicantToUpdate.LastName + " Applicant Number: " + applicantToUpdate.ApplicationNumber + " Email: " + applicantToUpdate.Email
                                    + "<br/>"
                                    + " Kindly vet this account and approve or reject it as soon as possible."
                                    + "<br/>"
                                    + " Thank you."
                ;
                EmailMessage.Destination = user.Email;
                EmailMessage.Subject = "New Applicant Received: " + applicantToUpdate.ApplicationNumber ;


                EmailService emailService = new EmailService();
                await emailService.SendAsync(EmailMessage);

                //Send user verification SMS
                IdentityMessage SmsMessage = new IdentityMessage();
                //send accompanying SMS reminding user to verify their email
                string smsBody = "A new user has just confirmed their email."
                                    + "Applicant Number: " + applicantToUpdate.ApplicationNumber
                                    + " Kindly vet this account and approve or reject it as soon as possible.";

                SmsMessage.Body = smsBody;
                SmsMessage.Destination = user.PhoneNumber;
                SmsMessage.Subject = "Confirm your account-Resend";

                SmsService smsSerice = new SmsService();
                await smsSerice.SendAsync(SmsMessage);

            }
        }
        //Notification to be sent when a users certificate has been approved for printing
        public async Task<ActionResult> NotifyONSASupportOfApprovedCertificate(List<ApplicationUser> onsaSupportUsers, string applicationNumber, string tokenCode)
        {
            foreach (var user in onsaSupportUsers)
            {
                //Send user verification email            
                IdentityMessage EmailMessage = new IdentityMessage();

                EmailMessage.Body = "Dear " + user.FirstName + " " + user.LastName + ". "
                                    + "<br/>"
                                    + "An EUC application has just just been approved for collection."
                                    + "<br/>"
                                    + "Application Number: " + applicationNumber
                                    + "</br>"
                                    + "The collection reference is: <b>" + tokenCode + "</b>"
                                    + "<br/>"
                                    + " Thank you.";
                EmailMessage.Destination = user.Email;
                EmailMessage.Subject = "EUC Application Approved: " + applicationNumber;


                EmailService emailService = new EmailService();
                await emailService.SendAsync(EmailMessage);

                //Send user verification SMS
                IdentityMessage SmsMessage = new IdentityMessage();
                //send accompanying SMS reminding user to verify their email
                string smsBody = "An EUC application has just just been approved for collection."
                                    + " Application Number: " + applicationNumber
                                    + "\n"
                                    + " The collection reference is: " + tokenCode 
                                    + "\n"
                                    + " Thank you.";

                SmsMessage.Body = smsBody;
                SmsMessage.Destination = user.PhoneNumber;
                SmsMessage.Subject = "EUC Application Approved: " + applicationNumber;

                SmsService smsSerice = new SmsService();
                await smsSerice.SendAsync(SmsMessage);

                //Return server result of 'OK' if  messages are sent successfully
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            //Return server result of 'OK' if  messages are sent successfully
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        public async Task NotifyONSASupportOfNewEUCApplication(List<ApplicationUser> users, ApplicationUser eucApplicant, string applicationNumber)
        {
            foreach (var supportUser in users)
            {
                //Send user verification email            
                IdentityMessage EmailMessage = new IdentityMessage();

                EmailMessage.Body = "Dear " + supportUser.FirstName + " " + supportUser.LastName + ". "
                                    + "<br/>"
                                    + "A new EUC application has been made on the ONSA End-User Certificate portal."
                                    + "<br/>"
                                    + "Application Number: " + applicationNumber
                                    + "<br/>"
                                    + "Application Details: First Name: " + eucApplicant.FirstName + " Last Name: " + eucApplicant.LastName + " Applicant Number: " + eucApplicant.ApplicationNumber + " Email: " + eucApplicant.Email
                                    + "<br/>"
                                    + " Kindly vet this account and approve or reject it as soon as possible."
                                    + "<br/>"
                                    + " Thank you.";
                EmailMessage.Destination = supportUser.Email;
                EmailMessage.Subject = "New EUC Application Received: " + applicationNumber;


                EmailService emailService = new EmailService();
                await emailService.SendAsync(EmailMessage);

                //Send user verification SMS
                IdentityMessage SmsMessage = new IdentityMessage();
                //send accompanying SMS reminding user to verify their email
                string smsBody = "A new EUC application has been made."
                                    + "Application Number: " + applicationNumber
                                    + " Kindly vet this account and approve or reject it as soon as possible.";

                SmsMessage.Body = smsBody;
                SmsMessage.Destination = supportUser.PhoneNumber;
                SmsMessage.Subject = "New EUC Application Received: " + applicationNumber;

                SmsService smsSerice = new SmsService();
                await smsSerice.SendAsync(SmsMessage);

            }
        }
        //Notify ONSA support of newly endorsed applications by agencies
        public async Task NotifyONSASupportOfEndorsedEUCApplication(List<ApplicationUser> users, string applicationNumber, string agencyName)
        {
            foreach (var supportUser in users)
            {
                //Send user verification email            
                IdentityMessage EmailMessage = new IdentityMessage();

                EmailMessage.Body = "Dear " + supportUser.FirstName + " " + supportUser.LastName + ". "
                                    + "<br/>"
                                    + "An EUC application has just been endorsed by " + agencyName
                                    + "<br/>"
                                    + "Application Number: " + applicationNumber
                                    + "<br/>"
                                    + " Kindly review for rejection or forward to NSA for approval."
                                    + "<br/>"
                                    + " Thank you.";
                EmailMessage.Destination = supportUser.Email;
                EmailMessage.Subject = "New Endorsed EUC Application Received: " + applicationNumber;


                EmailService emailService = new EmailService();
                await emailService.SendAsync(EmailMessage);

                //Send user verification SMS
                IdentityMessage SmsMessage = new IdentityMessage();
                //send accompanying SMS reminding user to verify their email
                string smsBody = "An EUC application has just been endorsed by " + agencyName
                                    + "\n"
                                    + "Application Number: " + applicationNumber
                                    + "\n"
                                    + " Kindly review for rejection or forward to NSA for approval.";

                SmsMessage.Body = smsBody;
                SmsMessage.Destination = supportUser.PhoneNumber;
                SmsMessage.Subject = "New Endorsed EUC Application Received: " + applicationNumber;

                SmsService smsSerice = new SmsService();
                await smsSerice.SendAsync(SmsMessage);

            }
        }
        //Notify ONSA support of newly dis-endorsed applications by agencies
        public async Task NotifyONSASupportOfDisEndorsedEUCApplication(List<ApplicationUser> users, string applicationNumber, string agencyName)
        {
            foreach (var supportUser in users)
            {
                //Send user verification email            
                IdentityMessage EmailMessage = new IdentityMessage();

                EmailMessage.Body = "Dear " + supportUser.FirstName + " " + supportUser.LastName + ". "
                                    + "<br/>"
                                    + "An EUC application has just been dis-endorsed by " + agencyName
                                    + "<br/>"
                                    + "Application Number: " + applicationNumber
                                    + "<br/>"
                                    + "Thank you.";
                EmailMessage.Destination = supportUser.Email;
                EmailMessage.Subject = "Dis-Endorsed EUC Application: " + applicationNumber;


                EmailService emailService = new EmailService();
                await emailService.SendAsync(EmailMessage);

                //Send user verification SMS
                IdentityMessage SmsMessage = new IdentityMessage();
                //send accompanying SMS reminding user to verify their email
                string smsBody = "An EUC application has just been dis-endorsed by " + agencyName + ". "
                                    + "Application Number: " + applicationNumber + "."
                                    + " Thank you.";

                SmsMessage.Body = smsBody;
                SmsMessage.Destination = supportUser.PhoneNumber;
                SmsMessage.Subject = "Dis-Endorsed EUC Application: " + applicationNumber;

                SmsService smsSerice = new SmsService();
                await smsSerice.SendAsync(SmsMessage);
            }
        }
    }
}