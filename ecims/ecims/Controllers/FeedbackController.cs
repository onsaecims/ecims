﻿using ecims.Models;
using ecims.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace ecims.Controllers
{
    public class FeedbackController : Controller
    {
        ApplicationDbContext _context;
        
        AuditTrailGenerator.AuditTrail _auditTrail = new AuditTrailGenerator.AuditTrail();

        public FeedbackController()
        {
            _context = new ApplicationDbContext();
        }

        [Authorize(Roles = "Client User, ONSA Support, Agency, Customs User, Exclusive User")]
        public ActionResult Feedback()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Create a feedback. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Viewed Dispute Certificate page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return View(new FeedbackViewModel
            {
                user = user,
                role = roles
            });
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Feedback(FeedbackViewModel model)
        {
            var FeedBack = new FeedBack
            {
                UserId = User.Identity.GetUserId(),
                Subject = model.Subject,
                Message = model.Message,
                FeedbackDate = DateTime.Now.Date,
                status = FeedBackStatus.Pending
            };
            _context.FeedBack.Add(FeedBack);
            _context.SaveChanges();

            TempData["Success"] = "Feedback sent Successfully!";
            
            var userId = User.Identity.GetUserId();
            model.user = _context.Users.Where(u => u.Id == userId).Single();
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            model.role = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            return RedirectToAction("MainDashboard", "Admin");
        }
        [Authorize(Roles = "ONSA Support")]
        public ActionResult AllFeedback()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Create a feedback. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Viewed Dispute Certificate page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var allFeedback = _context.FeedBack.ToList();
            var userList = _context.Users.ToList();
            return View(new FeedbackViewModel
            {
                user = user,
                role = roles,
                AllFeedBacks = allFeedback,
                UsersToQuery = userList
            });
        }
        [Authorize(Roles = "ONSA Support")]
        public ActionResult PendingFeedback()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Create a feedback. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Viewed Dispute Certificate page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var allFeedback = _context.FeedBack.Where(f => f.status == FeedBackStatus.Pending).ToList();
            var userList = _context.Users.ToList();
            return View(new FeedbackViewModel
            {
                user = user,
                role = roles,
                PendingFeedbacks = allFeedback,
                UsersToQuery = userList
            });
        }
        [Authorize(Roles = "ONSA Support")]
        public ActionResult ResolvedFeedback()
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Create a feedback. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Viewed Dispute Certificate page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var allFeedback = _context.FeedBack.Where(f => f.status == FeedBackStatus.Resolved).ToList();
            var userList = _context.Users.ToList();
            return View(new FeedbackViewModel
            {
                user = user,
                role = roles,
                ResolvedFeedbacks = allFeedback,
                UsersToQuery = userList
            });
        }
        [Authorize(Roles = "ONSA Support")]
        public ActionResult FeedbackDetails(int? Id)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Create a feedback. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Viewed Dispute Certificate page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var Details = _context.FeedBack.Where(f => f.Id == Id).Single();
            var userList = _context.Users.ToList();
            return View(new FeedbackViewModel
            {
                user = user,
                role = roles,
                FeedBackDetails = Details,
                UsersToQuery = userList
            });
        }
        [Authorize(Roles = "ONSA Support")]
        public ActionResult ResolveFeedback(int Id)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();
            //Create user object based off user ID
            var user = _context.Users.Where(u => u.Id == userId).Single();

            //Document the entry in the audit trail
            _auditTrail.CreateAuditTrail("Create a feedback. Applicant Number: " + user.ApplicationNumber + " User Name: " + user.FirstName + " " + user.LastName,
               User.Identity.GetUserId(),
               "Viewed Dispute Certificate page.",
               Request.UserHostAddress);

            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            var roleClaimType = userIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).ToList();
            var Details = _context.FeedBack.Where(f => f.Id == Id).Single();
            var userList = _context.Users.ToList();
            return View(new FeedbackViewModel
            {
                user = user,
                role = roles,
                FeedBackDetails = Details,
                UsersToQuery = userList,
                FeedBackId = Id
            });
        }
        [Authorize(Roles = "ONSA Support")]
        [HttpPost]
        public ActionResult ResolveFeedback(FeedbackViewModel model)
        {
            //Get ID of the currently logged in user
            var userId = User.Identity.GetUserId();

            int Id = model.FeedBackId;
            var feedback = _context.FeedBack.Where(f => f.Id == Id).FirstOrDefault();

            feedback.resolveAction = model.ResolveMessage;
            feedback.resolvedBy = userId;
            feedback.resolvedDate = DateTime.Now.Date;
            feedback.status = FeedBackStatus.Resolved;

            _context.SaveChanges();
            TempData["Success"] = "Updated Successfully!";
            
            return RedirectToAction("ResolvedFeedback", "Feedback");
        }
    }
}
