﻿'use strict';
var $ = jQuery;
$.getScript("https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js", function () {

    $('#myTable').DataTable({
        "paging": true,
        "ordering": true,
        "info": false
    });
});