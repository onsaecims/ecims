﻿$(document).ready(function () {

    $("#myTable #checkHead").on('change', function (e) {
        if ($("#myTable #checkHead").is(':checked')) {
            $("#myTable #checkall").each(function () {
                $(this).prop('checked', true);
            });

        } else {
            $("#myTable #checkall").each(function () {
                $(this).prop('checked', false);
                //this.checked = false;
            });
        }
    });

    $("#myTable2 #checkHead").on('change', function (e) {
        if ($("#myTable2 #checkHead").is(':checked')) {
            $("#myTable2 #checkall").each(function () {
                $(this).prop('checked', true);
            });

        } else {
            $("#myTable2 #checkall").each(function () {
                $(this).prop('checked', false);
                //this.checked = false;
            });
        }
    });

    $("[data-toggle=tooltip]").tooltip();
});
