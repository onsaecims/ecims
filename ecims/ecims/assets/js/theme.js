var theme = function () {

    // ---------------------------------------------------------------------------------------
    // prevent empty links
    function handlePreventEmptyLinks() {
        $('a[href=#]').click(function (event) {
            event.preventDefault();
        });
    }

    // ---------------------------------------------------------------------------------------
    // fix html5 placeholder attribute for ie7 & ie8
    function handlePlaceholder() {
        if ($.browser.msie && $.browser.version.substr(0, 1) < 9) { // ie7&ie8
            $('input[placeholder], textarea[placeholder]').each(function () {
                var input = $(this);

                $(input).val(input.attr('placeholder'));

                $(input).focus(function () {
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                });

                $(input).blur(function () {
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.val(input.attr('placeholder'));
                    }
                });
            });
        }
    }

    // ---------------------------------------------------------------------------------------
    // Placeholdem
    function handlePlaceholdem() {
        Placeholdem(document.querySelectorAll('[placeholder]'));
    }

    // ---------------------------------------------------------------------------------------
    // add hover class for correct view on mobile devices
    function handleHoverClass() {
        var hover = $('.thumbnail');
        hover.hover(
            function () {
                $(this).addClass('hover');
            },
            function () {
                $(this).removeClass('hover');
            }
        );
    }

    // ---------------------------------------------------------------------------------------
    // superfish menu
    function handleSuperFish() {
//        $('ul.sf-menu').superfish();
        /*$('[data-spy="scroll"]').each(function () {
            var $spy = $(this).scrollspy('refresh')
        })*/
    }

    // ---------------------------------------------------------------------------------------
    // create mobile menu from exist superfish menu
    function handleMobileMenu() {
        var $menu = $('.navigation > ul'),
            optionsList = '<option value="" selected> - - Main Navigation - - </option>';

        $menu.find('li').each(function () {
            var $this = $(this),
                $anchor = $this.children('a'),
                depth = $this.parents('ul').length - 1,
                indent = '';

            if (depth) {
                while (depth > 0) {
                    indent += ' ::: ';
                    depth--;
                }
            }

            optionsList += '<option value="' + $anchor.attr('href') + '">' + indent + ' ' + $anchor.text() + '</option>';
        }).end().parent().parent().find('#mobile-menu').append('<select class="mobile-menu">' + optionsList + '</select><div class="mobile-menu-title"><i class="fa fa-bars"></i></div>');

        $('.mobile-menu').on('change', function () {
            window.location = $(this).val();
        });
    }

    // ---------------------------------------------------------------------------------------
    // Sticky Menu
    function handleStickyMenu() {

        function addStickyClass() {
            if ($(window).scrollTop() > 150) {
                $('header.header').addClass('sticky-header');
            }
            else {
                $('header.header').removeClass('sticky-header');
            }
        }

        addStickyClass();

        $(window).scroll(function () {
            addStickyClass()
        });
    }

    // Smooth scrolling
    // ---------------------------------------------------------------------------------------
    function handleSmoothScroll(){
        $(".sf-menu a, .scroll-to").click(function () {

            var headerH = $('header').outerHeight();
            $(".sf-menu a").removeClass('active');
            $(this).addClass('active');
            $("html, body").animate({
                scrollTop: $($(this).attr("href")).offset().top - 44 + "px"
            }, {
                duration: 1200,
                easing: "easeInOutExpo"
            });
            return false;
        });

    }

    // ---------------------------------------------------------------------------------------
    // prettyPhoto
    function handlePrettyPhoto() {
        $("a[data-gal^='prettyPhoto']").prettyPhoto({theme: 'dark_square'});
    }

    // ---------------------------------------------------------------------------------------
    // Scroll totop button
    function handleToTopButton() {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 1) {
                $('.totop').css({bottom: "20px"});
            } else {
                $('.totop').css({bottom: "-100px"});
            }
        });
        $('.totop').click(function () {
            $('html, body').animate({scrollTop: '0px'}, 800);
            return false;
        });
    }

    // ---------------------------------------------------------------------------------------
    // Tabs
    function handleTabs() {
        $('#tabs a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
    }

    // ---------------------------------------------------------------------------------------
    // CountDown
    function handleCountDown() {
        //
        var austDay = new Date();
        austDay = new Date(austDay.getFullYear() + 1, 1 - 1, 26);
        $('#defaultCountdown').countdown({until: austDay});
        $('#year').text(austDay.getFullYear());

        $('#defaultCountdown2').countdown({until: austDay});
        $('#defaultCountdown3').countdown({until: austDay});
    }

    // ---------------------------------------------------------------------------------------
    // Resize main slider
    /*
    function resizeSlider() {
        $('.main-slider').css("min-height", $(window).height() - $('.header').height());
        $('.main-slider .owl-wrapper-outer').css("margin-top", (($('.main-slider').height() - $('.main-slider .owl-wrapper-outer').height())/2));
    }
    $(window).load(function(){ resizeSlider() });
    $(window).resize(function(){ resizeSlider() });
    */

    // ---------------------------------------------------------------------------------------
    //

    // INIT FUNCTIONS
    // ---------------------------------------------------------------------------------------
    return {
        init: function () {
            handlePreventEmptyLinks();
            handlePlaceholder();
            handlePlaceholdem();
            handleHoverClass();
            handleSuperFish();
            handleMobileMenu();
            handleStickyMenu();
            handleSmoothScroll();
            handleCountDown();
            handlePrettyPhoto();
            handleToTopButton();
            handleTabs();
        },
        // Isotope
        initIsotope: function () {
            var $container = $('#timeline');
            var $container2 = $('#timeline2');
            var $container3 = $('#timeline3');
            var $container4 = $('#timeline4');
            function addMarker() {
                // add marker
                $container.addClass('vline');
                $container.find('.item').each(function () {
                    var str = $(this).css('transform');
                    var substr = str.split(', ');

                    if ($.browser.msie) {
                        substrNumber = substr[substr.length - 4];
                    } else {
                        substrNumber = substr[substr.length - 2];
                    }

                    if (substrNumber == '0') {
                        $(this).removeClass('item-left').removeClass('item-right');
                        $(this).addClass('item-left');
                    } else {
                        $(this).removeClass('item-left').removeClass('item-right');
                        $(this).addClass('item-right');
                    }
                });
            }
            function addMarker2() {
                $container2.addClass('vline');
                $container2.find('.item').each(function () {
                    var str = $(this).css('transform');
                    var substr = str.split(', ');

                    if ($.browser.msie) {
                        substrNumber = substr[substr.length - 4];
                    } else {
                        substrNumber = substr[substr.length - 2];
                    }

                    if (substrNumber == '0') {
                        $(this).removeClass('item-left').removeClass('item-right');
                        $(this).addClass('item-left');
                    } else {
                        $(this).removeClass('item-left').removeClass('item-right');
                        $(this).addClass('item-right');
                    }
                });
            }
            function addMarker3() {
                $container3.addClass('vline');
                $container3.find('.item').each(function () {
                    var str = $(this).css('transform');
                    var substr = str.split(', ');

                    if ($.browser.msie) {
                        substrNumber = substr[substr.length - 4];
                    } else {
                        substrNumber = substr[substr.length - 2];
                    }

                    if (substrNumber == '0') {
                        $(this).removeClass('item-left').removeClass('item-right');
                        $(this).addClass('item-left');
                    } else {
                        $(this).removeClass('item-left').removeClass('item-right');
                        $(this).addClass('item-right');
                    }
                });
            }
            function addMarker4() {
                $container4.addClass('vline');
                $container4.find('.item').each(function () {
                    var str = $(this).css('transform');
                    var substr = str.split(', ');

                    if ($.browser.msie) {
                        substrNumber = substr[substr.length - 4];
                    } else {
                        substrNumber = substr[substr.length - 2];
                    }

                    if (substrNumber == '0') {
                        $(this).removeClass('item-left').removeClass('item-right');
                        $(this).addClass('item-left');
                    } else {
                        $(this).removeClass('item-left').removeClass('item-right');
                        $(this).addClass('item-right');
                    }
                });
            }
            $(window).resize(function () {
                // relayout on window resize
                $container.isotope('reLayout');
                $container2.isotope('reLayout');
                $container3.isotope('reLayout');
                $container4.isotope('reLayout');
                addMarker();
                addMarker2();
                addMarker3();
                addMarker4();
            });
            $(window).load(function () {
                // initialize isotope
                $container.isotope({itemSelector: '.item'});
                $container.isotope('reLayout');
                addMarker();
                $container2.isotope({itemSelector: '.item'});
                $container2.isotope('reLayout');
                addMarker2();
                $container3.isotope({itemSelector: '.item'});
                $container3.isotope('reLayout');
                addMarker3();
                $container4.isotope({itemSelector: '.item'});
                $container4.isotope('reLayout');
                addMarker4();
            });
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                $('.timeline').find('.item').removeClass('item-left').removeClass('item-right');
                $('.timeline').removeClass('vline');
            });
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
				$container.isotope('reLayout', function(){ addMarker(); });
				$container2.isotope('reLayout', function(){ addMarker2(); });
				$container3.isotope('reLayout', function(){ addMarker3(); });
				$container4.isotope('reLayout', function(){ addMarker4(); });
			});
			
        },
        // EventSlider
        initEventSlider: function () {
            $("#event-slider").owlCarousel({
                //autoPlay: true,
                singleItem:true,
                pagination: false,
                navigation : true
            });
        },
        // Twitter / Last Tweet Carousel
        initLastTweet: function () {
            $("#last-tweets").owlCarousel({
                singleItem: true,
                autoPlay: true,
                pagination: true
            });
            $("#next-tweet").click(function () {
                $("#last-tweets").trigger('owl.next');
                return false;
            });
            $("#prev-tweet").click(function () {
                $("#last-tweets").trigger('owl.prev');
                return false;
            });
        },
        // Animation on Scroll
        initAnimation: function () {
            var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
            if (isMobile == false) {
                $('*[data-animation]').addClass('animated');
                $('.animated').waypoint(function (down) {
                    var elem = $(this);
                    var animation = elem.data('animation');
                    if (!elem.hasClass('visible')) {
                        var animationDelay = elem.data('animation-delay');
                        if (animationDelay) {
                            setTimeout(function () {
                                elem.addClass(animation + " visible");
                            }, animationDelay);
                        } else {
                            elem.addClass(animation + " visible");
                        }
                    }
                }, {
                    offset: $.waypoints('viewportHeight')
                    //offset: 'bottom-in-view'
                    //offset: '95%'
                });
            }
        },
        // Google map
        initGoogleMap: function() {
            var map;
            function initialize() {
                var mapOptions = {
                    zoom: 10,
                    //scrollwheel: false,
                    draggable: false,
                    center: new google.maps.LatLng(-34.397, 150.644)
                };
                map = new google.maps.Map(document.getElementById('map-canvas'),
                    mapOptions);
            }
            google.maps.event.addDomListener(window, 'load', initialize);
        }
    };
}();
