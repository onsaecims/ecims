﻿using ecims.Controllers.FluentScheduler;
using FluentScheduler;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ecims
{
    public class MvcApplication : System.Web.HttpApplication
    {
       protected void Application_Start()
        {
            DevExpress.XtraReports.Web.WebDocumentViewer.Native.WebDocumentViewerBootstrapper.SessionState = System.Web.SessionState.SessionStateBehavior.Disabled;
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ValueProviderFactories.Factories.Add(new JsonValueProviderFactory());
            JobManager.Initialize(new MyRegistry());
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            Server.ClearError();
            Response.Redirect("/ErrorHandling/HandleError");
        }
        //Force redirect all users to use SSL
        protected void Application_BeginRequest()
        {
            if (!Context.Request.IsSecureConnection)
            {
               //This is an insecure connection, so redirect to the secure version
               UriBuilder uri = new UriBuilder(Context.Request.Url);
                if (!uri.Host.Equals("localhost"))
                {
                    uri.Port = 443;
                    uri.Scheme = "https";
                    Response.Redirect(uri.ToString());
                }
            }

            //Set Date format
            CultureInfo info = new CultureInfo(System.Threading.Thread.CurrentThread.CurrentCulture.ToString());
            info.DateTimeFormat.ShortDatePattern = "dd-MM-yyyy";
            System.Threading.Thread.CurrentThread.CurrentCulture = info;
        }
    }
}
