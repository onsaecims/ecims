namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateEUCWorkflow : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EUCWorkFlows", "Reciepient_Id", c => c.String(maxLength: 128));
            AddColumn("dbo.EUCWorkFlows", "Source_Id", c => c.String(maxLength: 128));
            AlterColumn("dbo.EUCWorkFlows", "actionSource", c => c.String(nullable: false));
            AlterColumn("dbo.EUCWorkFlows", "actionReciepient", c => c.String(nullable: false));
            CreateIndex("dbo.EUCWorkFlows", "Reciepient_Id");
            CreateIndex("dbo.EUCWorkFlows", "Source_Id");
            AddForeignKey("dbo.EUCWorkFlows", "Reciepient_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.EUCWorkFlows", "Source_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EUCWorkFlows", "Source_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.EUCWorkFlows", "Reciepient_Id", "dbo.AspNetUsers");
            DropIndex("dbo.EUCWorkFlows", new[] { "Source_Id" });
            DropIndex("dbo.EUCWorkFlows", new[] { "Reciepient_Id" });
            AlterColumn("dbo.EUCWorkFlows", "actionReciepient", c => c.String());
            AlterColumn("dbo.EUCWorkFlows", "actionSource", c => c.String());
            DropColumn("dbo.EUCWorkFlows", "Source_Id");
            DropColumn("dbo.EUCWorkFlows", "Reciepient_Id");
        }
    }
}
