namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EUCApplicationsComments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EUCApplicationComments",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        applicationNo = c.String(nullable: false),
                        FlagDate = c.DateTime(nullable: false),
                        userFrom = c.String(nullable: false),
                        userComment = c.String(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.EUCApplicationEndorsers",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        applicationNo = c.String(nullable: false),
                        EndorsementDate = c.DateTime(nullable: false),
                        userFrom = c.String(nullable: false),
                        EndorsementDesk = c.String(),
                        AgencyName = c.String(),
                        AgencyType = c.String(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.EUCApplicationFlags",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        applicationNo = c.String(nullable: false),
                        FlagDate = c.DateTime(nullable: false),
                        userFrom = c.String(nullable: false),
                        userComment = c.String(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EUCApplicationFlags", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.EUCApplicationEndorsers", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.EUCApplicationComments", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.EUCApplicationFlags", new[] { "User_Id" });
            DropIndex("dbo.EUCApplicationEndorsers", new[] { "User_Id" });
            DropIndex("dbo.EUCApplicationComments", new[] { "User_Id" });
            DropTable("dbo.EUCApplicationFlags");
            DropTable("dbo.EUCApplicationEndorsers");
            DropTable("dbo.EUCApplicationComments");
        }
    }
}
