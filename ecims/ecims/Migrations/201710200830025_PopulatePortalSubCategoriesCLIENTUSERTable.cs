namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PopulatePortalCategoriesCLIENTUSERTable : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT PortalCategories ON");
            
            Sql("INSERT INTO PortalCategories (Id, Name) VALUES (6, 'Client User')");

            Sql("SET IDENTITY_INSERT PortalCategories OFF");
        }

        public override void Down()
        {
            Sql("DELETE FROM PortalCategories WHERE Id IN (6)");
        }
    }
}
