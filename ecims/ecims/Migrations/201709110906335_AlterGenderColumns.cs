namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterGenderColumns : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Gender", c => c.Int(nullable: false));
            DropColumn("dbo.AspNetUsers", "TheGender");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "TheGender", c => c.Int(nullable: false));
            DropColumn("dbo.AspNetUsers", "Gender");
        }
    }
}
