namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCountryToDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CountryLists",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        CountryName = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CountryLists");
        }
    }
}
