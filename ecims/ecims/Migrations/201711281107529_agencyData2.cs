namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agencyData2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Agencies", "AgencyT_id", "dbo.AgencyTypes");
            DropIndex("dbo.Agencies", new[] { "AgencyT_id" });
            DropColumn("dbo.Agencies", "AgencyT_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Agencies", "AgencyT_id", c => c.Int());
            CreateIndex("dbo.Agencies", "AgencyT_id");
            AddForeignKey("dbo.Agencies", "AgencyT_id", "dbo.AgencyTypes", "id");
        }
    }
}
