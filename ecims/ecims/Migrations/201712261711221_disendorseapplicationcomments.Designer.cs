// <auto-generated />
namespace ecims.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class disendorseapplicationcomments : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(disendorseapplicationcomments));
        
        string IMigrationMetadata.Id
        {
            get { return "201712261711221_disendorseapplicationcomments"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
