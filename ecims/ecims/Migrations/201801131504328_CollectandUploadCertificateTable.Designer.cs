// <auto-generated />
namespace ecims.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class CollectandUploadCertificateTable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CollectandUploadCertificateTable));
        
        string IMigrationMetadata.Id
        {
            get { return "201801131504328_CollectandUploadCertificateTable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
