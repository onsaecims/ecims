namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class certificateStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EndUserCertificates", "certificateStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EndUserCertificates", "certificateStatus");
        }
    }
}
