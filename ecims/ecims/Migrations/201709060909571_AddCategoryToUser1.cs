namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCategoryToUser1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "Category_Id", "dbo.AccountCategories");
            DropIndex("dbo.AspNetUsers", new[] { "Category_Id" });
            AlterColumn("dbo.AspNetUsers", "Category_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.AspNetUsers", "Category_Id");
            AddForeignKey("dbo.AspNetUsers", "Category_Id", "dbo.AccountCategories", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "Category_Id", "dbo.AccountCategories");
            DropIndex("dbo.AspNetUsers", new[] { "Category_Id" });
            AlterColumn("dbo.AspNetUsers", "Category_Id", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Category_Id");
            AddForeignKey("dbo.AspNetUsers", "Category_Id", "dbo.AccountCategories", "Id");
        }
    }
}
