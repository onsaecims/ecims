namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class DropGenderIndexOnUserTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "GenderId", "dbo.Genders");
            DropIndex("dbo.AspNetUsers", new[] { "GenderId" });
            //DropColumn("dbo.AspNetUsers", "GenderId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "GenderId", c => c.Int(nullable: false));
            CreateIndex("dbo.AspNetUsers", "GenderId");
            AddForeignKey("dbo.AspNetUsers", "GenderId", "dbo.Genders", "Id", cascadeDelete: true);
        }
    }
}
