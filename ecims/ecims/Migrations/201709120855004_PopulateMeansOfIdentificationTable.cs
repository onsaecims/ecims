namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PopulateMeansOfIdentificationTable : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT MeansOfIdentifications ON");

            Sql("INSERT INTO MeansOfIdentifications (Id, Name) VALUES (1, 'National ID')");
            Sql("INSERT INTO MeansOfIdentifications (Id, Name) VALUES (2, 'Drivers License')");
            Sql("INSERT INTO MeansOfIdentifications (Id, Name) VALUES (3, 'International Passport')");
            Sql("INSERT INTO MeansOfIdentifications (Id, Name) VALUES (4, 'Permanent Voters Card')");

            Sql("SET IDENTITY_INSERT MeansOfIdentifications OFF");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM MeansOfIdentifications WHERE Id IN (1, 2, 3, 4)");
        }
    }
}
