// <auto-generated />
namespace ecims.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class blacklistDataUpdate : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(blacklistDataUpdate));
        
        string IMigrationMetadata.Id
        {
            get { return "201712151203161_blacklistDataUpdate"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
