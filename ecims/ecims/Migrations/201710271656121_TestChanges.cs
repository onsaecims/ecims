namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TestChanges : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EndUserCertificates", "applicationStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EndUserCertificates", "applicationStatus");
        }
    }
}
