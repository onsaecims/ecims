namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsOrganisationFieldToEUCModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EndUserCertificates", "IsOrganisation", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EndUserCertificates", "IsOrganisation");
        }
    }
}
