namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatePortalUserSubCategories : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PortalSubCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PortalCategoryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PortalCategories", t => t.PortalCategoryId, cascadeDelete: true)
                .Index(t => t.PortalCategoryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PortalSubCategories", "PortalCategoryId", "dbo.PortalCategories");
            DropIndex("dbo.PortalSubCategories", new[] { "PortalCategoryId" });
            DropTable("dbo.PortalSubCategories");
        }
    }
}
