namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBlackListHistoryIdToUserTable : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.AspNetUsers", "blacklistID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "blacklistID");
        }
    }
}
