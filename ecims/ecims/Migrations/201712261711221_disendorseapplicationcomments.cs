namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class disendorseapplicationcomments : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DisendorsedEUCAppComments",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        EUCAppID = c.String(),
                        DisendorsedBy = c.String(),
                        DisendorsedDate = c.DateTime(nullable: false),
                        DisendorsedComment = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DisendorsedEUCAppComments");
        }
    }
}
