namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PopulateHSCodeData : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT HSCodes ON");

            Sql("INSERT INTO HSCodes (Id, Name) VALUES (1, '8802110000 - Helicopters of an unladen weight not exceeding 2000kg')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (2, '8802110012 - Helicopters of an unladen weight not exceeding 2000kg - Commercial')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (3, '8802110013 - Helicopters of an unladen weight not exceeding 2000kg - Others')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (4, '8802120000 - Helicopters of an unladen weight exceeding 2000kg')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (5, '8802120014 - Helicopters of an unladen weight exceeding 2000kg - Commercial')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (6, '8802120015 - Helicopters of an unladen weight exceeding 2000kg - Others')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (7, '8802200000 - Aeroplanes and other aircraft of an unladen weight not exceeding 2000kg')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (8, '8802200021 - Aeroplanes and other aircraft of an unladen weight not exceeding 2000kg - Commercial')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (9, '8802200023 - Aeroplanes and other aircraft of an unladen weight not exceeding 2000kg - Others')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (10, '8802300000 - Aeroplanes and other aircraft of an unladen weight exceeding 2000 but not exceed 15000kg')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (11, '8802300031 - Aeroplanes and other aircraft of an unladen weight exceeding 2000 - Commercial')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (12, '8802300032 - Aeroplanes and other aircraft of an unladen weight exceeding 2000 - Others')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (13, '8802400000 - Aeroplanes and other aircraft of an unladen weight exceeding 15000kg')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (14, '8802400041 - Aeroplanes and other aircraft of an unladen weight exceeding 15000kg - Commercial')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (15, '8802600000 - Spacecraft (inc.satellites) and suborbital and spacecraft launch vehicles')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (16, '8803100000 - Propellers and rotors and parts thereof')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (17, '8803200000 - Under-carriages and parts thereof')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (18, '8803300000 - Other parts of aeroplanes or helicopters')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (19, '8803900000 - Other parts of goods of heading 88.01 or 88.02')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (20, '8804000000 - Parachutes (inc.dirigible parachutes,paragliders) and rotochutes; parts and accessories')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (21, '8805100000 - Aircraft launching gear, deck-arrestors... and parts thereof')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (22, '8805210000 - Air combat simulators and parts')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (23, '8805290000 - Other ground flying traniners and parts thereof')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (24, '8901101100 - Cruise ships and similar vessels for the transport of persons or goods <=500 tonnes')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (25, '8901101200 - Cruise ships and similar vessels for the transport of persons or goods >500 tonnes')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (26, '8901109000 - Other ships and similar vessels for the transport of goods')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (27, '8901200000 - Tankers')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (28, '8901300000 - Refrigerated vessels, other than those of subheading 8901.20')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (29, '8901901100 - Refrigerated vessels, other than those of subheading 8901.20 <= 500 tonnes')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (30, '8901901200 - Refrigerated vessels, other than those of subheading 8901.20, of a capacity > 500 tonnes')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (31, '8902001000 - Fishing vessels of a carrying capacity not exceeding 5 tonnes')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (32, '8902002000 - Fishing vessels of a carrying capacity > 5 tonnes <=40 tonnes')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (33, '8902003100 - Fishing vessels of a carrying capacity >=40 tonnes <=300 tonnes, equipped with a freezer')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (34, '8902003900 - Fishing vessels of a carrying capacity not >40 tonnes <=300 tonnes, other')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (35, '8902004100 - Fishing vessels of a carrying capacity not >300 tonnes, equipped with a freezer')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (36, '8902004900 - Fishing vessels of a carrying capacity not >300 tonnes, other')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (37, '8902009000 - Other fishing vessels')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (38, '8903100000 - Inflatable boats and other vessels for pleasure or sports')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (39, '8903920000 - Motorboats for pleasure or sports, other than outboard motorboats')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (40, '8903990000 - Other Vessels for pleasure or sports, nes; rowing boats and canoes')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (41, '8904000000 - Tugs and pusher craft')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (42, '8905100000 - Dredgers')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (43, '8905200000 - Floating or submersible drilling or production platforms')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (44, '8905900000 - Other Light vessels, fire-floats, floating cranes, etc, nes')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (45, '8906100000 - Warships')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (46, '8906900000 - Other vessels, including warships and lifeboats other rowing boats')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (47, '8907100000 - Inflatable rafts')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (48, '8907900000 - Other floating structures e.g rafts,tanks,coffer-dams,landing stages,buoys and beacons)')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (49, '8908000000 - Vessels and other floating structures for breaking up')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (50, '9401100000 - Seats of a kind used for aircraft')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (51, '9401200000 - Seats of a kind used for motor vehicles')");
            Sql("INSERT INTO HSCodes (Id, Name) VALUES (52, '8705900000 - Automobiles (Specialized Vehicles)')");

            Sql("SET IDENTITY_INSERT HSCodes OFF");
        }

        public override void Down()
        {
            Sql("DELETE FROM HSCodes WHERE Id IN (1, 2, 3, 5, 6)");
        }
    }
}
