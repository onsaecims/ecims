namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class portLanding : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PortOfLandings", "portState", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PortOfLandings", "portState");
        }
    }
}
