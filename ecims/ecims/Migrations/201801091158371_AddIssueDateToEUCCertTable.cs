namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIssueDateToEUCCertTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EUCCertificates", "issueDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EUCCertificates", "issueDate");
        }
    }
}
