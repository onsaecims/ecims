namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateQtyToEnum : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.Agencies",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            dateCreated = c.DateTime(nullable: false),
            //            agencyType = c.Int(nullable: false),
            //            agencyName = c.String(),
            //            contactName = c.String(),
            //            agencyPhone = c.String(),
            //            agencyEmail = c.String(),
            //            AgencyT_id = c.Int(),
            //        })
            //    .PrimaryKey(t => t.id)
            //    .ForeignKey("dbo.AgencyTypes", t => t.AgencyT_id)
            //    .Index(t => t.AgencyT_id);
            
            //CreateTable(
            //    "dbo.AgencyTypes",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            Name = c.String(nullable: false),
            //        })
            //    .PrimaryKey(t => t.id);
            
            AlterColumn("dbo.EndUserCertificateDetails", "unit", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Agencies", "AgencyT_id", "dbo.AgencyTypes");
            DropIndex("dbo.Agencies", new[] { "AgencyT_id" });
            AlterColumn("dbo.EndUserCertificateDetails", "unit", c => c.String());
            DropTable("dbo.AgencyTypes");
            DropTable("dbo.Agencies");
        }
    }
}
