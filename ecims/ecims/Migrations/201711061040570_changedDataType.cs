namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changedDataType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.EndUserCertificateDetails", "hsCode", c => c.Int(nullable: false));
            AlterColumn("dbo.EndUserCertificateDetails", "itemDescription", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EndUserCertificateDetails", "itemDescription", c => c.String());
            AlterColumn("dbo.EndUserCertificateDetails", "hsCode", c => c.String());
        }
    }
}
