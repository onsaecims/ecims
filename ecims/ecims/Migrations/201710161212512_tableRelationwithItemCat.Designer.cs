// <auto-generated />
namespace ecims.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class tableRelationwithItemCat : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(tableRelationwithItemCat));
        
        string IMigrationMetadata.Id
        {
            get { return "201710161212512_tableRelationwithItemCat"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
