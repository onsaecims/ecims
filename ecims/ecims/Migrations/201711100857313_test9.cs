namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test9 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.EUCApplicationEndorsers", "EndorsementDesk", c => c.Int(nullable: false));
            AlterColumn("dbo.EUCApplicationEndorsers", "AgencyName", c => c.Int(nullable: false));
            AlterColumn("dbo.EUCApplicationEndorsers", "AgencyType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EUCApplicationEndorsers", "AgencyType", c => c.String());
            AlterColumn("dbo.EUCApplicationEndorsers", "AgencyName", c => c.String());
            AlterColumn("dbo.EUCApplicationEndorsers", "EndorsementDesk", c => c.String());
        }
    }
}
