namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PopulatePortOfLandingTable : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT PortOfLandings ON");

            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (1, 'Port Code (01AP - APAPA PORT)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (2, 'Port Code (01CA - COMET SHIPP BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (3, 'Port Code (01CV - CARGO VISION BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (4, 'Port Code (01DM - DUNCAN MANTH BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (5, 'Port Code (01FO - FEDERAL OPERATIONS)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (6, 'Port Code (01GP - GENERAL POST OFFICE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (7, 'Port Code (01HQ - ZONAL HEADQUARTERS HARVEY ROAD)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (8, 'Port Code (01IK - IKORODU CUSTOMS COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (10, 'Port Code (01LP - LILYPOND PORT)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (11, 'Port Code (01MC - MUHAMMED MURTALA CARGO)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (12, 'Port Code (01MG - MIGFO BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (13, 'Port Code (01MH - MICHELLE BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (14, 'Port Code (01MM - MUHAMMED MURTALA INTERNATIONAL)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (15, 'Port Code (01MS - MID-MARITIME BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (16, 'Port Code (01PA - KIRIKIRI LIGHTER TERMINAL CMD.)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (17, 'Port Code (01PE - PORT EXPRESS BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (18, 'Port Code (01SA - SAPID AGENCIES BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (19, 'Port Code (01SM - SEME BORDER POST)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (20, 'Port Code (01TC - TIN CAN ISLAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (21, 'Port Code (01TE - TROPEX BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (22, 'Port Code (01TG - PTML CUSTOMS OFFICE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (23, 'Port Code (01UL - UNILEVER BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (24, 'Port Code (01WA - WAMCO BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (25, 'Port Code (01ZA - ZONAL OFFICE (ZONE A))')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (26, 'Port Code (02JG - JIGAWA AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (27, 'Port Code (02KD - KADUNA COLLECTION)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (28, 'Port Code (02KN - KANO AIRPORT)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (29, 'Port Code (02KT - KATSINA COLLECTION)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (30, 'Port Code (03AD - ADAMAWA AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (31, 'Port Code (03BA - BAUCHI AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (32, 'Port Code (03BO - BORNO AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (33, 'Port Code (03PL - PLATEAU AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (34, 'Port Code (03TA - TARABA AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (35, 'Port Code (03YB - YOBE AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (36, 'Port Code (04AB - ABUJA AIRPORT)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (37, 'Port Code (04BE - BENUE AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (38, 'Port Code (04IL - ILORIN AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (39, 'Port Code (04KE - KEBBI AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (40, 'Port Code (04KG - KOGI AREA)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (41, 'Port Code (04KW - KWARA AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (42, 'Port Code (04NG - NIGER AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (43, 'Port Code (04SO - SOKOTO AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (44, 'Port Code (05AB - ABIA AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (45, 'Port Code (05AK - AKWA IBOM AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (46, 'Port Code (05CA - CALABAR)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (47, 'Port Code (05EN - ENUGU AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (48, 'Port Code (05IM - IMO AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (49, 'Port Code (05OG - OIL AND GAS TERMINAL)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (50, 'Port Code (05PA - PORT HARCOURT (2))')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (51, 'Port Code (05PH - PORT HARCOURT(1) Area-1)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (52, 'Port Code (05PM - PORT HARCOURT(2) Airport)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (53, 'Port Code (05PN - PORT HARCOURT(3) Onne)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (54, 'Port Code (06ED - EDO AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (55, 'Port Code (06ID - IDIROKO BORDER STATION)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (56, 'Port Code (06OG - OGUN AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (57, 'Port Code (06ON - ONDO AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (58, 'Port Code (06OS - OSHUN AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (59, 'Port Code (06OY - OYO AREA COMMAND)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (60, 'Port Code (06WR - WARRI PORT)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (61, 'Port Code (11AP - SAPID AGENCIES BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (62, 'Port Code (11LP - MICHELLE HOLD. BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (63, 'Port Code (11MC - WAMCO BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (64, 'Port Code (11OY - PROCTER & GAMBLE BONDED WAREHOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (65, 'Port Code (11PA - MID-MARITIME BOUNDED WAREHOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (66, 'Port Code (12AP - MIGFO BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (67, 'Port Code (12LP - CARGO VISIONS BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (68, 'Port Code (12MC - TROPEX BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (69, 'Port Code (12PA - COMET SHIPPING BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (70, 'Port Code (12TC - TOYOTA (NIGERIA) LTD)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (71, 'Port Code (13LP - NIGERIA CLEARING&FORWARDING)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (72, 'Port Code (13MC - UNILEVER BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (73, 'Port Code (13PA - DUNCAN MARITIME BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (74, 'Port Code (13TC - STALLION (ILASAMAJA))')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (75, 'Port Code (14AP - DENCA BONDED TERMINAL)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (76, 'Port Code (14LP - MICHELLE 2)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (77, 'Port Code (14PA - PORT EXPRESS BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (78, 'Port Code (14TC - STALLION (AMUWO-ODOFIN))')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (79, 'Port Code (15AP - CRESEADA INTERNATIONAL LTD)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (80, 'Port Code (15PA - AIMIE & SATCHEL LTD)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (81, 'Port Code (16AP - SIFAX BONDED W/HOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (82, 'Port Code (16PA - WEMPCO BONDED WAREHOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (83, 'Port Code (16TC - STALLION (GBAGADA))')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (84, 'Port Code (17AP - PZ BONDED WAREHOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (85, 'Port Code (17TC - DANA MOTORS BONDED WAREHOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (86, 'Port Code (18TC - FAREAST MERCANTILE CO. LTD)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (87, 'Port Code (19AP - ITC BONDED TERMINAL)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (88, 'Port Code (19PA - SAVOL BOUNDED WAREHOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (89, 'Port Code (19TC - ALLIANZ BONDED WHAREHOUSE))')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (90, 'Port Code (20AP - SAPIDII BOUNDED WAREHOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (91, 'Port Code (20TC - THA BOUNDED WAREHOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (92, 'Port Code (21AP - JEALITH BOUNDED WAREHOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (93, 'Port Code (21KD - INLAND CONTAINERS)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (94, 'Port Code (21KN - INLAND CONTAINERS (KN))')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (95, 'Port Code (21TC - ADVANCE MOTORS AND LOGISTICS LTD)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (96, 'Port Code (22KD - PEUGEOT AUTOMOBILE OF NIG. KADUNA)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (97, 'Port Code (23KN - NTM BONDED WAREHOUSE KANO)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (98, 'Port Code (41AP - CLASSIC BOUNDED WAREHOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (99, 'Port Code (51PH - IDEKE BOUNDED WAREHOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (100, 'Port Code (52PH - JOE EBOJE BOUNDED WAREHOUSE)')");
            Sql("INSERT INTO PortOfLandings (Id, Name) VALUES (101, 'Port Code (53PH - QUICKA BOUNDED WAREHOUSE)')");

            Sql("SET IDENTITY_INSERT PortOfLandings OFF");
        }

        public override void Down()
        {
            Sql("DELETE FROM PortOfLandings WHERE Id IN (1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101)");
        }
    }
}
