﻿namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PopulateCurrencyTableWithData : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT Currencies ON");

            Sql("INSERT INTO Currencies (Id, Name, Symbol) VALUES (1, 'Naira', 'N')");
            Sql("INSERT INTO Currencies (Id, Name, Symbol) VALUES (2, 'US Dollars', '$')");
            Sql("INSERT INTO Currencies (Id, Name, Symbol) VALUES (3, 'GB Pounds', '£')");
            Sql("INSERT INTO Currencies (Id, Name, Symbol) VALUES (4, 'Yen', '¥')");

            Sql("SET IDENTITY_INSERT Currencies OFF");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Currencies WHERE Id IN (1, 2, 3, 4)");
        }
    }
}
