namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEndUserCertificateIdToEndUserCertificateDetailsModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EndUserCertificateDetails", "EndUserCertificateId", c => c.Int(nullable: false));
            CreateIndex("dbo.EndUserCertificateDetails", "EndUserCertificateId");
            AddForeignKey("dbo.EndUserCertificateDetails", "EndUserCertificateId", "dbo.EndUserCertificates", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EndUserCertificateDetails", "EndUserCertificateId", "dbo.EndUserCertificates");
            DropIndex("dbo.EndUserCertificateDetails", new[] { "EndUserCertificateId" });
            DropColumn("dbo.EndUserCertificateDetails", "EndUserCertificateId");
        }
    }
}
