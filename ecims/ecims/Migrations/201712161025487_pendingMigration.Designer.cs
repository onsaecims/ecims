// <auto-generated />
namespace ecims.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class pendingMigration : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(pendingMigration));
        
        string IMigrationMetadata.Id
        {
            get { return "201712161025487_pendingMigration"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
