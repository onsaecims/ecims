namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agencyData3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AgencyDesks", "AgencyName_id", "dbo.Agencies");
            DropIndex("dbo.AgencyDesks", new[] { "AgencyName_id" });
            DropColumn("dbo.AgencyDesks", "AgencyName_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AgencyDesks", "AgencyName_id", c => c.Int());
            CreateIndex("dbo.AgencyDesks", "AgencyName_id");
            AddForeignKey("dbo.AgencyDesks", "AgencyName_id", "dbo.Agencies", "id");
        }
    }
}
