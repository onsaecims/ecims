namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PopulateStateOfOriginTableInModels : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "StateOfOriginId", c => c.Int(nullable: true));
            CreateIndex("dbo.AspNetUsers", "StateOfOriginId");
            AddForeignKey("dbo.AspNetUsers", "StateOfOriginId", "dbo.StateOfOrigins", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "StateOfOriginId", "dbo.StateOfOrigins");
            DropIndex("dbo.AspNetUsers", new[] { "StateOfOriginId" });
            DropColumn("dbo.AspNetUsers", "StateOfOriginId");
        }
    }
}
