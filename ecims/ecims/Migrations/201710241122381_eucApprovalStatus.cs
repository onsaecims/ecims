namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class eucApprovalStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EndUserCertificates", "EUCApprovalStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EndUserCertificates", "EUCApprovalStatus");
        }
    }
}
