namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatePortOfLandingTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PortOfLandings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PortOfLandings");
        }
    }
}
