namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class DropGenderTable : DbMigration
    {
        public override void Up()
        {
            Sql("DELETE FROM Genders WHERE Id IN (1, 2)");
        }
        
        public override void Down()
        {
            Sql("SET IDENTITY_INSERT Genders ON");

            Sql("INSERT INTO Genders (Id, Name) VALUES (1, 'Male')");
            Sql("INSERT INTO Genders (Id, Name) VALUES (2, 'Female')");

            Sql("SET IDENTITY_INSERT Genders OFF");
        }
    }
}
