namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGenderIdToUserTable : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.AspNetUsers", name: "Gender_Id", newName: "GenderId");
            RenameIndex(table: "dbo.AspNetUsers", name: "IX_Gender_Id", newName: "IX_GenderId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.AspNetUsers", name: "IX_GenderId", newName: "IX_Gender_Id");
            RenameColumn(table: "dbo.AspNetUsers", name: "GenderId", newName: "Gender_Id");
        }
    }
}
