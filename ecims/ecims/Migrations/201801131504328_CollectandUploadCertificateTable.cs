namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CollectandUploadCertificateTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CollectEUCCerts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CollectToken = c.String(),
                        CertNo = c.String(),
                        UserID = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UploadedEUCCertificates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CertNo = c.String(),
                        UploadUrl = c.String(),
                        UserID = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UploadedEUCCertificates");
            DropTable("dbo.CollectEUCCerts");
        }
    }
}
