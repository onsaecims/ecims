namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createEUCApplicationTable1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.EndUserCertificateDetails", "unit", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EndUserCertificateDetails", "unit", c => c.Int(nullable: false));
        }
    }
}
