namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PopulateCategoriesTable : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT Categories ON");

            Sql("INSERT INTO Categories (Id, Name) VALUES (1, 'Individual')");
            Sql("INSERT INTO Categories (Id, Name) VALUES (2, 'Corporate')");

            Sql("SET IDENTITY_INSERT Categories OFF");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Categories WHERE Id IN (1, 2)");
        }
    }
}
