namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAddressLinesToDB : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "AddressLine1", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "AddressLine1");
        }
    }
}
