namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class eucApplicationTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EndUserCertificates", "dateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.EndUserCertificates", "applicationStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EndUserCertificates", "applicationStatus");
            DropColumn("dbo.EndUserCertificates", "dateCreated");
        }
    }
}
