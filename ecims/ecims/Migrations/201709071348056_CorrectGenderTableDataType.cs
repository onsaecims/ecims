namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CorrectGenderTableDataType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Genders", "Name", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Genders", "Name", c => c.Int(nullable: false));
        }
    }
}
