namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateEUCWorkFlowTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EUCWorkFlows", "Reciepient_Id", "dbo.AspNetUsers");
            DropIndex("dbo.EUCWorkFlows", new[] { "Reciepient_Id" });
            AlterColumn("dbo.EUCWorkFlows", "actionReciepient", c => c.String());
            DropColumn("dbo.EUCWorkFlows", "Reciepient_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EUCWorkFlows", "Reciepient_Id", c => c.String(maxLength: 128));
            AlterColumn("dbo.EUCWorkFlows", "actionReciepient", c => c.String(nullable: false));
            CreateIndex("dbo.EUCWorkFlows", "Reciepient_Id");
            AddForeignKey("dbo.EUCWorkFlows", "Reciepient_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
