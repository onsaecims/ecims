namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddApplicationNumberColumnToUserTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "ApplicationNumber", c => c.Int(nullable: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "ApplicationNumber");
        }
    }
}
