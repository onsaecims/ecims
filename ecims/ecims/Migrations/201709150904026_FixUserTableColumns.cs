namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixUserTableColumns : DbMigration
    {
        public override void Up()
        {
            //DropTable("dbo.AccountCategories");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.AccountCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Category = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
    }
}
