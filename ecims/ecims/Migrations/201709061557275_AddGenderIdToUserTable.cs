namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGenderIdToUserTable : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("dbo.AspNetUsers", "Gender_Id", "dbo.Genders");
            //DropIndex("dbo.AspNetUsers", new[] { "Gender_Id" });
            //DropColumn("dbo.AspNetUsers", "Gender_Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "Gender_Id", "dbo.Genders");
            DropIndex("dbo.AspNetUsers", new[] { "Gender_Id" });
            DropColumn("dbo.AspNetUsers", "Gender_Id");
        }
    }
}
