namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateEUCCertTable : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.DisputeEUCCertificates",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            DisputeDate = c.DateTime(nullable: false),
            //            DisputeDetails = c.String(),
            //            DisputeBy = c.String(),
            //            DisputeStatus = c.String(),
            //        })
            //    .PrimaryKey(t => t.id);
            
            //CreateTable(
            //    "dbo.EUCAttachmentValidationStatus",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            EUCId = c.Int(nullable: false),
            //            ApplicationNumber = c.String(nullable: false),
            //            DocumentName = c.String(nullable: false),
            //            isValidated = c.Boolean(nullable: false),
            //            EndUserCert_id = c.Int(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.EndUserCertificates", t => t.EndUserCert_id)
            //    .Index(t => t.EndUserCert_id);
            
            //CreateTable(
            //    "dbo.EUCCertificates",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            CertNo = c.String(),
            //            ApplicantNo = c.String(),
            //            ExpiryDate = c.DateTime(nullable: false),
            //            IssuanceDate = c.DateTime(nullable: false),
            //            CertStatus = c.Int(nullable: false),
            //            AgencyId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.id);
            
            //AddColumn("dbo.EndUserCertificates", "IsApplicationValid", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EUCAttachmentValidationStatus", "EndUserCert_id", "dbo.EndUserCertificates");
            DropIndex("dbo.EUCAttachmentValidationStatus", new[] { "EndUserCert_id" });
            DropColumn("dbo.EndUserCertificates", "IsApplicationValid");
            DropTable("dbo.EUCCertificates");
            DropTable("dbo.EUCAttachmentValidationStatus");
            DropTable("dbo.DisputeEUCCertificates");
        }
    }
}
