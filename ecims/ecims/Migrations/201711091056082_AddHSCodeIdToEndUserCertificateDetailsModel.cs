namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHSCodeIdToEndUserCertificateDetailsModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EndUserCertificateDetails", "HSCodeId", c => c.Int(nullable: false));
            CreateIndex("dbo.EndUserCertificateDetails", "HSCodeId");
            AddForeignKey("dbo.EndUserCertificateDetails", "HSCodeId", "dbo.HSCodes", "Id", cascadeDelete: true);
            DropColumn("dbo.EndUserCertificateDetails", "hsCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EndUserCertificateDetails", "hsCode", c => c.Int(nullable: false));
            DropForeignKey("dbo.EndUserCertificateDetails", "HSCodeId", "dbo.HSCodes");
            DropIndex("dbo.EndUserCertificateDetails", new[] { "HSCodeId" });
            DropColumn("dbo.EndUserCertificateDetails", "HSCodeId");
        }
    }
}
