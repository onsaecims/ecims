namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateStateOfOriginWithDataInDatabaseAndChangeStateFromIntToString : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.StateOfOrigins", "Name", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.StateOfOrigins", "Name", c => c.Int(nullable: false));
        }
    }
}
