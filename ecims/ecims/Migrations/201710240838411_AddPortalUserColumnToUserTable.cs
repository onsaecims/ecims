namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddPortalUserColumnToUserTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "PortalUserCategoryId", c => c.Int(nullable: false, defaultValue: 1));
            CreateIndex("dbo.AspNetUsers", "PortalUserCategoryId");
            AddForeignKey("dbo.AspNetUsers", "PortalUserCategoryId", "dbo.PortalSubCategories", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "PortalUserCategoryId", "dbo.PortalSubCategories");
            DropIndex("dbo.AspNetUsers", new[] { "PortalUserCategoryId" });
            DropColumn("dbo.AspNetUsers", "PortalUserCategoryId");
        }
    }
}
