namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEUCOrganisationsTableToDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EUCOrganisations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApplicationNumber = c.String(),
                        Name = c.String(),
                        AwardContract = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EUCOrganisations");
        }
    }
}
