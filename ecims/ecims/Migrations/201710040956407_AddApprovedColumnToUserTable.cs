namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddApprovedColumnToUserTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Approved", c => c.Boolean(nullable: false, defaultValue: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Approved");
        }
    }
}
