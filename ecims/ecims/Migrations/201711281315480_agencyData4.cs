namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agencyData4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Agencies", "AgencyT_id", c => c.Int());
            AddColumn("dbo.AgencyDesks", "AgencyDsk_Id", c => c.Int());
            CreateIndex("dbo.Agencies", "AgencyT_id");
            CreateIndex("dbo.AgencyDesks", "AgencyDsk_Id");
            AddForeignKey("dbo.Agencies", "AgencyT_id", "dbo.AgencyTypes", "id");
            AddForeignKey("dbo.AgencyDesks", "AgencyDsk_Id", "dbo.AgencyDesks", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AgencyDesks", "AgencyDsk_Id", "dbo.AgencyDesks");
            DropForeignKey("dbo.Agencies", "AgencyT_id", "dbo.AgencyTypes");
            DropIndex("dbo.AgencyDesks", new[] { "AgencyDsk_Id" });
            DropIndex("dbo.Agencies", new[] { "AgencyT_id" });
            DropColumn("dbo.AgencyDesks", "AgencyDsk_Id");
            DropColumn("dbo.Agencies", "AgencyT_id");
        }
    }
}
