// <auto-generated />
namespace ecims.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addCommentToEndorseTable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addCommentToEndorseTable));
        
        string IMigrationMetadata.Id
        {
            get { return "201712262049083_addCommentToEndorseTable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
