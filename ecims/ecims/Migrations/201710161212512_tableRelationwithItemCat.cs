namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tableRelationwithItemCat : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EndUserCertificates", "itemCategory_id", c => c.Int());
            CreateIndex("dbo.EndUserCertificates", "itemCategory_id");
            AddForeignKey("dbo.EndUserCertificates", "itemCategory_id", "dbo.EUCItemCategories", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EndUserCertificates", "itemCategory_id", "dbo.EUCItemCategories");
            DropIndex("dbo.EndUserCertificates", new[] { "itemCategory_id" });
            DropColumn("dbo.EndUserCertificates", "itemCategory_id");
        }
    }
}
