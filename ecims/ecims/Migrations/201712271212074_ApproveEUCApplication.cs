namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApproveEUCApplication : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApproveEUCApps",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        AppID = c.String(),
                        AproveDate = c.DateTime(nullable: false),
                        ApproveToken = c.String(),
                        ApprovedBy = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ApproveEUCApps");
        }
    }
}
