namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateDisputeStatusColumn : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DisputeEUCCertificates", "DisputeStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DisputeEUCCertificates", "DisputeStatus", c => c.String());
        }
    }
}
