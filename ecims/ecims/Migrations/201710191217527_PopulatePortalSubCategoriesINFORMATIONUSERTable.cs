namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulatePortalSubCategoriesINFORMATIONUSERTable : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT PortalSubCategories ON");

            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (2, 2, 'NCS')");

            Sql("SET IDENTITY_INSERT PortalSubCategories OFF");
        }

        public override void Down()
        {
            Sql("DELETE FROM PortalSubCategories WHERE Id IN (2)");
        }
    }
}
