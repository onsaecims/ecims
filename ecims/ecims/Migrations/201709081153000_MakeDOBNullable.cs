namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MakeDOBNullable : DbMigration
    {
        public override void Up()
        {
            //DropColumn("dbo.AspNetUsers", "DOB");
        }

        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "DOB");
        }
    }
}
