namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMeansOfIdentificationToModelAndUserTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MeansOfIdentifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AspNetUsers", "MeansOfIdentificationId", c => c.Int(nullable: false));
            CreateIndex("dbo.AspNetUsers", "MeansOfIdentificationId");
            AddForeignKey("dbo.AspNetUsers", "MeansOfIdentificationId", "dbo.MeansOfIdentifications", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "MeansOfIdentificationId", "dbo.MeansOfIdentifications");
            DropIndex("dbo.AspNetUsers", new[] { "MeansOfIdentificationId" });
            DropColumn("dbo.AspNetUsers", "MeansOfIdentificationId");
            DropTable("dbo.MeansOfIdentifications");
        }
    }
}
