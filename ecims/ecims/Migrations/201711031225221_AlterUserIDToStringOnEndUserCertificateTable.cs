namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterUserIDToStringOnEndUserCertificateTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EndUserCertificates", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.EndUserCertificates", new[] { "User_Id" });
            DropColumn("dbo.EndUserCertificates", "UserID");
            RenameColumn(table: "dbo.EndUserCertificates", name: "User_Id", newName: "UserID");
            AlterColumn("dbo.EndUserCertificates", "UserID", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.EndUserCertificates", "UserID", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.EndUserCertificates", "UserID");
            AddForeignKey("dbo.EndUserCertificates", "UserID", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EndUserCertificates", "UserID", "dbo.AspNetUsers");
            DropIndex("dbo.EndUserCertificates", new[] { "UserID" });
            AlterColumn("dbo.EndUserCertificates", "UserID", c => c.String(maxLength: 128));
            AlterColumn("dbo.EndUserCertificates", "UserID", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.EndUserCertificates", name: "UserID", newName: "User_Id");
            AddColumn("dbo.EndUserCertificates", "UserID", c => c.Int(nullable: false));
            CreateIndex("dbo.EndUserCertificates", "User_Id");
            AddForeignKey("dbo.EndUserCertificates", "User_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
