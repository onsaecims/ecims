namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class approvedStatusToTable1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "ApprovalStatus", c => c.Int(nullable: false, defaultValue: 3));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "ApprovalStatus");
        }
    }
}
