namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PopulateStatesOfOrigin : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT StateOfOrigins ON");

            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (1, 'Federal Capital Territory (FCT)')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (2, 'Abia')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (3, 'Adamawa')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (4, 'Akwa Ibom')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (5, 'Anambra')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (6, 'Bauchi')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (7, 'Bayelsa')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (8, 'Benue')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (9, 'Borno')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (10, 'Cross River')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (11, 'Delta')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (12, 'Ebonyi')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (13, 'Enugu')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (14, 'Ekiti')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (15, 'Gombe')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (16, 'Imo')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (17, 'Jigawa')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (18, 'Kaduna')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (19, 'Kano')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (20, 'Katsina')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (21, 'Kebbi')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (22, 'Kogi')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (23, 'Kwara')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (24, 'Lagos')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (25, 'Nasarawa')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (26, 'Niger')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (27, 'Ogun')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (28, 'Ondo')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (29, 'Osun')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (30, 'Oyo')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (31, 'Plateau')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (32, 'Rivers')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (33, 'Sokoto')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (34, 'Taraba')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (35, 'Yobe')");
            Sql("INSERT INTO StateOfOrigins (Id, Name) VALUES (36, 'Zamfara')");

            Sql("SET IDENTITY_INSERT StateOfOrigins OFF");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM StateOfOrigins WHERE Id IN (1, 2, 3)");
        }
    }
}
