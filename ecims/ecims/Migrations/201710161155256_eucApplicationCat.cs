namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class eucApplicationCat : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EUCItemCategories",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        categoryName = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EUCItemCategories");
        }
    }
}
