namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test5 : DbMigration
    {
        public override void Up()
        {
            
        }
        
        public override void Down()
        {
            DropColumn("dbo.EndUserCertificates", "certificateStatus");
            DropColumn("dbo.EndUserCertificates", "agencyAssignedTo");
        }
    }
}
