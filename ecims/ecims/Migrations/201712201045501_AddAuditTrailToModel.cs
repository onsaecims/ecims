namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAuditTrailToModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AuditTrails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DateOfActivity = c.DateTime(nullable: false),
                        ActionCarriedOut = c.String(),
                        CarriedOutBy = c.String(),
                        ActionDescription = c.String(),
                        UserIPAddress = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AuditTrails");
        }
    }
}
