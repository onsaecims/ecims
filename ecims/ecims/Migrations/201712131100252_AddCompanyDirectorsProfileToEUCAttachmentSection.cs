namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCompanyDirectorsProfileToEUCAttachmentSection : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EUCApplicationAttachments", "companyDirectorsProfile", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EUCApplicationAttachments", "companyDirectorsProfile");
        }
    }
}
