namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGenderToUserTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Gender_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.AspNetUsers", "Gender_Id");
            AddForeignKey("dbo.AspNetUsers", "Gender_Id", "dbo.Genders", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "Gender_Id", "dbo.Genders");
            DropIndex("dbo.AspNetUsers", new[] { "Gender_Id" });
            DropColumn("dbo.AspNetUsers", "Gender_Id");
        }
    }
}
