namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PopulatePortalSubCategoriesExclusiveUserTable : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT PortalSubCategories ON");

            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (18, 5, 'Exclusive User')");

            Sql("SET IDENTITY_INSERT PortalSubCategories OFF");
        }

        public override void Down()
        {
            Sql("DELETE FROM PortalSubCategories WHERE Id IN (18)");
        }
    }
}
