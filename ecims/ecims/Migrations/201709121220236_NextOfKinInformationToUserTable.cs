namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NextOfKinInformationToUserTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "NextOfKinFullname", c => c.String());
            AddColumn("dbo.AspNetUsers", "NextOfKinAddress", c => c.String());
            AddColumn("dbo.AspNetUsers", "NextOfKinRelationship", c => c.String());
            AddColumn("dbo.AspNetUsers", "NextOfKinPhoneNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "NextOfKinPhoneNumber");
            DropColumn("dbo.AspNetUsers", "NextOfKinRelationship");
            DropColumn("dbo.AspNetUsers", "NextOfKinAddress");
            DropColumn("dbo.AspNetUsers", "NextOfKinFullname");
        }
    }
}
