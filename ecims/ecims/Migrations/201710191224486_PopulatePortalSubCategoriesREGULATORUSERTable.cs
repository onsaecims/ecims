namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PopulatePortalSubCategoriesREGULATORUSERTable : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT PortalSubCategories ON");

            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (3, 3, 'NAFDAC')");
            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (4, 3, 'NPF')");
            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (5, 3, 'MMSD')");
            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (6, 3, 'Min Agric')");
            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (7, 3, 'NSCDC')");
            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (8, 3, 'CBN')");
            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (9, 3, 'MOD')");
            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (10, 3, 'MOD Army')");
            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (11, 3, 'MOD JSD')");
            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (12, 3, 'MOD Navy')");
            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (13, 3, 'MOD Air Force')");
            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (14, 3, 'NCAA')");

            Sql("SET IDENTITY_INSERT PortalSubCategories OFF");
        }

        public override void Down()
        {
            Sql("DELETE FROM PortalSubCategories WHERE Id IN (3,4,5,6,7,8,9,10,11,12,13,14)");
        }
    }
}
