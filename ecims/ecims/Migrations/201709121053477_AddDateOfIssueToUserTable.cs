namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDateOfIssueToUserTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "LicenseDateOfIssuance", c => c.DateTime(nullable: false));
            AddColumn("dbo.AspNetUsers", "LicenseDateOfExpiry", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "LicenseDateOfExpiry");
            DropColumn("dbo.AspNetUsers", "LicenseDateOfIssuance");
        }
    }
}
