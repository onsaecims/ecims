namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class pendingMigrations : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("dbo.Agencies", "AgencyT_id", "dbo.AgencyTypes");
            //DropIndex("dbo.Agencies", new[] { "AgencyT_id" });
            //DropTable("dbo.Agencies");
            //DropTable("dbo.AgencyTypes");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.AgencyTypes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Agencies",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        dateCreated = c.DateTime(nullable: false),
                        agencyType = c.Int(nullable: false),
                        agencyName = c.String(),
                        contactName = c.String(),
                        agencyPhone = c.String(),
                        agencyEmail = c.String(),
                        AgencyT_id = c.Int(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateIndex("dbo.Agencies", "AgencyT_id");
            AddForeignKey("dbo.Agencies", "AgencyT_id", "dbo.AgencyTypes", "id");
        }
    }
}
