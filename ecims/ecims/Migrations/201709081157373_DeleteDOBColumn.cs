namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class DeleteDOBColumn : DbMigration
    {
        public override void Up()
        {
            //DropColumn("dbo.AspNetUsers", "DOB");
        }

        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "DOB");
        }
    }
}
