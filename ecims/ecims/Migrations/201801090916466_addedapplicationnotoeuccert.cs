namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedapplicationnotoeuccert : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EUCCertificates", "applicationNo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EUCCertificates", "applicationNo");
        }
    }
}
