namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class approvedBy : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EndUserCertificates", "ApprovedBy", c => c.String());
            AddColumn("dbo.EndUserCertificates", "ApproveUser_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.EndUserCertificates", "ApproveUser_Id");
            AddForeignKey("dbo.EndUserCertificates", "ApproveUser_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EndUserCertificates", "ApproveUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.EndUserCertificates", new[] { "ApproveUser_Id" });
            DropColumn("dbo.EndUserCertificates", "ApproveUser_Id");
            DropColumn("dbo.EndUserCertificates", "ApprovedBy");
        }
    }
}
