namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCategoryToUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Category_Id", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Category_Id");
            AddForeignKey("dbo.AspNetUsers", "Category_Id", "dbo.AccountCategories", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "Category_Id", "dbo.AccountCategories");
            DropIndex("dbo.AspNetUsers", new[] { "Category_Id" });
            DropColumn("dbo.AspNetUsers", "Category_Id");
        }
    }
}
