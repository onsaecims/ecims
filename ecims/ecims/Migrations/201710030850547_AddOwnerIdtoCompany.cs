namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddOwnerIdtoCompany : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Companies", "Owner_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Companies", new[] { "Owner_Id" });
            DropColumn("dbo.Companies", "OwnerId");
            RenameColumn(table: "dbo.Companies", name: "Owner_Id", newName: "OwnerId");
            AlterColumn("dbo.Companies", "OwnerId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Companies", "OwnerId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Companies", "OwnerId");
            AddForeignKey("dbo.Companies", "OwnerId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Companies", "OwnerId", "dbo.AspNetUsers");
            DropIndex("dbo.Companies", new[] { "OwnerId" });
            AlterColumn("dbo.Companies", "OwnerId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Companies", "OwnerId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.Companies", name: "OwnerId", newName: "Owner_Id");
            AddColumn("dbo.Companies", "OwnerId", c => c.Int(nullable: false));
            CreateIndex("dbo.Companies", "Owner_Id");
            AddForeignKey("dbo.Companies", "Owner_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
