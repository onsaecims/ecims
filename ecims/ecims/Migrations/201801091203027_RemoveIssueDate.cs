namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveIssueDate : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.EUCCertificates", "issueDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EUCCertificates", "issueDate", c => c.DateTime(nullable: false));
        }
    }
}
