namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class blacklist : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.BlackListApplicants",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            BlacklistDate = c.DateTime(nullable: false),
            //            ApplicantId = c.String(nullable: false, maxLength: 128),
            //            action = c.String(),
            //            BlacklistedBy = c.String(nullable: false),
            //            BlacklistReason = c.String(),
            //            Blacklister_Id = c.String(maxLength: 128),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.AspNetUsers", t => t.ApplicantId, cascadeDelete: true)
            //    .ForeignKey("dbo.AspNetUsers", t => t.Blacklister_Id)
            //    .Index(t => t.ApplicantId)
            //    .Index(t => t.Blacklister_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BlackListApplicants", "Blacklister_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.BlackListApplicants", "ApplicantId", "dbo.AspNetUsers");
            DropIndex("dbo.BlackListApplicants", new[] { "Blacklister_Id" });
            DropIndex("dbo.BlackListApplicants", new[] { "ApplicantId" });
            DropTable("dbo.BlackListApplicants");
        }
    }
}
