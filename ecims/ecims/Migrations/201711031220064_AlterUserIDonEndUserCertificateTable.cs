namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterUserIDonEndUserCertificateTable : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.EndUserCertificates", name: "userId_Id", newName: "User_Id");
            RenameIndex(table: "dbo.EndUserCertificates", name: "IX_userId_Id", newName: "IX_User_Id");
            AddColumn("dbo.EndUserCertificates", "UserID", c => c.Int(nullable: false));
            DropColumn("dbo.EndUserCertificates", "UserIdentification");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EndUserCertificates", "UserIdentification", c => c.Int(nullable: false));
            DropColumn("dbo.EndUserCertificates", "UserID");
            RenameIndex(table: "dbo.EndUserCertificates", name: "IX_User_Id", newName: "IX_userId_Id");
            RenameColumn(table: "dbo.EndUserCertificates", name: "User_Id", newName: "userId_Id");
        }
    }
}
