namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AgencyTableUpdate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EUCAppEndorsersDesks",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        AgencyId = c.Int(nullable: false),
                        EndorsedBy = c.String(),
                        AppId = c.String(),
                        EndorsementDesk = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            AddColumn("dbo.PortalSubCategories", "dateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.PortalSubCategories", "contactName", c => c.String());
            AddColumn("dbo.PortalSubCategories", "agencyPhone", c => c.String());
            AddColumn("dbo.PortalSubCategories", "agencyEmail", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PortalSubCategories", "agencyEmail");
            DropColumn("dbo.PortalSubCategories", "agencyPhone");
            DropColumn("dbo.PortalSubCategories", "contactName");
            DropColumn("dbo.PortalSubCategories", "dateCreated");
            DropTable("dbo.EUCAppEndorsersDesks");
        }
    }
}
