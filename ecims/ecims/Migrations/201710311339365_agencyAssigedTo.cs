namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agencyAssigedTo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EndUserCertificates", "agencyAssignedTo", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.EndUserCertificates", "agencyAssignedTo");
        }
    }
}
