namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createEUCApplicationTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EndUserCertificates",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        applicationNo = c.String(nullable: false),
                        itemCat = c.Int(nullable: false),
                        originCountry = c.Int(nullable: false),
                        purposeOfUsage = c.String(),
                        currencyOfPurchase = c.Int(nullable: false),
                        portOfLanding = c.Int(nullable: false),
                        UserIdentification = c.Int(nullable: false),
                        userId_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.AspNetUsers", t => t.userId_Id)
                .Index(t => t.userId_Id);
            
            CreateTable(
                "dbo.EndUserCertificateDetails",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        applicationNo = c.String(nullable: false),
                        hsCode = c.String(),
                        itemDescription = c.String(),
                        qty = c.Int(nullable: false),
                        unit = c.String(),
                        cost = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.EUCApplicationAttachments",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        applicationNo = c.String(nullable: false),
                        cbnApplication = c.String(),
                        minOfAgricApplication = c.String(),
                        NCCALetter = c.String(),
                        ARPIdentification = c.String(),
                        memoOfAssociation = c.String(),
                        bilOfLaden = c.String(),
                        analysisCert = c.String(),
                        incorporationCert = c.String(),
                        ac_gad001 = c.String(),
                        taxClerance = c.String(),
                        previousNetworkDistribution = c.String(),
                        privateSecurityReg = c.String(),
                        citLicense = c.String(),
                        firsCertificate = c.String(),
                        formCO7 = c.String(),
                        cbnFormM = c.String(),
                        identityCard = c.String(),
                        last1YearEUCS = c.String(),
                        acquirerIntroLetter = c.String(),
                        magazineLicenses = c.String(),
                        manufacturersSafety = c.String(),
                        NAFDACAttachment = c.String(),
                        NPFFIreworksPermit = c.String(),
                        packingList = c.String(),
                        importPermitforExplosive = c.String(),
                        prevStorageAttachment = c.String(),
                        proformaInvoice = c.String(),
                        qualifiedTechOfficerDetails = c.String(),
                        FFDReg = c.String(),
                        specificationOfGoods = c.String(),
                        storageFacilityAttachment = c.String(),
                        transitDetails = c.String(),
                        transporterParticulars = c.String(),
                        vatCert = c.String(),
                        vendorIDDetails = c.String(),
                        operationYears = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EndUserCertificates", "userId_Id", "dbo.AspNetUsers");
            DropIndex("dbo.EndUserCertificates", new[] { "userId_Id" });
            DropTable("dbo.EUCApplicationAttachments");
            DropTable("dbo.EndUserCertificateDetails");
            DropTable("dbo.EndUserCertificates");
        }
    }
}
