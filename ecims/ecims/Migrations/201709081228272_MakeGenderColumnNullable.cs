namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class MakeGenderColumnNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "GenderId", c => c.Int(nullable: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "GenderId", c => c.Int(nullable: false));
        }
    }
}
