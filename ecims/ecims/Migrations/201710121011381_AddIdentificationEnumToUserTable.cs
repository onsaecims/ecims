namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddIdentificationEnumToUserTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Identification", c => c.Int(nullable: false, defaultValue: 1));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Identification");
        }
    }
}
