namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GenderTable : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("dbo.AspNetUsers", "Gender_Id", "dbo.Genders");
            //DropIndex("dbo.AspNetUsers", new[] { "Gender_Id" });
            //DropColumn("dbo.AspNetUsers", "Gender_Id");
            //DropTable("dbo.Genders");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Genders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.AspNetUsers", "Gender_Id", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Gender_Id");
            AddForeignKey("dbo.AspNetUsers", "Gender_Id", "dbo.Genders", "Id");
        }
    }
}
