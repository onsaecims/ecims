namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class blacklistDataUpdate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BlackListApplicants", "ApplicantId", "dbo.AspNetUsers");
            DropForeignKey("dbo.BlackListApplicants", "Blacklister_Id", "dbo.AspNetUsers");
            DropIndex("dbo.BlackListApplicants", new[] { "ApplicantId" });
            DropIndex("dbo.BlackListApplicants", new[] { "Blacklister_Id" });
            AlterColumn("dbo.BlackListApplicants", "ApplicantId", c => c.String());
            AlterColumn("dbo.BlackListApplicants", "BlacklistedBy", c => c.String());
            DropColumn("dbo.BlackListApplicants", "Blacklister_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BlackListApplicants", "Blacklister_Id", c => c.String(maxLength: 128));
            AlterColumn("dbo.BlackListApplicants", "BlacklistedBy", c => c.String(nullable: false));
            AlterColumn("dbo.BlackListApplicants", "ApplicantId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.BlackListApplicants", "Blacklister_Id");
            CreateIndex("dbo.BlackListApplicants", "ApplicantId");
            AddForeignKey("dbo.BlackListApplicants", "Blacklister_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.BlackListApplicants", "ApplicantId", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
    }
}
