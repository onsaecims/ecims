namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PopulateGenderTableWithGenderData : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT Genders ON");

            Sql("INSERT INTO Genders (Id, Name) VALUES (1, 'Male')");
            Sql("INSERT INTO Genders (Id, Name) VALUES (2, 'Female')");

            Sql("SET IDENTITY_INSERT Genders OFF");
        }

        public override void Down()
        {
            Sql("DELETE FROM Genders WHERE Id IN (1, 2)");
        }
    }
}
