namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agencyData1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Agencies", "dateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.AgencyDesks", "status", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AgencyDesks", "status");
            DropColumn("dbo.Agencies", "dateCreated");
        }
    }
}
