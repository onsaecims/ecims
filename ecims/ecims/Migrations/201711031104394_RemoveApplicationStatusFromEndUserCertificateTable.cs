namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveApplicationStatusFromEndUserCertificateTable : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.EndUserCertificates", "applicationStatus");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EndUserCertificates", "applicationStatus", c => c.Int(nullable: false));
        }
    }
}
