namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class hscodeusercat : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HSCodeUserCats",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        UserCatName = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.HSCodeUserCats");
        }
    }
}
