namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PopulatePortalSubCategoriesONSATable : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT PortalSubCategories ON");

            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (1, 1, 'ONSA')");

            Sql("SET IDENTITY_INSERT PortalSubCategories OFF");
        }

        public override void Down()
        {
            Sql("DELETE FROM PortalSubCategories WHERE Id IN (1)");
        }
    }
}
