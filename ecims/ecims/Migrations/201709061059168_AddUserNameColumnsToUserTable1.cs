namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddUserNameColumnsToUserTable1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "OtherName", c => c.String(nullable: true, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "OtherName", c => c.String(nullable: true, maxLength: 100));
        }
    }
}
