namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PopulateItemCategoryTable : DbMigration
    {
        public override void Up()
        {
            //Sql("SET IDENTITY_INSERT EUCItemCategories ON");

            //Sql("INSERT INTO EUCItemCategories (Id, categoryName) VALUES (1, 'FERTILIZER CHEMICALS')");
            //Sql("INSERT INTO EUCItemCategories (Id, categoryName) VALUES (2, 'TREATED VEHICLES')");
            //Sql("INSERT INTO EUCItemCategories (Id, categoryName) VALUES (3, 'PYROTECHNICS')");
            //Sql("INSERT INTO EUCItemCategories (Id, categoryName) VALUES (4, 'REMOTELY PILOTED AIRCRAFT/UAVs')");
            //Sql("INSERT INTO EUCItemCategories (Id, categoryName) VALUES (5, 'FIREWORKS')");
            //Sql("INSERT INTO EUCItemCategories (Id, categoryName) VALUES (6, 'SECURITY CLEARANCE FOR RPAs')");

            //Sql("SET IDENTITY_INSERT EUCItemCategories OFF");
        }

        public override void Down()
        {
            //Sql("DELETE FROM EUCItemCategories WHERE Id IN (1, 2, 3, 5, 6)");
        }
    }
}
