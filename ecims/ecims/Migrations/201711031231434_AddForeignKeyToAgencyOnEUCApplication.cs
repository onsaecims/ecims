namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForeignKeyToAgencyOnEUCApplication : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EndUserCertificates", "AgencyId", c => c.Int(nullable: true));
            CreateIndex("dbo.EndUserCertificates", "AgencyId");
            AddForeignKey("dbo.EndUserCertificates", "AgencyId", "dbo.PortalSubCategories", "Id", cascadeDelete: false);
            DropColumn("dbo.EndUserCertificates", "agencyAssignedTo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.EndUserCertificates", "agencyAssignedTo", c => c.Int(nullable: false));
            DropForeignKey("dbo.EndUserCertificates", "AgencyId", "dbo.PortalSubCategories");
            DropIndex("dbo.EndUserCertificates", new[] { "AgencyId" });
            DropColumn("dbo.EndUserCertificates", "AgencyId");
        }
    }
}
