namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agencyData : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Agencies",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        agencyType = c.Int(nullable: false),
                        agencyName = c.String(),
                        contactName = c.String(),
                        agencyPhone = c.String(),
                        agencyEmail = c.String(),
                        AgencyT_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.AgencyTypes", t => t.AgencyT_id)
                .Index(t => t.AgencyT_id);
            
            CreateTable(
                "dbo.AgencyTypes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.AgencyDesks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        agencyName = c.Int(nullable: false),
                        deskName = c.String(),
                        AgencyName_id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Agencies", t => t.AgencyName_id)
                .Index(t => t.AgencyName_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AgencyDesks", "AgencyName_id", "dbo.Agencies");
            DropForeignKey("dbo.Agencies", "AgencyT_id", "dbo.AgencyTypes");
            DropIndex("dbo.AgencyDesks", new[] { "AgencyName_id" });
            DropIndex("dbo.Agencies", new[] { "AgencyT_id" });
            DropTable("dbo.AgencyDesks");
            DropTable("dbo.AgencyTypes");
            DropTable("dbo.Agencies");
        }
    }
}
