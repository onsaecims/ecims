namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropApplicationNumberColumnFromUserTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "ApplicationNumber", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "ApplicationNumber", c => c.Int(nullable: false));
        }
    }
}
