namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Company : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OwnerId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 200),
                        Address = c.String(nullable: false),
                        State = c.Int(nullable: false),
                        PhoneNo = c.String(),
                        Email = c.String(),
                        Website = c.String(),
                        CACNo = c.String(),
                        DateOfIncorporation = c.DateTime(nullable: false),
                        Logo = c.String(),
                        CertificateOfIncorporation = c.String(),
                        Owner_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.Owner_Id)
                .Index(t => t.Owner_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Companies", "Owner_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Companies", new[] { "Owner_Id" });
            DropTable("dbo.Companies");
        }
    }
}
