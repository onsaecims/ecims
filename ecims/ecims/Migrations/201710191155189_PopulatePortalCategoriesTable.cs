namespace ecims.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class PopulatePortalCategoriesTable : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT PortalCategories ON");

            Sql("INSERT INTO PortalCategories (Id, Name) VALUES (1, 'ONSA User')");
            Sql("INSERT INTO PortalCategories (Id, Name) VALUES (2, 'Information User')");
            Sql("INSERT INTO PortalCategories (Id, Name) VALUES (3, 'Regulator User')");
            Sql("INSERT INTO PortalCategories (Id, Name) VALUES (4, 'Vetting Agency User')");
            Sql("INSERT INTO PortalCategories (Id, Name) VALUES (5, 'Exclusive User')");
            Sql("INSERT INTO PortalCategories (Id, Name) VALUES (6, 'Client User')");

            Sql("SET IDENTITY_INSERT PortalCategories OFF");
        }

        public override void Down()
        {
            Sql("DELETE FROM PortalCategories WHERE Id IN (1, 2, 3, 4, 5, 6)");
        }
    }
}
