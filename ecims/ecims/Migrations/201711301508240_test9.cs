namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test9 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EUCItemCategories", "userTypeAllowed", c => c.Int(nullable: false));
            AddColumn("dbo.HSCodes", "itemCatName", c => c.Int(nullable: false));
            AddColumn("dbo.HSCodes", "itemSubCatName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.HSCodes", "itemSubCatName");
            DropColumn("dbo.HSCodes", "itemCatName");
            DropColumn("dbo.EUCItemCategories", "userTypeAllowed");
        }
    }
}
