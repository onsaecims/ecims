namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class eucWorkFlow : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.EUCWorkFlows",
            //    c => new
            //        {
            //            id = c.Int(nullable: false, identity: true),
            //            activityDate = c.DateTime(nullable: false),
            //            applicationNumber = c.String(),
            //            actionSource = c.String(nullable: false),
            //            actionReciepient = c.String(nullable: false),
            //            handledStatus = c.String(),
            //            currentStatus = c.String(),
            //            Reciepient_Id = c.String(maxLength: 128),
            //            Source_Id = c.String(maxLength: 128),
            //        })
            //    .PrimaryKey(t => t.id)
            //    .ForeignKey("dbo.AspNetUsers", t => t.Reciepient_Id)
            //    .ForeignKey("dbo.AspNetUsers", t => t.Source_Id)
            //    .Index(t => t.Reciepient_Id)
            //    .Index(t => t.Source_Id);
            
            //AddColumn("dbo.EndUserCertificates", "ApprovedBy", c => c.String());
            //AddColumn("dbo.EndUserCertificates", "ApproveUser_Id", c => c.String(maxLength: 128));
            //CreateIndex("dbo.EndUserCertificates", "ApproveUser_Id");
            //AddForeignKey("dbo.EndUserCertificates", "ApproveUser_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EUCWorkFlows", "Source_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.EUCWorkFlows", "Reciepient_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.EndUserCertificates", "ApproveUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.EUCWorkFlows", new[] { "Source_Id" });
            DropIndex("dbo.EUCWorkFlows", new[] { "Reciepient_Id" });
            DropIndex("dbo.EndUserCertificates", new[] { "ApproveUser_Id" });
            DropColumn("dbo.EndUserCertificates", "ApproveUser_Id");
            DropColumn("dbo.EndUserCertificates", "ApprovedBy");
            DropTable("dbo.EUCWorkFlows");
        }
    }
}
