namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulatePortalSubCategoriesVETTINGAGENCYUSERTable : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT PortalSubCategories ON");

            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (15, 4, 'CTD')");
            Sql("INSERT INTO PortalSubCategories (Id, PortalCategoryId, Name) VALUES (16, 4, 'DSS')");

            Sql("SET IDENTITY_INSERT PortalSubCategories OFF");
        }

        public override void Down()
        {
            Sql("DELETE FROM PortalSubCategories WHERE Id IN (15, 16)");
        }
    }
}
