namespace ecims.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class blacklistDetailToUserTable : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.AspNetUsers", "blacklistedDate", c => c.DateTime(nullable: false));
            //AddColumn("dbo.AspNetUsers", "blacklistedBy", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "blacklistedBy");
            DropColumn("dbo.AspNetUsers", "blacklistedDate");
        }
    }
}
