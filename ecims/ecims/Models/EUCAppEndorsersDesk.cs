﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class EUCAppEndorsersDesk
    {
        public int id { get; set; }
        public int AgencyId { get; set; }
        public string EndorsedBy { get; set; }
        public string AppId { get; set; }
        public int EndorsementDesk { get; set; }
    }
}