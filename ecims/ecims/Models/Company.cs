﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace ecims.Models
{
    public class Company
    {
        public int Id { get; set; }

        public ApplicationUser Owner { get; set; }

        [Required]
        public string OwnerId { get; set; }
       
        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        public string Address { get; set; }

        public StateOfOrigin State { get; set; }

        [Required]
        public int StateId { get; set; }

        public IEnumerable<SelectListItem> States
        {
            get
            {
                var enumType = typeof(State);
                var values = Enum.GetValues(enumType).Cast<State>();

                var converter = TypeDescriptor.GetConverter(enumType);

                return
                    from value in values
                    select new SelectListItem
                    {
                        Text = converter.ConvertToString(value),
                        Value = value.ToString(),
                    };
            }
        }

        public string PhoneNo { get; set; }

        public string Email { get; set; }

        public string Website { get; set; }

        public string CACNo { get; set; }

        public DateTime DateOfIncorporation { get; set; }

        public string Logo { get; set; }

        public string CertificateOfIncorporation { get; set; }
    }
}