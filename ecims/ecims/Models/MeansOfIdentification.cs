﻿namespace ecims.Models
{
    public class MeansOfIdentification
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}