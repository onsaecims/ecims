﻿namespace ecims.Models
{
    public enum Identification
    {
        NationalID = 1,
        DriversLicense = 2,
        InternationalPassport = 3,
        PermanentVotersCard = 4,
        DiplomaticCard = 5
    }
}