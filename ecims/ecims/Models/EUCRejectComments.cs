﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class EUCRejectComments
    {
        public int id { get; set; }
        public string EUCAppID { get; set; }
        public string RejectedBy { get; set; }
        public string RejectComment { get; set; }
        public DateTime RejectDate { get; set; }
    }
}