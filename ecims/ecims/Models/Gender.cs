﻿namespace ecims.Models
{
    public enum Gender
    {
        Male = 1,
        Female = 2,
        Unspecified = 3
    }
}