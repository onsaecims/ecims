﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class ForwardEUCAppComment
    {
        public int id { get; set; }
        public DateTime ForwardDate { get; set; }
        public string EUCAppID { get; set; }
        public string CreatedBy { get; set; }
        public string ForwardedBy { get; set; }
        public string AgencyAssignedTo { get; set; }
        public string Comment { get; set; }
    }
}