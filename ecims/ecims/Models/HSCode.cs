﻿namespace ecims.Models
{
    public class HSCode
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int itemCatName { get; set; }
        public string itemSubCatName { get; set; }
    }
}