﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class HSCodeUserCat
    {
        [Required]
        public int id { get; set; }
        public string UserCatName { get; set; }
    }
}