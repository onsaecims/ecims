﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class AuditTrail
    {
        [Required]
        public int Id { get; set; }
        public DateTime DateOfActivity { get; set; }

        public String ActionCarriedOut { get; set; }

        public string CarriedOutBy { get; set; }

        public string ActionDescription { get; set; }

        public string UserIPAddress { get; set; }
    }
}