﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public enum ReportType
    {
        All = 1,
        Approved = 7,
        Forwarded = 8,
        Rejected = 9,
        Disendorsed = 10,
        Applicant = 2,
        Item_Category = 4,
        Request_Country = 5,
        Ports = 6,
        Detailed_Item_Category = 11
    }
}