﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web.Mvc;

namespace ecims.Models
{
    public class EndUserCertificate
    {
        [Required]
        public int id { get; set; }
        [Required]
        public string applicationNo { get; set; }
        //Date the EUC application was made by the applicant
        public DateTime dateCreated { get; set; }
        //Date the EUC application was forwarded to an agency
        public DateTime dateForwarded { get; set; }
        public EUCItemCategory itemCategory { get; set; }
        [Required]
        public int itemCat { get; set; }
        public Country originCountry { get; set; }
        public string purposeOfUsage { get; set; }
        public int currencyOfPurchase { get; set; }
        public int ? portOfLanding { get; set; }
        public ApplicationUser User { get; set; }
        [Required]
        public string UserID { get; set; }
        public EUCApprovedStatus EUCApprovalStatus { get; set; }
        public IEnumerable<SelectListItem> Status
        {
            get
            {
                var enumType = typeof(EUCApprovedStatus);
                var values = Enum.GetValues(enumType).Cast<EUCApprovedStatus>();

                var converter = TypeDescriptor.GetConverter(enumType);

                return
                    from value in values
                    select new SelectListItem
                    {
                        Text = converter.ConvertToString(value),
                        Value = value.ToString(),
                    };
            }
        }
        public PortalSubCategories Agency { get; set; }
        public int AgencyId { get; set; }
        public CertificateStatus certificateStatus { get; set; }
        public ApplicationUser ApproveUser { get; set; }
        public string ApprovedBy { get; set; }
        public bool IsOrganisation { get; set; }
        public bool IsApplicationValid { get; set; }
        //Token code when NSA clicks approve
        public string TokenCode { get; set; }
        public PortalSubCategories AgencyForwarded { get; set; }
        [ForeignKey("AgencyForwarded")]
        public int AgencyForwardedTo { get; set; }
        public int AgencyForwardedFrom { get; set; }
    }
}