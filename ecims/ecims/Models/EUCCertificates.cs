﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecims.Models
{
    public class EUCCertificates
    {
        public int id { get; set; }
        public string CertNo { get; set; }
        public string applicationNo { get; set; }
        public ApplicationUser User { get; set; }
        //[Key]
        //[ForeignKey("ApplicationUser")]
        [Required]
        public string UserId { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime IssuanceDate { get; set; }
        public CertificateStatus CertStatus { get; set; }
        public IEnumerable<SelectListItem> Status
        {
            get
            {
                var enumType = typeof(CertificateStatus);
                var values = Enum.GetValues(enumType).Cast<CertificateStatus>();

                var converter = TypeDescriptor.GetConverter(enumType);

                return
                    from value in values
                    select new SelectListItem
                    {
                        Text = converter.ConvertToString(value),
                        Value = value.ToString(),
                    };
            }
        }
        public int AgencyId { get; set; }
    }
}