﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class EUCWorkFlow
    {
        [Required]
        public int id { get; set; }
        public DateTime activityDate { get; set; }
        public string applicationNumber { get; set; }
        public ApplicationUser Source { get; set; }
        [Required]
        public string actionReciepient { get; set; }
        public string actionSource { get; set; }
        public PortalCategories PortalCat { get; set; }
        public int? SourceCategory { get; set; }
        public PortalSubCategories UserAgency { get; set; }
        public int? SourceAgency { get; set; }
        public string handledStatus { get; set; }
        public string currentStatus { get; set; }
        public int isWorkedOn { get; set; }
        public Int64 AgencyWorkedOn { get; set; }
    }
}