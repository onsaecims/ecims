﻿using System.ComponentModel.DataAnnotations;

namespace ecims.Models
{
    public class EndUserCertificateDetails
    {
        [Required]
        public int id { get; set; }

        [Required]
        public string applicationNo { get; set; }

        public EndUserCertificate EndUserCertificate { get; set; }

        [Required]
        public int EndUserCertificateId { get; set; }

        public HSCode HSCode { get; set; }

        [Required]
        public int HSCodeId { get; set; }

        public string itemDescription { get; set; }

        public double qty { get; set; }

        public QuantityUnit unit { get; set; }

        public double cost { get; set; }

        public CountryList ItemCountry { get; set; }
        
        public int ItemCountryId { get; set; }
    }
}