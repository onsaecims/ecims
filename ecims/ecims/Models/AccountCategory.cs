﻿using System.ComponentModel.DataAnnotations;

namespace ecims.Models
{
    public class AccountCategory
    {
        public int Id { get; set; }

        [Required]
        public string Category { get; set; }
    }
}