﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public enum UserType
    {
        ClientUser = 1,
        PortalUser = 2
    };
}