﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class FeedBack
    {
        [Required]
        public int Id { get; set; }
        public ApplicationUser User { get; set; }
        [Required]
        public string UserId { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public DateTime FeedbackDate { get; set; }
        public FeedBackStatus status { get; set; }
        public string resolvedBy { get; set; }
        public DateTime resolvedDate { get; set; }
        public string resolveAction { get; set; }
    }
}