﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class VerifiedCertificates
    {
        public int Id { get; set; }
        public EUCCertificates Certificate { get; set; }
        [Required]
        public int CertificateId { get; set; }
        public ApplicationUser User { get; set; }
        [Required]
        public string UserId { get; set; }
        public DateTime DateVerified { get; set; }
    }
}