﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class EUCApplicationFlags
    {
        [Required]
        public int id { get; set; }
        [Required]
        public string applicationNo { get; set; }
        public DateTime FlagDate { get; set; }
        public ApplicationUser User { get; set; }
        [Required]
        public string userFrom { get; set; }
        public string userComment { get; set; }
    }
}