﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ecims.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public StateOfOrigin StateOfOrigin { get; set; }

        [Required]
        public int StateOfOriginId { get; set; }

        [Required]
        public DateTime RequestedDate { get; set; }

        public PortalSubCategories PortalUserCategory { get; set; }

        [Required]
        public int PortalUserCategoryId { get; set; }

        public Category Category { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [Required]
        public CategoryList CategoryList { get; set; }

        public string ApplicationNumber { get; set; }
        public string OrganizationName { get; set; }
        public IEnumerable<SelectListItem> CategoryLists
        {
            get
            {
                var enumType = typeof(CategoryList);
                var values = Enum.GetValues(enumType).Cast<CategoryList>();

                var converter = TypeDescriptor.GetConverter(enumType);

                return
                    from value in values
                    select new SelectListItem
                    {
                        Text = converter.ConvertToString(value),
                        Value = value.ToString(),
                    };
            }
        }

        public Identification Identification { get; set; }

        public IEnumerable<SelectListItem> Identifications { get; set; }

        [Required(ErrorMessage = "Please enter student name.")]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(100)]
        public string OtherName { get; set; }

        public byte[] Passport { get; set; }

        public string PassportUrl { get; set; }

        public string PhoneNo { get; set; }

        public Gender Gender { get; set; }

        public IEnumerable<SelectListItem> Genders
        {
            get
            {
                var enumType = typeof(Gender);
                var values = Enum.GetValues(enumType).Cast<Gender>();

                var converter = TypeDescriptor.GetConverter(enumType);

                return
                    from value in values
                    select new SelectListItem
                    {
                        Text = converter.ConvertToString(value),
                        Value = value.ToString(),
                    };
            }
        }

        public MaritalStatus MaritalStatus { get; set; }

        public IEnumerable<SelectListItem> MaritalStatuses
        {
            get
            {
                var enumType = typeof(MaritalStatus);
                var values = Enum.GetValues(enumType).Cast<MaritalStatus>();

                var converter = TypeDescriptor.GetConverter(enumType);

                return
                    from value in values
                    select new SelectListItem
                    {
                        Text = converter.ConvertToString(value),
                        Value = value.ToString(),
                    };
            }
        }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public CountryList Countries { get; set; }

        public int Country { get; set; }

        public string State { get; set; }

        public string City { get; set; }

        public string TIN { get; set; }

        public MeansOfIdentification MeansOfIdentification { get; set; }

        [Required]
        public int MeansOfIdentificationId { get; set; }

        public string DriversLicenseNumber { get; set; }

        public string PlaceOfIssue { get; set; }

        public DateTime LicenseDateOfIssuance { get; set; }

        public DateTime LicenseDateOfExpiry { get; set; }

        public string NextOfKinFullname { get; set; }

        public string NextOfKinAddress { get; set; }

        public string NextOfKinRelationship { get; set; }

        public string NextOfKinPhoneNumber { get; set; }

        public string NationalIdNumber { get; set; }

        public string InternationalPassportIdNumber { get; set; }

        public string NationalVotersCardIdNumber { get; set; }

        public DateTime DOB { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public bool Approved { get; set; }

        public string IdUrl { get; set; }

        public ApprovedStatus ApprovalStatus { get; set; }
        public IEnumerable<SelectListItem> Status
        {
            get
            {
                var enumType = typeof(ApprovedStatus);
                var values = Enum.GetValues(enumType).Cast<ApprovedStatus>();

                var converter = TypeDescriptor.GetConverter(enumType);

                return
                    from value in values
                    select new SelectListItem
                    {
                        Text = converter.ConvertToString(value),
                        Value = value.ToString(),
                    };
            }
        }

        public UserType UserType { get; set; }
        public IEnumerable<SelectListItem> TypeOfUser
        {
            get
            {
                var enumType = typeof(UserType);
                var values = Enum.GetValues(enumType).Cast<UserType>();

                var converter = TypeDescriptor.GetConverter(enumType);

                return
                    from value in values
                    select new SelectListItem
                    {
                        Text = converter.ConvertToString(value),
                        Value = value.ToString(),
                    };
            }
        }
        public string ApplicationRejectReason { get; set; }
        public bool IsBlackListed { get; set; }
        public DateTime? blacklistedDate { get; set; }
        public string blacklistedBy { get; set; }
        public int blacklistID { get; set; }
        //Checks if Google Authenticator is active on specific account
        public bool isGoogleAuthenticatorActive { get; set; }
        [DefaultValue(false)]
        public bool canVerifyEndUserCertificates { get; set; }
    }
}