﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class CertificateWorkflow
    {
        public int Id{ get; set; }
        public EUCCertificates Certificate { get; set; }
        public int CertificateId { get; set; }
        public DateTime DateOfAction { get; set; }
        public string ActionTaken { get; set; }
        public ApplicationUser User { get; set; }
        public string UserId { get; set; }

    }
}