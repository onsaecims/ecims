﻿using System.ComponentModel.DataAnnotations;

namespace ecims.Models
{
    public class Currency
    {
        [Required]
        public int Id { get; set; }

        public string Name { get; set; }

        public string Symbol { get; set; }
    }
}