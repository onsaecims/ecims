﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class DisputeEUCCertificates
    {
        public int id { get; set; }
        public DateTime DisputeDate { get; set; }
        public string DisputeDetails { get; set; }
        public string DisputeBy { get; set; }
        public CertificateStatus DisputeStatus { get; set; }
        public string CertNo { get; set; }
    }
}