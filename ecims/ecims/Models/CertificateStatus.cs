﻿namespace ecims.Models
{
    public enum CertificateStatus
    {
        Disputed = 1,
        Utilized = 2,
        Expired = 3,
        Recalled = 4,
        Pending = 5,
        Issued = 6,
        Certificate_Collected = 7,
        Undispute = 8
    }
}