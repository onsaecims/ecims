﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public enum UserPermForHSCode
    {
        CORPORATE = 1,
        INDIVIDUAL = 2,
        BOTH = 3
    }
}