﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class EUCApplicationEndorsers
    {
        [Required]
        public int id { get; set; }
        [Required]
        public string applicationNo { get; set; }
        public DateTime EndorsementDate { get; set; }
        public ApplicationUser User { get; set; }
        [Required]
        public string userFrom { get; set; }
        public int EndorsementDesk { get; set; }
        public int AgencyName { get; set; }
        public int AgencyType { get; set; }
        public string EndorseComment { get; set; }
    }
}