﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public enum ApprovedStatus
    {
        Approved = 2, 
        Rejected = 3,
        Pending = 1
    };
}