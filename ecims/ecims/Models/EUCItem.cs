﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class EUCItem
    {
        public string ItemNumber { get; set; }
        public int ItemId { get; set; }
        public string WeightUnit { get; set; }
        public int HSCode { get; set; }
        public string HsCodeName;
        public string ItemDescription { get; set; }
        public int Quantity { get; set; }
        public int Weight { get; set; }
        public double Cost { get; set; }
        public string Currency { get; set; }
        public string ApplicationNumber { get; internal set; }
    }
}