﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class BlackListApplicant
    {
        [Required]
        public int Id { get; set; }
        public DateTime BlacklistDate { get; set; }
        public ApplicationUser Applicant { get; set; }
        [Required]
        public string ApplicantId { get; set; }
        public string action { get; set; }
        public string BlacklistedBy { get; set; }
        public string BlacklistReason { get; set; }
    }
}