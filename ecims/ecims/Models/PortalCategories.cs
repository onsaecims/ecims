﻿using System.ComponentModel.DataAnnotations;

namespace ecims.Models
{
    public class PortalCategories
    {
        [Required]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}