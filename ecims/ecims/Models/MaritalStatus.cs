﻿namespace ecims.Models
{
    public enum MaritalStatus
    {
        Single = 1,
        Married = 2,
        Others = 3,
        Unspecified = 4
    }
}