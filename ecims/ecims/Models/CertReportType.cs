﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public enum CertReportType
    {
        All = 1,
        Application_Number = 2,
        Certificate_Status = 3,
        Expired_Certificates = 4
    }
}