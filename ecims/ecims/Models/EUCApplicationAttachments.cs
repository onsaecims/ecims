﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class EUCApplicationAttachments
    {
        [Required]
        public int id { get; set; }
        [Required]
        public string applicationNo { get; set; }
        public string cbnApplication { get; set; }
        public string minOfAgricApplication { get; set; }
        public string NCCALetter { get; set; }
        public string ARPIdentification { get; set; }
        public string memoOfAssociation { get; set; }
        public string bilOfLaden { get; set; }
        public string analysisCert { get; set; }
        public string incorporationCert { get; set; }
        public string companyDirectorsProfile { get; set; }
        public string ac_gad001 { get; set; }
        public string taxClerance { get; set; }
        public string StockBalance { get; set; }
        public string previousNetworkDistribution { get; set; }
        public string privateSecurityReg { get; set; }
        public string citLicense { get; set; }
        public string firsCertificate { get; set; }
        public string formCO7 { get; set; }
        public string cbnFormM { get; set; }
        public string identityCard { get; set; }
        public string last1YearEUCS { get; set; }
        public string acquirerIntroLetter { get; set; }
        public string magazineLicenses { get; set; }
        public string manufacturersSafety { get; set; }
        public string NAFDACAttachment { get; set; }
        public string NPFFIreworksPermit { get; set; }
        public string packingList { get; set; }
        public string importPermitforExplosive { get; set; }
        public string prevStorageAttachment { get; set; }
        public string proformaInvoice { get; set; }
        public string qualifiedTechOfficerDetails { get; set; }
        public string FFDReg { get; set; }
        public string specificationOfGoods { get; set; }
        public string storageFacilityAttachment { get; set; }
        public string transitDetails { get; set; }
        public string transporterParticulars { get; set; }
        public string vatCert { get; set; }
        public string vendorIDDetails { get; set; }
        public string operationYears { get; set; }
        public string LicenseFromNBCOrNCC { get; set; }
    }
}