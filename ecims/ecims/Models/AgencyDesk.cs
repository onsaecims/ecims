﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class AgencyDesk
    {
        [Required]
        public int Id { get; set; }
        public AgencyDesk AgencyDsk { get; set; }
        [Required]
        public int agencyName { get; set; }
        public string deskName { get; set; }
        public string status { get; set; }
    }
}