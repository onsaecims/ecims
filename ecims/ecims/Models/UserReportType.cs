﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public enum UserReportType
    {
        All = 1,
        User_Agency = 2,
        Approval_Status = 3
    }
}