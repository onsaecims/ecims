﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class ApprovedEUCTokens
    {
        public int id { get; set; }
        public string EUCAppID { get; set; }
        public string TokenType { get; set; }
        public string TokenCode { get; set; }
        public DateTime TokenDate { get; set; }
    }
}