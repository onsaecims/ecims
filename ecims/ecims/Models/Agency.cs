﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class Agency
    {
        [Required]
        public int id { get; set; }
        public DateTime dateCreated { get; set; }   
        public AgencyType AgencyT { get; set; }   
        [Required]  
        public int agencyType { get; set; }
        public string agencyName { get; set; }
        public string contactName { get; set; }
        public string agencyPhone { get; set; }
        public string agencyEmail { get; set; }
    }
}