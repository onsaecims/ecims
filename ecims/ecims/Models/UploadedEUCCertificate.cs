﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class UploadedEUCCertificate
    {
        public int Id { get; set; }
        public string CertNo { get; set; }
        public string UploadUrl { get; set; }
        public ApplicationUser User { get; set; }
        [Required]
        public string UserID { get; set; }
    }
}