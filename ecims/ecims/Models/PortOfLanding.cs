﻿namespace ecims.Models
{
    public class PortOfLanding
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public int portState { get; set; }

    }
}