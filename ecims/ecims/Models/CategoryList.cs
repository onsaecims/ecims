﻿namespace ecims.Models
{
    public enum CategoryList
    {
        Individual = 1,
        Corporate = 2,
        Exclusive = 3
    }
}