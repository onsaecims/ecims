﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class CollectEUCCert
    {
        public int Id { get; set; }
        public string CollectToken { get; set; }
        public string CertNo { get; set; }
        public string UserID { get; set; }
    }
}