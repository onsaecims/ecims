﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public enum EUCApprovedStatus
    {
        Pending = 1,
        Approved = 2,
        Rejected = 3,
        Forwarded = 4,
        Endorsed = 5,
        Disendorsed = 6,
        Requiring_Approval = 7,
        Certificate_Issued = 8
    }
}