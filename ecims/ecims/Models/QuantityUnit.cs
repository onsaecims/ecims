﻿namespace ecims.Models
{
    public enum QuantityUnit
    {
       KG = 1,
       Lbs = 2,
       Tonnes = 3,
       Packages = 4,
       Units = 5,
       Litres = 6
    }
}