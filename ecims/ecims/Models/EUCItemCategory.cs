﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class EUCItemCategory
    {
        public int id { get; set; }
        public string categoryName { get; set; }
        public int userTypeAllowed { get; set; }
    }
}