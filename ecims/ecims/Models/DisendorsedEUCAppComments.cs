﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class DisendorsedEUCAppComments
    {
        public int id { get; set; }
        public string EUCAppID { get; set; }
        public string DisendorsedBy { get; set; }
        public DateTime DisendorsedDate { get; set; }
        public string DisendorsedComment { get; set; }
    }
}