﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class ApproveEUCApp
    {
        [Required]
        public int id { get; set; }
        public string AppID { get; set; }
        public DateTime AproveDate { get; set; }
        public string ApproveToken { get; set; }
        public string ApprovedBy { get; set; }
    }
}