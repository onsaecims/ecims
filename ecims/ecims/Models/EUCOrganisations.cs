﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class EUCOrganisations
    {
        [Required]
        public int Id { get; set; }
        public string ApplicationNumber { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string AwardContract { get; set; }
    }
}