﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class EUCAttachmentValidationStatus
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int EUCId { get; set; }
        public EndUserCertificate EndUserCert { get; set; }
        [Required]
        public string ApplicationNumber { get; set; }
        [Required]
        public string DocumentName { get; set; }
        public bool isValidated { get; set; }
    }
}