﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ecims.Models
{
    public class PortalSubCategories
    {
        [Required]
        public int Id { get; set; }

        public string Name { get; set; }

        public PortalCategories PortalCategory { get; set; }

        [Required]
        public int PortalCategoryId { get; set; }
        public DateTime dateCreated { get; set; }
        public string contactName { get; set; }
        public string agencyPhone { get; set; }
        public string agencyEmail { get; set; }
    }
}