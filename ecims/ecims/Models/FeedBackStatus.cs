﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public enum FeedBackStatus
    {
        Pending = 0,
        Resolved = 1
    }
}