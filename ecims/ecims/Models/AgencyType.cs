﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ecims.Models
{
    public class AgencyType
    {
        [Required]
        public int id { get; set; }
        [Required]
        public string Name { get; set; }

    }
}