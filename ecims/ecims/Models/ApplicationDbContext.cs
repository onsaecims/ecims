﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace ecims.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<StateOfOrigin> States { get; set; }
        public DbSet<HSCode> HSCodes { get; set; }
        public DbSet<PortOfLanding> PortsOfLanding { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<PortalSubCategories> PortalSubCategories { get; set; }
        public DbSet<PortalCategories> PortalCategories { get; set; }
        public DbSet<EndUserCertificate> EndUserCertificate { get; set; }
        public DbSet<EndUserCertificateDetails> EndUserCertificateDetails { get; set; }
        public DbSet<EUCApplicationAttachments> EUCApplicationAttachments { get; set; }
        public DbSet<EUCItemCategory> EUCItemCategory { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<MeansOfIdentification> Identification { get; set; }
        public DbSet<EUCApplicationComments> EUCApplicationComments { get; set; }
        public DbSet<EUCApplicationEndorsers> EUCApplicationEndorsers { get; set; }
        public DbSet<EUCApplicationFlags> EUCApplicationFlags { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<AgencyDesk> AgencyDesk { get; set; }
        public DbSet<HSCodeUserCat> HSCodeUserCat { get; set; }
        public DbSet<EUCWorkFlow> EUCWorkFlow { get; set; }
        public DbSet<EUCOrganisations> EUCOrganisations { get; set; }
        public DbSet<BlackListApplicant> BlackListApplicant { get; set; }
        public DbSet<EUCCertificates> EUCCertificates { get; set; }
        public DbSet<DisputeEUCCertificates> DisputeEUCCertificates { get; set; }
        public DbSet<EUCAttachmentValidationStatus> EUCAttachmentValidations { get; set; }
        public DbSet<AuditTrail> AuditTrail { get; set; }
        public DbSet<ForwardEUCAppComment> ForwardEUCAppComment { get; set; }
        public DbSet<EUCRejectComments> EUCRejectComments { get; set; }
        public DbSet<DisendorsedEUCAppComments> DisendorsedEUCAppComments { get; set; }
        public DbSet<ApproveEUCApp> ApproveEUCApp { get; set; }
        public DbSet<EUCAppEndorsersDesk> EUCAppEndorsersDesk { get; set; }
        public DbSet<CountryList> CountryList { get; set; }
        public DbSet<Agency> Agency { get; set; }
        public DbSet<CollectEUCCert> CollectEUCCert { get; set; }
        public DbSet<UploadedEUCCertificate> UploadedEUCCertificate { get; set; }
        public DbSet<AgencyType> AgencyType { get; set; }
        public DbSet<FeedBack> FeedBack { get; set; }
        public DbSet<VerifiedCertificates> VerifiedCertificates { get; set; }
        public DbSet<CertificateWorkflow> CertificateWorkflow { get; set; }
        [Required]
        public int CategoryId { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        //public System.Data.Entity.DbSet<ecims.Models.ApplicationUser> ApplicationUsers { get; set; }
    }
}